#ifndef __WEAPON_H__
#define __WEAPON_H__


//バレットクラス
class Weapon_Class;
class Bullet_Class{
protected:
	IEXMESH bullet;

	D3DXMATRIX	wmat;

	D3DXVECTOR3 pos;
	D3DXVECTOR3 move;

	float	scale;
	float	speed;
	float	dist;

	int		type;
	int		damage;
	int		eftype;

	BOOL	flg;
	BOOL	lflg;

	Bullet_Class *before;
	Bullet_Class *next;
public:
	D3DXVECTOR3 GetPos();
	D3DXVECTOR3 GetMove();

	BOOL			GetFlag();
	void			NextBullet( Bullet_Class **bullet );
	void			SetBefore( Bullet_Class *bullet );
	Bullet_Class	*GetBefore();
	void			SetNext( Bullet_Class *bullet );
	Bullet_Class	*GetNext();
	int				GetType();
	float			GetDist();
	void			Clear( Bullet_Class **bullet );

	BOOL HitBullet();
	void Init( IEXMESH bullet, D3DXMATRIX mat, D3DXVECTOR3 move, float scale, float speed, int type, int damage, int eftype, BOOL flg );
	void Release();
	void Move();
	void UpData();
	void Proc();
	void Draw();
};

class MissileBullet:public Bullet_Class{
private:
	int tno;
public:
	void Init( IEXMESH bullet, D3DXMATRIX mat, D3DXVECTOR3 move, float scale, float speed, int type, int damage, int eftype, BOOL flg, int tno );
	int GetTno(){
		return tno;
	}
};

//武器クラス
class Weapon_Class{
protected:
	IEXMESH	weapon;
	IEXMESH bullet;

	D3DXMATRIX	wmat;

	D3DXVECTOR3 pos;
	D3DXVECTOR3 move;

	BOOL	mflg;

	float	scale;

	int		MaxBullet;
	int		Bullet;
	float	BulletSpeed;
	float	BulletScale;

	BOOL	ChainFlag;
	int		ChainSpeed;
	int		ChainTimer;

	BOOL	flg;
	int		lflg;
public:
	void SetPos( D3DXVECTOR3 pos );
	D3DXVECTOR3 GetPos();
	BOOL		GetMoveFlag(){ return mflg; }
	int			GetChainSpeed(){ return ChainSpeed; }
	void		SetFlag(int flg ){ this->flg = flg; }

	void		SetBullet( int Bullet );
	int			GetBullet();

	virtual		Bullet_Class *SetBullet() = 0;

	virtual		void SetLost() = 0;

	int GetLost(){
		return lflg;
	}

	virtual float GetChage() = 0;
	virtual void Init() = 0;
	virtual void Release() = 0;
	virtual void Move() = 0;
	virtual void UpData() = 0;
	virtual void Proc() = 0;
	virtual void Draw() = 0;
};

class Gun : public Weapon_Class{
public:
	Bullet_Class *SetBullet();

	float GetChage();
	void SetLost();
	void Init();
	void Release();
	void Move();
	void UpData();
	void Proc();
	void Draw();
};

class Enemy_Class;
class Missile : public Weapon_Class{
private:
	BOOL Change;
	int cnt;
	int num;
	int tno[MAXTARGET];
public:
	Bullet_Class *SetBullet();

	void ClearTargetNo( int no );
	D3DXVECTOR3 GetTarget( int no );
	BOOL SearchSelect( Enemy_Class *enemy, BOOL flg );
	void Search();

	float GetChage();
	void SetLost();
	void Init();
	void Release();
	void Move();
	void UpData();
	void Proc();
	void Draw();
};

class Laser : public Weapon_Class{
private:
	int cnt;
public:
	Bullet_Class *SetBullet();

	float GetChage();
	void SetLost();
	void Init();
	void Release();
	void Move();
	void UpData();
	void Proc();
	void Draw();
};
#endif