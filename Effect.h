#ifndef __EFFECT_H__
#define __EFFECT_H__


//エフェクト
void	EffectBoost(float,float,float,D3DXVECTOR3,D3DXVECTOR3);
void	EffectFire(float,float,float,D3DXVECTOR3);

void	EffectCloud( float z, float mz );

void	EffectChage1( D3DXVECTOR3 pos, float mz, float size );
void	EffectChage2( D3DXVECTOR3 pos, float size );

void	EffectTarget( D3DXVECTOR3 Pos, BOOL flg );

void	EffectStar( float z );
#endif