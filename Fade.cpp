#include "All.h"

//*****************************************************************************************************************************
//
//		フェード関連
//
//*****************************************************************************************************************************

void	Fade_Class::Set2DObj( LPIEX2DOBJ lpObj )
{	
	CopyMemory( &Obj, lpObj, sizeof(IEX2DOBJ) );
}

//フェードカラーセット
void	Fade_Class::SetColor( int c ){
	switch( c ){
	case BLACK:
		r = g = b = 0;
		break;
	case WHITE:
		r = g = b = 255;
		break;
	case RED:
		r = 255;
		g = b = 0;
		break;
	case GREEN:
		g = 255;
		r = b = 0;
		break;
	case BLUE:
		b = 255;
		r = g = 0;
		break;
	default:
		r = g = b = 0;
	}
}

//フェードセット 
void	Fade_Class::SetFade( int m, int a, int c, int s ){
	mode = m;
	alpha = a;
	speed = s;
	flag = FALSE;
	SetColor( c );
}

//フェードイン
void	Fade_Class::FadeIn( int c, int s )
{
	mode = 1;
	alpha = 255;
	speed = s;
	flag = FALSE;
	SetColor( c );
	color = (r<<16) + (g<<8) + b;
}

//フェードイン
void	Fade_Class::FadeIn( DWORD c, int s )
{
	mode = 1;
	alpha = 255;
	speed = s;
	flag = FALSE;
	color = c;
}

//フェードアウト
void	Fade_Class::FadeOut( int c, int s )
{
	mode = 2;
	alpha = 0;
	speed = s;
	flag = FALSE;
	SetColor( c );
	color = (r<<16) + (g<<8) + b;
}

//フェードアウト
void	Fade_Class::FadeOut( DWORD c, int s )
{
	mode = 2;
	alpha = 0;
	speed = s;
	flag = FALSE;
	color = c;
}

//フェードイン
void	Fade_Class::FadeIn( int s )
{
	mode = 3;
	alpha = 255;
	speed = s;
	flag = FALSE;
}

//フェードアウト
void	Fade_Class::FadeOut( int s )
{
	mode = 4;
	alpha = 0;
	speed = s;
	flag = FALSE;
}

//処理
void	Fade_Class::Proc()
{
	if( mode==1 || mode==3 ){
		alpha -= speed;
		if( alpha<=0 )alpha = 0;
	}else if( mode==2 || mode==4 ){
		alpha += speed;
		if( alpha>=255 )alpha = 255;
	}
	if( alpha==0 || alpha==255 )flag = TRUE;
	else flag = FALSE;
}

//描画
void	Fade_Class::Draw()
{
	if( !mode )return;
	DWORD argb = (alpha<<24) + color;
	if( mode<3 ){
		IEX_DrawRect( 0, 0, 1024, 1024, RM_NORMAL, argb );
		return;
	}

	argb = (alpha<<24) + 0xffffff;
	IEX_Render2DObject( 0, 0, 640, 480, &Obj, 0, 0, Obj.width, Obj.height, RS_COPY, argb );
}
