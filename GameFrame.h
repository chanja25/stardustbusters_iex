#ifndef __GAMEFRAME_H__
#define __GAMEFRAME_H__

//ゲームの管理クラス
class GameManage_Class{
private:
	int GameMode;
	int NextMode;
	int LoadCnt;
	LPIEX2DOBJ lpLoad[3];
	HANDLE hThread;

	int score;
	BOOL flg;
	int HiScore[5];
	
	FileC *File;
public:
	GameManage_Class();
	~GameManage_Class();

	void	Release();

	void	LoadInit();
	BOOL	LoadProc();
	BOOL	LoadDraw();
	void	LoadRelease();

	void	SetGameMode( int mode );
	int		GetGameMode();
	void	SetNextMode( int mode );
	int		GetNextMode();

	void	SetFlg();
	void	SetScore( int score );
	int		GetScore();
	void	SetHiScore( int no, int score );
	int		GetHiScore( int no );

	FileC	*GetFile();
};

DWORD LoadThread( void* arg );


//ゲームの制御クラス
class GameFrame_Class{
protected:
	Fade_Class		Fade;
	Script_Class	*Script;
public:
	GameFrame_Class(){
		Script = new Script_Class();
	}
	~GameFrame_Class(){
		delete Script;
	}
	Fade_Class		*GetFade();
	Script_Class	*GetScript();

	virtual void Init() = 0;
	virtual void Release() = 0;
	virtual void MainFrame() = 0;
	virtual void Proc() = 0;
	virtual void DrawFrame() = 0;
	virtual void Draw() = 0;
};

//タイトルの制御クラス
class TitleFrame_Class:public GameFrame_Class{
private:
	LPIEX2DOBJ lpTitle[6];
	int cnt;
	int	mode;
	int select;
	BOOL fg;
public:
	void Init();
	void Release();
	void Proc();
	void Draw();
	void MainFrame();
	void DrawFrame();
	void DrawScore( int score, int x, int y, int width, int height );
};

//ゲームのメインクラス
class MainFrame_Class:public GameFrame_Class{
private:
	LPIEXMESH	lpStage;
	//LPIEXMESH	lpArea;
	LPIEXMESH	lpSky;
	LPIEXMESH	lpKabe;
	LPIEX3DOBJ	lpObj[11];
	LPIEXMESH	lpWeapon[6];
	LPIEX2DOBJ	lp2DObj[39];

	Camera_Class		Camera;
	Event_Class			*Event;
	Player_Class		*Player;
	EnemyManage_Class	*Enemy;
	Bullet_Class		*Bullet;
	Display_Class		*Display;
	int mode;
	int score;
	BOOL flg;
public:
	LPIEXMESH GetStage(){
		return lpStage;
	}
	void SetChara( IEX3DOBJ *Obj, int no );
	void SetWeapon( IEXMESH *Mesh, int no );
	void Set2DObj( IEX2DOBJ *Obj, int no );
	void SetFlg();

	Camera_Class *GetCamera(){return &Camera;}
	Player_Class *GetPlayer(){return Player;}
	EnemyManage_Class *GetEnemy(){return Enemy;}
	void SetBullet( Bullet_Class *bullet );
	Bullet_Class **GetBullet(){return &Bullet;}
	Display_Class *GetDisp(){ return Display; }
	void SetMode( int mode ){ this->mode = mode; }
	int	 GetMode(){ return mode; }
	void AddScore( int score ){ this->score += score; }
	int	 GetScore(){ return score; }
	Event_Class *GetEvent(){ return Event; }

	void Init();
	void Release();
	void Proc();
	void Draw();
	void MainFrame();
	void DrawFrame();
};

//タイトルの制御クラス
class EndFrame_Class:public GameFrame_Class{
private:
	LPIEX2DOBJ lpEnd[3];
	int cnt;
	int	mode;
public:
	void Init();
	void Release();
	void Proc();
	void Draw();
	void MainFrame();
	void DrawFrame();
};

//タイトルの制御クラス
class ClearFrame_Class:public GameFrame_Class{
private:
	LPIEX2DOBJ lpClear[4];
	int cnt;
	int	mode;
public:
	void Init();
	void Release();
	void Proc();
	void Draw();
	void MainFrame();
	void DrawFrame();
};

#endif