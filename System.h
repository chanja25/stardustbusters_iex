#ifndef __SYSTEM_H__
#define __SYSTEM_H__


//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

//
//
//

extern	DWORD	dwMainMode;

#define	MODE_MAIN	0
#define	MODE_TITLE	1
#define MODE_LOAD	2
#define	MODE_CLEAR	254
#define	MODE_END	255

//
//
//

extern	BOOL	bFullScreen;

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

BOOL	SYS_Initialize( void );
void	SYS_Release( void );
BOOL	SYS_Main( void );
BOOL	SYS_Draw( void );

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

BOOL	MAIN_Initialize( void );
BOOL	MAIN_Frame( void );
BOOL	MAIN_DrawFrame( void );


//ゲーム管理クラスのextern
class GameManage_Class;
class GameFrame_Class;
class MainFrame_Class;

extern GameManage_Class	GameManage;
extern GameFrame_Class	*MainFrame;
extern MainFrame_Class	*MainFrameC;


#endif