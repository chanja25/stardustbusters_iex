#include "All.h"

//*****************************************************************************************************************************
//
//		サウンド関連
//
//*****************************************************************************************************************************
int Sound::SoundType[SOUND_MAX];

//コンストラクタ
void Sound::Init()
{
	int i;
	for( i=SE_START;i<SE_END;i++ ){
		SoundType[i] = SE;
	}
	for( i=PVOICE_START;i<PVOICE_END;i++ ){
		SoundType[i] = SE;
	}
	for( i=EVOICE_START;i<EVOICE_END;i++ ){
		SoundType[i] = SE;
	}

	IEX_SetWAV( 0, "DATA\\SOUND\\SE\\gun1.wav" );
	IEX_SetWAV( 1, "DATA\\SOUND\\SE\\gun2.wav" );
	IEX_SetWAV( 2, "DATA\\SOUND\\SE\\gun3.wav" );
	IEX_SetWAV( 3, "DATA\\SOUND\\SE\\bomb0.wav" );
	IEX_SetWAV( 18, "DATA\\SOUND\\タイトル.wav" );
	IEX_SetWAV( 19, "DATA\\SOUND\\通常時.wav" );
	IEX_SetWAV( 20, "DATA\\SOUND\\ボス1.wav" );
	IEX_SetWAV( 21, "DATA\\SOUND\\ボス2.wav" );
}

//再生
void	Sound::PlaySound( int no, BOOL loop )
{
	//IEX_SetSoundVolume( no, -100 );

	IEX_PlaySound( no, loop );
}

void	Sound::StopSound( int no )
{
	if( no<SOUND_MAX ){
		IEX_StopSound( no );
	}
}

void	Sound::StopSound()
{
	for( int i=0;i<SOUND_MAX;i++ ){
		StopSound( i );
	}
}

void	Sound::SetValue( int no, int value )
{
	IEX_SetSoundVolume( no, value );
}

//void	Sound::FadeOut( int no )
//{
//	IEX_SetSoundVolume( no, value );
//}