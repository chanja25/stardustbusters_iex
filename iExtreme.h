#ifndef __IEXTREME_H__
#define __IEXTREME_H__

//*****************************************************************************
//
//	IEX Version3.0
//
//*****************************************************************************

#include	<windows.h>
#include	<d3dx9.h>
#include	<dsound.h>

#define		DIRECTINPUT_VERSION	0x0800 
#include	<dinput.h>

//*****************************************************************************
//
//		変数型定義
//
//*****************************************************************************

#define	s8		signed char
#define	s16		short
#define	s32		int

#define	u8		BYTE
#define	u16		WORD
#define	u32		DWORD

#define	ARGB	DWORD

#define	VECTOR3		D3DVECTOR
#define	LPVECTOR3	LPD3DVECTOR


//*****************************************************************************
//
//
//
//*****************************************************************************

#define RELEASE(obj) if( obj ){ (obj)->Release(); (obj) = NULL; }

//*****************************************************************************
//
//		カスタム頂点
//
//*****************************************************************************

//	２Ｄ用頂点（ライティング＆トランスフォーム済み）
#define D3DFVF_TLVERTEX		( D3DFVF_XYZRHW | D3DFVF_DIFFUSE | D3DFVF_TEX1 )
typedef struct tagTLVERTEX {
	float	sx, sy, sz;
	float	rhw;
	ARGB	color;
	float	tu, tv;
} TLVERTEX, *LPTLVERTEX;

//	２Ｄ用頂点（ライティング＆トランスフォーム済み、色なし）
#define D3DFVF_TLVERTEX2	( D3DFVF_XYZRHW | D3DFVF_TEX1 )
typedef struct tagTLVERTEX2 {
	float	sx, sy, sz;
	float	rhw;
	float	tu, tv;
} TLVERTEX2, *LPTLVERTEX2;

//	３Ｄ用頂点(法線なし）
#define D3DFVF_VERTEX		( D3DFVF_XYZ | D3DFVF_TEX1 )
typedef struct tagVERTEX {
	float	x, y, z;
	float	tu, tv;
} VERTEX, *LPVERTEX;

//	３Ｄ用頂点(ライティング済み）
#define D3DFVF_LVERTEX		( D3DFVF_XYZ | D3DFVF_DIFFUSE | D3DFVF_TEX1 )
typedef struct tagLVERTEX {
	float	x, y, z;
	ARGB	color;
	float	tu, tv;
} LVERTEX, *LPLVERTEX;

//	３Ｄメッシュ用頂点
#define D3DFVF_MESHVERTEX		( D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1 )
typedef struct tagMESHVERTEX {
	float	x, y, z;
	float	nx, ny, nz;
	float	tu, tv;
} MESHVERTEX, *LPMESHVERTEX;

//*****************************************************************************
//
//		行列
//
//*****************************************************************************

extern	D3DXMATRIX				matView;
extern	D3DXMATRIX				matProjection;

//*****************************************************************************
//
//		i-Extremeシステム初期化	
//
//*****************************************************************************

#define	SCREEN640	0
#define	SCREEN800	1
#define	SCREEN1024	2

#define	SCREEN480	10

extern	DWORD	IEXMode;
extern	float	ScreenAdjust;

BOOL	IEX_Initialize( HWND hWnd, BOOL bFullScreen, DWORD ScreenSize, BOOL bZBuf=TRUE );
void	IEX_Release( void );

LPDIRECT3DDEVICE9 			IEX_GetD3DDevice( void );
D3DPRESENT_PARAMETERS*		IEX_GetPresentParam( void );
HWND						IEX_GetWindow( void );

//*****************************************************************************
//
//		各種設定
//
//*****************************************************************************

extern	LPDIRECT3DTEXTURE9	lpLastTexture;

void	IEX_SetProjection( float FovY, float Near, float Far );
void	IEX_SetProjectionEx( float FovY, float Near, float Far, float asp );

//	レンダーステート設定関連
void	IEX_InitRenderState( void );
BOOL	IEX_SetRenderState( DWORD state, D3DMATERIAL9* lpMaterial, LPDIRECT3DTEXTURE9 lpTexture );
void	IEX_RestoreRenderState( void );
//	フィルター設定
void	IEX_UseFilter( BOOL bFilter );
//	ライト関連
void	IEX_ResetLight( int index );

void	IEX_SetAmbient( ARGB Color );
void	IEX_SetDirLight( s32 index, D3DVECTOR* dir, float r, float g, float b );
void	IEX_SetPointLight( s32 index, D3DVECTOR* Pos, float r, float g, float b, float range );

void	IEX_SetFog( u32 Mode, float Param1, float Param2, ARGB Color );


//*****************************************************************************
//
//		視界関連
//
//*****************************************************************************

struct	VIEWPOINT {
	D3DXVECTOR3		Pos;		//	視点座標
	D3DXVECTOR3		Angle;		//	視点回転量
};

extern	VIEWPOINT	ViewPoint;

void	IEX_SetViewMatrix( void );
void	IEX_SetView( float posx, float posy, float posz, float anglex, float angley, float anglez );
void	IEX_SetViewTarget( D3DXVECTOR3 *pos, D3DXVECTOR3 *target );
void	IEX_SetViewPos( float posx, float posy, float posz );
void	IEX_SetViewAngle( float anglex, float angley, float anglez );

//*****************************************************************************************************************************
//
//		入力関連
//
//*****************************************************************************************************************************

#define		KEYCODE		u8

#define		KEY_UP		0
#define		KEY_DOWN	1
#define		KEY_LEFT	2
#define		KEY_RIGHT	3
#define		KEY_A		4
#define		KEY_B		5
#define		KEY_C		6
#define		KEY_D		7
#define		KEY_L		8
#define		KEY_R		9

#define		KEY_X		6
#define		KEY_Y		7
#define		KEY_ENTER	10
#define		KEY_SPACE	11

#define		KEY_B1		4
#define		KEY_B2		5
#define		KEY_B3		6
#define		KEY_B4		7
#define		KEY_B5		8
#define		KEY_B6		9
#define		KEY_B7		10
#define		KEY_B8		11
#define		KEY_B9		12
#define		KEY_B10		13
#define		KEY_B11		14
#define		KEY_B12		15
#define		KEY_B13		16
#define		KEY_B14		17
#define		KEY_B15		18

BOOL	KEY_SetInfo( void );
u8		KEY_Get( KEYCODE key );
#define KEY KEY_Get

s32		KEY_GetAxisX( void );
s32		KEY_GetAxisY( void );

void	KEY_SetFF( s32 XForce, s32 YForce, u32 Magnitude );

BOOL	IEX_InitInput( HWND hWnd );
void	IEX_ReleaseInput( void );

//	マウス
typedef struct tagMOUSEDATA {
	long	X, Y, Z;
	BYTE	L, R, M;
} MOUSEDATA, *LPMOUSEDATA;

void	KEY_InitializeMouse( HWND hWnd );
void	KEY_SetMouseData( void );
void	KEY_GetMouseData( LPMOUSEDATA md );


//*****************************************************************************
//
//		メッシュ関連
//
//*****************************************************************************

//
//		メッシュ
//

#define	RM_NORMAL	0
#define	RM_ADD		1
#define	RM_SUB		2
#define	RM_ADD2		3
#define	RM_MUL		4
#define	RM_MUL2		5
#define	RM_NEGA		6

#define	RM_INVERT	30

#define	RM_NOMATRIX	0x100
#define	RM_NOZ		0x200
#define	RM_FOG		0x400

#define	RM_NONE		0x8000

typedef struct tagIEXMESH {
	u32					dwFlags;		//	フラグ

	D3DVECTOR			Pos;			//	メッシュ座標
	D3DVECTOR			Angle;			//	メッシュ回転量
	D3DVECTOR			Scale;			//	メッシュ回転量
	D3DXMATRIX			TransMatrix;	//	転送行列

	LPD3DXMESH			lpMesh;			//	メッシュ
	D3DMATERIAL9		*lpMaterial;	//	材質
	LPDIRECT3DTEXTURE9	*lpTexture;		//	テクスチャ
	u32					MaterialCount;	//	材質数
	u8					bFlags[32];		//	材質フラグ
} IEXMESH, *LPIEXMESH;

//
LPIEXMESH	IEX_LoadXFile( LPSTR path, LPSTR filename );
LPIEXMESH	IEX_LoadMeshFromX( LPSTR path, LPSTR filename );
LPIEXMESH	IEX_LoadMeshFromX( LPSTR filename );
void		IEX_ReleaseMesh( LPIEXMESH lpMesh );

//
void	IEX_SetTransMatrix( LPIEXMESH lpMesh );
void	IEX_SetMeshPos( LPIEXMESH lpMesh, float x, float y, float z );
void	IEX_SetMeshAngle( LPIEXMESH lpMesh, float x, float y, float z );
void	IEX_SetMeshScale( LPIEXMESH lpMesh, float x, float y, float z );

//
void	IEX_RenderMesh( LPIEXMESH lpMesh, u32 dwFlags, float param );

//*****************************************************************************
//
//		３Ｄオブジェクト
//
//*****************************************************************************

#define	RS_COPY		RM_NORMAL
#define	RS_COPY2	0x08
#define	RS_ADD		RM_ADD
#define	RS_SUB		RM_SUB
#define	RS_MUL		RM_MUL
#define	RS_MUL2		RM_MUL2
#define	RS_NEGA		RM_NEGA

#define	RS_TRANS	0x10

#define	RS_BB		0x40
#define	RS_YBB		0x80

#define	RS_NOMATRIX	RM_NOMATRIX
#define	RS_NOZ		RM_NOZ
#define	RS_FOG		RM_FOG

#define	RS_NONE		RM_NONE

//
//		多関節オブジェクト
//

typedef struct tagIEXANIME {
	u32					rotNum;
	LPWORD				rotFrame;
	LPD3DXQUATERNION	rot;

	u32					posNum;
	LPWORD				posFrame;
	LPD3DVECTOR			pos;
} IEXANIME, *LPIEXANIME;

typedef	struct tagIEX3DOBJ {
	u8				bFlag;			//	フラグ

	LPIEXMESH		lpMesh;			//	メッシュ

	D3DVECTOR		Pos;			//	オブジェクト座標
	D3DVECTOR		Angle;			//	オブジェクト回転量
	D3DVECTOR		Scale;			//	オブジェクトスケール
	D3DXMATRIX		TransMatrix;	//	転送行列

	u8				Param[16];

	u8				Motion;			//	現在のモーション番号
	u16				M_Offset[256];	//	モーション先頭フレーム

	u32				dwFrame;		//	現在のフレーム
	u32				NumFrame;		//	総フレーム数
	u16*			dwFrameFlag;	//	フレーム情報

	u32				RenderFrame;	//	レンダリングフレーム
	u8				bChanged;		//	変更フラグ

	LPIEXANIME		lpAnime;		//	ボーンアニメーション

	DWORD			NumVertex;
	LPMESHVERTEX	lpVertex;
	// スキンメッシュ関係
	LPD3DXSKININFO		lpSkinInfo;					// スキン情報

	u32					NumBone;
	LPWORD				BoneParent;
	LPD3DXMATRIX		lpBoneMatrix;
	LPD3DXMATRIX		lpOffsetMatrix;
	LPD3DXMATRIX		lpMatrix;

	LPD3DXQUATERNION	orgPose;
	LPD3DVECTOR			orgPos;

	LPD3DXQUATERNION	CurPose;
	LPD3DVECTOR			CurPos;


} IEX3DOBJ, *LPIEX3DOBJ;

//	ロード・解放
LPIEX3DOBJ	IEX_Load3DObject( LPSTR filename );
void		IEX_Release3DObject( LPIEX3DOBJ lpObj );

//	スキンメッシュ
void	IEX_UpdateSkinMatrix( LPIEX3DOBJ lpObj );
void	IEX_UpdateSkinMeshFrame( LPIEX3DOBJ lpObj, float frame );
void	IEX_UpdateSkinMesh( LPIEX3DOBJ lpObj ); 

//	レンダリング
void	IEX_NoRender3DObject( LPIEX3DOBJ lpObj );
void	IEX_Render3DObject( LPIEX3DOBJ lpObj );
void	IEX_Render3DObject( LPIEX3DOBJ lpObj, DWORD flag, float alpha=-1.0f );

//	モーション
void	IEX_SetObjectMotion( LPIEX3DOBJ lpObj, s32 motion );
s32		IEX_GetObjectMotion( LPIEX3DOBJ lpObj );
void	IEX_ObjectFrameNext( LPIEX3DOBJ lpObj );

//	情報
void	IEX_SetObjectPos( LPIEX3DOBJ lpObj, float x, float y, float z  );
void	IEX_SetObjectAngle( LPIEX3DOBJ lpObj, float x, float y, float z  );
void	IEX_SetObjectScale( LPIEX3DOBJ lpObj, float scale );
void	IEX_SetObjectScale( LPIEX3DOBJ lpObj, float scaleX, float scaleY, float scaleZ );

//	パラメータ
u8		IEX_GetObjectParam( LPIEX3DOBJ lpObj, s32 index );
void	IEX_SetObjectParam( LPIEX3DOBJ lpObj, s32 index, u8 param );


//*****************************************************************************
//	判定
//*****************************************************************************

typedef struct tagIEXRAYPICK {
	//	メッシュ情報
	u16				NumVertex;			//	頂点数
	LPLVERTEX		lpVertex;			//	頂点バッファ

	u16				NumFace;			//	ポリゴン数
	u16*			lpFace;				//	ポリゴンインデックス
	u32*			lpAtr;				//	ポリゴン材質
} IEXRAYPICK, *LPIEXRAYPICK;

LPIEXRAYPICK	IEX_LoadRayPick( LPSTR filename );
void			IEX_ReleaseRayPick( LPIEXRAYPICK obj );

int		IEX_RayPick( LPIEXRAYPICK lpMesh, D3DVECTOR* out, D3DVECTOR* pos, D3DVECTOR* vec, float *Dist );
int		IEX_RayPickMesh( LPIEXMESH lpMesh, D3DVECTOR* out, D3DVECTOR* pos, D3DVECTOR* vec, float *Dist );
int		IEX_RayPick3DObj( LPIEX3DOBJ lpObj, D3DVECTOR* out, D3DVECTOR* pos, D3DVECTOR* vec, float *Dist );

//
//
//

typedef struct tagIEMBONE1 {
	D3DXMATRIX		BoneMatrix;			//	ボーン行列
	u16				parent;				//	親ボーン

	D3DXQUATERNION	orgPose;			//	基本姿勢
	D3DVECTOR		orgPos;				//	基本座標

	u16				IndexNum;			//	影響頂点数
	u32				Index[1000];		//	影響頂点Index
	float			Influence[1000];	//	影響力
} IEMBONE1, *LPIEMBONE1;

typedef struct tagIEMMOTION1 {
	u16				NumRotate;			//	回転キーフレーム数
	u16				RotateFrame[512];	//	回転キーフレーム
	D3DXQUATERNION	Rotate[512];		//	ボーンの状態クォータニオン

	u16				NumPosition;		//	座標キーフレーム数
	u16				PositionFrame[512];	//	座標キーフレーム
	D3DXVECTOR3		Position[512];		//	座標
} IEMMOTION1, *LPIEMMOTION1;


typedef struct tagIEMBONE {
	D3DXMATRIX		BoneMatrix;			//	ボーン行列
	u16				parent;				//	親ボーン

	D3DXQUATERNION	orgPose;			//	基本姿勢
	D3DVECTOR		orgPos;				//	基本座標

	u16				IndexNum;			//	影響頂点数
	u32*			Index;				//	影響頂点Index
	float*			Influence;			//	影響力
} IEMBONE, *LPIEMBONE;

typedef struct tagIEMMOTION {
	u16				NumRotate;			//	回転キーフレーム数
	u16*			RotateFrame;		//	回転キーフレーム
	D3DXQUATERNION*	Rotate;				//	ボーンの状態クォータニオン

	u16				NumPosition;		//	座標キーフレーム数
	u16*			PositionFrame;		//	座標キーフレーム
	D3DXVECTOR3*	Position;			//	座標
} IEMMOTION, *LPIEMMOTION;

typedef struct tagIEMFILE {
	//	メッシュ情報
	u16				NumVertex;			//	頂点数
	LPMESHVERTEX	lpVertex;			//	頂点バッファ

	u16				NumFace;			//	ポリゴン数
	u16*			lpFace;				//	ポリゴンインデックス
	u32*			lpAtr;				//	ポリゴン材質

	u16				NumMaterial;		//	マテリアル数
	D3DMATERIAL9	Material[32];		//	マテリアル
	char			Texture[32][64];	//	テクスチャファイル

	//	ボーン情報
	u16				NumBone;
	LPIEMBONE		lpBone;

	//	モーション情報
	u16				MaxFrame;
	u16				NumMotion;
	u16				M_Offset[256];
	u16				FrameFlag[65535];

	LPIEMMOTION		lpMotion;

} IEMFILE, *LPIEMFILE;

LPIEX3DOBJ	IEX_Create3DObjectFromIEM( LPIEMFILE lpIem );
BOOL		IEX_LoadiEM( LPIEMFILE lpIem, LPSTR filename );
BOOL		IEX_Save3DObject( LPIEMFILE lpIem, LPSTR filename );

//
//		頂点カラーメッシュ
//

//	IMOオブジェクト
typedef struct tagIMOOBJ {
	DWORD		id;	
	int			type;
	DWORD		NumVertex;
	LPLVERTEX	lpVertex;

	DWORD		NumFace;
	LPWORD		lpFace;
	LPDWORD		lpAtr;

	DWORD			NumMaterial;	//	マテリアル数
	D3DMATERIAL9	Material[32];	//	マテリアル
	char			Texture[32][32];//	テクスチャファイル

} IMOOBJ, *LPIMOOBJ;

LPIEXMESH	IEX_LoadIMO( LPSTR filename );

//*****************************************************************************
//
//		テクスチャ関連
//
//*****************************************************************************

LPDIRECT3DTEXTURE9	IEX_LoadTexture( LPSTR filename );
void	IEX_ReleaseTexture( LPDIRECT3DTEXTURE9 lpTexture );

void	IEX_InitTextureManager( void );
void	IEX_ReleaseAllTexture( void );

//*****************************************************************************
//
//		２Ｄオブジェクト関連
//
//*****************************************************************************

#define	IEX2D_RENDERTARGET	1
#define	IEX2D_USEALPHA		2

//	２Ｄオブジェクト
typedef struct tagIEX2DOBJ {
	u32					dwFlags;		//	フラグ					
	LPDIRECT3DTEXTURE9	lpTexture;		//	テクスチャ
	u32					width;			//	幅
	u32					height;			//	高さ
} IEX2DOBJ, *LPIEX2DOBJ;

LPIEX2DOBJ	IEX_Load2DObject( LPSTR filename );
LPIEX2DOBJ	IEX_Create2DObject( s32 width, s32 height, u8 flag );
void		IEX_Release2DObject( LPIEX2DOBJ lpObj );


void	IEX_Render2DObject( s32 DstX, s32 DstY, s32 DstW, s32 DstH, LPIEX2DOBJ lpObj, s32 SrcX, s32 SrcY, s32 width, s32 height, u32 dwFlags=RS_COPY, ARGB color=0xFFFFFFFF );
void	IEX_Render2DObject( int DstX, int DstY, int DstW, int DstH, float Rotation, int flg, LPIEX2DOBJ lpObj, int SrcX, int SrcY, int width, int height, u32 dwFlags=RS_COPY, u32 color=0xFFFFFFFF );
void	IEX_Render2DObjectNA( int DstX, int DstY, int DstW, int DstH, LPIEX2DOBJ lpObj, int SrcX, int SrcY, int width, int height, DWORD dwFlags=RS_COPY, ARGB color=0xFFFFFFFF );

//*****************************************************************************
//
//		３Ｄポリゴン関連
//
//*****************************************************************************

//	４頂点ポリゴン描画
void	IEX_Render3DPolygon( LPVERTEX v, LPIEX2DOBJ lpObj, u32 dwFlags, ARGB color );
void	IEX_Render3DPolygon( LPVERTEX v, LPIEX2DOBJ lpObj, u32 dwFlags, float r, float g, float b, float a );
void	IEX_Render3DPolygon( D3DVECTOR p[4], float tu[4], float tv[4], LPIEX2DOBJ lpObj, u32 dwFlags, float r, float g, float b, float a );
void	IEX_Render3DPolygon( D3DVECTOR p[4], float tu[4], float tv[4], LPIEX2DOBJ lpObj, u32 dwFlags, ARGB color );

//	ストリップポリゴン描画
void	IEX_RenderStripPolygon( LPVERTEX lpVertex, s32 StripNum, LPIEX2DOBJ lpObj, u32 dwFlags, float r, float g, float b, float a );

//*****************************************************************************
//
//		２Ｄポリゴン関連
//
//*****************************************************************************

void	IEX_DrawRect( s32 DstX, s32 DstY, s32 DstW, s32 DstH, u32 dwFlags, ARGB color );
void	IEX_DrawRectZ( s32 DstX, s32 DstY, s32 DstW, s32 DstH, float z, u32 dwFlags, ARGB color );

void	IEX_Render2DPolygon( LPTLVERTEX v, int NumPoly, LPIEX2DOBJ lpObj, u32 dwFlags );
void	IEX_Render2DPolygon( float* vx, float* vy, float* tu, float* tv, int NumPoly, LPIEX2DOBJ lpObj, u32 dwFlags, ARGB* color );
void	IEX_Render2DPolygon( float* vx, float* vy, float* tu, float* tv, int NumPoly, LPIEX2DOBJ lpObj, u32 dwFlags, u32 color );

//*****************************************************************************
//
//		パーティクル関連
//
//*****************************************************************************

typedef struct tagPARTICLE	{
	int		type;			//	形

	int		aFrame;			//	出現フレーム
	ARGB	aColor;			//	出現カラー
	
	int		eFrame;			//	消滅フレーム
	ARGB	eColor;			//	出現カラー

	int		mFrame;			//	最高フレーム
	ARGB	mColor;			//	最高カラー

	D3DVECTOR	Pos;
	D3DVECTOR	Move;
	D3DVECTOR	Power;
	float		rotate;
	float		stretch;

	float	scale;
	
	u8	flag;
	
} PARTICLE, *LPPARTICLE;

//
//
//

void	IEX_InitParticle( LPSTR filename );
void	IEX_ReleaseParticle( void );
void	IEX_DrawParticles( void );
void	IEX_ExecuteParticles( void );

//	構造体指定
void	IEX_SetParticle( LPPARTICLE p );

//
void	IEX_SetParticle( int type, int aFrame, ARGB aColor, int eFrame, ARGB eColor, int mFrame, ARGB mColor, 
						D3DVECTOR* Pos, D3DVECTOR* Move, D3DVECTOR* Power, float rotate, float stretch, float scale, u8 flag );

//	色変化、スケール変化なし
void	IEX_SetParticle( int type, int aFrame, float aAlpha, int eFrame, float eAlpha, int mFrame, float mAlpha, 
						D3DVECTOR* Pos, D3DVECTOR* Move, D3DVECTOR* Power,
						float r, float g, float b, float scale, u8 flag );

//*****************************************************************************************************************************
//
//		数学関連
//
//*****************************************************************************************************************************

float	IEX_GetAngle( D3DXVECTOR3 pos, D3DXVECTOR3 target );

//	行列関連
void	IEX_SetTransMatrixEx( LPD3DXMATRIX lpMatrix, D3DVECTOR* Pos, D3DVECTOR* Angle, D3DVECTOR* Scale );
void	SetTransformMatrixZXY( D3DXMATRIX *Mat, float x, float y, float z, float ax, float ay, float az );
void	SetTransformMatrixXYZ( D3DXMATRIX *Mat, float x, float y, float z, float ax, float ay, float az );

//*****************************************************************************
//
//		２Ｄフォント関連
//
//*****************************************************************************

void	IEX_InitText( void );
void	IEX_ReleaseText( void );

void	IEX_DrawText( LPSTR str, s32 x, s32 y, s32 width, s32 height, ARGB color, BOOL bMini=FALSE );

//*****************************************************************************
//
//		オーディオ関連
//
//*****************************************************************************

#include "ogg/vorbisfile.h"

# pragma comment(lib, "vorbis_static.lib")
# pragma comment(lib, "ogg_static.lib")
# pragma comment(lib, "vorbisfile_static.lib")

#define	STR_NORMAL	0
#define	STR_FADEIN	1
#define	STR_FADEOUT	2

#define	TYPE_WAV	0
#define	TYPE_OGG	1

//	サウンドシステム初期化・終了
BOOL	IEX_InitAudio( HWND hWnd );
void	IEX_ReleaseAudio( void );
//	サウンドセット・再生・停止	
BOOL	IEX_SetWAV( s32 no, LPSTR fname );
void	IEX_PlaySound( s32 no, BOOL loop );
void	IEX_StopSound( s32 no );
//	情報設定・取得
BOOL	IEX_GetSoundStatus( s32 no );
void	IEX_SetSoundVolume( s32 no, s32 volume );

//
//		ストリームサウンド
//

typedef struct tagSTREAM {
	BYTE	type;
	LPDIRECTSOUNDBUFFER	lpStream;		// ストリーム用二次バッファ

	HANDLE	hStrThread;
	HANDLE	hEvent[3];

	int		rate;

	u32	dwThreadId;
	u32	dwThrdParam;

	HANDLE	hStrFile;
	OggVorbis_File	vf;
	u32	StrSize;
	u32	StrPos;
	u32	LoopPtr;

	u8		mode;
	s32		param;
	s32		volume;
} DSSTREAM, *LPDSSTREAM;

//	再生・停止・解放	
LPDSSTREAM	IEX_PlayStreamSound( LPSTR filename );
LPDSSTREAM	IEX_PlayStreamSoundEx( LPSTR filename, u8 mode, s32 param );
BOOL		IEX_StopStreamSound( LPDSSTREAM lpStream );
BOOL		IEX_ReleaseStreamSound( LPDSSTREAM lpStream );

//	情報設定
void	IEX_SetStreamSoundVolume( LPDSSTREAM lpStream, s32 volume );
void	IEX_SetStreamMode( LPDSSTREAM lpStream, u8 mode, s32 param );

//*****************************************************************************
//
//		Ｗｉｎｄｏｗｓ関連関数
//
//*****************************************************************************

BOOL	IEX_WIN_InitCpuUsage( void );
void	IEX_WIN_ReleaseCpuUsage( void );
s32		IEX_WIN_GetCpuUsage( void );

//*****************************************************************************
//
//		デバッグ関連関数
//
//*****************************************************************************

void	IEX_DEBUG_ScreenShot( LPSTR filename );

#endif

