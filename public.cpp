#include "All.h"

//クォーターニオンによる任意軸回転用マトリックス作成
LPD3DXMATRIX	RotQuaternion( float x, float y, float z, float angle )
{
	LPD3DXMATRIX mat = new D3DXMATRIX;
	D3DXQUATERNION qt(.0f,.0f,.0f,1.0f);
	D3DXVECTOR3 v;

	v.x = x;
	v.y = y;
	v.z = z;

	D3DXQuaternionRotationAxis( &qt, &v, angle );
	D3DXMatrixRotationQuaternion( mat, &qt );

	return mat;
}

//クォーターニオンによる任意軸回転用マトリックス作成
LPD3DXMATRIX	RotQuaternion( D3DXVECTOR3 vec, float angle )
{
	LPD3DXMATRIX mat = new D3DXMATRIX;
	D3DXQUATERNION qt(.0f,.0f,.0f,1.0f);

	D3DXQuaternionRotationAxis( &qt, &vec, angle );
	D3DXMatrixRotationQuaternion( mat, &qt );

	return mat;
}

//マトリックスを初期化
void	SetInitMatrix( D3DXMATRIX *mat )
{
	mat->_11 = mat->_22 = mat->_33 = mat->_44 = 1.0f;
	mat->_12 = mat->_13 = mat->_14 = .0f;
	mat->_21 = mat->_23 = mat->_24 = .0f;
	mat->_31 = mat->_32 = mat->_34 = .0f;
	mat->_41 = mat->_42 = mat->_43 = .0f;
}

//マトリックスにベクトルをセット
void	SetVecMatrix( D3DXMATRIX *mat, D3DXVECTOR3 *pos, int flg )
{
	if( flg==0 ){ mat->_11 = pos->x; mat->_12 = pos->y; mat->_13 = pos->z; }
	if( flg==1 ){ mat->_21 = pos->x; mat->_22 = pos->y; mat->_23 = pos->z; }
	if( flg==2 ){ mat->_31 = pos->x; mat->_32 = pos->y; mat->_33 = pos->z; }
	if( flg==3 ){ mat->_41 = pos->x; mat->_42 = pos->y; mat->_43 = pos->z; }
}

//マトリックスに位置をセット
void	SetPosMatrix( D3DXMATRIX *mat, float x, float y, float z )
{
	mat->_41 = x;
	mat->_42 = y;
	mat->_43 = z;
}

//マトリックスに位置をセット
void	SetPosMatrix( D3DXMATRIX *mat, D3DXVECTOR3 pos )
{
	mat->_41 = pos.x;
	mat->_42 = pos.y;
	mat->_43 = pos.z;
}

//マトリックスにスケールをセット
void	SetScaleMatrix( D3DXMATRIX *mat, float scale )
{
	if( scale!=1.0 ){
		mat->_11*=scale; mat->_12*=scale; mat->_13*=scale;
		mat->_21*=scale; mat->_22*=scale; mat->_23*=scale;
		mat->_31*=scale; mat->_32*=scale; mat->_33*=scale;
	}
}

//マトリックスにスケールをセット
void	SetScaleMatrix( D3DXMATRIX *mat, float x, float y, float z )
{
	if( x!=1.0 ){ mat->_11*=x; mat->_12*=x; mat->_13*=x; }
	if( y!=1.0 ){ mat->_21*=y; mat->_22*=y; mat->_23*=y; }
	if( z!=1.0 ){ mat->_31*=z; mat->_32*=z; mat->_33*=z; }
}

//マトリックスにスケールをセット
void	SetScaleMatrix( D3DXMATRIX *mat, D3DXVECTOR3 scale )
{
	if( scale.x!=1.0 ){ mat->_11*=scale.x; mat->_12*=scale.x; mat->_13*=scale.x; }
	if( scale.y!=1.0 ){ mat->_21*=scale.y; mat->_22*=scale.y; mat->_23*=scale.y; }
	if( scale.z!=1.0 ){ mat->_31*=scale.z; mat->_32*=scale.z; mat->_33*=scale.z; }
}

//マトリックスの位置情報を取得
void	GetPosMatrix( D3DXMATRIX *mat, D3DXVECTOR3 *pos )
{
	pos->x = mat->_41;
	pos->y = mat->_42;
	pos->z = mat->_43;
}

//マトリックスのベクトル情報を取得
void	GetVecMatrix( D3DXMATRIX *mat, D3DXVECTOR3 *pos, int flg )
{
	if( flg==0 ){ pos->x = mat->_11; pos->y = mat->_12; pos->z = mat->_13; }
	if( flg==1 ){ pos->x = mat->_21; pos->y = mat->_22; pos->z = mat->_23; }
	if( flg==2 ){ pos->x = mat->_31; pos->y = mat->_32; pos->z = mat->_33; }
	if( flg==3 ){ pos->x = mat->_41; pos->y = mat->_42; pos->z = mat->_43; }
}

//マトリックスのXYZ情報を正規化
void MatrixNormalize( D3DXMATRIX *mat )
{
	D3DXVECTOR3 vec;
	vec.x = mat->_11;
	vec.y = mat->_12;
	vec.z = mat->_13;
	D3DXVec3Normalize( &vec, &vec );
	mat->_11 = vec.x;
	mat->_12 = vec.y;
	mat->_13 = vec.z;

	vec.x = mat->_21;
	vec.y = mat->_22;
	vec.z = mat->_23;
	D3DXVec3Normalize( &vec, &vec );
	mat->_21 = vec.x;
	mat->_22 = vec.y;
	mat->_23 = vec.z;

	vec.x = mat->_31;
	vec.y = mat->_32;
	vec.z = mat->_33;
	D3DXVec3Normalize( &vec, &vec );
	mat->_31 = vec.x;
	mat->_32 = vec.y;
	mat->_33 = vec.z;
}

//ベクトルとマトリックスの掛け算
void	VecXMatrix( D3DXVECTOR3 *vec, D3DXMATRIX *mat )
{
	vec->x = vec->x*mat->_11 + vec->y*mat->_21 + vec->z*mat->_31;
	vec->y = vec->x*mat->_12 + vec->y*mat->_22 + vec->z*mat->_32;
	vec->z = vec->x*mat->_13 + vec->y*mat->_23 + vec->z*mat->_33;
}

//3次元外積を算出
float	Cross3D( D3DXVECTOR3 *out, D3DXVECTOR3 A, D3DXVECTOR3 B )
{
	out->x = A.z*B.y-A.y*B.z;
	out->y = A.x*B.z-A.z*B.x;
	out->z = A.y*B.x-A.x*B.y;
	return out->x - out->y - out->z;
}

//3次元内積を算出
float	Dot3D( D3DXVECTOR3 A, D3DXVECTOR3 B )
{	
	return (A.x*B.x)+(A.y*B.y)+(A.z*B.z);
}

//2次元外積を算出
float	Cross2D( float ax, float ay, float bx, float by, float angle )
{
	float x1 = bx - ax;
	float y1 = by - ay;
	float x2 = cosf(angle);
	float y2 = sinf(angle);
	float l = sqrtf( x1*y2 + x2*y1 );
	return ( y1*x2 - x1*y2 )/l;
}

float	Cross2D( D3DXVECTOR3 p1, D3DXVECTOR3 p2, D3DXVECTOR3 v2 )
{
	D3DXVECTOR3 v1;
	v1.x = p2.x - p1.x;
	v1.z = p2.z - p1.z;
	float l = sqrtf( p1.x*p1.x + p1.z*p1.z );
	return ( v1.x*v2.z - v2.z*v1.x )/l;
}

//2次元内積を算出
float	Dot2D( float ax, float ay, float bx, float by, float angle )
{
	float x1 = bx - ax;
	float y1 = by - ay;
	float x2 = cosf(angle);
	float y2 = sinf(angle);
	float l = sqrtf( x1*x1 + y1*y1 );
	return ( x1*x2 + y1*y2 )/l;
}
float	Dot2D( D3DXVECTOR3 p1, D3DXVECTOR3 p2, D3DXVECTOR3 v2 )
{
	D3DXVECTOR3 v1;
	v1.x = p2.x - p1.x;
	v1.z = p2.z - p1.z;
	float l = sqrtf( p1.x*p1.x + p1.z*p1.z );
	return ( v1.x*v2.z + v2.z*v1.x )/l;
}

float	GetDist3D( D3DXVECTOR3 p1, D3DXVECTOR3 p2 )
{
	D3DXVECTOR3 p = p1 - p2;

	return sqrtf( p.x*p.x + p.y*p.y + p.z*p.z );
}

void	UVAnimetion( LPIEXMESH lpMesh, float tu, float tv )
{
	DWORD vcnt;
	LPMESHVERTEX lpV;


	lpMesh->lpMesh->LockVertexBuffer( 0, (void**)&lpV );

	vcnt = lpMesh->lpMesh->GetNumVertices();

	for( DWORD i=0; i<vcnt; i++ ){
		lpV[i].tu += tu;
		lpV[i].tv += tv;
	}

	lpMesh->lpMesh->UnlockVertexBuffer();
}