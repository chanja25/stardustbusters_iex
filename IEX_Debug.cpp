#include	"iExtreme.h"

//*****************************************************************************
//
//		デバッグ関連
//
//*****************************************************************************




//*****************************************************************************
//
//		スクリーンショット
//
//*****************************************************************************

static	BOOL	SaveBMP( LPSTR filename, LPBYTE lpSurface, int width, int height )
{
    HANDLE hf;                  /* file handle */
    BITMAPFILEHEADER hdr;       /* bitmap file-header */
    BITMAPINFOHEADER pbmih;
    DWORD	dwTmp;

	ZeroMemory( &pbmih, sizeof(BITMAPINFOHEADER) );
	ZeroMemory( &hdr, sizeof(BITMAPFILEHEADER) );

    pbmih.biSize     = sizeof(BITMAPINFOHEADER);
	pbmih.biWidth    = width;
	pbmih.biHeight   = -height;
    pbmih.biPlanes   = 1;
    pbmih.biBitCount = 24;
    pbmih.biCompression  = BI_RGB;
    pbmih.biSizeImage    = 0;
    pbmih.biClrImportant = 0;

    hf = CreateFile( filename, GENERIC_READ | GENERIC_WRITE, (DWORD) 0, (LPSECURITY_ATTRIBUTES) NULL,
                   CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL);

    hdr.bfType = 0x4d42;        /* 0x42 = "B" 0x4d = "M" */ 
    hdr.bfSize = (DWORD) (sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER) + width*3*height);
    hdr.bfReserved1 = 0;
    hdr.bfReserved2 = 0;
    hdr.bfOffBits = sizeof(BITMAPFILEHEADER) + sizeof(BITMAPINFOHEADER);

    WriteFile(hf, &hdr, sizeof(BITMAPFILEHEADER),(LPDWORD) &dwTmp, (LPOVERLAPPED) NULL);
    WriteFile(hf, &pbmih, sizeof(BITMAPINFOHEADER),(LPDWORD) &dwTmp, (LPOVERLAPPED) NULL);


	WriteFile(hf, lpSurface, width*height*3, (LPDWORD)&dwTmp, (LPOVERLAPPED)NULL );

	CloseHandle( hf);

	return TRUE;
}

void	IEX_DEBUG_ScreenShot( LPSTR filename )
{
	int		x, y;
	LPBYTE	temp, bits;
	IDirect3DSurface9 *pDestSurface;
	D3DLOCKED_RECT	rect;
	int		width, height;

	int deskTopX = GetSystemMetrics(SM_CXSCREEN);
    int deskTopY = GetSystemMetrics(SM_CYSCREEN);
	IEX_GetD3DDevice()->CreateOffscreenPlainSurface(deskTopX,deskTopY,D3DFMT_A8R8G8B8,D3DPOOL_SYSTEMMEM,&pDestSurface, NULL );
	IEX_GetD3DDevice()->GetFrontBufferData( 0, pDestSurface );

	POINT	p = { 0, 0 };
	RECT	rc;
	ClientToScreen( IEX_GetWindow(), &p ); 
	GetClientRect( IEX_GetWindow(), &rc );
	width  = rc.right  - rc.left;
	height = rc.bottom - rc.top;

	pDestSurface->LockRect( &rect, NULL, D3DLOCK_READONLY );
	bits = new BYTE[width*height*3];
	for( x=0 ; x<width ; x++ ){
		for( y=0 ; y<height ; y++ ){
			temp = (LPBYTE)(rect.pBits) + (y+p.y)*rect.Pitch + (x+p.x)*4;
			CopyMemory( &bits[ (y*width+x) * 3 ], temp, 3 );
		}
	}

	pDestSurface->UnlockRect();
	pDestSurface->Release();

	SaveBMP( filename, bits, width,height );

	delete[] bits;
}

