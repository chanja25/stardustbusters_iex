#ifndef __SCRIPT_H__
#define __SCRIPT_H__


//スクリプトクラス
class Script_Class{
private:
	FileC File;
	int ScriptSize;
	int TextIP;
	char *TextBuf;

	int WaitCnt;
	BOOL WaitFlag;
	BOOL WaitStrFlag;
	BOOL EndFlag;
public:
	void Init();

	void LoadScript( char *filename );
	void Proc();

	char *GetTextBuf();
	BOOL GetScript();
	BOOL GetParamTop();
	char *GetParam( char *param );
	void ParamNext();
	char *GetParamStr();
	int GetParamInt();
	float GetParamFloat();
	LONG GetParamLong();

	void SetEventFlag( BOOL flg );
	void SetWait( int time );
	void SetWaitFlag( BOOL flg );
	void SetWaitStrFlag( BOOL flg );
	void SetEndFlag( BOOL flg );
};

#endif