#include	"iExtreme.h"

//*****************************************************************************
//
//
//
//*****************************************************************************


//*****************************************************************************
//
//		３Ｄオブジェクト関連
//
//*****************************************************************************

//
//		メッシュ作成
//

static	LPD3DXSKININFO	CreateSkinInfo( LPIEMFILE lpIem )
{
	int				i;
	LPD3DXSKININFO	lpInfo;
	u32				Declaration = D3DFVF_MESHVERTEX;

	//	スキン情報作成
	D3DXCreateSkinInfoFVF( lpIem->NumVertex, Declaration, lpIem->NumBone, &lpInfo );
	//	ボーン設定
	for( i=0 ; i<lpIem->NumBone ; i++ ){
		lpInfo->SetBoneInfluence( i, lpIem->lpBone[i].IndexNum, lpIem->lpBone[i].Index, lpIem->lpBone[i].Influence );
	}
	return lpInfo;
}

static	LPD3DXMESH		CreateMesh( LPIEMFILE lpIem )
{
	LPD3DXMESH	lpMesh;
	u32			Declaration = D3DFVF_XYZ|D3DFVF_NORMAL|D3DFVF_TEX1;
	u8			*pVertex, *pFace;
	u32			*pData;

	//	メッシュ作成
	D3DXCreateMeshFVF( lpIem->NumFace, lpIem->NumVertex, D3DXMESH_MANAGED, Declaration, IEX_GetD3DDevice(), &lpMesh );
	//	頂点設定
	lpMesh->LockVertexBuffer( 0, (void**)&pVertex );
	CopyMemory( pVertex, lpIem->lpVertex, sizeof(MESHVERTEX)*lpIem->NumVertex );
	lpMesh->UnlockVertexBuffer();
	
	//	面設定
	lpMesh->LockIndexBuffer( 0, (void**)&pFace );
	CopyMemory( pFace, lpIem->lpFace, sizeof(u16)*lpIem->NumFace*3 );
	lpMesh->UnlockIndexBuffer();

	//	属性設定
	lpMesh->LockAttributeBuffer( 0, &pData );
	CopyMemory( pData, lpIem->lpAtr, sizeof(u32)*lpIem->NumFace );
	lpMesh->UnlockAttributeBuffer();

	return lpMesh;
}

//
//
//

LPIEX3DOBJ	IEX_Create3DObjectFromIEM( LPIEMFILE lpIem )
{
	u32		i, j;

	LPIEX3DOBJ	lpObj;

	lpObj = new IEX3DOBJ;
	ZeroMemory( lpObj, sizeof(IEX3DOBJ) );
	lpObj->lpMesh = new IEXMESH;

	lpObj->bFlag   = 0;
	lpObj->dwFrame = 0;

	//
	//	メッシュ作成
	//

	LPIEXMESH	lpMesh;
	lpMesh = lpObj->lpMesh;
	ZeroMemory( lpMesh, sizeof(IEXMESH) );

	//	メッシュ作成
	lpObj->lpSkinInfo = CreateSkinInfo( lpIem );
	lpMesh->lpMesh    = CreateMesh( lpIem );
	//	頂点情報コピー
	lpObj->NumVertex = lpIem->NumVertex;
	lpObj->lpVertex  = new MESHVERTEX[lpObj->NumVertex];
	CopyMemory( lpObj->lpVertex, lpIem->lpVertex, sizeof(MESHVERTEX)*lpObj->NumVertex );

	//	マテリアル設定
	lpMesh->MaterialCount = lpIem->NumMaterial;
	lpMesh->lpMaterial = new D3DMATERIAL9[ lpMesh->MaterialCount ];
	CopyMemory( lpMesh->lpMaterial, lpIem->Material, sizeof(D3DMATERIAL9)*lpMesh->MaterialCount );
	//	テクスチャ設定
	lpMesh->lpTexture  = new LPDIRECT3DTEXTURE9[ lpMesh->MaterialCount ];
	ZeroMemory( lpMesh->lpTexture, sizeof(LPDIRECT3DTEXTURE9)*lpMesh->MaterialCount );
	ZeroMemory( lpMesh->bFlags, 32 );

	for( i=0 ; i<lpMesh->MaterialCount ; i++ ){
		if( lpIem->Texture[i][0] == '\0' ) continue;
		lpMesh->lpTexture[i] = IEX_LoadTexture( lpIem->Texture[i] );
		//	フラグ設定
		if( lpMesh->lpMaterial[i].Emissive.r == 1.0f ){
			lpMesh->bFlags[i] = 1;
			lpMesh->lpMaterial[i].Emissive.r = .0f;
			lpMesh->lpMaterial[i].Emissive.g = .0f;
			lpMesh->lpMaterial[i].Emissive.b = .0f;
			lpMesh->lpMaterial[i].Emissive.a = .0f;
		}
	}

	IEX_SetMeshPos( lpMesh, .0f, .0f, .0f );
	IEX_SetMeshAngle( lpMesh, .0f, .0f, .0f );
	IEX_SetMeshScale( lpMesh, 1.0f, 1.0f, 1.0f );
	lpMesh->dwFlags = 0;

	IEX_SetTransMatrixEx( &lpMesh->TransMatrix, &lpMesh->Pos, &lpMesh->Angle, &lpMesh->Scale );

	//
	//	ボーン情報
	//
	lpObj->NumBone = lpIem->NumBone;
	lpObj->BoneParent     = new WORD[ lpObj->NumBone ];
	lpObj->lpBoneMatrix   = new D3DXMATRIX[ lpObj->NumBone ];
	lpObj->lpOffsetMatrix = new D3DXMATRIX[ lpObj->NumBone ];
	lpObj->lpMatrix       = new D3DXMATRIX[ lpObj->NumBone ];

	lpObj->orgPose		= new D3DXQUATERNION[ lpObj->NumBone ];
	lpObj->orgPos		= new D3DVECTOR[ lpObj->NumBone ];
	lpObj->CurPose		= new D3DXQUATERNION[ lpObj->NumBone ];
	lpObj->CurPos		= new D3DVECTOR[ lpObj->NumBone ];

	//
	lpObj->NumFrame = lpIem->MaxFrame;
	CopyMemory( lpObj->M_Offset, lpIem->M_Offset, 2*256 );
	lpObj->dwFrameFlag = new u16[lpObj->NumFrame];
	CopyMemory( lpObj->dwFrameFlag, lpIem->FrameFlag, 2*lpObj->NumFrame );

	//	アニメーション設定
	lpObj->lpAnime = new IEXANIME[ lpObj->NumBone ];

	for( i=0 ; i<lpIem->NumBone ; i++ ){
		lpObj->BoneParent[i] = lpIem->lpBone[i].parent;

		CopyMemory( &lpObj->lpOffsetMatrix[i], &lpIem->lpBone[i].BoneMatrix, sizeof(D3DXMATRIX) );
		CopyMemory( &lpObj->orgPos[i],  &lpIem->lpBone[i].orgPos,  sizeof(D3DVECTOR) );
		CopyMemory( &lpObj->orgPose[i], &lpIem->lpBone[i].orgPose, sizeof(D3DXQUATERNION) );

		//	クォータニオンコピー
		lpObj->lpAnime[i].rotNum = lpIem->lpMotion[i].NumRotate;
		lpObj->lpAnime[i].rotFrame = new WORD[ lpObj->lpAnime[i].rotNum ];
		lpObj->lpAnime[i].rot      = new D3DXQUATERNION[ lpObj->lpAnime[i].rotNum ];
		for( j=0 ; j<lpObj->lpAnime[i].rotNum ; j++ ){
			lpObj->lpAnime[i].rotFrame[j] = lpIem->lpMotion[i].RotateFrame[j];
			CopyMemory( &lpObj->lpAnime[i].rot[j], &lpIem->lpMotion[i].Rotate[j], sizeof(D3DXQUATERNION) );
		}
		//	ポジションコピー
		lpObj->lpAnime[i].posNum   = lpIem->lpMotion[i].NumPosition;
		lpObj->lpAnime[i].posFrame = new WORD[ lpObj->lpAnime[i].posNum ];
		lpObj->lpAnime[i].pos      = new D3DVECTOR[ lpObj->lpAnime[i].posNum ];
		for( j=0 ; j<lpObj->lpAnime[i].posNum ; j++ ){
			lpObj->lpAnime[i].posFrame[j] = lpIem->lpMotion[i].PositionFrame[j];
			CopyMemory( &lpObj->lpAnime[i].pos[j], &lpIem->lpMotion[i].Position[j], sizeof(D3DXVECTOR3) );
		}
	}

	//
	IEX_SetObjectPos( lpObj, .0f, .0f, .0f );
	IEX_SetObjectAngle( lpObj, .0f, .0f, .0f );
	IEX_SetObjectScale( lpObj, 1.0f );

	return	lpObj;
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

//
//	iEMファイル読み込み
//

int		IEX_LoadiEM( LPIEMFILE lpIem, LPSTR filename )
{
	HANDLE	hfile;
	u32		dum, FileID;
	int		version, i;

	hfile = CreateFile( filename, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( hfile == INVALID_HANDLE_VALUE ) return 0;

	//	ID
	ReadFile( hfile, &FileID, 4, &dum, NULL );
	if( FileID == '1MEI' ) version = 1;
	 else if( FileID == '2MEI' ) version = 2;
	 else version = -1;

	//	頂点	
	ReadFile( hfile, &lpIem->NumVertex, sizeof(u16), &dum, NULL );
	lpIem->lpVertex = new MESHVERTEX[ lpIem->NumVertex ];
	ReadFile( hfile, lpIem->lpVertex,   sizeof(MESHVERTEX)*lpIem->NumVertex, &dum, NULL );
	//	ポリゴン	
	ReadFile( hfile, &lpIem->NumFace, sizeof(u16), &dum, NULL );
	lpIem->lpFace = new u16[ lpIem->NumFace*3 ];
	lpIem->lpAtr  = new u32[ lpIem->NumFace ];
	ReadFile( hfile, lpIem->lpFace,   sizeof(u16)*lpIem->NumFace*3, &dum, NULL );
	ReadFile( hfile, lpIem->lpAtr,    sizeof(u32)*lpIem->NumFace, &dum, NULL );
	//	マテリアル	
	ReadFile( hfile, &lpIem->NumMaterial, sizeof(u16), &dum, NULL );
	ReadFile( hfile, lpIem->Material,     sizeof(D3DMATERIAL9)*lpIem->NumMaterial, &dum, NULL );
	ReadFile( hfile, lpIem->Texture,      sizeof(char)*lpIem->NumMaterial*64, &dum, NULL );

	//	ボーン
	ReadFile( hfile, &lpIem->NumBone, sizeof(u16), &dum, NULL );
	lpIem->lpBone = new IEMBONE[ lpIem->NumBone ];
	if( version == 1 ){		//	バージョン１用
		LPIEMBONE1	lpBone;	
		lpBone = new IEMBONE1[ lpIem->NumBone ];
		ReadFile( hfile, lpBone, sizeof(IEMBONE1)*lpIem->NumBone, &dum, NULL );

		for( i=0 ; i<lpIem->NumBone ; i++ ){
			lpIem->lpBone[i].IndexNum   = lpBone[i].IndexNum;
			lpIem->lpBone[i].parent     = lpBone[i].parent;
			lpIem->lpBone[i].BoneMatrix = lpBone[i].BoneMatrix;
			lpIem->lpBone[i].orgPos     = lpBone[i].orgPos;
			lpIem->lpBone[i].orgPose    = lpBone[i].orgPose;

			lpIem->lpBone[i].Index = new DWORD[lpBone[i].IndexNum];
			CopyMemory( lpIem->lpBone[i].Index, lpBone[i].Index, sizeof(DWORD)*lpBone[i].IndexNum );

			lpIem->lpBone[i].Influence = new float[lpBone[i].IndexNum];
			CopyMemory( lpIem->lpBone[i].Influence, lpBone[i].Influence, sizeof(float)*lpBone[i].IndexNum );
		}
		delete[]	lpBone;
	} else {
		for( i=0 ; i<lpIem->NumBone ; i++ ){
			ReadFile( hfile, &lpIem->lpBone[i], sizeof(IEMBONE), &dum, NULL );

			lpIem->lpBone[i].Index = new DWORD[lpIem->lpBone[i].IndexNum];
			ReadFile( hfile, lpIem->lpBone[i].Index, sizeof(DWORD)*lpIem->lpBone[i].IndexNum, &dum, NULL );

			lpIem->lpBone[i].Influence = new float[lpIem->lpBone[i].IndexNum];
			ReadFile( hfile, lpIem->lpBone[i].Influence, sizeof(float)*lpIem->lpBone[i].IndexNum, &dum, NULL );
		}
	}
	
	//	モーション
	ReadFile( hfile, &lpIem->NumMotion, sizeof(u16), &dum, NULL );
	ReadFile( hfile, &lpIem->MaxFrame,  sizeof(u16), &dum, NULL );
	ReadFile( hfile, &lpIem->M_Offset,  sizeof(u16)*256, &dum, NULL );
	ReadFile( hfile, &lpIem->FrameFlag, sizeof(u16)*lpIem->MaxFrame, &dum, NULL );
	lpIem->lpMotion = new IEMMOTION[ lpIem->NumBone ];

	if( version == 1 ){		//	バージョン１用
		LPIEMMOTION1	lpMotion;	
		lpMotion = new IEMMOTION1[ lpIem->NumBone ];
		ReadFile( hfile, lpMotion,   sizeof(IEMMOTION1)*lpIem->NumBone, &dum, NULL );
		for( i=0 ; i<lpIem->NumBone ; i++ ){
			lpIem->lpMotion[i].NumPosition = lpMotion[i].NumPosition;
			lpIem->lpMotion[i].NumRotate   = lpMotion[i].NumRotate;

			lpIem->lpMotion[i].Rotate       = new D3DXQUATERNION[lpMotion[i].NumRotate];
			lpIem->lpMotion[i].RotateFrame  = new WORD[lpMotion[i].NumRotate];
			CopyMemory( lpIem->lpMotion[i].Rotate, lpMotion[i].Rotate, sizeof(D3DXQUATERNION)*lpMotion[i].NumRotate );
			CopyMemory( lpIem->lpMotion[i].RotateFrame, lpMotion[i].RotateFrame, sizeof(WORD)*lpMotion[i].NumRotate );

			lpIem->lpMotion[i].Position      = new D3DXVECTOR3[lpMotion[i].NumPosition];
			lpIem->lpMotion[i].PositionFrame = new WORD[lpMotion[i].NumPosition];
			CopyMemory( lpIem->lpMotion[i].Position, lpMotion[i].Position, sizeof(D3DXVECTOR3)*lpMotion[i].NumPosition );
			CopyMemory( lpIem->lpMotion[i].PositionFrame, lpMotion[i].PositionFrame, sizeof(WORD)*lpMotion[i].NumPosition );
		}
		delete[] lpMotion;
	} else {
		for( i=0 ; i<lpIem->NumBone ; i++ ){
			ReadFile( hfile, &lpIem->lpMotion[i], sizeof(IEMMOTION), &dum, NULL );

			lpIem->lpMotion[i].Rotate       = new D3DXQUATERNION[lpIem->lpMotion[i].NumRotate];
			lpIem->lpMotion[i].RotateFrame  = new WORD[lpIem->lpMotion[i].NumRotate];
			ReadFile( hfile, lpIem->lpMotion[i].Rotate, sizeof(D3DXQUATERNION)*lpIem->lpMotion[i].NumRotate, &dum, NULL );
			ReadFile( hfile, lpIem->lpMotion[i].RotateFrame, sizeof(WORD)*lpIem->lpMotion[i].NumRotate, &dum, NULL );

			lpIem->lpMotion[i].Position      = new D3DXVECTOR3[lpIem->lpMotion[i].NumPosition];
			lpIem->lpMotion[i].PositionFrame = new WORD[lpIem->lpMotion[i].NumPosition];
			ReadFile( hfile, lpIem->lpMotion[i].Position, sizeof(D3DXVECTOR3)*lpIem->lpMotion[i].NumPosition, &dum, NULL );
			ReadFile( hfile, lpIem->lpMotion[i].PositionFrame, sizeof(WORD)*lpIem->lpMotion[i].NumPosition, &dum, NULL );
		}
	}

	CloseHandle(hfile);

	return version;
}

//
//		３Ｄオブジェクト読み込み
//

LPIEX3DOBJ	IEX_Load3DObject( LPSTR filename )
{
	LPIEX3DOBJ	lpObj;
	IEMFILE		iem;
	char		path[MAX_PATH], workpath[MAX_PATH];
	int			version;

	version = IEX_LoadiEM( &iem, filename );
	
	GetCurrentDirectory( MAX_PATH, path );
	CopyMemory( workpath, filename, lstrlen(filename)+1 );
	for( int i=lstrlen(filename) ; i>0 ; i-- ){
		if( IsDBCSLeadByte(workpath[i-2]) ){
			i--;
			continue;
		}
		if( workpath[i-1] == '\\' || workpath[i-1] == '/' ){
			workpath[i] = '\0';
			SetCurrentDirectory(workpath);
			break;
		}
	}
	lpObj = IEX_Create3DObjectFromIEM( &iem );

	SetCurrentDirectory(path);

	for( int i=0 ; i<iem.NumBone ; i++ ){
		delete[]	iem.lpBone[i].Index;
		delete[]	iem.lpBone[i].Influence;
		delete[]	iem.lpMotion[i].Rotate;
		delete[]	iem.lpMotion[i].RotateFrame;
		delete[]	iem.lpMotion[i].Position;
		delete[]	iem.lpMotion[i].PositionFrame;
	}
	delete[]	iem.lpVertex;
	delete[]	iem.lpFace;
	delete[]	iem.lpAtr;
	delete[]	iem.lpBone;
	delete[]	iem.lpMotion;
	return lpObj;
}

//
//		３Ｄオブジェクト保存
//

BOOL	IEX_Save3DObject( LPIEMFILE lpIem, LPSTR filename )
{
	HANDLE	hfile;
	int		i;
	u32		dum;
	u32		FileID = '2MEI';

	hfile = CreateFile( filename, GENERIC_WRITE, FILE_SHARE_WRITE, (LPSECURITY_ATTRIBUTES)NULL, CREATE_ALWAYS, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( hfile == INVALID_HANDLE_VALUE ) return FALSE;
	//	ID
	WriteFile( hfile, &FileID, 4, &dum, NULL );
	//	頂点	
	WriteFile( hfile, &lpIem->NumVertex, sizeof(u16), &dum, NULL );
	WriteFile( hfile, &lpIem->lpVertex[0], sizeof(MESHVERTEX)*lpIem->NumVertex, &dum, NULL );
	//	ポリゴン	
	WriteFile( hfile, &lpIem->NumFace, sizeof(u16), &dum, NULL );
	WriteFile( hfile, lpIem->lpFace,   sizeof(u16)*lpIem->NumFace*3, &dum, NULL );
	WriteFile( hfile, lpIem->lpAtr,    sizeof(u32)*lpIem->NumFace, &dum, NULL );
	//	マテリアル	
	WriteFile( hfile, &lpIem->NumMaterial, sizeof(u16), &dum, NULL );
	WriteFile( hfile, lpIem->Material,     sizeof(D3DMATERIAL9)*lpIem->NumMaterial, &dum, NULL );
	WriteFile( hfile, lpIem->Texture,      sizeof(char)*lpIem->NumMaterial*64, &dum, NULL );

	//	ボーン
	WriteFile( hfile, &lpIem->NumBone, sizeof(u16), &dum, NULL );
	for( i=0 ; i<lpIem->NumBone ; i++ ){
		WriteFile( hfile, &lpIem->lpBone[i],   sizeof(IEMBONE), &dum, NULL );
		WriteFile( hfile, lpIem->lpBone[i].Index,     sizeof(DWORD) *lpIem->lpBone[i].IndexNum, &dum, NULL );
		WriteFile( hfile, lpIem->lpBone[i].Influence, sizeof(float)*lpIem->lpBone[i].IndexNum, &dum, NULL );
	}

	//	モーション
	WriteFile( hfile, &lpIem->NumMotion, sizeof(u16), &dum, NULL );
	WriteFile( hfile, &lpIem->MaxFrame,  sizeof(u16), &dum, NULL );
	WriteFile( hfile, &lpIem->M_Offset,  sizeof(u16)*256, &dum, NULL );
	WriteFile( hfile, &lpIem->FrameFlag, sizeof(u16)*lpIem->MaxFrame, &dum, NULL );

	for( i=0 ; i<lpIem->NumBone ; i++ ){
		WriteFile( hfile, &lpIem->lpMotion[i],   sizeof(IEMMOTION), &dum, NULL );
		WriteFile( hfile, lpIem->lpMotion[i].Rotate,      sizeof(D3DXQUATERNION)*lpIem->lpMotion[i].NumRotate, &dum, NULL );
		WriteFile( hfile, lpIem->lpMotion[i].RotateFrame, sizeof(WORD)          *lpIem->lpMotion[i].NumRotate, &dum, NULL );
		WriteFile( hfile, lpIem->lpMotion[i].Position,      sizeof(D3DXVECTOR3)*lpIem->lpMotion[i].NumPosition, &dum, NULL );
		WriteFile( hfile, lpIem->lpMotion[i].PositionFrame, sizeof(WORD)       *lpIem->lpMotion[i].NumPosition, &dum, NULL );
	}

	CloseHandle(hfile);

	return TRUE;
}

//
//		解放
//

void	IEX_Release3DObject( LPIEX3DOBJ lpObj )
{
	if( !lpObj ) return;
	/*	メッシュ解放	*/ 
	IEX_ReleaseMesh( lpObj->lpMesh );

	if( lpObj->lpSkinInfo ) lpObj->lpSkinInfo->Release();
	delete[] lpObj->BoneParent;
	delete[] lpObj->lpBoneMatrix;
	delete[] lpObj->lpOffsetMatrix;
	delete[] lpObj->lpMatrix;

	delete[] lpObj->orgPose;
	delete[] lpObj->orgPos;
	delete[] lpObj->CurPose;
	delete[] lpObj->CurPos;

	delete[] lpObj->lpVertex;

	//	アニメーションデータ解放
	for( DWORD i=0 ; i<lpObj->NumBone ; i++ ){
		delete[] lpObj->lpAnime[i].rotFrame;
		delete[] lpObj->lpAnime[i].rot;
		delete[] lpObj->lpAnime[i].posFrame;
		delete[] lpObj->lpAnime[i].pos;
	}
	delete[] lpObj->dwFrameFlag;
	delete[] lpObj->lpAnime;
	/*	解放			*/ 
	delete lpObj;
}

/*	転送行列作成	*/ 
void	IEX_SetTransMatrixEx( LPD3DXMATRIX lpMatrix, D3DVECTOR* Pos, D3DVECTOR* Angle, D3DVECTOR* Scale )
{
	D3DXMATRIX	MatScale;

	/*	変換行列の作成		*/ 
	SetTransformMatrixXYZ( lpMatrix, Pos->x, Pos->y, Pos->z, Angle->x, Angle->y, Angle->z );
	/*	スケーリング		*/ 
	if( Scale->x != 1.0f ){ lpMatrix->_11*=Scale->x;	lpMatrix->_12*=Scale->x;	lpMatrix->_13*=Scale->x; }
	if( Scale->y != 1.0f ){ lpMatrix->_21*=Scale->y;	lpMatrix->_22*=Scale->y;	lpMatrix->_23*=Scale->y; }
	if( Scale->z != 1.0f ){ lpMatrix->_31*=Scale->z;	lpMatrix->_32*=Scale->z;	lpMatrix->_33*=Scale->z; }
}

//
//		オブジェクトのレンダリング
//

void	IEX_NoRender3DObject( LPIEX3DOBJ lpObj )
{
	if( !lpObj ) return;
	/*	メイン行列設定		*/ 
	if( lpObj->bChanged ) IEX_SetTransMatrixEx( &lpObj->TransMatrix, &lpObj->Pos, &lpObj->Angle, &lpObj->Scale );
	/*	スキンメッシュ更新	*/ 
	IEX_UpdateSkinMeshFrame(lpObj, (float)lpObj->dwFrame );
	IEX_UpdateSkinMatrix(lpObj);
	IEX_UpdateSkinMesh(lpObj);

	lpObj->bChanged = FALSE;
}

void	IEX_Render3DObject( LPIEX3DOBJ lpObj )
{
	if( !lpObj ) return;

	//	情報更新
	if( lpObj->bChanged ) IEX_NoRender3DObject( lpObj );
	//	行列更新
	IEX_SetTransMatrixEx( &lpObj->lpMesh->TransMatrix, &lpObj->lpMesh->Pos, &lpObj->lpMesh->Angle, &lpObj->lpMesh->Scale );
	lpObj->lpMesh->TransMatrix = lpObj->TransMatrix * lpObj->lpMesh->TransMatrix;

	IEX_RenderMesh( lpObj->lpMesh, RM_NORMAL, -1.0f);

	lpObj->RenderFrame = lpObj->dwFrame;
	return;
}

void	IEX_Render3DObject( LPIEX3DOBJ lpObj, DWORD flag, float alpha )
{
	if( !lpObj ) return;

	//	情報更新
	if( lpObj->bChanged ) IEX_NoRender3DObject( lpObj );
	//	行列更新
	IEX_SetTransMatrixEx( &lpObj->lpMesh->TransMatrix, &lpObj->lpMesh->Pos, &lpObj->lpMesh->Angle, &lpObj->lpMesh->Scale );
	lpObj->lpMesh->TransMatrix = lpObj->TransMatrix * lpObj->lpMesh->TransMatrix;

	IEX_RenderMesh( lpObj->lpMesh, flag, alpha );

	lpObj->RenderFrame = lpObj->dwFrame;
	return;
}

//
//		フレーム進行
//

void	IEX_ObjectFrameNext( LPIEX3DOBJ lpObj )
{
	int		Param;
	u32	work;

	if( !lpObj ) return;

	work = lpObj->dwFrame;
	Param = lpObj->dwFrameFlag[lpObj->dwFrame];
	if( Param & 0x4000 ) Param = 0xFFFF;
	if( Param != 0xFFFF ){
		//	アニメーションジャンプ
		if( Param & 0x8000 ){
			IEX_SetObjectMotion( lpObj, Param&0xFF );
		} else lpObj->dwFrame = Param;
	} else {
		lpObj->dwFrame ++;
		if( lpObj->dwFrame >= lpObj->NumFrame ) lpObj->dwFrame = 0;
	}

	if( lpObj->dwFrame != work ) lpObj->bChanged = TRUE;

	Param = lpObj->dwFrameFlag[lpObj->dwFrame];
	if( (Param!=0xFFFF) && (Param&0x4000) ) lpObj->Param[(Param&0x0F00)>>8] = (u8)(Param&0x00FF);
}

//
//		モーション設定
//

void	IEX_SetObjectMotion( LPIEX3DOBJ lpObj, int motion )
{
	int		Param;

	if( !lpObj ) return;
	if( lpObj->M_Offset[motion] == 65535 ) return;
	lpObj->Motion  = motion;
	lpObj->dwFrame = lpObj->M_Offset[motion];
	lpObj->bChanged = TRUE;

	Param = lpObj->dwFrameFlag[lpObj->dwFrame];
	if( (Param!=0xFFFF) && (Param&0x4000) ) lpObj->Param[(Param&0x0F00)>>8] = (u8)(Param&0x00FF);
}

//
//		モーション取得
//

int		IEX_GetObjectMotion( LPIEX3DOBJ lpObj )
{
	if( !lpObj ) return -1;
	return	lpObj->Motion;
}

//
//		座標変更
//

void	IEX_SetObjectPos( LPIEX3DOBJ lpObj, float x, float y, float z  )
{
	if( !lpObj ) return;
	lpObj->Pos.x = x;
	lpObj->Pos.y = y;
	lpObj->Pos.z = z;
	lpObj->bChanged = TRUE;
}

//
//			回転変更
//

void	IEX_SetObjectAngle( LPIEX3DOBJ lpObj, float x, float y, float z  )
{
	if( !lpObj ) return;
	lpObj->Angle.x = x;
	lpObj->Angle.y = y;
	lpObj->Angle.z = z;
	lpObj->bChanged = TRUE;
}

//
//		スケール変更
//

void	IEX_SetObjectScale( LPIEX3DOBJ lpObj, float scale )
{
	if( !lpObj ) return;
	lpObj->Scale.x = scale;
	lpObj->Scale.y = scale;
	lpObj->Scale.z = scale;
	lpObj->bChanged = TRUE;
}

void	IEX_SetObjectScale( LPIEX3DOBJ lpObj, float scaleX, float scaleY, float scaleZ )
{
	if( !lpObj ) return;
	lpObj->Scale.x = scaleX;
	lpObj->Scale.y = scaleY;
	lpObj->Scale.z = scaleZ;
	lpObj->bChanged = TRUE;
}

//
//		パラメータ取得
//

u8	IEX_GetObjectParam( LPIEX3DOBJ lpObj, int index )
{
	return	lpObj->Param[index];
}

void	IEX_SetObjectParam( LPIEX3DOBJ lpObj, int index, u8 param )
{
	lpObj->Param[index] = param;
}

//*****************************************************************************
//
//
//
//*****************************************************************************

//	t = (d- (apx + bpy + cpz)) / (avx + bvy + cvz)
//	(a, b, c) = 平面の法線ベクトル
//	(x, y, z) = 平面ｊ内の任意の一点
//	p = (px, py, pz) = 直線上の一点
//	v = (vx, vy, vz) = 直線の方向ベクトル

//
//		レイピック（真上・真下）
//

int	IEX_RayPickMeshUD( LPIEXMESH lpMesh, D3DVECTOR* out, D3DVECTOR* pos, D3DVECTOR* vec, float *Dist )
{
	float	t, neart, dist;
	float	px, py, pz;
	float	vy;
	int		ret = -1;

	u32		j;

	float	*pVertices;
	u16		*pIndices;
	u32		NumIndices;

	u32		fvf;
	int		VertexSize;

	D3DVECTOR	v1, v2, v3, n;

	px = pos->x;	py = pos->y;	pz = pos->z;
	vy = vec->y;

	neart = 1000000.0f;
	dist = *Dist;

	//	情報取得	
	fvf = lpMesh->lpMesh->GetFVF();
	//	頂点サイズ計算
	VertexSize = D3DXGetFVFVertexSize(fvf) / sizeof(float);

	//	バッファロック
	NumIndices = lpMesh->lpMesh->GetNumFaces();
	lpMesh->lpMesh->LockVertexBuffer( D3DLOCK_READONLY, (void**)&pVertices );
	lpMesh->lpMesh->LockIndexBuffer( D3DLOCK_READONLY, (void**)&pIndices );

	for( j=0 ; j<NumIndices*3 ; j+=3 ){
		//	面法線算出
		u16 a = pIndices[j+0];
		u16 b = pIndices[j+1];
		u16 c = pIndices[j+2];
		v1.x = pVertices[a*VertexSize];		v1.y = pVertices[a*VertexSize+1];		v1.z = pVertices[a*VertexSize+2];
		v2.x = pVertices[b*VertexSize];		v2.y = pVertices[b*VertexSize+1];		v2.z = pVertices[b*VertexSize+2];
		v3.x = pVertices[c*VertexSize];		v3.y = pVertices[c*VertexSize+1];		v3.z = pVertices[c*VertexSize+2];
		
		//	内点判定（全外積がマイナス）
		float	ax, az, bx, bz;
		
		ax = v1.x - px;		az = v1.z - pz;
		bx = v2.x - v1.x;	bz = v2.z - v1.z;
		if( (ax*bz - az*bx)*vy < 0 ) continue;

		ax = v2.x - px;		az = v2.z - pz;
		bx = v3.x - v2.x;	bz = v3.z - v2.z;
		if( (ax*bz - az*bx)*vy < 0 ) continue;

		ax = v3.x - px;		az = v3.z - pz;
		bx = v1.x - v3.x;	bz = v1.z - v3.z;
		if( (ax*bz - az*bx)*vy < 0 ) continue;
		//	外積による法線算出		
		n.x = (v2.y-v1.y)*(v3.z-v1.z) - (v2.z-v1.z)*(v3.y-v1.y);
		n.y = (v2.z-v1.z)*(v3.x-v1.x) - (v2.x-v1.x)*(v3.z-v1.z);
		n.z = (v2.x-v1.x)*(v3.y-v1.y) - (v2.y-v1.y)*(v3.x-v1.x);
		//	表裏判定
		if( vec->y*n.y >= .0f ) continue;
		//	交点算出
		if( n.y == .0f ) continue;

		t = (n.x*(v1.x-px) + n.y*(v1.y-py) + n.z*(v1.z-pz)) / (n.y*vy);
		if( t < .0f || t > neart ) continue;

		out->x = px;
		out->y = t*vy + py;
		out->z = pz;
		ret = j/3;
		neart = t;
	}
	lpMesh->lpMesh->UnlockVertexBuffer();
	lpMesh->lpMesh->UnlockIndexBuffer();
	*Dist = neart;
	return	ret;
}

//
//		レイピック（通常）
//

int	IEX_RayPickMesh( LPIEXMESH lpMesh, D3DVECTOR* out, D3DVECTOR* pos, D3DVECTOR* vec, float *Dist )
{
	float	t, neart, dist;
	float	px, py, pz;
	float	vx, vy, vz;
	float	dot;
	int		ret = -1;
	D3DVECTOR	cp;

	u32		j;

	float	*pVertices;
	u16		*pIndices;
	u32		NumIndices;

	u32		fvf;
	int		VertexSize;

	D3DVECTOR	v1, v2, v3, n;

	if( vec->x == .0f && vec->z == .0f ) return IEX_RayPickMeshUD( lpMesh, out, pos, vec, Dist );

	px = pos->x;	py = pos->y;	pz = pos->z;
	vx = vec->x;	vy = vec->y;	vz = vec->z;

	neart = 1000000.0f;
	dist = *Dist;
	dist = dist*dist;

	//	情報取得	
	fvf = lpMesh->lpMesh->GetFVF();
	//	頂点サイズ計算
	VertexSize = D3DXGetFVFVertexSize(fvf) / sizeof(float);

	//	バッファロック
	NumIndices = lpMesh->lpMesh->GetNumFaces();
	lpMesh->lpMesh->LockVertexBuffer( D3DLOCK_READONLY, (void**)&pVertices );
	lpMesh->lpMesh->LockIndexBuffer( D3DLOCK_READONLY, (void**)&pIndices );

	for( j=0 ; j<NumIndices*3 ; j+=3 ){
		//	面法線算出
		u16 a = pIndices[j+0];
		u16 b = pIndices[j+1];
		u16 c = pIndices[j+2];
		v1.x = pVertices[a*VertexSize];		v1.y = pVertices[a*VertexSize+1];		v1.z = pVertices[a*VertexSize+2];
		v2.x = pVertices[b*VertexSize];		v2.y = pVertices[b*VertexSize+1];		v2.z = pVertices[b*VertexSize+2];
		v3.x = pVertices[c*VertexSize];		v3.y = pVertices[c*VertexSize+1];		v3.z = pVertices[c*VertexSize+2];
		
		//	距離判定		
		float	xx, yy, zz;
		xx = (v1.x + v2.x + v3.x) / 3.0f - px;
		yy = (v1.y + v2.y + v3.y) / 3.0f - py;
		zz = (v1.z + v2.z + v3.z) / 3.0f - pz;
		if( (xx*xx + yy*yy + zz*zz) > dist ) continue;
		//	外積による法線算出		
		n.x = (v2.y-v1.y)*(v3.z-v1.z) - (v2.z-v1.z)*(v3.y-v1.y);
		n.y = (v2.z-v1.z)*(v3.x-v1.x) - (v2.x-v1.x)*(v3.z-v1.z);
		n.z = (v2.x-v1.x)*(v3.y-v1.y) - (v2.y-v1.y)*(v3.x-v1.x);
		//	内積の結果がプラスならば裏向き
		dot = vx*n.x + vy*n.y + vz*n.z;
		if( dot > 0 ) continue;
		//	交点算出
		t = (n.x*(v1.x-px) + n.y*(v1.y-py) + n.z*(v1.z-pz)) / (n.x*vx + n.y*vy + n.z*vz );
		if( t < .0f || t > neart ) continue;

		cp.x = t*vx + px;
		cp.y = t*vy + py;
		cp.z = t*vz + pz;
		//	内点判定
		D3DVECTOR	a1, a2, a3;

		a1.x = (v1.y-cp.y)*(v2.z-v1.z) - (v1.z-cp.z)*(v2.y-v1.y);
		a1.y = (v1.z-cp.z)*(v2.x-v1.x) - (v1.x-cp.x)*(v2.z-v1.z);
		a1.z = (v1.x-cp.x)*(v2.y-v1.y) - (v1.y-cp.y)*(v2.x-v1.x);
		if( n.x*a1.x + n.y*a1.y + n.z*a1.z < .0f ) continue;

		a2.x = (v2.y-cp.y)*(v3.z-v2.z) - (v2.z-cp.z)*(v3.y-v2.y);
		a2.y = (v2.z-cp.z)*(v3.x-v2.x) - (v2.x-cp.x)*(v3.z-v2.z);
		a2.z = (v2.x-cp.x)*(v3.y-v2.y) - (v2.y-cp.y)*(v3.x-v2.x);
		if( n.x*a2.x + n.y*a2.y + n.z*a2.z < .0f ) continue;

		a3.x = (v3.y-cp.y)*(v1.z-v3.z) - (v3.z-cp.z)*(v1.y-v3.y);
		a3.y = (v3.z-cp.z)*(v1.x-v3.x) - (v3.x-cp.x)*(v1.z-v3.z);
		a3.z = (v3.x-cp.x)*(v1.y-v3.y) - (v3.y-cp.y)*(v1.x-v3.x);
		if( n.x*a3.x + n.y*a3.y + n.z*a3.z < .0f ) continue;

		out->x = cp.x;
		out->y = cp.y;
		out->z = cp.z;
		vec->x = n.x;
		vec->y = n.y;
		vec->z = n.z;
		ret = j / 3;
		neart = t;
	}
	lpMesh->lpMesh->UnlockVertexBuffer();
	lpMesh->lpMesh->UnlockIndexBuffer();
	*Dist = neart;
	return	ret;
}

BOOL	IEX_RayPick3DObj( LPIEX3DOBJ lpObj, D3DVECTOR* out, D3DVECTOR* pos, D3DVECTOR* vec, float *Dist )
{
	LPIEXMESH	lpMesh;
	D3DVECTOR	apos, wpos, wvec, outw;
	float		neart = 1000000.0f, dist;
	D3DMATRIX	mat;
	BOOL		ret = FALSE;

	if( !lpObj ) return ret;

	dist = *Dist;
	dist *= dist;

	lpMesh = lpObj->lpMesh;
	CopyMemory( &mat, &lpObj->lpMesh->TransMatrix, sizeof(D3DMATRIX) );
	//	メッシュに対する相対座標
	wpos.x = (pos->x-mat._41);
	wpos.y = (pos->y-mat._42);
	wpos.z = (pos->z-mat._43);
	//	座標をメッシュの座標系にあわせる
	apos.x = (wpos.x*mat._11 + wpos.y*mat._12 + wpos.z*mat._13);
	apos.y = (wpos.x*mat._21 + wpos.y*mat._22 + wpos.z*mat._23);
	apos.z = (wpos.x*mat._31 + wpos.y*mat._32 + wpos.z*mat._33);
	apos.x /= lpObj->Scale.x * lpObj->Scale.x;
	apos.y /= lpObj->Scale.y * lpObj->Scale.y;
	apos.z /= lpObj->Scale.z * lpObj->Scale.z;

	wvec.x = (vec->x*mat._11 + vec->y*mat._12 + vec->z*mat._13);
	wvec.y = (vec->x*mat._21 + vec->y*mat._22 + vec->z*mat._23);
	wvec.z = (vec->x*mat._31 + vec->y*mat._32 + vec->z*mat._33);
	
	if( !IEX_RayPickMesh( lpMesh, &outw, &apos, &wvec, &dist ) ) return FALSE;
	if( dist < neart ){
		neart = dist;
		//	変換
		out->x = (outw.x*mat._11 + outw.y*mat._21 + outw.z*mat._31) + mat._41;
		out->y = (outw.x*mat._12 + outw.y*mat._22 + outw.z*mat._32) + mat._42;
		out->z = (outw.x*mat._13 + outw.y*mat._23 + outw.z*mat._33) + mat._43;
		ret = TRUE;
	}

	*Dist = neart;
	return	ret;
}

