#include	"iExtreme.h"

//*****************************************************************************
//
//		３Ｄポリゴン関連
//
//*****************************************************************************

//
//		３Ｄポリゴンレンダリング
//

void	IEX_Render3DPolygon( LPVERTEX v, LPIEX2DOBJ lpObj, u32 dwFlags, float r, float g, float b, float a )
{
	IEX_RenderStripPolygon( v, 2, lpObj, dwFlags, r, g, b, a );
}

void	IEX_Render3DPolygon( LPVERTEX v, LPIEX2DOBJ lpObj, u32 dwFlags, ARGB color )
{
	float	a, r, g, b;
	
	a = ( color >> 24)          / 255.0f;
	r = ((color >> 16) & 0xFF ) / 255.0f;
	g = ((color >>  8) & 0xFF ) / 255.0f;
	b = ((color      ) & 0xFF ) / 255.0f;
	IEX_RenderStripPolygon( v, 2, lpObj, dwFlags, r, g, b, a );
}

void	IEX_Render3DPolygon( D3DVECTOR p[4], float tu[4], float tv[4], LPIEX2DOBJ lpObj, u32 dwFlags, float r, float g, float b, float a )
{
	VERTEX	v[4];

	v[0].x = p[0].x;	v[0].y = p[0].y;	v[0].z = p[0].z;
	v[1].x = p[1].x;	v[1].y = p[1].y;	v[1].z = p[1].z;
	v[2].x = p[3].x;	v[2].y = p[3].y;	v[2].z = p[3].z;
	v[3].x = p[2].x;	v[3].y = p[2].y;	v[3].z = p[2].z;

	v[0].tu = tu[0];	v[0].tv = tv[0];
	v[1].tu = tu[1];	v[1].tv = tv[1];
	v[2].tu = tu[3];	v[2].tv = tv[3];
	v[3].tu = tu[2];	v[3].tv = tv[2];

	IEX_Render3DPolygon( v, lpObj, dwFlags, r, g, b, a );
}

void	IEX_Render3DPolygon( D3DVECTOR p[4], float tu[4], float tv[4], LPIEX2DOBJ lpObj, u32 dwFlags, ARGB color )
{
	float	r, g, b, a;

	a = ( color >> 24)          / 255.0f;
	r = ((color >> 16) & 0xFF ) / 255.0f;
	g = ((color >>  8) & 0xFF ) / 255.0f;
	b = ((color      ) & 0xFF ) / 255.0f;
	IEX_Render3DPolygon( p, tu, tv, lpObj, dwFlags, r, g, b, a );
}

//
//		ストリップポリゴンレンダリング
//

void	IEX_RenderStripPolygon( LPVERTEX lpVertex, int StripNum, LPIEX2DOBJ lpObj, u32 dwFlags, float r, float g, float b, float a )
{
	LPDIRECT3DDEVICE9	lpDevice = IEX_GetD3DDevice();

	//	材質設定
	D3DMATERIAL9		Material;
	ZeroMemory( &Material, sizeof(Material) );
	Material.Diffuse.r = Material.Ambient.r = r;
	Material.Diffuse.g = Material.Ambient.g = g;
	Material.Diffuse.b = Material.Ambient.b = b;
	Material.Diffuse.a = Material.Ambient.a = a;

	//	ワールド行列設定
	D3DXMATRIX	mat;
	D3DXMatrixIdentity(&mat);
	lpDevice->SetTransform( D3DTS_WORLD, (D3DMATRIX*)&mat );

	//	レンダーステート更新
	if( lpObj ) IEX_SetRenderState( dwFlags & 0x0F, &Material, lpObj->lpTexture );
	 else		IEX_SetRenderState( dwFlags & 0x0F, &Material, NULL );

	 //	レンダリング
	lpDevice->SetFVF(D3DFVF_VERTEX);
	lpDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, StripNum, lpVertex, sizeof(VERTEX) );
}

//
//		矩形描画
//

void	IEX_DrawRectZ( s32 DstX, s32 DstY, s32 DstW, s32 DstH, float z, u32 dwFlags, ARGB color )
{
	TLVERTEX	v[4];

	v[0].sx = v[2].sx = (FLOAT)DstX * ScreenAdjust;
	v[1].sx = v[3].sx = (FLOAT)(DstX+DstW) * ScreenAdjust;

	v[0].sy = v[1].sy = (FLOAT)DstY * ScreenAdjust;
	v[2].sy = v[3].sy = (FLOAT)(DstY+DstH) * ScreenAdjust;

	v[0].sz    = v[1].sz    = v[2].sz    = v[3].sz    = z;
	v[0].color = v[1].color = v[2].color = v[3].color = color;
	v[0].rhw   = v[1].rhw   = v[2].rhw   = v[3].rhw   = 1.0f;

	IEX_Render2DPolygon( v, 2, NULL, dwFlags );
} 

void	IEX_DrawRect( s32 DstX, s32 DstY, s32 DstW, s32 DstH, u32 dwFlags, ARGB color )
{
	IEX_DrawRectZ( DstX, DstY, DstW, DstH, .0f, dwFlags, color );
}

//
//		２Ｄポリゴンレンダリング
//

void	IEX_Render2DPolygon( LPTLVERTEX v, int NumPoly, LPIEX2DOBJ lpObj, u32 dwFlags )
{
	LPDIRECT3DDEVICE9	lpDevice = IEX_GetD3DDevice();

	if( lpObj ) IEX_SetRenderState( dwFlags & 0x0F, NULL, lpObj->lpTexture );
	 else IEX_SetRenderState( dwFlags & 0x0F, NULL, NULL );

	lpDevice->SetRenderState( D3DRS_FOGENABLE, FALSE );

	//	レンダリング
	lpDevice->SetFVF(D3DFVF_TLVERTEX);
	lpDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, NumPoly, v, sizeof(TLVERTEX) );

	lpDevice->SetRenderState( D3DRS_FOGENABLE, TRUE );
}

void	IEX_Render2DPolygon( float* vx, float* vy, float* tu, float* tv, int NumPoly, LPIEX2DOBJ lpObj, u32 dwFlags, ARGB* color )
{
	int		i;
	TLVERTEX	v[34];

	if( NumPoly > 32 ) NumPoly = 32;

	for( i=0 ; i<NumPoly+2 ; i++ ){
		//	頂点情報設定
		v[i].sx = vx[i];
		v[i].sy = vy[i];
		v[i].sz = .0f;
		v[i].tu = tu[i];
		v[i].tv = tv[i];
		v[i].color = color[i];
		v[i].rhw   = 1.0f;
	}
	IEX_Render2DPolygon( v, NumPoly, lpObj, dwFlags );
} 

void	IEX_Render2DPolygon( float* vx, float* vy, float* tu, float* tv, int NumPoly, LPIEX2DOBJ lpObj, u32 dwFlags, u32 color )
{
	int		i;
	TLVERTEX	v[34];

	if( NumPoly > 32 ) NumPoly = 32;

	for( i=0 ; i<NumPoly+2 ; i++ ){
		//	頂点情報設定
		v[i].sx = vx[i];
		v[i].sy = vy[i];
		v[i].sz = .0f;
		v[i].tu = tu[i];
		v[i].tv = tv[i];
		v[i].color = color;
		v[i].rhw   = 1.0f;
	}
	IEX_Render2DPolygon( v, NumPoly, lpObj, dwFlags );
} 
