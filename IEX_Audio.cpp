#include "iextreme.h"

#define	NUM_WAV		256
static	HWND	hWndWAV;
/* DirectSound構造体 */ 
static	LPDIRECTSOUND			lpDS = NULL;				// DirectSoundオブジェクト
static	LPDIRECTSOUNDBUFFER		lpPrimaryBuf = NULL;		// 一時バッファ
static	LPDIRECTSOUNDBUFFER		lpSecondaryBuf[NUM_WAV];	// 二時バッファ

static	BOOL	bLock;

//**************************************************************************************************************
//
//		Ｄｉｒｅｃｔ　Ｓｏｕｎｄの初期化
//
//**************************************************************************************************************

BOOL	IEX_InitAudio( HWND hWnd )
{
	int				i;
	DSBUFFERDESC	dsbd;
	WAVEFORMATEX	wfx;

	bLock = FALSE;
	hWndWAV = hWnd;
	/*	DirectSoundの初期化		*/ 
	if( DirectSoundCreate( NULL, &lpDS, NULL ) != DS_OK ){
		lpDS = NULL;
		MessageBox( hWnd, "Direct Sound initialize error!!", "Error", MB_OK );
		return FALSE;
	}
	lpDS->SetCooperativeLevel( hWnd, DSSCL_PRIORITY );
	/*	一次バッファの初期化	*/ 
	ZeroMemory( &dsbd, sizeof(DSBUFFERDESC) );
	dsbd.dwSize  = sizeof(DSBUFFERDESC);
	dsbd.dwFlags = DSBCAPS_PRIMARYBUFFER;
	lpDS->CreateSoundBuffer( &dsbd, &lpPrimaryBuf, NULL);
	for(i=0; i<NUM_WAV; i++) lpSecondaryBuf[i] = NULL;

	/*	ＷＡＶＥフォーマット初期化	*/ 
	ZeroMemory( &wfx, sizeof(WAVEFORMATEX) );
	wfx.wFormatTag      = WAVE_FORMAT_PCM;
	wfx.nChannels       = 2;			/*	stereo	*/ 
	wfx.nSamplesPerSec  = 44100;		/*	44.1K		*/ 
	wfx.wBitsPerSample  = 16;			/*	16bit	*/ 
	wfx.nBlockAlign     = 4;
	wfx.nAvgBytesPerSec = 44100 * 4;
	lpPrimaryBuf->SetFormat(&wfx);

    return TRUE;
}

//**************************************************************************************************************
//
//		Ｄｉｒｅｃｔ　Ｓｏｕｎｄの解放
//
//**************************************************************************************************************

void	IEX_ReleaseAudio(void)
{
	int		i;

	while( bLock );
	bLock = TRUE;
	/*	バッファの解放	*/ 
	for(i=0 ; i<NUM_WAV ; i++){
		if(lpSecondaryBuf[i] != NULL) lpSecondaryBuf[i]->Release();
		lpSecondaryBuf[i] = NULL;
	}

	/*	Direct Sound解放	*/ 
	if(lpPrimaryBuf != NULL) lpPrimaryBuf->Release();
	if(lpDS != NULL) lpDS->Release();
	
	lpDS = NULL;
	lpPrimaryBuf = NULL;
	bLock = FALSE;
}

//**************************************************************************************************************
//
//		ＷＡＶファイルの読み込み
//
//**************************************************************************************************************

static	LPBYTE IEX_LoadWAV( LPSTR fname, LPDWORD size, LPWAVEFORMATEX wfx)
{
	LPBYTE			buf = NULL;			/*	読み込みバッファ	*/ 
	HMMIO			hMMIO= NULL;		/*	ファイルハンドル	*/ 
	PCMWAVEFORMAT	pwf;				/*	WAVデータ形式		*/ 
	MMCKINFO		ckparent, ckinfo;	/*	RIFFチャンク情報	*/ 
	MMIOINFO		mminfo;				/*	ファイル情報		*/ 
	DWORD			i;

	/* オープン	*/ 
	if( (hMMIO = mmioOpen( fname, NULL, MMIO_ALLOCBUF | MMIO_READ)) == NULL) return NULL;
	if( mmioDescend(hMMIO, &ckparent, NULL, 0) != 0) goto WAVE_LoadError;
	/*	ＷＡＶ(RIFF)ファイルチェック		*/ 
	if( (ckparent.ckid!=FOURCC_RIFF) || (ckparent.fccType!=mmioFOURCC('W', 'A', 'V', 'E')) ) goto WAVE_LoadError;
	/*	ｆｍｔチャンクに侵入		*/ 
	ckinfo.ckid = mmioFOURCC('f', 'm', 't', ' ');
	if( mmioDescend(hMMIO, &ckinfo, &ckparent, MMIO_FINDCHUNK) != 0) goto WAVE_LoadError;
	if( ckinfo.cksize < sizeof(PCMWAVEFORMAT)) goto WAVE_LoadError;
    /*	チャンクからリード	*/ 
	if( mmioRead(hMMIO, (HPSTR)&pwf, sizeof(pwf)) != sizeof(pwf)) goto WAVE_LoadError;
	if( pwf.wf.wFormatTag != WAVE_FORMAT_PCM) goto WAVE_LoadError;
	/*	WAVフォーマットの保存	*/ 
	memset( wfx,0,sizeof(WAVEFORMATEX));
	memcpy( wfx, &pwf, sizeof(pwf));
	/*	データの読み込み	*/ 
	if( mmioSeek(hMMIO, ckparent.dwDataOffset + sizeof(FOURCC), SEEK_SET) == -1) goto WAVE_LoadError;
	/*	ｄａｔａチャンクに侵入		*/ 
	ckinfo.ckid = mmioFOURCC('d', 'a', 't', 'a');
	if( mmioDescend(hMMIO, &ckinfo, &ckparent, MMIO_FINDCHUNK) != 0) goto WAVE_LoadError;
	if( mmioGetInfo(hMMIO, &mminfo, 0) != 0) goto WAVE_LoadError;
	/*	バッファサイズ保存	*/ 
	if( size != NULL) *size = ckinfo.cksize;
	/*	ＷＡＶ用バッファの取得	*/ 
	buf = (LPBYTE)GlobalAlloc( LPTR, ckinfo.cksize );
	if( buf == NULL) goto WAVE_LoadError;
	/*	データの読みとり	*/ 
	for(i = 0; i < ckinfo.cksize; i++){
		/*	エラーチェック	*/ 
		if( mminfo.pchNext >= mminfo.pchEndRead ){
			if( mmioAdvance(hMMIO, &mminfo, MMIO_READ) != 0 ) goto WAVE_LoadError;
			if( mminfo.pchNext >= mminfo.pchEndRead ) goto WAVE_LoadError;
		}
		*(buf + i) = *((LPBYTE)mminfo.pchNext);
		mminfo.pchNext++;
	}
	/*	ファイルアクセス終了	*/ 
	mmioSetInfo(hMMIO, &mminfo, 0);
	mmioClose(hMMIO, 0);
	return buf;

WAVE_LoadError:	/*	エラー終了	*/ 
	mmioClose(hMMIO, 0);
	if( buf != NULL ) GlobalFree(buf);
	return NULL;
}

//**************************************************************************************************************
//
//		Ｄｉｒｅｃｔ　Ｓｏｕｎｄ　バッファの再生
//
//**************************************************************************************************************

//
//		再生関連
//

/*	バッファ準備	*/ 
BOOL	IEX_SetWAV( int no, LPSTR fname )
{
	DSBUFFERDESC	dsbd;
	LPVOID			lpbuf1, lpbuf2;
	DWORD			dwbuf1, dwbuf2;

	WAVEFORMATEX	wfx;
	LPBYTE			lpWBuf;
	DWORD			size;

	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return FALSE;
	/*	既存のバッファの解放	*/ 
	if( lpSecondaryBuf[no] != NULL ) lpSecondaryBuf[no]->Release();
	lpSecondaryBuf[no] = NULL;
	/*	WAVファイルのロード	*/ 
	lpWBuf = IEX_LoadWAV( fname,&size,&wfx );
	/*	ロード失敗	*/ 
	if( lpWBuf == NULL ) return FALSE;
    
	/* 二次バッファ作成	*/ 
	ZeroMemory(&dsbd, sizeof(DSBUFFERDESC));
	dsbd.dwSize = sizeof(DSBUFFERDESC);
	dsbd.dwFlags = DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME; 
 	dsbd.dwBufferBytes = size;
	dsbd.lpwfxFormat = &wfx; 
	if( lpDS->CreateSoundBuffer( &dsbd, &lpSecondaryBuf[no],NULL) != DS_OK) return FALSE;
	/* 二次バッファのロック	*/ 
	lpSecondaryBuf[no]->Lock( 0, size,&lpbuf1, &dwbuf1, &lpbuf2, &dwbuf2, 0);
	/* 音源データの設定	*/ 
	memcpy(lpbuf1, lpWBuf, dwbuf1);
	if(dwbuf2 != 0) memcpy(lpbuf2,lpWBuf+dwbuf1, dwbuf2);
	/* 音源データの解放	*/ 
	GlobalFree(lpWBuf);
	/* 二次バッファのロック解除	*/ 
	lpSecondaryBuf[no]->Unlock(lpbuf1, dwbuf1, lpbuf2, dwbuf2); 

	return TRUE;
}

/*	再生	*/ 
void	IEX_PlaySound( int no, BOOL loop )
{
	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return;
	/*	データが無い！！	*/ 
	if( lpSecondaryBuf[no] == NULL ) return;

	lpSecondaryBuf[no]->Stop();
	lpSecondaryBuf[no]->SetCurrentPosition(0);
	/*	ループ再生	*/ 
	if( loop ) lpSecondaryBuf[no]->Play(0, 0, DSBPLAY_LOOPING);
	/*	ノーマル再生	*/ 
	 else	   lpSecondaryBuf[no]->Play(0, 0, 0);
}

/*	停止	*/ 
void	IEX_StopSound( int no )
{
	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return;
	/*	データが無い！！	*/ 
	if( lpSecondaryBuf[no] == NULL ) return;
	/*	再生停止	*/ 
	lpSecondaryBuf[no]->Stop();
}

//
//		状態変更・取得
//

/*	ボリュームの設定	*/ 
void	IEX_SetSoundVolume( int no, int volume )
{
	int		vol;

	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return;
	/*	データが無い！！	*/ 
	if( lpSecondaryBuf[no] == NULL ) return;
	/*	音量セット	*/ 
	vol = volume;
	/*	音量セット	*/ 
	lpSecondaryBuf[no]->SetVolume( vol );
}

/*	再生状況のチェック	*/ 
BOOL	IEX_GetSoundStatus( int no )
{
	DWORD	dwAns;

	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return FALSE;
	/*	データが無い！！	*/ 
	if( lpSecondaryBuf[no] == NULL ) return FALSE;

	lpSecondaryBuf[no]->GetStatus( &dwAns );
	if( dwAns == DSBSTATUS_PLAYING ) return TRUE;
	 else return FALSE;
}

//**************************************************************************************************************
//
//		Ｄｉｒｅｃｔ　Ｓｏｕｎｄ　バッファのストリーム関連
//
//**************************************************************************************************************

#define	STRSECOND	1

//
//		初期化・開放
//

/*	初期化	*/
BOOL	IEX_InitStreamSound( LPDSSTREAM lpStream, int rate )
{
	DSBUFFERDESC	dsbd;
	WAVEFORMATEX	wfx;

	LPDIRECTSOUNDNOTIFY	lpStrNotify;
	DSBPOSITIONNOTIFY	pn[3];

	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return FALSE;
	/*	ＷＡＶＥフォーマット初期化	*/ 
	ZeroMemory( &wfx, sizeof(WAVEFORMATEX) );
	wfx.wFormatTag      = WAVE_FORMAT_PCM;
	wfx.nChannels       = 2;			/*	stereo	*/ 
	wfx.nSamplesPerSec  = rate;			/*	44K		*/ 
	wfx.wBitsPerSample  = 16;			/*	16bit	*/ 
	wfx.nBlockAlign     = 4;
	wfx.nAvgBytesPerSec = rate * 4;
	/* 二次バッファの初期化	*/ 
	ZeroMemory( &dsbd, sizeof(DSBUFFERDESC) );
	dsbd.dwSize        = sizeof(DSBUFFERDESC);
	dsbd.dwFlags       = DSBCAPS_CTRLPAN | DSBCAPS_CTRLVOLUME | DSBCAPS_CTRLFREQUENCY | DSBCAPS_CTRLPOSITIONNOTIFY | DSBCAPS_LOCSOFTWARE;
	dsbd.dwBufferBytes = rate*4 * STRSECOND*2;
	dsbd.lpwfxFormat   = &wfx; 
	if( lpDS->CreateSoundBuffer( &dsbd, &lpStream->lpStream, NULL) != DS_OK) return FALSE;
	lpStream->lpStream->SetFormat(&wfx);

	if(	lpStream->lpStream->QueryInterface( IID_IDirectSoundNotify, (LPVOID*)&lpStrNotify ) != DS_OK ) return FALSE;
	/*	位置イベント作成	*/ 
	lpStream->hEvent[0] = CreateEvent( NULL, FALSE, FALSE, NULL );
	lpStream->hEvent[1] = CreateEvent( NULL, FALSE, FALSE, NULL );
	lpStream->hEvent[2] = CreateEvent( NULL, FALSE, FALSE, NULL );

	pn[0].dwOffset     = 0;
	pn[0].hEventNotify = lpStream->hEvent[0];
	pn[1].dwOffset     = rate*4 * STRSECOND;
	pn[1].hEventNotify = lpStream->hEvent[1];

	pn[2].dwOffset     = DSBPN_OFFSETSTOP;
	pn[2].hEventNotify = lpStream->hEvent[2];

	if( lpStrNotify->SetNotificationPositions( 3, pn ) != DS_OK ) return FALSE; 
	lpStrNotify->Release();

	lpStream->rate = rate;
	return TRUE;
}

BOOL	IEX_ReleaseStreamSound( LPDSSTREAM lpStream )
{
	while( bLock );
	bLock = TRUE;
	if( !lpStream ) return TRUE;

	if( lpStream->lpStream != NULL ){
		CloseHandle( lpStream->hEvent[0] );
		CloseHandle( lpStream->hEvent[1] );
		CloseHandle( lpStream->hEvent[2] );
		lpStream->hEvent[0] = NULL;
		lpStream->hEvent[1] = NULL;
		lpStream->hEvent[2] = NULL;

		if( lpStream->type == TYPE_OGG ) ov_clear(&lpStream->vf);
		 else if( lpStream->hStrFile != (HANDLE)0xffffffff ) CloseHandle( lpStream->hStrFile );

		/*	バッファ開放	*/ 
		if( lpDS ) lpStream->lpStream->Release();
		lpStream->lpStream = NULL;
	}
	delete	lpStream;
	bLock = FALSE;
	return TRUE;
}

//
//		ブロック作成
//

BOOL	OGGRead( LPDSSTREAM lpStream, LPBYTE dst, unsigned long size )
{
	DWORD	remain = size;
	char*	dstPtr = (char*)dst;
	while( remain > 0 ){
		long actualRead = ov_read( &lpStream->vf, dstPtr, remain, 0, 2, 1, NULL);
		//終端チェック
		if(actualRead <= 0){
			if( ov_pcm_seek( &lpStream->vf, 0) ) return FALSE;
		}
		remain -= actualRead;
		dstPtr += actualRead;
	}
	return TRUE;
}

BOOL	IEX_SetStreamBlockOGG( LPDSSTREAM lpStream, int block )
{
	LPBYTE	blk1, blk2;
	DWORD	bs1, bs2;

	DWORD	CurPos;

	CurPos = lpStream->StrSize-lpStream->StrPos;
	//	バッファロック
	lpStream->lpStream->Lock( (lpStream->rate*4*STRSECOND)*block, lpStream->rate*4*STRSECOND, (LPVOID*)&blk1, &bs1, (LPVOID*)&blk2, &bs2, 0 );
	//	ブロック１へのデータセット
	OGGRead( lpStream, blk1, bs1 );
	lpStream->StrPos += bs1;
	//	ブロック２へのデータセット
	if( blk2 ){
		OGGRead( lpStream, blk2, bs2 );
		lpStream->StrPos += bs2;
	}

	lpStream->lpStream->Unlock( blk1,bs1, blk2,bs2 );
	return TRUE;
}


BOOL	IEX_SetStreamBlockWAV( LPDSSTREAM lpStream, int block )
{
	LPBYTE	blk1, blk2;
	DWORD	bs1, bs2, work;

	DWORD	CurPos;

	CurPos = lpStream->StrSize-lpStream->StrPos;
	lpStream->lpStream->Lock( (lpStream->rate*4*STRSECOND)*block, lpStream->rate*4*STRSECOND, (LPVOID*)&blk1, &bs1, (LPVOID*)&blk2, &bs2, 0 );

	ReadFile( lpStream->hStrFile, blk1, bs1, &work, NULL );
	lpStream->StrPos += work;
	if( work < bs1 ){
		/*	巻き戻し	*/ 
		SetFilePointer( lpStream->hStrFile, lpStream->LoopPtr, NULL, FILE_BEGIN );
		ReadFile( lpStream->hStrFile, blk1+work, bs1-work, &work, NULL );
		lpStream->StrPos = work;
	}

	if( blk2 ){
		ReadFile( lpStream->hStrFile, blk2, bs2, &work, NULL );
		lpStream->StrPos += work;
		if( work < bs2 ){
			/*	巻き戻し	*/ 
			SetFilePointer( lpStream->hStrFile, lpStream->LoopPtr, NULL, FILE_BEGIN );
			ReadFile( lpStream->hStrFile, blk2+work, bs2-work, &work, NULL );
			lpStream->StrPos = work;
		}
	}

	lpStream->lpStream->Unlock( blk1,bs1, blk2,bs2 );
	return TRUE;
}

//
//		管理スレッド
//

DWORD	IEX_StreamThread( LPDWORD lpdwParam )
{
	DWORD	param;
	LPDSSTREAM	lpStream;

	lpStream = (LPDSSTREAM)(lpdwParam);
	for(;;){
		param = WaitForMultipleObjects( 3, lpStream->hEvent, FALSE, 100 );
		switch(param){
			case WAIT_OBJECT_0:		if( lpStream->type == TYPE_WAV ) IEX_SetStreamBlockWAV( lpStream, 1 );
									if( lpStream->type == TYPE_OGG ) IEX_SetStreamBlockOGG( lpStream, 1 );
									break;
			case WAIT_OBJECT_0+1:	if( lpStream->type == TYPE_WAV ) IEX_SetStreamBlockWAV( lpStream, 0 );
									if( lpStream->type == TYPE_OGG ) IEX_SetStreamBlockOGG( lpStream, 0 );
									break;

			case WAIT_TIMEOUT:		switch( lpStream->mode ){
									case STR_NORMAL:		break;
									case STR_FADEIN:	lpStream->volume += lpStream->param;
														if( lpStream->volume > 255 ) lpStream->volume = 255;
														IEX_SetStreamSoundVolume( lpStream, lpStream->volume );
														break;
									case STR_FADEOUT:	lpStream->volume -= lpStream->param;
														if( lpStream->volume < 0 ){
															lpStream->lpStream->Stop();
															IEX_ReleaseStreamSound(lpStream);
															ExitThread(TRUE);
															return 0;
														}
														IEX_SetStreamSoundVolume( lpStream, lpStream->volume );
														break;
									}
									break;
			default:
									if( lpStream->lpStream ){
										lpStream->lpStream->Stop();
										IEX_ReleaseStreamSound(lpStream);
									}
									ExitThread(TRUE);
									return 0;

		}
	}
	return 0;
}

//
//	WAV用ストリーム初期化
//

BOOL	IEX_SetWAVStream( LPDSSTREAM lpStream, LPSTR filename )
{
	HMMIO			hMMIO= NULL;		/*	ファイルハンドル	*/ 
	MMCKINFO		ckinfo, ckparent;	/*	RIFFチャンク情報	*/ 
	LRESULT			ptr;

	/* オープン	*/ 
	hMMIO = mmioOpen( filename, NULL, MMIO_ALLOCBUF | MMIO_READ);
	mmioDescend(hMMIO, &ckparent, NULL, 0);
	/*	ｄａｔａチャンクに侵入		*/ 
	ckinfo.ckid = mmioFOURCC('d', 'a', 't', 'a');
	mmioDescend(hMMIO, &ckinfo, &ckparent, MMIO_FINDCHUNK);
	//	現在位置取得
	ptr = mmioSeek(hMMIO, 0, SEEK_CUR);
	/*	ファイルアクセス終了	*/ 
	mmioClose(hMMIO, 0);
	if( ptr == -1 ) return FALSE;

	/*	ファイルオープン	*/ 
	lpStream->hStrFile = CreateFile( filename, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( lpStream->hStrFile == (HANDLE)0xffffffff ) return FALSE;
	//	ストリーム情報設定
	lpStream->StrPos  = 0;
	lpStream->LoopPtr = ptr;
	lpStream->StrSize = GetFileSize( lpStream->hStrFile, NULL ) - lpStream->LoopPtr;
	//	ファイルシーク
	SetFilePointer( lpStream->hStrFile, lpStream->LoopPtr, NULL, FILE_BEGIN );

	lpStream->type = TYPE_WAV;
	IEX_InitStreamSound(lpStream, 22050);

	/*	再生準備	*/ 
	IEX_SetStreamBlockWAV( lpStream, 0 );

	return TRUE;
}

//
//
//


//Vorbisfileコールバック(ファイル読み込み）
size_t CallbackRead(void *ptr, size_t size, size_t nmemb, void *datasource)
{
	HANDLE	hfile = (HANDLE)datasource;
	DWORD	work = 0;
	ReadFile( hfile, ptr, (DWORD)(size * nmemb), &work, NULL);
	return work;
}

//Vorbisfileコールバック(ファイルシーク）
int CallbackSeek(void *datasource, ogg_int64_t offset, int whence)
{
	HANDLE	hfile = (HANDLE)datasource;
	DWORD	result;

	if(whence == SEEK_CUR)		 result = SetFilePointer( hfile, (long)offset, NULL, FILE_CURRENT);
	 else if(whence == SEEK_END) result = SetFilePointer( hfile, (long)offset, NULL, FILE_END);
	 else if(whence == SEEK_SET) result = SetFilePointer( hfile, (long)offset, NULL, FILE_BEGIN);
	 else return -1;

	 return (result == 0xffffffff) ? -1 : 0;
}

//Vorbisfileコールバック(ファイルクローズ）
int CallbackClose(void *datasource)
{
	HANDLE hfile = (HANDLE)datasource;
	BOOL result = CloseHandle(hfile);
	return result ? 0 : EOF;
}


//Vorbisfileコールバック(現在位置取得）
long CallbackTell(void *datasource)
{
	HANDLE	hfile = (HANDLE)datasource;
	DWORD	offset = SetFilePointer(hfile, 0, NULL, FILE_CURRENT);
	return offset;
}

//
//	OGG用ストリーム初期化
//

BOOL	IEX_SetOGGStream( LPDSSTREAM lpStream, LPSTR filename )
{
	//	ファイルオープン 
	lpStream->hStrFile = CreateFile( filename, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( lpStream->hStrFile == (HANDLE)0xffffffff ) return FALSE;

	//Oggを開く
	ov_callbacks oggCallbacks =	{ CallbackRead, CallbackSeek, CallbackClose, CallbackTell };
	if( ov_open_callbacks( lpStream->hStrFile, &lpStream->vf, NULL, 0, oggCallbacks) < 0 ){
		CloseHandle(lpStream->hStrFile);
		return FALSE;
	}
	//シーク可能?
	if( !ov_seekable(&lpStream->vf) ){
		ov_clear(&lpStream->vf);
		return FALSE;
	}

	//情報設定
	const vorbis_info *info = ov_info(&lpStream->vf, -1);
	IEX_InitStreamSound(lpStream, info->rate);

	lpStream->type = TYPE_OGG;
	lpStream->StrPos  = 0;
	lpStream->StrSize = (DWORD)ov_pcm_total(&lpStream->vf, -1);

	/*	再生準備	*/ 
	IEX_SetStreamBlockOGG( lpStream, 0 );

	return TRUE;
}

//
//		停止
//

BOOL	IEX_StopStreamSound( LPDSSTREAM lpStream )
{
	if( !lpDS ) return TRUE;
	if( lpStream->lpStream == NULL ) return FALSE;
	if( lpStream->hStrFile == (HANDLE)0xffffffff ) return FALSE;

	lpStream->lpStream->Stop();
	return TRUE;
}

//
//		再生
//

LPDSSTREAM	IEX_PlayStreamSoundEx( LPSTR filename, BYTE mode, int param )
{
	LPDSSTREAM	lpStream;
	LPSTR	work;
	BOOL	bInit;

	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return NULL;

	lpStream = new DSSTREAM;
	
	//	ストリームファイルを開く
	work = &filename[ lstrlen(filename) - 4 ];
	if( lstrcmpi( work, ".wav" ) == 0 ) bInit = IEX_SetWAVStream( lpStream, filename );
	if( lstrcmpi( work, ".ogg" ) == 0 ) bInit = IEX_SetOGGStream( lpStream, filename );

	if( !bInit ){
		delete	lpStream;
		return NULL;
	}

	lpStream->lpStream->SetCurrentPosition( 0 );
	if( mode != STR_FADEIN ) IEX_SetStreamSoundVolume( lpStream, 255);
	else IEX_SetStreamSoundVolume( lpStream, 0);
	/*	管理スレッドの作成	*/ 
	lpStream->hStrThread = CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)IEX_StreamThread, lpStream, 0, &lpStream->dwThreadId );
	if( lpStream->hStrThread == NULL ) return FALSE;
	/*	再生開始	*/ 
	lpStream->lpStream->Play( 0, 0, DSBPLAY_LOOPING );

	lpStream->mode  = mode;
	lpStream->param = param;

	return lpStream;
}

LPDSSTREAM	IEX_PlayStreamSound( LPSTR filename )
{
	return IEX_PlayStreamSoundEx( filename, STR_NORMAL, 0 );
}

void	IEX_SetStreamMode( LPDSSTREAM lpStream, BYTE mode, int param )
{
	lpStream->mode  = mode;
	lpStream->param = param;
}

/*	ボリューム変更	*/ 
void	IEX_SetStreamSoundVolume( LPDSSTREAM lpStream, int volume )
{
	int		vol;
	/*	初期化チェック	*/ 
	if( lpDS == NULL ) return;
	if( lpStream->lpStream == NULL ) return;
	/*	音量セット	*/ 
	lpStream->volume = volume;
	volume -= 255;
	vol = (volume*volume*100 / (255*255) );
	lpStream->lpStream->SetVolume( -vol*vol );
}

