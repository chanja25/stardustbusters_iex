#include "All.h"

//*****************************************************************************************************************************
//
//		タイトル関連
//
//*****************************************************************************************************************************

//初期化
void	ClearFrame_Class::Init()
{
	lpClear[0] = IEX_Load2DObject( "DATA\\END\\Clear_bg.png" );
	lpClear[1] = IEX_Load2DObject( "DATA\\END\\Clear.png" );
	lpClear[2] = IEX_Load2DObject( "DATA\\END\\Result.png" );
	lpClear[3] = IEX_Load2DObject( "DATA\\2D\\ScoreCnt.png" );

	cnt = 0;
	mode = 0;

	int score = GameManage.GetScore();
	int j;
	for( int i=4;i>=0;i-- ){
		j = GameManage.GetHiScore( i );
		if( score<=j )break;

		GameManage.SetHiScore( i, score );
		if( i<4 ){
			GameManage.SetHiScore( i+1, j );
		}
	}

	GetScript()->LoadScript( "NCLEAR" );
}

//解放
void	ClearFrame_Class::Release(){
	for(int i=0;i<4;i++){
		IEX_Release2DObject( lpClear[i] );
	}
}

//処理
void	ClearFrame_Class::MainFrame()
{
	Script->Proc();

	Proc();

	//フェード処理
	if( !Fade.GetFlag() )Fade.Proc();
}

//処理
void	ClearFrame_Class::Proc()
{
	switch( mode ){
		case 1:
			if( (KEY(KEY_A) | KEY(KEY_ENTER)) == 3 ){
				cnt = 0;
				Fade.FadeOut( BLACK, 3 );
				mode++;
			}
			break;
		case 4:
			if( (KEY(KEY_A) | KEY(KEY_ENTER)) == 3 ){
				cnt = 0;
				Fade.FadeOut( BLACK, 3 );
				mode++;
			}
			break;
		case 6:
			GameManage.SetNextMode( MODE_TITLE );
			mode++;
			break;
	}
}

//描画
void	ClearFrame_Class::DrawFrame()
{
	Draw();

	//フェード描画
	Fade.Draw();
}

//描画
void	ClearFrame_Class::Draw()
{
	int i, j, k;
	switch( mode ){
		case 0:
			Fade.FadeIn( BLACK, 3 );
			mode++;
			break;
		case 1:
			IEX_Render2DObject( 0, 0, 640, 480, lpClear[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-256, 40, 512, 128, lpClear[2], 0, 0, 512, 128 );
			i = 0;
			j = 10;
			k = GameManage.GetScore();

			while(k != 0){
				int a = k%j;
				IEX_Render2DObject( (320+128)-i*64, 240, 64, 64, lpClear[3], a%4*64, a/4*64, 64, 64 );
				i++;
				k /= 10;
			}
			break;
		case 2:
			IEX_Render2DObject( 0, 0, 640, 480, lpClear[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-256, 40, 512, 128, lpClear[2], 0, 0, 512, 128 );
			i = 0;
			j = 10;
			k = GameManage.GetScore();

			while(k != 0){
				int a = k%j;
				IEX_Render2DObject( (320+128)-i*64, 240, 64, 64, lpClear[3], a%4*64, a/4*64, 64, 64 );
				i++;
				k /= 10;
			}
			if( ++cnt==120 )mode++;
			break;
		case 3:
			Fade.FadeIn( BLACK, 3 );
			mode++;
			break;
		case 4:
			IEX_Render2DObject( 0, 0, 640, 480, lpClear[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-256, 240-320, 512, 512, lpClear[1], 0, 0, 512, 512 );
			break;
		case 5:
			IEX_Render2DObject( 0, 0, 640, 480, lpClear[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-256, 240-320, 512, 512, lpClear[1], 0, 0, 512, 512 );
			if( ++cnt==120 )mode++;
			break;
	}
}
