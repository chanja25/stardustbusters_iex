#include	"iextreme.h"

//*****************************************************************************
//
//
//
//*****************************************************************************
extern	LPDIRECT3DDEVICE9		lpD3DDevice;

#define	MAX_TEXTURE	256

struct	tagTEXINFO {
	char	filename[64];
	int		UseCount;
	LPDIRECT3DTEXTURE9	lpTexture;
} TexInfo[MAX_TEXTURE];

LPDIRECT3DTEXTURE9	lpLastTexture;

//*****************************************************************************
//
//
//
//*****************************************************************************

void	IEX_InitTextureManager( void )
{
	int		i;

	for( i=0 ; i<MAX_TEXTURE ; i++ ){
		TexInfo[i].lpTexture = NULL;
		TexInfo[i].UseCount  = 0;
	}
}

void	IEX_ReleaseAllTexture( void )
{
	int		i;

	for( i=0 ; i<MAX_TEXTURE ; i++ ){
		if( !TexInfo[i].lpTexture ) continue;
		TexInfo[i].lpTexture->Release();
		TexInfo[i].lpTexture = NULL;
		TexInfo[i].UseCount  = 0;
	}
}

//
//
//

LPDIRECT3DTEXTURE9	IEX_LoadTexture( LPSTR filename )
{
	int			i, no;
	HRESULT		hr;
	D3DFORMAT	fmt = D3DFMT_UNKNOWN;

	no = -1;
	/*	テクスチャの検索	*/ 
	for( i=0 ; i<MAX_TEXTURE ; i++ ){
		if( !TexInfo[i].lpTexture ) continue;
		/*	ファイル名の比較		*/ 
		if( lstrcmpi( TexInfo[i].filename, filename ) != 0 ) continue; 
		no = i;
		break;
	}
	/*	新規読み込み			*/ 
	if( no == -1 ){
		//	TGAチェック
		DWORD	work;
		work = *(DWORD*)(&filename[lstrlen(filename)-4]);
		if( work == 'AGT.' ) fmt = D3DFMT_A8R8G8B8;
		if( work == 'agt.' ) fmt = D3DFMT_A8R8G8B8;

		/*	空き検索	*/ 
		for( no=0 ; no<MAX_TEXTURE ; no++ ) if( !TexInfo[no].lpTexture ) break;
		CopyMemory( TexInfo[no].filename, filename, strlen(filename)+1 );
		hr = D3DXCreateTextureFromFileEx( IEX_GetD3DDevice(), filename, D3DX_DEFAULT, D3DX_DEFAULT, 1, 0,
										fmt, D3DPOOL_MANAGED,
										D3DX_FILTER_POINT, D3DX_FILTER_POINT,
										0x00000000, NULL, NULL, &TexInfo[no].lpTexture );
	}
	TexInfo[no].UseCount++;
	return TexInfo[no].lpTexture;
}

void	IEX_ReleaseTexture( LPDIRECT3DTEXTURE9 lpTexture )
{
	int			i, no;

	if( lpD3DDevice == NULL ) return;

	no = -1;
	/*	テクスチャの検索	*/ 
	for( i=0 ; i<MAX_TEXTURE ; i++ ){
		if( !TexInfo[i].lpTexture ) continue;
		/*	ファイル名の比較		*/ 
		if( TexInfo[i].lpTexture != lpTexture ) continue;
		no = i;
		break;
	}
	if( i != MAX_TEXTURE ){
		TexInfo[no].UseCount--;
		if( TexInfo[no].UseCount > 0 ) return;
		//	解放テクスチャチェック
		LPDIRECT3DBASETEXTURE9	lpWork;
		IEX_GetD3DDevice()->GetTexture( 0, &lpWork );
		if( lpWork == TexInfo[no].lpTexture ){
			/*	テクスチャ設定		*/ 
			IEX_GetD3DDevice()->SetTexture( 0, NULL );
			lpLastTexture = NULL;
		}
		//	テクスチャ解放
		if( TexInfo[no].lpTexture->Release() == D3D_OK){
			TexInfo[no].lpTexture = NULL;
		}
		TexInfo[no].UseCount  = 0;
	} else {
		lpTexture->Release();
	}
}

