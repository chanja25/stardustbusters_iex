#include "All.h"


//*****************************************************************************************************************************
//
//		バレット関連
//
//*****************************************************************************************************************************

//初期化
void	Bullet_Class::Init( IEXMESH bullet, D3DXMATRIX mat, D3DXVECTOR3 move, float scale, float speed,
						   int type, int damage, int eftype, BOOL flg )
{
	before = NULL;
	next = NULL;

	this->bullet = bullet;

	this->wmat = mat;
	MatrixNormalize( &wmat );

	GetVecMatrix( &wmat, &pos, 3 );

	this->move	= move;
	this->scale	= scale;
	this->speed	= speed;

	dist = BSIZE;
	if( !flg && type==1 ){
	dist = ELSIZE;
	}else if( type==3 ){
		dist = ELSIZE2;
	}

	SetScaleMatrix( &wmat, scale );
	bullet.TransMatrix = wmat;

	this->type		= type;
	this->damage	= damage;
	this->eftype	= eftype;
	this->flg		= flg;

	lflg = FALSE;
}

void	Bullet_Class::Release()
{
}

BOOL	Bullet_Class::HitBullet()
{
	if( type==1 && flg ){
		MissileBullet *mi = (MissileBullet*)this;
		Enemy_Class *enemy;
		enemy = MainFrameC->GetEnemy()->GetEnemy( mi->GetTno() );
		if( enemy ){
 			D3DXVECTOR3 t = enemy->GetHitPos();
			D3DXVECTOR3 p = pos;
			p.z = t.z;
			float dist = GetDist3D( p, t );
			D3DXVECTOR3 move = t - p;
			D3DXVec3Normalize( &move, &move );

			if( t.z > pos.z )pos += move*MSP*dist;
			if( dist<MSP/2 ){
				pos.x = t.x;
				pos.y = t.y;
			}
		}else type = 0;
	}

	if( flg ){
		Enemy_Class *enemy;
		enemy = MainFrameC->GetEnemy()->GetEnemy();
		while( enemy ){
			if( enemy->GetType()==7 ){
				D3DXVECTOR3 Pos,Target;
				for( int i=0;i<BHNUM;i++ ){
					Pos = pos;
					Target = ((MS*)enemy)->GetHitPos(i);
					if( Pos.z<Target.z && Pos.z+move.z*speed>Target.z ){
						Pos.z = 0;
						Target.z = 0;
						if( GetDist3D(Pos,Target)<((MS*)enemy)->GetHitDist(i)+GetDist() ){
							enemy->DamageHp( damage, pos );
							if( type!=2 || enemy->GetType()>4 ){
								return TRUE;
							}
						}
					}
				}
			}else{
				D3DXVECTOR3 Pos,Target;
				Pos = pos;
				Target = enemy->GetHitPos();
				if( Pos.z<Target.z && Pos.z+move.z*speed>Target.z ){
					Pos.z = 0;
					Target.z = 0;
					if( GetDist3D(Pos,Target)<enemy->GetHitDist()+GetDist() ){
						if( enemy->GetType()==9 && type==2 )damage *= 2;
						enemy->DamageHp( damage, pos );
						if( type!=2 || enemy->GetType()>4 ){
							return TRUE;
						}
					}
				}
			}
			MainFrameC->GetEnemy()->EnemyNext( &enemy );
		}
	}else{
		Player_Class *p = MainFrameC->GetPlayer();
		D3DXVECTOR3 Pos,Target;
		Pos = pos;
		Target = p->GetHitPos();
		if( Pos.z>Target.z && Pos.z+move.z*speed<Target.z ){
			Pos.z = 0;
			Target.z = 0;
			if( GetDist3D(Pos,Target)<p->GetHitDist()+GetDist() ){
				MainFrameC->GetPlayer()->DamageHp( damage, pos );
				if( type!=2 )return TRUE;
			}
		}
		Bullet_Class *bu;
		bu = *MainFrameC->GetBullet();
		while( bu ){
			if( bu->GetFlag() && (bu->GetType()==1 || bu->GetType()==3) ){
				D3DXVECTOR3 Pos,Target;
				Pos = pos;
				Target = bu->GetPos();
				if( Pos.z>Target.z && Pos.z+move.z*speed<Target.z ){
					Pos.z = 0;
					Target.z = 0;
					if( GetDist3D(Pos,Target)<GetDist()+bu->GetDist() ){
 						Bullet_Class *b = bu;
						bu = bu->GetBefore();
						Clear( &b );
					}
				}
			}
			if( bu )bu->NextBullet( &bu );
		}
	}
	return FALSE;
}

void	Bullet_Class::Move()
{
	pos += move*speed;
}

void	Bullet_Class::UpData()
{
	SetPosMatrix( &wmat, pos );
	bullet.TransMatrix = wmat;
}

void	Bullet_Class::Proc()
{
	Move();

	UpData();
}

void	Bullet_Class::Draw()
{
	if( type==1 && flg ){
		MissileBullet *mi = (MissileBullet*)this;
		D3DXVECTOR3 t;
		t = MainFrameC->GetEnemy()->GetEnemy( mi->GetTno() )->GetHitPos();
		if( t.z-pos.z>0 ){
			t.z -= MainFrameC->GetEnemy()->GetEnemy( mi->GetTno() )->GetHitDist();
			EffectTarget( t, FALSE );
		}
	}

	if( &bullet )IEX_RenderMesh( &bullet, RS_COPY, 1.0f );
}

BOOL	Bullet_Class::GetFlag()
{
	return flg;
}

void	Bullet_Class::NextBullet( Bullet_Class **bullet )
{
	*bullet = (*bullet)->GetNext();
}

void	Bullet_Class::SetBefore( Bullet_Class *bullet )
{
	before = bullet;
}

Bullet_Class *Bullet_Class::GetBefore()
{
	return before;
}

void	Bullet_Class::SetNext( Bullet_Class *bullet )
{
	next = bullet;
}

Bullet_Class *Bullet_Class::GetNext()
{
	return next;
}

float	Bullet_Class::GetDist()
{
	return dist;
}

D3DXVECTOR3	Bullet_Class::GetPos()
{
	return pos;
}

D3DXVECTOR3	Bullet_Class::GetMove()
{
	return move;
}

int		Bullet_Class::GetType()
{
	return type;
}

void	Bullet_Class::Clear( Bullet_Class **bullet )
{
	if( (*bullet)->GetBefore() ){
		Bullet_Class *bu;
  		bu = (*bullet)->GetBefore();
		if( (*bullet)->GetNext() ){
			bu->SetNext( (*bullet)->GetNext() );
			delete *bullet;
			*bullet = bu->GetNext();
			(*bullet)->SetBefore( bu );
		}else{
			bu->SetNext( NULL );
			delete *bullet;
			*bullet = NULL;
		}
	}else if( (*bullet)->GetNext() ){
		Bullet_Class **bu;
		bu = MainFrameC->GetBullet();
 		*bu = (*bullet)->GetNext();
		(*bu)->SetBefore( NULL );
		delete *bullet;
		*bullet = *bu;
	}else{
		Bullet_Class **bu;
		bu = MainFrameC->GetBullet();
		delete *bullet;
		*bullet = NULL;
		*bu = NULL;
	}
}

void	MissileBullet::Init( IEXMESH bullet, D3DXMATRIX mat, D3DXVECTOR3 move, float scale, float speed, int type, int damage, int eftype, BOOL flg, int tno )
{
	Bullet_Class::Init( bullet, mat, move, scale, speed, type, damage, eftype, flg );
	this->tno = tno;
}

//*****************************************************************************************************************************
//
//		武器関連
//
//*****************************************************************************************************************************

//位置取得
D3DXVECTOR3 Weapon_Class::GetPos()
{
	return pos;
}

void	Weapon_Class::SetBullet( int Bullet )
{
	this->Bullet = Bullet;
}

int		Weapon_Class::GetBullet()
{
	return Bullet;
}

//************************
//		 機関銃
//************************
//初期化
void	Gun::Init()
{
	MainFrameC->SetWeapon( &weapon, 0 );
	MainFrameC->SetWeapon( &bullet, 3 );

	wmat = MainFrameC->GetPlayer()->GetObj()->TransMatrix;

	MatrixNormalize( &wmat );
	pos = MainFrameC->GetPlayer()->GetPos();
	pos.z -= 30.0f;
	pos.y -= 30.0f;
	SetVecMatrix( &wmat, &pos, 3 );

	mflg = TRUE;

	scale = .02f;
	SetScaleMatrix( &wmat, scale );
	MaxBullet = MaxBullet1;
	Bullet = MaxBullet;
	BulletSpeed = BulletSpeed2;
	BulletScale = BScale2;

	ChainFlag = FALSE;
	ChainSpeed = GCSP2;
	ChainTimer = 0;

	flg = TRUE;
	lflg = 0;
}

//解放
void	Gun::Release()
{
}

//銃弾セット
Bullet_Class *Gun::SetBullet()
{
	if( mflg || !flg || Bullet<=0 )return NULL;
	else Bullet--;
	Bullet_Class *bu1 = new Bullet_Class;
	Bullet_Class *bu2 = new Bullet_Class;

	D3DXVECTOR3 vec;
	vec = MainFrameC->GetPlayer()->GetBonePos( 4 );
	SetVecMatrix( &wmat, &vec, 3 );
	GetVecMatrix( &wmat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu1->Init( bullet, wmat, vec, BulletScale, BulletSpeed, 0, BDamage2, 0, TRUE );
	vec = MainFrameC->GetPlayer()->GetBonePos( 8 );
	SetVecMatrix( &wmat, &vec, 3 );
	GetVecMatrix( &wmat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu2->Init( bullet, wmat, vec, BulletScale, BulletSpeed, 0, BDamage2, 0, TRUE );
	bu1->SetNext( bu2 );
	bu2->SetBefore( bu1 );
	ChainTimer = 0;
	flg = FALSE;
	Sound::PlaySound( 0, FALSE );
	return bu1;
}

float	Gun::GetChage()
{
	return (float)(MaxBullet-Bullet)/MaxBullet;
}

void	Gun::SetLost()
{
	lflg = 1;
	move = MainFrameC->GetPlayer()->GetMove()*WDSPXZ;
	move.y = WDSP;
}

//移動
void	Gun::Move()
{
	if( lflg ){
		pos += move;
		if( GetDist3D(pos,MainFrameC->GetPlayer()->GetPos())>=30.0f ){
			lflg = 2;
		}
		return;
	}
	D3DXVECTOR3 p;
	GetVecMatrix( &MainFrameC->GetPlayer()->GetObj()->TransMatrix, &p, 3 );
	wmat = MainFrameC->GetPlayer()->GetObj()->TransMatrix;
	if( mflg ){
		move = p - pos;
		D3DXVec3Normalize( &move, &move );
		float dist = GetDist3D( p, pos );
		if( GetDist3D( p, pos )<=1.0f ){
			mflg = FALSE;
		}
		move *= PSP+WSP*dist;
		pos += move;
	}else{
		pos = p;
	}

	if( !flg )ChainTimer++;
	if( ChainTimer>=ChainSpeed ){
		flg = TRUE;
	}

	if( KEY(SHOOT) ){
		int flg = MainFrameC->GetPlayer()->GetCflg();
		if(  flg == 5 || flg < 0 )return;
		MainFrameC->SetBullet( SetBullet() );
	}
}

//更新
void	Gun::UpData()
{
	weapon.TransMatrix = wmat;
	SetVecMatrix( &wmat, &pos, 3 );
	weapon.TransMatrix = wmat;
}

void	Gun::Proc()
{
	Move();

	UpData();
}

void	Gun::Draw()
{
	IEX_RenderMesh( &weapon, RS_COPY, 1.0f );
}

//************************
//		 ミサイル
//************************
//初期化
void	Missile::Init()
{
	MainFrameC->SetWeapon( &weapon, 1 );
	MainFrameC->SetWeapon( &bullet, 4 );

	wmat = MainFrameC->GetPlayer()->GetObj()->TransMatrix;

	MatrixNormalize( &wmat );
	pos = MainFrameC->GetPlayer()->GetPos();
	pos.z -= 30.0f;
	pos.y -= 30.0f;
	SetVecMatrix( &wmat, &pos, 3 );

	mflg = TRUE;

	scale = .02f;
	SetScaleMatrix( &wmat, scale );
	MaxBullet = MaxBullet2;
	Bullet = MaxBullet;
	BulletSpeed = BulletSpeed3;
	BulletScale = BScale3;

	ChainFlag = FALSE;
	ChainSpeed = GCSP3;
	ChainTimer = 0;

	flg = FALSE;

	lflg = 0;

	Change = FALSE;

	cnt = 0;
	num = 0;

	for( int i=0;i<MAXTARGET;i++ )tno[i] = -1;
}

//解放
void	Missile::Release()
{
}

//銃弾セット
Bullet_Class *Missile::SetBullet()
{
  	if( mflg || !flg || Bullet<=0 )return NULL;
	if( num ){
		MissileBullet *bu = new MissileBullet;

		D3DXVECTOR3 vec;
		vec = MainFrameC->GetPlayer()->GetBonePos( Change*4 + 4 );
		SetVecMatrix( &wmat, &vec, 3 );
		GetVecMatrix( &wmat, &vec, 2 );
		D3DXVec3Normalize( &vec, &vec );
		bu->Init( bullet, wmat, vec, BulletScale, BulletSpeed, 1, BDamage3, 0, TRUE, tno[0] );
		ChainTimer = 0;
		flg = FALSE;
		if( Change )Change = FALSE;
		else Change = TRUE;

		MissileBullet *bu1 = bu;
		for( int i=1;i<num;i++ ){
			MissileBullet *bu2 = new MissileBullet;

			D3DXVECTOR3 vec;
			vec = MainFrameC->GetPlayer()->GetBonePos( Change*4 + 4 );
			SetVecMatrix( &wmat, &vec, 3 );
			GetVecMatrix( &wmat, &vec, 2 );
			D3DXVec3Normalize( &vec, &vec );
			bu2->Init( bullet, wmat, vec, BulletScale, BulletSpeed, 1, BDamage3, 0, TRUE, tno[i] );
			ChainTimer = 0;
			if( Change )Change = FALSE;
			else Change = TRUE;

			bu1->SetNext( bu2 );
			bu2->SetBefore( bu1 );
			bu1->NextBullet( (Bullet_Class**)&bu1 );
		}
		Bullet -= num;
		flg = FALSE;
		num = 0;

		Sound::PlaySound( 1, FALSE );
		for( int i=0;i<MAXTARGET;i++ )tno[i] = -1;
		return (Bullet_Class*)bu;
	}
	return NULL;
}

BOOL	Missile::SearchSelect( Enemy_Class *enemy, BOOL flg )
{
	if( !enemy ){
		return TRUE;
	}

	if( enemy->GetType()==5 && enemy->GetMode()<7 ){
		return TRUE;
	}

	if( enemy->GetType()==10 && enemy->GetObj()->Motion==1 ){
		return TRUE;
	}

	if( enemy->GetPos().z<pos.z+50.0f || enemy->GetPos().z>pos.z+400.0f/*+!flg*40*/ ){
		return TRUE;
	}

	return FALSE;
}

void	Missile::ClearTargetNo( int no )
{
	if( no>=num )return;

	for( int i=no;i<num;i++ ){
		tno[i] = ( i+1>=MAXTARGET )? -1: tno[i+1];
	}
	num--;
}

void	Missile::Search()
{
 	if( num>=MAXTARGET )return;
	Enemy_Class *enemy;
    enemy = MainFrameC->GetEnemy()->GetEnemy();
    if( !enemy ){
		num = 0;
		return;
	}
	D3DXVECTOR3 p,t;
	float d1,d2;
	p = pos;
	while( SearchSelect( enemy, FALSE ) ){
		MainFrameC->GetEnemy()->EnemyNext( &enemy );
		if( !enemy )return;
	}
	for( int i=0;i<num;i++ ){
		if( enemy->GetNo()==tno[i] ){
			MainFrameC->GetEnemy()->EnemyNext( &enemy );
			if( !enemy )return;
			i = -1;
		}
	}
	t = enemy->GetHitPos();
	d1 = GetDist3D( p, t );
	tno[num] = enemy->GetNo();

	MainFrameC->GetEnemy()->EnemyNext( &enemy );
	while( enemy ){
		while( SearchSelect( enemy, FALSE ) ){
			MainFrameC->GetEnemy()->EnemyNext( &enemy );
			if( !enemy ){
				num++;
				return;
			}
		}
		for( int i=0;i<num;i++ ){
			if( enemy->GetNo()==tno[i] ){
				MainFrameC->GetEnemy()->EnemyNext( &enemy );
				if( !enemy ){
					num++;
					return;
				}
				i = -1;
			}
		}
		t = enemy->GetHitPos();
		d2 = GetDist3D( p, t );
		d1 = ( d1<d2 )?d1:d2;
		tno[num] = (d1==d2)?enemy->GetNo():tno[num];
		MainFrameC->GetEnemy()->EnemyNext( &enemy );
	}
 	num++;
}

D3DXVECTOR3	Missile::GetTarget( int no )
{
	if( no>=MAXTARGET )return D3DXVECTOR3( 0, 0, -100 );
	return MainFrameC->GetEnemy()->GetEnemy( tno[no] )->GetHitPos();
}

float	Missile::GetChage()
{
	return (float)num/(float)MAXTARGET;
}

void	Missile::SetLost()
{
	lflg = 1;
}

//移動
void	Missile::Move()
{
	if( lflg ){
		move = MainFrameC->GetPlayer()->GetMove()*WDSPXZ;
		move.y = WDSP;
		pos += move;
		if( GetDist3D(pos,MainFrameC->GetPlayer()->GetPos())>=30.0f ){
			lflg = 2;
		}
		return;
	}
	D3DXVECTOR3 p;
	GetVecMatrix( &MainFrameC->GetPlayer()->GetObj()->TransMatrix, &p, 3 );
	wmat = MainFrameC->GetPlayer()->GetObj()->TransMatrix;
	if( mflg ){
		move = p - pos;
		D3DXVec3Normalize( &move, &move );
		float dist = GetDist3D( p, pos );
		if( GetDist3D( p, pos )<=1.0f ){
			mflg = FALSE;
		}
		move *= PSP+WSP*dist;
	}else{
		pos = p;
	}
    pos += move;

	static int cnt = MLockTime;
	if( KEY(SHOOT) ){
		if( --cnt<0 ){
            Search();
			cnt = MLockTime;
		}
	}else{
		num = 0;

		for( int i=0;i<MAXTARGET;i++ )tno[i] = -1;
		cnt = MLockTime;
	}

	if( KEY(SHOOT)==2 && num ){
		flg = TRUE;
	}

	
	int flg = MainFrameC->GetPlayer()->GetCflg();
	if(  flg == 5 || flg < 0 )return;
	MainFrameC->SetBullet( SetBullet() );
}

//更新
void	Missile::UpData()
{
	weapon.TransMatrix = wmat;
	SetVecMatrix( &wmat, &pos, 3 );
	weapon.TransMatrix = wmat;
}

void	Missile::Proc()
{
	Move();

	UpData();

	if( lflg && GetDist3D(pos,MainFrameC->GetPlayer()->GetPos())>=30.0f ){
		MainFrameC->GetPlayer()->LostWeapon();
	}
}

void	Missile::Draw()
{
	for( int i=0;i<num;i++ ){
		if( SearchSelect( MainFrameC->GetEnemy()->GetEnemy( tno[i] ), TRUE ) ){
   			ClearTargetNo( i );
			i--;
			continue;
		}
		D3DXVECTOR3 t = GetTarget( i );
		t.z -= MainFrameC->GetEnemy()->GetEnemy( tno[i] )->GetHitDist();
   		EffectTarget( t, TRUE );
	}
	IEX_RenderMesh( &weapon, RS_COPY, 1.0f );
}


//************************
//		 レーザー
//************************
//初期化
void	Laser::Init()
{
	MainFrameC->SetWeapon( &weapon, 2 );
	MainFrameC->SetWeapon( &bullet, 5 );

	wmat = MainFrameC->GetPlayer()->GetObj()->TransMatrix;

	MatrixNormalize( &wmat );
	pos = MainFrameC->GetPlayer()->GetPos();
	pos.z -= 30.0f;
	pos.y -= 30.0f;
	SetVecMatrix( &wmat, &pos, 3 );

	mflg = TRUE;

	scale = .02f;
	SetScaleMatrix( &wmat, scale );
	MaxBullet = MaxBullet3;
	Bullet = MaxBullet;
	BulletSpeed = BulletSpeed3;
	BulletScale = BScale4;

	ChainFlag = FALSE;
	ChainSpeed = GCSP4;
	ChainTimer = 0;

	flg = TRUE;

	lflg = 0;

	cnt = 0;
}

//解放
void	Laser::Release()
{
}

//銃弾セット
Bullet_Class *Laser::SetBullet()
{
	if( cnt>=MAXLASER*ELASER || mflg || !flg || Bullet<=0 )return NULL;
	Bullet--;
	cnt+=ELASER;
	if( cnt>MAXLASER*ELASER )cnt = MAXLASER*ELASER;
	Bullet_Class *bu1 = new Bullet_Class;
	Bullet_Class *bu2 = new Bullet_Class;

	D3DXVECTOR3 vec;
	vec = MainFrameC->GetPlayer()->GetBonePos( 4 );
	SetVecMatrix( &wmat, &vec, 3 );
	GetVecMatrix( &wmat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu1->Init( bullet, wmat, vec, BulletScale, BulletSpeed, 2, BDamage4, 0, TRUE );
	vec = MainFrameC->GetPlayer()->GetBonePos( 8 );
	SetVecMatrix( &wmat, &vec, 3 );
	GetVecMatrix( &wmat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu2->Init( bullet, wmat, vec, BulletScale, BulletSpeed, 2, BDamage4, 0, TRUE );
	bu1->SetNext( bu2 );
	bu2->SetBefore( bu1 );
	ChainTimer = 0;
	flg = FALSE;
	Sound::PlaySound( 2, FALSE );
	return bu1;
}

float	Laser::GetChage()
{
	return (float)cnt/(float)(MAXLASER*ELASER);
}

void	Laser::SetLost()
{
	lflg = 1;
}

//移動
void	Laser::Move()
{
	if( lflg ){
		move = MainFrameC->GetPlayer()->GetMove()*WDSPXZ;
		move.y = WDSP;
		pos += move;
		if( GetDist3D(pos,MainFrameC->GetPlayer()->GetPos())>=30.0f ){
			lflg = 2;
		}
		return;
	}
	D3DXVECTOR3 p;
	GetVecMatrix( &MainFrameC->GetPlayer()->GetObj()->TransMatrix, &p, 3 );
	wmat = MainFrameC->GetPlayer()->GetObj()->TransMatrix;
	if( mflg ){
		move = p - pos;
		D3DXVec3Normalize( &move, &move );
		float dist = GetDist3D( p, pos );
		if( GetDist3D( p, pos )<=1.0f ){
			mflg = FALSE;
		}
		move *= PSP+WSP*dist;
		pos += move;
	}else{
		pos = p;
	}

	static int count = 0;

	if( cnt==MAXLASER*ELASER ){
		count = LIMIT;
	}
	count = ( count )? count-1: 0;

	BOOL fg;
	fg = ( count==0 && MainFrameC->GetPlayer()->GetCflg()<6 )? TRUE: FALSE;

	if( !flg )ChainTimer++;
	if( ChainTimer>=ChainSpeed ){
		if( fg )flg = TRUE;
	}

	if( !KEY(SHOOT) || !fg ){
		if( cnt>0 )cnt-=ULASER;
	}

	if( KEY(SHOOT) ){
		int flg = MainFrameC->GetPlayer()->GetCflg();
		if( flg == 5 || flg < 0 )return;
		MainFrameC->SetBullet( SetBullet() );
	}
}

//更新
void	Laser::UpData()
{
	weapon.TransMatrix = wmat;
	SetVecMatrix( &wmat, &pos, 3 );
	weapon.TransMatrix = wmat;
}

void	Laser::Proc()
{
	Move();

	UpData();
}

void	Laser::Draw()
{
	IEX_RenderMesh( &weapon, RS_COPY, 1.0f );
}
