#include	"iExtreme.h"

//*****************************************************************************
//
//
//
//*****************************************************************************

//
//		座標系設定
//

/*	転送行列作成	*/ 
void	IEX_SetTransMatrix( LPIEXMESH lpMesh )
{
	D3DXMATRIX MatScale;
 
	if( !lpMesh ) return;
	/*	スケーリング		*/ 
	D3DXMatrixScaling( &MatScale, lpMesh->Scale.x, lpMesh->Scale.y, lpMesh->Scale.z ); 
	/*	座標系設定	*/ 
	SetTransformMatrixXYZ( &lpMesh->TransMatrix, lpMesh->Pos.x, lpMesh->Pos.y, lpMesh->Pos.z, lpMesh->Angle.x, lpMesh->Angle.y, lpMesh->Angle.z );
	/*	転送行列作成		*/ 
	D3DXMatrixMultiply( &lpMesh->TransMatrix, &MatScale, &lpMesh->TransMatrix );
}

/*	座標設定	*/ 
void	IEX_SetMeshPos( LPIEXMESH lpMesh, float x, float y, float z )
{
	if( !lpMesh ) return;

	lpMesh->Pos.x = x;
	lpMesh->Pos.y = y;
	lpMesh->Pos.z = z;
	IEX_SetTransMatrix( lpMesh );
}

/*	方向設定	*/ 
void	IEX_SetMeshAngle( LPIEXMESH lpMesh, float x, float y, float z )
{
	if( !lpMesh ) return;

	lpMesh->Angle.x = x;
	lpMesh->Angle.y = y;
	lpMesh->Angle.z = z;
	IEX_SetTransMatrix( lpMesh );
}

/*	スケール設定	*/ 
void	IEX_SetMeshScale( LPIEXMESH lpMesh, float x, float y, float z )
{
	if( !lpMesh ) return;

	lpMesh->Scale.x = x;
	lpMesh->Scale.y = y;
	lpMesh->Scale.z = z;
	IEX_SetTransMatrix( lpMesh );
}

void	IEX_SetMeshScale( LPIEXMESH lpMesh, float scale )
{
	if( !lpMesh ) return;

	lpMesh->Scale.x = scale;
	lpMesh->Scale.y = scale;
	lpMesh->Scale.z = scale;
	IEX_SetTransMatrix( lpMesh );
}

//*****************************************************************************
//
//		メッシュのレンダリング
//
//*****************************************************************************

void	IEX_RenderMesh( LPIEXMESH lpMesh, u32 dwFlags, float param )
{
	u32	dwFlagW;

	if( !lpMesh ) return;

	LPDIRECT3DDEVICE9	lpDevice = IEX_GetD3DDevice();
    /*	転送行列設定	*/ 
	lpDevice->SetTransform( D3DTS_WORLD, &lpMesh->TransMatrix );
	/*	メッシュのレンダリング	*/ 
	for( u32 i=0 ; i<lpMesh->MaterialCount ; i++ ){
		if( lpMesh->bFlags[i]==1 && dwFlags==RM_NORMAL ) dwFlagW = RM_ADD; else dwFlagW = dwFlags;
		if( param != -1.0f ) lpMesh->lpMaterial[i].Diffuse.a = lpMesh->lpMaterial[i].Ambient.a = param;

		IEX_SetRenderState( dwFlagW, &lpMesh->lpMaterial[i], lpMesh->lpTexture[i] );

		switch( lpMesh->bFlags[i] ){
		case 2:	IEX_UseFilter(FALSE);	break;
		}


		lpMesh->lpMesh->DrawSubset( i );

		switch( lpMesh->bFlags[i] ){
		case 2:	IEX_UseFilter(TRUE);	break;
		}
	}
}

//*****************************************************************************
//
//
//
//*****************************************************************************

//
//		Ｘファイル読み込み
//

LPIEXMESH	IEX_LoadXFile( LPSTR path, LPSTR filename )
{
	HRESULT hr;
	LPD3DXBUFFER	lpD3DXMtrlBuffer;
	LPIEXMESH		lpMesh;

	char			file[MAX_PATH];
	wsprintf( file, "%s%s", path, filename );

	lpMesh = new IEXMESH;
	ZeroMemory( lpMesh, sizeof(IEXMESH) );

	// X ファイルのロード
	hr = D3DXLoadMeshFromX( file, D3DXMESH_MANAGED, IEX_GetD3DDevice(),NULL, &lpD3DXMtrlBuffer, NULL, &lpMesh->MaterialCount, &lpMesh->lpMesh );

 	if( FAILED( hr ) ){
		delete	lpMesh;
		return NULL;
	}

	// lpD3DXMtrlBufferから、質感やテクスチャーの情報を取得する
	D3DXMATERIAL* d3dxMaterial = (D3DXMATERIAL *)lpD3DXMtrlBuffer->GetBufferPointer();

	// テクスチャ＆マテリアル用バッファ生成
	lpMesh->lpMaterial = new D3DMATERIAL9 [ lpMesh->MaterialCount ];
	lpMesh->lpTexture  = new LPDIRECT3DTEXTURE9 [ lpMesh->MaterialCount ];

	// データ読み込み
	for( u32 i=0 ; i<lpMesh->MaterialCount ; i++ ){
		// マテリアル
		lpMesh->lpMaterial[i] = d3dxMaterial[i].MatD3D;
		lpMesh->lpMaterial[i].Ambient.r = lpMesh->lpMaterial[i].Diffuse.r;
		lpMesh->lpMaterial[i].Ambient.g = lpMesh->lpMaterial[i].Diffuse.g;
		lpMesh->lpMaterial[i].Ambient.b = lpMesh->lpMaterial[i].Diffuse.b;
		// テクスチャ
		char temp[256] = "";

		if( d3dxMaterial[i].pTextureFilename != NULL ){
			wsprintf( temp, "%s%s", path, d3dxMaterial[i].pTextureFilename );
			lpMesh->lpTexture[i] = IEX_LoadTexture( temp );
		} else lpMesh->lpTexture[i] = NULL;
		//	フラグ設定
		if( d3dxMaterial[i].MatD3D.Emissive.r == lpMesh->lpMaterial[i].Diffuse.r ){
			lpMesh->bFlags[i] = 1;
			lpMesh->lpMaterial[i].Emissive.r = .0f;
			lpMesh->lpMaterial[i].Emissive.g = .0f;
			lpMesh->lpMaterial[i].Emissive.b = .0f;
			lpMesh->lpMaterial[i].Emissive.a = .0f;
		}
	}
	lpD3DXMtrlBuffer->Release();
	return lpMesh;
}

//
//		Ｘファイルメッシュ作成
//

LPIEXMESH	IEX_LoadMeshFromX( LPSTR path, LPSTR filename )
{
	LPIEXMESH		lpMesh;

	lpMesh = IEX_LoadXFile( path, filename );

	IEX_SetMeshPos( lpMesh, .0f, .0f, .0f );
	IEX_SetMeshAngle( lpMesh, .0f, .0f, .0f );
	IEX_SetMeshScale( lpMesh, 1.0f, 1.0f, 1.0f );
	lpMesh->dwFlags = 0;

	return lpMesh;
}
 
LPIEXMESH	IEX_LoadMeshFromX( LPSTR filename )
{
	LPIEXMESH	lpMesh;
	char		workpath[MAX_PATH];

	//	パス分割
	CopyMemory( workpath, filename, lstrlen(filename)+1 );
	for( int i=lstrlen(filename) ; i>0 ; i-- ){
		if( IsDBCSLeadByte(workpath[i-2]) ){
			i--;
			continue;
		}
		if( workpath[i-1] == '\\' || workpath[i-1] == '/' ){
			workpath[i] = '\0';
			break;
		}
	}

	lpMesh = IEX_LoadXFile( workpath, &filename[lstrlen(workpath)] );
	if( !lpMesh ) return NULL;

	IEX_SetMeshPos( lpMesh, .0f, .0f, .0f );
	IEX_SetMeshAngle( lpMesh, .0f, .0f, .0f );
	IEX_SetMeshScale( lpMesh, 1.0f, 1.0f, 1.0f );
	lpMesh->dwFlags = 0;

	return lpMesh;
}

//*****************************************************************************
//
//		iMoファイル
//
//*****************************************************************************

LPIEXMESH	IEX_LoadIMO( LPSTR filename )
{
	LPIEXMESH	lpObj;
	IMOOBJ		imo;
	DWORD		i;
	//	ファイル読み込み
	HANDLE	hfile;
	DWORD	dum;
	hfile = CreateFile( filename, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( hfile == INVALID_HANDLE_VALUE ) return NULL;

	ReadFile( hfile, &imo, sizeof(IMOOBJ), &dum, NULL );
	//	メッシュ作成
	DWORD	Declaration = D3DFVF_LVERTEX;
	BYTE	*pVertex, *pFace;
	DWORD	*pData;

	lpObj = new IEXMESH;
	ZeroMemory( lpObj, sizeof(IEXMESH) );
	D3DXCreateMeshFVF( imo.NumFace, imo.NumVertex, D3DXMESH_MANAGED, Declaration, IEX_GetD3DDevice(), &lpObj->lpMesh );
	//	頂点設定
	lpObj->lpMesh->LockVertexBuffer( 0, (void**)&pVertex );
	ReadFile( hfile, pVertex, sizeof(LVERTEX)*imo.NumVertex, &dum, NULL );
	lpObj->lpMesh->UnlockVertexBuffer();
	
	//	面設定
	lpObj->lpMesh->LockIndexBuffer( 0, (void**)&pFace );
	ReadFile( hfile, pFace, sizeof(WORD)*imo.NumFace*3, &dum, NULL );
	lpObj->lpMesh->UnlockIndexBuffer();

	//	属性設定
	lpObj->lpMesh->LockAttributeBuffer( 0, &pData );
	ReadFile( hfile, pData, sizeof(DWORD)*imo.NumFace, &dum, NULL );
	lpObj->lpMesh->UnlockAttributeBuffer();

	//	パス分割
	char	workpath[MAX_PATH];
	CopyMemory( workpath, filename, lstrlen(filename)+1 );
	for( i=lstrlen(filename) ; i>0 ; i-- ){
		if( IsDBCSLeadByte(workpath[i-2]) ){
			i--;
			continue;
		}
		if( workpath[i-1] == '\\' || workpath[i-1] == '/' ){
			workpath[i] = '\0';
			break;
		}
	}

	//	マテリアル設定
	lpObj->MaterialCount = imo.NumMaterial;
	lpObj->lpMaterial = new D3DMATERIAL9[ lpObj->MaterialCount ];
	CopyMemory( lpObj->lpMaterial, imo.Material, sizeof(D3DMATERIAL9)*imo.NumMaterial );
	//	テクスチャ設定
	lpObj->lpTexture  = new LPDIRECT3DTEXTURE9[ imo.NumMaterial ];
	ZeroMemory( lpObj->lpTexture, sizeof(LPDIRECT3DTEXTURE9)*imo.NumMaterial );

	char temp[256];
	for( i=0 ; i<lpObj->MaterialCount ; i++ ){
		if( imo.Texture[i][0] == '\0' ) continue;

		wsprintf( temp, "%s%s", workpath, imo.Texture[i] );
		lpObj->lpTexture[i] = IEX_LoadTexture( temp );
		//	フラグ設定
		if( lpObj->lpMaterial[i].Emissive.r == 1.0f ){
			lpObj->bFlags[i] = 1;
			lpObj->lpMaterial[i].Emissive.r = .0f;
			lpObj->lpMaterial[i].Emissive.g = .0f;
			lpObj->lpMaterial[i].Emissive.b = .0f;
			lpObj->lpMaterial[i].Emissive.a = .0f;
		}
		switch( imo.Texture[i][31] ){
		case '*':	lpObj->bFlags[i] = 2; break;
		}

	}
	IEX_SetMeshPos( lpObj, .0f, .0f, .0f );
	IEX_SetMeshAngle( lpObj, .0f, .0f, .0f );
	IEX_SetMeshScale( lpObj, 1.0f, 1.0f, 1.0f );
	lpObj->dwFlags = 0;

	CloseHandle(hfile);
	return lpObj;
}

//*****************************************************************************
//
//
//
//*****************************************************************************
 
//
//		メッシュの解放
//

void	IEX_ReleaseMesh( LPIEXMESH lpMesh )
{
	if( !lpMesh ) return;
	if( lpMesh->lpMesh ) lpMesh->lpMesh->Release();

	//	テクスチャ解放
	DWORD	i;

	for( i=0 ; i<lpMesh->MaterialCount ; i++ ){
		if( lpMesh->lpTexture[i] != NULL ) IEX_ReleaseTexture(lpMesh->lpTexture[i]);
	}

	delete[] lpMesh->lpMaterial;
	delete[] lpMesh->lpTexture;
	delete lpMesh;
}


//*****************************************************************************
//
//		RayPick
//
//*****************************************************************************
 
LPIEXRAYPICK	IEX_LoadRayPick( LPSTR filename )
{
	LPIEXRAYPICK	lpObj;
	IMOOBJ			imo;
	//	ファイル読み込み
	HANDLE	hfile;
	DWORD	dum;
	hfile = CreateFile( filename, GENERIC_READ, FILE_SHARE_READ, (LPSECURITY_ATTRIBUTES)NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL, (HANDLE) NULL );
	if( hfile == INVALID_HANDLE_VALUE ) return NULL;

	ReadFile( hfile, &imo, sizeof(IMOOBJ), &dum, NULL );
	//	メッシュ作成
	lpObj = new IEXRAYPICK;
	ZeroMemory( lpObj, sizeof(IEXRAYPICK) );
	//	頂点設定
	lpObj->lpVertex = new LVERTEX[imo.NumVertex];
	lpObj->NumVertex = (WORD)imo.NumVertex;
	ReadFile( hfile, lpObj->lpVertex, sizeof(LVERTEX)*imo.NumVertex, &dum, NULL );
	
	//	面設定
	lpObj->lpFace = new WORD[imo.NumFace*3];
	lpObj->NumFace = (WORD)imo.NumFace;
	ReadFile( hfile, lpObj->lpFace, sizeof(WORD)*imo.NumFace*3, &dum, NULL );

	//	属性設定
	lpObj->lpAtr = new DWORD[imo.NumFace];
	ReadFile( hfile, lpObj->lpAtr, sizeof(DWORD)*imo.NumFace, &dum, NULL );

	CloseHandle(hfile);
	return lpObj;
}

void	IEX_ReleaseRayPick( LPIEXRAYPICK obj )
{
	delete[]	obj->lpAtr;
	delete[]	obj->lpFace;
	delete[]	obj->lpVertex;
	delete		obj;
}

//
//
//

int	IEX_RayPick( LPIEXRAYPICK lpMesh, D3DVECTOR* out, D3DVECTOR* pos, D3DVECTOR* vec, float *Dist )
{
	float	t, neart, dist;
	float	px, py, pz;
	float	vx, vy, vz;
	float	dot;
	int		ret = -1;
	D3DVECTOR	cp;

	int		j;

	LVERTEX*	pVertices;
	u16			*pIndices;

	D3DVECTOR	v1, v2, v3, n;

	px = pos->x;	py = pos->y;	pz = pos->z;
	vx = vec->x;	vy = vec->y;	vz = vec->z;

	neart = 1000000.0f;
	dist = *Dist;
	dist = dist*dist;

	//	頂点サイズ計算
	pVertices = lpMesh->lpVertex;
	pIndices  = lpMesh->lpFace;

	for( j=0 ; j<lpMesh->NumFace*3 ; j+=3 ){
		//	面法線算出
		u16 a = pIndices[j+0];
		u16 b = pIndices[j+1];
		u16 c = pIndices[j+2];
		v1.x = pVertices[a].x;		v1.y = pVertices[a].y;		v1.z = pVertices[a].z;
		v2.x = pVertices[b].x;		v2.y = pVertices[b].y;		v2.z = pVertices[b].z;
		v3.x = pVertices[c].x;		v3.y = pVertices[c].y;		v3.z = pVertices[c].z;
		
		//	距離判定		
		float	xx, yy, zz;
		xx = (v1.x + v2.x + v3.x) / 3.0f - px;
		yy = (v1.y + v2.y + v3.y) / 3.0f - py;
		zz = (v1.z + v2.z + v3.z) / 3.0f - pz;
		if( (xx*xx + yy*yy + zz*zz) > dist ) continue;
		//	外積による法線算出		
		n.x = (v2.y-v1.y)*(v3.z-v1.z) - (v2.z-v1.z)*(v3.y-v1.y);
		n.y = (v2.z-v1.z)*(v3.x-v1.x) - (v2.x-v1.x)*(v3.z-v1.z);
		n.z = (v2.x-v1.x)*(v3.y-v1.y) - (v2.y-v1.y)*(v3.x-v1.x);
		//	内積の結果がプラスならば裏向き
		dot = vx*n.x + vy*n.y + vz*n.z;
		if( dot > 0 ) continue;
		//	交点算出
		t = (n.x*(v1.x-px) + n.y*(v1.y-py) + n.z*(v1.z-pz)) / (n.x*vx + n.y*vy + n.z*vz );
		if( t < .0f || t > neart ) continue;

		cp.x = t*vx + px;
		cp.y = t*vy + py;
		cp.z = t*vz + pz;
		//	内点判定
		D3DVECTOR	a1, a2, a3;

		a1.x = (v1.y-cp.y)*(v2.z-v1.z) - (v1.z-cp.z)*(v2.y-v1.y);
		a1.y = (v1.z-cp.z)*(v2.x-v1.x) - (v1.x-cp.x)*(v2.z-v1.z);
		a1.z = (v1.x-cp.x)*(v2.y-v1.y) - (v1.y-cp.y)*(v2.x-v1.x);
		if( n.x*a1.x + n.y*a1.y + n.z*a1.z < .0f ) continue;

		a2.x = (v2.y-cp.y)*(v3.z-v2.z) - (v2.z-cp.z)*(v3.y-v2.y);
		a2.y = (v2.z-cp.z)*(v3.x-v2.x) - (v2.x-cp.x)*(v3.z-v2.z);
		a2.z = (v2.x-cp.x)*(v3.y-v2.y) - (v2.y-cp.y)*(v3.x-v2.x);
		if( n.x*a2.x + n.y*a2.y + n.z*a2.z < .0f ) continue;

		a3.x = (v3.y-cp.y)*(v1.z-v3.z) - (v3.z-cp.z)*(v1.y-v3.y);
		a3.y = (v3.z-cp.z)*(v1.x-v3.x) - (v3.x-cp.x)*(v1.z-v3.z);
		a3.z = (v3.x-cp.x)*(v1.y-v3.y) - (v3.y-cp.y)*(v1.x-v3.x);
		if( n.x*a3.x + n.y*a3.y + n.z*a3.z < .0f ) continue;

		out->x = cp.x;
		out->y = cp.y;
		out->z = cp.z;
		vec->x = n.x;
		vec->y = n.y;
		vec->z = n.z;
		ret = j / 3;
		neart = t;
	}
	*Dist = neart;
	return	ret;
}

