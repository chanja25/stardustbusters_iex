#include "All.h"

//*****************************************************************************************************************************
//
//			エフェクト関連
//
//*****************************************************************************************************************************

//エフェクト
void	EffectBoost( float x, float y, float z, D3DXVECTOR3 move, D3DXVECTOR3 vec )
{
	
	D3DXVECTOR3 Pos,Move,Power;
	float stretch;
	for( int i=0;i<5;i++ ){
		Pos.x = x + (rand()%7-3)*.1f;
		Pos.y = y + (rand()%7-3)*.1f;
		Pos.z = z + move.z;
		Move = move;
		Move.x += (x - Pos.x)/(rand()%5+5);	//X移動量
		Move.y += (y - Pos.y)/(rand()%5+5);	//Y移動量
		Move.z += (z - Pos.z)/(rand()%5+5);	//Z移動量
		vec *= (rand()%3+3)*-0.05f;
		//Move += vec;
		Power.x = .0f;	//X力
		Power.y = .0f;	//Y力
		Power.z = .0f;	//Z力
		Power = vec;
		stretch = 0.95f;

		/*
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時透明度,
					　消滅フレーム, 消滅時透明度,
				　　	  中間フレーム, 中間透明度,
					　　 	  座標, 移動量, 力,　赤, 緑, 青,　スケール, フラグ );
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時カラー,
					　消滅フレーム, 消滅時カラー,
				　　	  中間フレーム, 中間カラー,
					　　 	  座標, 移動量, 力, ストレッチ, スケール, フラグ );
		*/
		IEX_SetParticle( 8, 0, 0xffffffff, 6, 0xffffffff, 3, 0xffffffff, &Pos, &Move, &Power, .0f, stretch, .8f, RS_ADD );
	}
}

//エフェクト
void	EffectFire( float x, float y, float z, D3DXVECTOR3 move )
{
	
	D3DXVECTOR3 Pos,Move,Power;
	for( int i=0;i<5;i++ ){
		Pos.x = x + (rand()%51-25)*.12f;
		Pos.y = y;
		Pos.z = z + (rand()%51-25)*.12f;
		Move = move;
		Move.x += (rand()%51-25)*0.002f;	//X移動量
		Move.y += (rand()%80+21)*0.002f;	//Y移動量
		Move.z += (rand()%51-25)*0.002f;	//Z移動量
		Power.x = .0f;//(rand()%10+1)*0.0005f;	//X力
		Power.y = .0f;	//Y力
		Power.z = .0f;	//Z力

		/*
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時透明度,
					　消滅フレーム, 消滅時透明度,
				　　	  中間フレーム, 中間透明度,
					　　 	  座標, 移動量, 力,　赤, 緑, 青,　スケール, フラグ );
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時カラー,
					　消滅フレーム, 消滅時カラー,
				　　	  中間フレーム, 中間カラー,
					　　 	  座標, 移動量, 力, ストレッチ, スケール, フラグ );
		*/
		int time = rand()%20+21;
		IEX_SetParticle( 9, 0, 0xffff2211, time, 0x88ff2211, time>>1, 0xbbff2211, &Pos, &Move, &Power, .0f, 1.0f, 1.0f, RS_ADD );
	}
}

//エフェクト
void	EffectCloud( float z, float mz )
{
	D3DXVECTOR3 Pos,Move,Power;
	for( int i=0;i<50;i++ ){
		Pos.x = (rand()%101-50)*0.05f;
		Pos.y = MAXTOP + 10.0f + (rand()%101-50)*0.05f;
		Pos.z = z + 300 + (rand()%101-50)*0.03f;
		Move.x = .0f;	//X移動量
		Move.y = .0f;	//Y移動量
		Move.z = .0f;	//Z移動量
		Power.x = .0f;	//X力
		Power.y = .0f;	//Y力
		Power.z = .0f;	//Z力

		/*
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時透明度,
					　消滅フレーム, 消滅時透明度,
				　　	  中間フレーム, 中間透明度,
					　　 	  座標, 移動量, 力,　赤, 緑, 青,　スケール, フラグ );
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時カラー,
					　消滅フレーム, 消滅時カラー,
				　　	  中間フレーム, 中間カラー,
					　　 	  座標, 移動量, 力, ストレッチ, スケール, フラグ );
		*/
		IEX_SetParticle( 4, 0, 0x33fffff, 200, 0x33ffffff, 50, 0x33ffffff, &Pos, &Move, &Power, .0f, 1.0f, 1.0f, RS_ADD );
	}
}

//エフェクト
void	EffectChage1( D3DXVECTOR3 pos, float mz, float size )
{
	D3DXVECTOR3 Pos,Move,Power;
	//for( int i=0;i<10;i++ ){
		Pos = pos;
		float rot = RAD*(rand()%360);
		Pos.x += cosf( rot )*size;
		Pos.y += sinf( rot )*size;
		Pos.z += cosf( rot )*size;
		Move.x = (pos.x - Pos.x)/40;	//X移動量
		Move.y = (pos.y - Pos.y)/40;	//Y移動量
		Move.z = mz + (pos.z - Pos.z)/40;	//Z移動量
		Power.x = .0f;	//X力
		Power.y = .0f;	//Y力
		Power.z = .0f;	//Z力

		/*
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時透明度,
					　消滅フレーム, 消滅時透明度,
				　　	  中間フレーム, 中間透明度,
					　　 	  座標, 移動量, 力,　赤, 緑, 青,　スケール, フラグ );
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時カラー,
					　消滅フレーム, 消滅時カラー,
				　　	  中間フレーム, 中間カラー,
					　　 	  座標, 移動量, 力, ストレッチ, スケール, フラグ );
		*/
		IEX_SetParticle( 0, 0, 0xffffff00, 40, 0xffffff00, 20, 0xffffff00, &Pos, &Move, &Power, .0f, 1.05f, 0.05f, RS_ADD );
	//}
}
void	EffectChage2( D3DXVECTOR3 pos, float size )
{
	D3DXVECTOR3 Pos,Move,Power;
	//for( int i=0;i<10;i++ ){
		Pos = pos;
		Move.x = 0;	//X移動量
		Move.y = 0;	//Y移動量
		Move.z = 0;	//Z移動量
		Power.x = .0f;	//X力
		Power.y = .0f;	//Y力
		Power.z = .0f;	//Z力

		/*
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時透明度,
					　消滅フレーム, 消滅時透明度,
				　　	  中間フレーム, 中間透明度,
					　　 	  座標, 移動量, 力,　赤, 緑, 青,　スケール, フラグ );
		IEX_SetParticle(　タイプ,　出現フレーム, 出現時カラー,
					　消滅フレーム, 消滅時カラー,
				　　	  中間フレーム, 中間カラー,
					　　 	  座標, 移動量, 力, ストレッチ, スケール, フラグ );
		*/
		IEX_SetParticle( 0, 0, 0xffffff00, 2, 0xffffff00, 1, 0xffffff00, &Pos, &Move, &Power, .0f,	1.0f, size, RS_ADD );
	//}
}

void	EffectTarget( D3DXVECTOR3 Pos, BOOL flg )
{
	D3DXVECTOR3 Move,Power;
	Move.x = .0f;	//X移動量
	Move.y = .0f;	//Y移動量
	Move.z = .0f;	//Z移動量
	Power.x = .0f;	//X力
	Power.y = .0f;	//Y力
	Power.z = .0f;	//Z力

	/*
	IEX_SetParticle(　タイプ,　出現フレーム, 出現時透明度,
				　消滅フレーム, 消滅時透明度,
			　　	  中間フレーム, 中間透明度,
				　　 	  座標, 移動量, 力,　赤, 緑, 青,　スケール, フラグ );
	IEX_SetParticle(　タイプ,　出現フレーム, 出現時カラー,
				　消滅フレーム, 消滅時カラー,
			　　	  中間フレーム, 中間カラー,
				　　 	  座標, 移動量, 力, ストレッチ, スケール, フラグ );
	*/
	float dist = fabs( Pos.z - MainFrameC->GetPlayer()->GetPos().z );
	dist /= 30;
	dist = ( dist<1.0f )?1.0f:dist;
	IEX_SetParticle( 13+flg, 0, 0xfffffff, 1, 0xffffffff, 0, 0xffffffff, &Pos, &Move, &Power, .0f, 1.0f, dist, RS_ADD );
}

void	EffectStar( float z )
{
	D3DXVECTOR3 Pos,Move,Power;
	Move.x = .0f;	//X移動量
	Move.y = .0f;	//Y移動量
	Move.z = -0.1f;	//Z移動量
	Power.x = .0f;	//X力
	Power.y = .0f;	//Y力
	Power.z = -0.05f;	//Z力
	Pos.x = (rand()%201-100) * 1.0f;
	Pos.y = (rand()%201-100) * 1.0f;
	Pos.z = z + (float)(rand()%50 + 300);

	/*
	IEX_SetParticle(　タイプ,　出現フレーム, 出現時透明度,
				　消滅フレーム, 消滅時透明度,
			　　	  中間フレーム, 中間透明度,
				　　 	  座標, 移動量, 力,　赤, 緑, 青,　スケール, フラグ );
	IEX_SetParticle(　タイプ,　出現フレーム, 出現時カラー,
				　消滅フレーム, 消滅時カラー,
			　　	  中間フレーム, 中間カラー,
				　　 	  座標, 移動量, 力, ストレッチ, スケール, フラグ );
	*/
	IEX_SetParticle( 0, 0, 0xfffff00, 120, 0xffffff00, 50, 0xffffff00, &Pos, &Move, &Power, .0f, 1.0f, 0.3f, RS_ADD );
}