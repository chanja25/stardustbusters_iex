#include "All.h"


//*****************************************************************************************************************************
//
//		ディスプレイ関連
//
//*****************************************************************************************************************************
//コンストラクタ
Display_Class::Display_Class()
{
	MainFrameC->Set2DObj( &HpGage[0], 0 );
	MainFrameC->Set2DObj( &HpGage[1], 1 );
	MainFrameC->Set2DObj( &HpGage[2], 2 );
	MainFrameC->Set2DObj( &HpGage[3], 28 );

	MainFrameC->Set2DObj( &WpGage[0], 4 );
	MainFrameC->Set2DObj( &WpGage[1], 5 );
	MainFrameC->Set2DObj( &WpGage[2], 6 );
	MainFrameC->Set2DObj( &WpGage[3], 7 );

	MainFrameC->Set2DObj( &BHpGage[0], 12 );
	MainFrameC->Set2DObj( &BHpGage[1], 13 );
	MainFrameC->Set2DObj( &BHpGage[2], 14 );

	MainFrameC->Set2DObj( &MsgObj[0], 17 );
	MainFrameC->Set2DObj( &MsgObj[1], 18 );

	MainFrameC->Set2DObj( &ScoreObj[0], 27 );
	MainFrameC->Set2DObj( &ScoreObj[1], 28 );

	wpType = -1;

	float maxhp = (float)MainFrameC->GetPlayer()->GetMaxHp();
	float hp = (float)MainFrameC->GetPlayer()->GetHp();
	Rotation = -RAD*91;

	Boss = -1;

	mflg	= 0;
	y		= MWHEIGHT;
	sp		= 2;
	score	= 0;
}

int		Display_Class::GetSp()
{
	return sp;
}
void	Display_Class::SetMsg()
{
	this->mflg = 1;
}

void	Display_Class::CloseMsg()
{
	this->mflg = 2;
}

void	Display_Class::SetMsgChara( int type )
{
	if( type<=8 ){
		type += 18;
	}else{
		type += 21;
	}
	MainFrameC->Set2DObj( &MsgObj[1], type );
}

void	Display_Class::BossReset()
{
	Boss = -1;
}

void	Display_Class::ScoreProc()
{
	score = ( score<MainFrameC->GetScore() )? score + SCOREUP: MainFrameC->GetScore();
}

//メッセージ用
void	Display_Class::MsgProc()
{
	switch( mflg ){
		case 1:
			y-=sp;
			mflg = ( y<=0 )? 3: mflg;
			y = ( y<0 )? 0: y;
			break;
		case 2:
			y+=sp;
			mflg = ( y>=MWHEIGHT )? 0: mflg;
			y = ( y>MWHEIGHT )? MWHEIGHT: y;
			break;
	}
}

//ボスHPゲージ管理
void	Display_Class::BHpProc()
{
	static Enemy_Class *en = NULL;
	static int cmaxhp = 0;
	Enemy_Class *enemy;
	enemy = MainFrameC->GetEnemy()->GetEnemy();
	while( enemy && Boss==-1 ){
		int type = enemy->GetType();
		if( type==5 ){
			if( enemy->GetHp()==0 )break;
			Boss = 0;
			en = enemy;
			cmaxhp = en->GetNext()->GetMaxHp();
			MainFrameC->Set2DObj( &BHpGage[2], 14 );
		}
		if( type==7 ){
			if( enemy->GetHp()==0 )break;
			Boss = 1;
			en = enemy;
			MainFrameC->Set2DObj( &BHpGage[2], 15 );
		}
		if( type==8 ){
			if( enemy->GetHp()==0 )break;
			Boss = 2;
			en = enemy;
			MainFrameC->Set2DObj( &BHpGage[2], 16 );
		}
		MainFrameC->GetEnemy()->EnemyNext( &enemy );
	}
	if( Boss==-1 )return;

	float maxhp;
	float hp;
	switch( Boss ){
		case 0:
			enemy = en;
			maxhp = (float)(enemy->GetMaxHp() + cmaxhp*6);
			hp = (float)enemy->GetHp();
			MainFrameC->GetEnemy()->EnemyNext( &enemy );

			while( enemy ){
				if( enemy->GetType()!=6 )break;

				hp += enemy->GetHp();
				MainFrameC->GetEnemy()->EnemyNext( &enemy );
			}
			BHp = hp / maxhp;
			break;
		case 1:
			maxhp = (float)en->GetMaxHp();
			hp = (float)en->GetHp();
			BHp = hp / maxhp;
			break;
		case 2:
			maxhp = (float)en->GetMaxHp();
			hp = (float)en->GetHp();
			BHp = hp / maxhp;
			break;
	}
	if( BHp<=0 ){
		Boss = -1;
		en = NULL;
	}
}

//HPゲージ管理
void	Display_Class::HpProc()
{
	float maxhp = (float)MainFrameC->GetPlayer()->GetMaxHp();
	float hp = (float)MainFrameC->GetPlayer()->GetHp();
	float rot = (hp / maxhp) * RAD*179 - RAD*91;
	Rotation = (rot>=Rotation)? (rot>=Rotation)? Rotation+RAD :rot : Rotation-RAD/4;
}

//武器ゲージ管理
void	Display_Class::WpProc()
{
	int type = MainFrameC->GetPlayer()->GetWeaponType();
	if( wpType==type )return;

	switch( type ){
	case -1:
		MainFrameC->Set2DObj( &WpGage[3], 7 );
		break;
	case 0:
		MainFrameC->Set2DObj( &WpGage[3], 8 );
		break;
	case 1:
		MainFrameC->Set2DObj( &WpGage[3], 9 );
		break;
	case 2:
		MainFrameC->Set2DObj( &WpGage[3], 10 );
		break;
	}

	wpType = type;
}

//ディスプレイ処理
void	Display_Class::Proc()
{
	ScoreProc();
	MsgProc();
	BHpProc();
	HpProc();
	WpProc();
}

void	Display_Class::ScoreDraw()
{
	IEX_Render2DObject( 0, 0, 256, 48, &ScoreObj[0], 0, 0, 256, 128 );

	if( score==0 ){
		IEX_Render2DObject( 160, 4, 32, 32, &ScoreObj[1], 0, 0, 64, 64 );
		return;
	}

	int i = 0;
	int j = 10;
	int k = score;
	if( k>=1000000 )k = 999999;

	while(k != 0){
		int a = k%j;
		IEX_Render2DObject( 170-i*32, 4, 32, 32, &ScoreObj[1], a%4*64, a/4*64, 64, 64 );
		i++;
		k /= 10;
	}
}


//メッセージ画面用
void	Display_Class::MsgDraw()
{
	if( mflg ){
		IEX_Render2DObject( 160, 480-96+y, 384, 96, &MsgObj[0], 0, 0, 512, 128 );
		IEX_Render2DObject( 160+11, 480-96+y+8, 82, 82, &MsgObj[1], 0, 0, 512, 512 );
	}
}

//ボスHPゲージ描画
void	Display_Class::BHpDraw()
{
	if( Boss==-1 )return;
	IEX_Render2DObject( 640-256, 0, 256, 64, &BHpGage[0], 0, 0, 512, 128 );
	IEX_Render2DObject( 640-188, 32, (int)(177*BHp), 21, &BHpGage[1], 0, 0, 256, 128 );
	IEX_Render2DObject( 640-238, 10, 40, 40, &BHpGage[2], 0, 0, 256, 256 );
}

//HPゲージ描画
void	Display_Class::HpDraw()
{
	IEX_Render2DObject( 0, 480-128, 256, 128, &HpGage[1], 0, 0, 512, 256 );
	IEX_Render2DObject( 0, 480-128, 256, 128, &HpGage[0], 0, 0, 512, 256 );
	
	static BOOL flg = FALSE;
	flg = !flg;
	float rot = (flg)?Rotation+RAD:Rotation-RAD;

	IEX_Render2DObject( 51, 480-62, 128,  64, rot, 1, &HpGage[2], 0, 0, 256, 128 );

	int remain = MainFrameC->GetPlayer()->GetRemain();

	IEX_Render2DObject( 112, 480-54, 50, 50, &HpGage[3], remain%4*64, remain/40*64, 64, 64 );
}

//武器ゲージ描画
void	Display_Class::WpDraw()
{
	IEX_Render2DObject( 640-128, 480-128, 128, 128, &WpGage[0], 0, 0, 256, 256 );
	Weapon_Class *w = MainFrameC->GetPlayer()->GetWeapon();
	if( w ){
		float x = w->GetChage();
		IEX_Render2DObject( 640-88, 480-28, (int)(83*x), 25, &WpGage[1], 0, 0, 128, 64 );
	}
	BOOL flg = (wpType==-1)?FALSE:TRUE;
	IEX_Render2DObject( 640-58, 480-104, 64, 32, &WpGage[2], 0, 64*flg, 128, 64 );
	IEX_Render2DObject( 640-84, 480-72, 128, 32, &WpGage[3], 0, 0, 256, 64 );
}

//ディスプレイ描画
void	Display_Class::Draw()
{
	ScoreDraw();
	MsgDraw();
	BHpDraw();
	HpDraw();
	WpDraw();
}