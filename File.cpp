#include "All.h"

//	コストラクタ
FileC::FileC()
{
	TextBuf = NULL;
	TextIP = 0;
	TextSize = 0;
}

//	デストラクタ
FileC::~FileC()
{
	if( TextBuf ){
		delete[] TextBuf;
	}
}

void	FileC::DataRelease()
{
	if( TextBuf ){
		delete[] TextBuf;
		TextBuf = NULL;
	}
}

//	ファイルの読み込み
char	*FileC::Read( char *filename, BOOL fg )
{
	HANDLE hfile;
	DWORD dum;
	char fname[256];

	if( TextBuf )delete[] TextBuf;
	wsprintf( fname, "%s", filename );

	//	ファイルを開く
	hfile = CreateFile( fname, GENERIC_READ,
						FILE_SHARE_READ,
						(LPSECURITY_ATTRIBUTES)NULL,
						OPEN_EXISTING,
						FILE_ATTRIBUTE_NORMAL,
						(HANDLE) NULL );

	//	ファイルが正常に開けていたら実行
	if( hfile != INVALID_HANDLE_VALUE ){
		TextSize = GetFileSize( hfile, NULL );
		TextBuf = new char[TextSize+1];
		ReadFile( hfile, TextBuf, TextSize, &dum, NULL );
		TextBuf[TextSize] = '\0';
		CloseHandle(hfile);
	}

	if( fg ){
		for( int i=0;i<TextSize;i++ ){
			TextBuf[i] -= PASS;
		}
	}

	return TextBuf;
}

//	ファイルの書き込み
void	FileC::Write( char *filename, char *str, BOOL mode, BOOL fg )
{
	HANDLE hfile;
	DWORD dwErrCd;
	DWORD dum;
	char fname[256];

	wsprintf( fname, "%s", filename );

	hfile = CreateFile( fname, 
			GENERIC_WRITE, 
			FILE_SHARE_WRITE,
			NULL,
			CREATE_NEW,
			FILE_ATTRIBUTE_NORMAL,
			NULL );

	DWORD Mode;
	Mode = ( mode )? CREATE_ALWAYS: OPEN_ALWAYS;

	if (hfile == INVALID_HANDLE_VALUE) 
	{
		dwErrCd = GetLastError();
		if(dwErrCd == ERROR_FILE_EXISTS)
		{
			CloseHandle(hfile);
			hfile = CreateFile( fname,
					GENERIC_WRITE,
					FILE_SHARE_WRITE,
					NULL,
					Mode,
					FILE_ATTRIBUTE_NORMAL,
					NULL );

			if (hfile == INVALID_HANDLE_VALUE) {
				//TEST_OUT_FILE.txtファイル追加モードオープンエラー発生
			}
		}
		SetFilePointer(hfile,0,NULL, FILE_END);
	}

	if( fg ){
		for( int i=0;i<lstrlen( str );i++ ){
			str[i] += PASS;
		}
	}

	//ここで指定された文字列を出力します。
	WriteFile( hfile, str, lstrlen( str ), &dum, NULL );

	CloseHandle(hfile);
}

//	ファイルの書き込み
void	FileC::Write( char *filename, LPVOID lpBuf, DWORD size, BOOL mode, BOOL fg )
{
	HANDLE hfile;
	DWORD dwErrCd;
	DWORD dum;
	char fname[256];

	wsprintf( fname, "%s", filename );

	hfile = CreateFile( fname, 
			GENERIC_WRITE, 
			FILE_SHARE_WRITE,
			NULL,
			CREATE_NEW,
			FILE_ATTRIBUTE_NORMAL,
			NULL );

	DWORD Mode;
	Mode = ( mode )? CREATE_ALWAYS: OPEN_ALWAYS;

	if (hfile == INVALID_HANDLE_VALUE) 
	{
		dwErrCd = GetLastError();
		if(dwErrCd == ERROR_FILE_EXISTS)
		{
			CloseHandle(hfile);
			hfile = CreateFile( fname,
					GENERIC_WRITE,
					FILE_SHARE_WRITE,
					NULL,
					Mode,
					FILE_ATTRIBUTE_NORMAL,
					NULL );

			if (hfile == INVALID_HANDLE_VALUE) {
				//TEST_OUT_FILE.txtファイル追加モードオープンエラー発生
			}
		}
		SetFilePointer(hfile,0,NULL, FILE_END);
	}

	if( fg ){
		char *str = (char*)lpBuf;
		for( DWORD i=0;i<size;i++ ){
			str[i] += PASS;
		}
	}

	//ここで指定された文字列を出力します。
	WriteFile( hfile, lpBuf, size, &dum, NULL );

	CloseHandle(hfile);
}

//	読み込んだファイルを受け取る
char	*FileC::GetFile()
{
	return TextBuf;
}

int		FileC::GetFSize()
{
	return TextSize;
}