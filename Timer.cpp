//----------------------------------------------------------------------------
//!
//!	@file	Timer.cpp
//!	@brief	時間計測
//!
//----------------------------------------------------------------------------
#include "All.h"


//----------------------------------------------------------------------------
//	コンストラクタ
//----------------------------------------------------------------------------
Timer::Timer(void)
{
	LARGE_INTEGER li;
	QueryPerformanceFrequency( &li );
	_freq = li.QuadPart;
}

//----------------------------------------------------------------------------
//	タイマースタート
//----------------------------------------------------------------------------
void	Timer::start(void)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter( &li );
	_begin = li.QuadPart;
}

//----------------------------------------------------------------------------
//	タイマー終了
//!	@retval	経過時間
//----------------------------------------------------------------------------
u64		Timer::end(void)
{
	LARGE_INTEGER li;
	QueryPerformanceCounter( &li );
	return ((li.QuadPart - _begin) * rate) / _freq;
}