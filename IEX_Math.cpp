#include	"iExtreme.h"

//*****************************************************************************
//
//
//
//*****************************************************************************

//
//		�Q�_�Ԃ̊p�x
//

float	IEX_GetAngle( D3DXVECTOR3 pos, D3DXVECTOR3 target )
{
	float	xx, zz, t, angle;
	float	rad;

	xx = target.x - pos.x;
	zz = target.z - pos.z;

	if( xx == 0 ){
		if( zz >= 0 ) return .0f;
		 else return D3DX_PI;
	}
	if( zz == 0 ){
		if( xx >= 0 ) return D3DX_PI/2;
		 else return D3DX_PI*3/2;
	}
	
	t = xx / zz;
	rad = atanf(t);
	if( zz < 0 ) angle = rad + D3DX_PI;
	 else if( xx < 0 ) angle = rad + D3DX_PI*2;
	 else angle = rad;

	return	angle;
}

