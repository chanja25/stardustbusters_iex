#include	"iextreme.h"

//*****************************************************************************
//
//		スキンメッシュ関連
//
//*****************************************************************************

//
//		スキンメッシュ更新
//

void	IEX_UpdateSkinMeshFrame( LPIEX3DOBJ lpObj, float frame )
{
	u32			i, j;
	LPIEXMESH	pMesh;
	LPIEXANIME	lpAnime;
	float		t;

	pMesh = lpObj->lpMesh;

	for( i=0 ; i<lpObj->NumBone ; i++ ){
		lpAnime = &lpObj->lpAnime[i];

		//	ポーズ設定
		if( lpAnime->rotNum <= 0 ){
			lpObj->CurPose[i] = lpObj->orgPose[i];
		} else if( lpAnime->rotNum == 1 ){
			CopyMemory( &lpObj->CurPose[i], &lpAnime->rot[0], sizeof(D3DXQUATERNION) );
		} else {
			for( j=0 ; j<lpAnime->rotNum-1 ; j++ ){
				if( (frame>=lpAnime->rotFrame[j]) && (frame<lpAnime->rotFrame[j+1]) ){
					t = (float)(frame-lpAnime->rotFrame[j]) / (float)(lpAnime->rotFrame[j+1] - lpAnime->rotFrame[j]);
					D3DXQuaternionSlerp( &lpObj->CurPose[i], &lpAnime->rot[j], &lpAnime->rot[j+1], t );
					break;
				}
			}
			if( j == lpAnime->rotNum-1 ){
				lpObj->CurPose[i] = lpAnime->rot[lpAnime->rotNum-1];
			}
		}
		//	座標設定
		if( lpObj->lpAnime[i].posNum == 0 ){
			lpObj->CurPos[i].x = lpObj->orgPos[i].x;
			lpObj->CurPos[i].y = lpObj->orgPos[i].y;
			lpObj->CurPos[i].z = lpObj->orgPos[i].z;
		} else {
			for( j=0 ; j<lpAnime->posNum-1 ; j++ ){
				if( (frame>=lpAnime->posFrame[j]) && (frame<lpAnime->posFrame[j+1]) ){
					t = (float)(frame-lpAnime->posFrame[j]) / (float)(lpAnime->posFrame[j+1] - lpAnime->posFrame[j]);
					lpObj->CurPos[i].x = lpAnime->pos[j].x + (lpAnime->pos[j+1].x-lpAnime->pos[j].x)*t;
					lpObj->CurPos[i].y = lpAnime->pos[j].y + (lpAnime->pos[j+1].y-lpAnime->pos[j].y)*t;
					lpObj->CurPos[i].z = lpAnime->pos[j].z + (lpAnime->pos[j+1].z-lpAnime->pos[j].z)*t;
					break;
				}
			}
			if( j == lpAnime->posNum-1 ){
				lpObj->CurPos[i] = lpAnime->pos[lpAnime->posNum-1];
			}
		}
	}
}

//
//		行列再構成および、メッシュの更新
//

void	IEX_UpdateSkinMatrix( LPIEX3DOBJ lpObj )
{
	u32			i;
	D3DXMATRIX	mat, WorkMatrix[128];

	//	ボーン更新
	for( i=0 ; i<lpObj->NumBone ; i++ ){
		D3DXMatrixRotationQuaternion( &lpObj->lpBoneMatrix[i], &lpObj->CurPose[i] );
		lpObj->lpBoneMatrix[i]._41 = lpObj->CurPos[i].x;
		lpObj->lpBoneMatrix[i]._42 = lpObj->CurPos[i].y;
		lpObj->lpBoneMatrix[i]._43 = lpObj->CurPos[i].z;

		if( lpObj->BoneParent[i] != 0xFFFF ) lpObj->lpBoneMatrix[i] *= lpObj->lpBoneMatrix[ lpObj->BoneParent[i] ];
	}
	for( i=0 ; i<lpObj->NumBone ; i++ ){
		lpObj->lpMatrix[i] = lpObj->lpOffsetMatrix[i] * lpObj->lpBoneMatrix[i];
	}
}

//
//		スキンメッシュ更新
//

void	IEX_UpdateSkinMesh( LPIEX3DOBJ lpObj )
{
	//	メッシュ更新
	LPMESHVERTEX	pVertex;
	lpObj->lpMesh->lpMesh->LockVertexBuffer( 0, (void**)&pVertex );
	lpObj->lpSkinInfo->UpdateSkinnedMesh( lpObj->lpMatrix, NULL, lpObj->lpVertex, pVertex );
	lpObj->lpMesh->lpMesh->UnlockVertexBuffer();
}
