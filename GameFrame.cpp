#include "All.h"


GameManage_Class::GameManage_Class()
{
	File = new FileC();
	GameMode = NextMode = -1;
	LoadCnt = 0;

	flg = TRUE;

	//char *fn = "DATA\\SCRIPT\\stage1_Clear.txt";
	//char *s = File->Read( fn, 0 );
	//int size = File->GetFSize();
	//File->Write( fn, s, size, 1, 1 );

	char *score = File->Read( "DATA\\SAVE\\Score.ini" );
	for( int i=0;i<5;i++ ){
		if( !score ){
			HiScore[i] = 0;
			continue;
		}
		HiScore[i] = atoi( score );
		while( *score!='\n' && *score!='\0' ){
			score++;
		}
		score++;
	}
}

GameManage_Class::~GameManage_Class()
{
	char score[256];

	wsprintf( score, "%d", HiScore[0] );
	for( int i=1;i<5;i++ ){
		wsprintf( score, "%s\r\n%d", score, HiScore[i] );
	}
	File->Write( "DATA\\SAVE\\Score.ini", score );

	delete File;
}

void	GameManage_Class::Release()
{
	DWORD ExitCode;
	GetExitCodeThread( hThread, &ExitCode );
	if( ExitCode != STILL_ACTIVE ) return;

	while( ExitCode==STILL_ACTIVE ) {
		GetExitCodeThread( hThread, &ExitCode );
		Sleep(0);
	}
	CloseHandle( hThread );
	LoadRelease();
}

void	GameManage_Class::SetGameMode( int mode )
{
	GameMode = mode;
}

int		GameManage_Class::GetGameMode()
{
	return GameMode;
}

void	GameManage_Class::SetNextMode( int mode )
{
	NextMode = mode;
	if(GameMode!=NextMode)LoadInit();
}

int		GameManage_Class::GetNextMode(){
	return NextMode;
}

void	GameManage_Class::SetFlg()
{
	flg = FALSE;
}

void	GameManage_Class::SetScore( int score )
{
	this->score = score;
}

int		GameManage_Class::GetScore()
{
	return (flg)? score*3: score;
}

void	GameManage_Class::SetHiScore( int no, int score )
{
	HiScore[no] = score;
}

int		GameManage_Class::GetHiScore( int no )
{
	return HiScore[no];
}

FileC	*GameManage_Class::GetFile()
{
	return File;
}

Fade_Class *GameFrame_Class::GetFade()
{
	return &Fade;
}

Script_Class *GameFrame_Class::GetScript()
{
	return Script;
}

void	MainFrame_Class::SetChara( IEX3DOBJ *Obj, int no )
{
	CopyMemory( Obj, lpObj[no], sizeof(IEX3DOBJ) );
}

void	MainFrame_Class::SetWeapon( IEXMESH *Mesh, int no )
{
	CopyMemory( Mesh, lpWeapon[no], sizeof(IEXMESH) );
}

void	MainFrame_Class::Set2DObj( IEX2DOBJ *Obj, int no )
{
	CopyMemory( Obj, lp2DObj[no], sizeof(IEX2DOBJ) );
}

void	MainFrame_Class::SetFlg()
{
	flg = TRUE;
}

void	MainFrame_Class::SetBullet( Bullet_Class *bullet )
{
	if( !bullet )return;
	Bullet_Class **bu,*b;
	bu = MainFrameC->GetBullet();
	b = *bu;
	if( !*bu ){
		*bu = bullet;
		return;
	}
 	while( (*bu)->GetNext() ){
		(*bu)->NextBullet( bu );
	}
 	bullet->SetBefore( *bu );
	(*bu)->SetNext( bullet );
	*bu = b;
}