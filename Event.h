#ifndef __EVENT_H__
#define __EVENT_H__


typedef struct{
	char str[256];
	int x;
	int y;
	int size;
	int speed;
	DWORD color;
	BOOL flg;
}FontData;

//イベントクラス
class Event_Class{
private:
	BOOL EventFlag;

	Font_Class	Font;
	FontData	fdata;
public:
	Event_Class();

	void SetEventFlag( BOOL flg );
	BOOL GetEventFlag();
	BOOL GetFontFlag();
	Font_Class *GetFont();
	void DrawFont();

	void SetString( char *str, int x, int y, int size, DWORD color, int speed );
	void DeleteFont();

	void Proc();
	void Draw();
};

#endif