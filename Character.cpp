#include "All.h"



//*****************************************************************************************************************************
//
//		キャラクター全般
//
//*****************************************************************************************************************************

LPIEX3DOBJ	Character_Class::GetObj()
{
	return &Obj;
}

D3DXMATRIX Character_Class::GetBone( int no )
{
	D3DXMATRIX mat;
	mat = Obj.lpBoneMatrix[no] * Obj.TransMatrix;
	return mat;
}

D3DXVECTOR3 Character_Class::GetBonePos( int no )
{
	D3DXMATRIX mat;
	D3DXVECTOR3 pos;
	mat = GetBone( no );
	pos.x = mat._41;
	pos.y = mat._42;
	pos.z = mat._43;
	return pos;
}

void	Character_Class::SetPos( D3DXVECTOR3 Pos )
{
	pos = Pos;
}

D3DXVECTOR3	Character_Class::GetPos()
{
	return pos;
}

void	Character_Class::SetAngle( D3DXVECTOR3 Angle )
{
	angle = Angle;
}

D3DXVECTOR3	Character_Class::GetAngle()
{
	return angle;
}

void	Character_Class::SetMove( D3DXVECTOR3 Move )
{
	move = Move;
}

D3DXVECTOR3	Character_Class::GetMove()
{
	return move;
}

int		Character_Class::GetMaxHp()
{
	return maxhp;
}

void	Character_Class::SetHp( int Hp )
{
	hp = Hp;
}

int		Character_Class::GetHp()
{
	return hp;
}

void	Character_Class::SetAlpha( float Alpha )
{
	alpha = Alpha;
}

float	Character_Class::GetAlpha()
{
	return alpha;
}

void	Character_Class::SetSpeed( float Speed )
{
	speed = Speed;
}

float	Character_Class::GetSpeed()
{
	return speed;
}

void	Character_Class::SetMode( int Mode )
{
	mode = Mode;
}

int		Character_Class::GetMode()
{
	return mode;
}

float	Character_Class::GetHitDist()
{
	return hDist;
}

void	Character_Class::SetNo( int No )
{
	no = No;
}

int		Character_Class::GetNo()
{
	return no;
}

void	Character_Class::SetDead()
{
	dflg = TRUE;
}

BOOL	Character_Class::GetDead()
{
	return dflg;
}

int		Character_Class::GetChainSpeed()
{
	return ChainSpeed;
}

Weapon_Class *Character_Class::GetWeapon()
{
	return weapon;
}

void	Player_Class::SetWeapon( int no )
{
	switch(no)
	{
	case 0:
		weapon = new Gun;
		break;
	case 1:
		weapon = new Missile;
		break;
	case 2:
		weapon = new Laser;
		break;
	}
	weapon->Init();
}

void	Character_Class::SetWeaponType( int wpType )
{
	this->wpType = wpType;
}

int		Character_Class::GetWeaponType()
{
	return wpType;
}

void	Player_Class::LostWeapon()
{
	delete weapon;
	weapon = NULL;
}

Bullet_Class *Player_Class::SetBullet()
{
	if( !flg )return NULL;
	Bullet_Class *bu1 = new Bullet_Class;
	Bullet_Class *bu2 = new Bullet_Class;

	D3DXMATRIX mat = Obj.TransMatrix;
	D3DXVECTOR3 vec;
	vec = GetBonePos( 0 );
	SetVecMatrix( &mat, &vec, 3 );
	GetVecMatrix( &mat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu1->Init( bullet, mat, vec, BScale1, BulletSpeed1, 0, BDamage1, 0, TRUE );
	vec = GetBonePos( 2 );
	SetVecMatrix( &mat, &vec, 3 );
	GetVecMatrix( &mat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu2->Init( bullet, mat, vec, BScale1, BulletSpeed1, 0, BDamage1, 0, TRUE );
	bu1->SetNext( bu2 );
	bu2->SetBefore( bu1 );
	ChainTimer = 0;
	flg = FALSE;
	Sound::PlaySound( 0, FALSE );
	return bu1;
}

Bullet_Class *Enemy_Class::SetBullet()
{
	if( !sflg )return NULL;
	Bullet_Class *bu = new Bullet_Class;

	D3DXMATRIX mat = Obj.TransMatrix;
	D3DXVECTOR3 vec;
	GetVecMatrix( &mat, &vec, 2 );
	vec = GetBonePos( 0 ) + vec*3.0f;

	SetVecMatrix( &mat, &vec, 3 );
	GetVecMatrix( &mat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu->Init( bullet, mat, -vec, EBScale, EBulletSpeed, 0, EBDamage, 0, FALSE );
	ChainTimer = 0;
	flg = FALSE;
	Sound::PlaySound( 0, FALSE );
	return bu;
}

void	Enemy_Class::SetType( int type )
{
	this->type = type;
}

int		Enemy_Class::GetType()
{
	return type;
}