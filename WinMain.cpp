#include "All.h"
#include "resource.h"

BOOL	bFullScreen = FALSE;
HWND	hWndMain;

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************


//*****************************************************************************************************************************
//
//		アプリケーション初期化
//
//*****************************************************************************************************************************

BOOL	InitApp( HWND hWnd )
{
	hWndMain = hWnd;
	/*	Direct3D初期化	*/ 
	IEX_Initialize( hWnd, bFullScreen, SCREEN640 );
	IEX_InitAudio( hWnd );
	IEX_InitInput( hWnd );
	SYS_Initialize();

	return TRUE;
}

//*****************************************************************************************************************************
//
//		ウィンドウプロシージャ
//
//*****************************************************************************************************************************

char	WinKeyBuf[64];

long FAR PASCAL	WindowProc(HWND hWnd, UINT message, WPARAM wParam, LPARAM lParam)
{
	int		i;

	switch (message){
		case WM_DESTROY:
			PostQuitMessage(0);
			return 0L;
		case WM_CHAR:
			for( i=63 ; i>0 ; i-- ) WinKeyBuf[i] = WinKeyBuf[i-1];
			WinKeyBuf[0] = (BYTE)wParam;
			break;

		case WM_KEYDOWN:
			switch (wParam){
				case VK_ESCAPE:
				case VK_F12:
					PostMessage(hWnd, WM_CLOSE, 0, 0);
					return 0L;
		}
			break;
    }
    return DefWindowProc(hWnd, message, wParam, lParam);
}

//*****************************************************************************************************************************
//
//		ウィンドウ作成
//
//*****************************************************************************************************************************

static	HWND	InitWindow( HINSTANCE hInstance, int nCmdShow )
{
	WNDCLASS		wc;
	RECT			rect = { 0,0,640,480 };
	HWND			hWnd;

	/*	ウィンドウクラス設定	*/ 
	wc.style = CS_HREDRAW | CS_VREDRAW;
	wc.lpfnWndProc = WindowProc;
	wc.cbClsExtra = 0;
	wc.cbWndExtra = 0;
	wc.hInstance = hInstance;
	wc.hIcon = LoadIcon(hInstance, (LPSTR)IDI_ICON1);
	wc.hCursor = LoadCursor(NULL, IDC_ARROW);
	wc.hbrBackground = (HBRUSH )GetStockObject(BLACK_BRUSH);
	wc.lpszMenuName = NULL;
	wc.lpszClassName = "DUNGEON";
	RegisterClass(&wc);

	/*	ウィンドウ作成		*/ 
	if( !bFullScreen ){
		AdjustWindowRect( &rect, WS_OVERLAPPEDWINDOW, FALSE );
		hWnd = CreateWindow( "DUNGEON", "StardustBusters", WS_OVERLAPPEDWINDOW, 
								0, 0, rect.right-rect.left, rect.bottom-rect.top,
								NULL, NULL, hInstance, NULL);
	} else {
		hWnd = CreateWindow( "DUNGEON", "StardustBusters", WS_POPUP, 0, 0, 640,480, NULL, NULL, hInstance, NULL);
		ShowCursor(FALSE);
	}

	if( !hWnd ) return FALSE;
	ShowWindow(hWnd, nCmdShow);
	UpdateWindow(hWnd);

    return hWnd;
}

//*****************************************************************************************************************************
//
//		ウインメイン
//
//*****************************************************************************************************************************

int PASCAL	WinMain( HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow )
{
	MSG		msg;
	HWND	hWnd;

	//if( GetAsyncKeyState(VK_CONTROL)&0x8000 ) bFullScreen = TRUE;
	int Ret;
	Ret = MessageBox( NULL, "フルスクリーンモードにしますか？？", "", MB_YESNO );
	bFullScreen = ( Ret==IDYES )? TRUE: FALSE;

	hWnd = InitWindow(hInstance, nCmdShow);
	InitApp(hWnd);
	/*	メインループ	*/ 
	for(;;){
		if( PeekMessage( &msg, NULL, 0, 0, PM_NOREMOVE ) ){
			if( !GetMessage(&msg, NULL, 0, 0) ) break;
			TranslateMessage(&msg);
			DispatchMessage(&msg);
		} else {
			if( SYS_Main() ) SYS_Draw();
		}

	}
	SYS_Release();
	IEX_Release();
	return msg.wParam;
}
