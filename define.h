#ifndef __DEFINE_H__
#define __DEFINE_H__


//残り残機
#define REMAIN 2

//最大HP
#define PHP	1000
#define EZHP1 5
#define EZHP2 10
#define EZHP3 20
#define EZHP4 5
#define EZHP5 300
#define EBHP1 1800
#define EBHP2 3000
#define EBHP3		5
#define EBHP3_WP	1500
#define EBHP3_CORE	3000

#define SCOREUP 39

//SCORE

#define EZSCORE1 500
#define EZSCORE2 600
#define EZSCORE3 700
#define EZSCORE4 300
#define EZSCORE5 2000
#define EBSCORE1 10000
#define EBSCORE2 20000

#define EBSCORE3		0
#define EBSCORE3_WP		5000
#define EBSCORE3_CORE	40000



//プレイヤーの当たり範囲
#define PHSIZE .0f
//敵の当たり範囲
#define EHSIZE1 4.0f
#define EHSIZE2 4.0f
#define EHSIZE3 4.0f
#define EHSIZE4 4.0f
#define EHSIZE5 15.0f
#define BHSIZE1 30.0f
#define BHSIZE2 15.0f
//赤い彗星？
#define BHNUM	8
//中心
#define BHSIZE21 12.0f
//右上レーザー
#define BHSIZE22 2.0f
//左上レーザー
#define BHSIZE23 2.0f
//体下
#define BHSIZE24 5.0f
//右膝
#define BHSIZE25 3.5f
//左膝
#define BHSIZE26 3.5f
//右足
#define BHSIZE27 3.5f
//左足
#define BHSIZE28 3.5f

#define BHSIZE3 30.0f

//敵と衝突時のダメージ
#define EHDAMAGE 60
#define BHDAMAGE 300
//コンテナが敵を作りだす速度
#define ECONSP	10
//ひまわりの攻撃速度
#define	EHIWP	10
//敵のスケール
#define ESCALE1 0.1f
#define ESCALE2 0.1f
#define ESCALE3 0.03f
#define ESCALE4 0.02f
#define ESCALE5 0.03f
#define BSCALE1 0.3f
#define BSCALE2 0.3f
#define BSCALE3 0.2f

//移動速度
#define PSP	1.0f
#define ESP 0.5f

//銃の移動速度
#define WSP .04f

//銃の落下速度
#define WDSPXZ	0.8f
#define WDSP	-0.5f

//銃の威力
//機銃
#define BDamage1 5
//マシンガン
#define BDamage2 4
//ミサイル
#define BDamage3 20
//レーザー
#define BDamage4 1
//敵の弾
#define EBDamage 10
#define EBDamage2 3
#define BBDamage 50

//銃の当たりサイズ
#define BSIZE	0.0f
#define ELSIZE	23.0f
#define ELSIZE2	4.0f

//銃弾の速度
#define BulletSpeed1 16.0f
#define BulletSpeed2 16.0f
#define BulletSpeed3 5.0f
#define BulletSpeed4 32.0f
#define EBulletSpeed 2.0f

//ミサイルの最大ターゲット数
#define MAXTARGET 8
//ミサイルのロック時間
#define MLockTime 6
//ミサイルのホーミング速度
#define MSP   0.1f

//レーザーの連続発射数
#define MAXLASER 500

//エネルギーを全消費後の使用不可時間
#define LIMIT 180

//エネルギーの消費数
#define ELASER	5
//エネルギーの回復数
#define ULASER	3

//銃の最大弾数
#define MaxBullet1 1000
#define MaxBullet2 999999999
#define MaxBullet3 999999999

//銃の連射速度
#define GCSP1 10
#define GCSP2 5
#define GCSP3 60
#define GCSP4 1
#define EGCSP 20

//弾のサイズ
#define BScale1 0.01f
#define BScale2 0.01f
#define BScale3 0.03f
#define BScale4 0.02f
#define EBScale 0.01f
#define EBScale2 1.0f
#define EBScale3 0.06f

//移動範囲
//#define MAXTOP 1000.0f
//#define MAXUNDER -10.0f
//#define MAXLEFT -24.5f
//#define MAXRIGHT 24.5f
#define MAXTOP 35.0f
#define MAXUNDER 0.0f
#define MAXLEFT -14.5f
#define MAXRIGHT 14.5f

//キー設定
#define SHOOT	KEY_SPACE
#define UP		KEY_UP
#define DOWN	KEY_DOWN
#define LEFT	KEY_LEFT
#define RIGHT	KEY_RIGHT
#define WP1		KEY_A
#define WP2		KEY_B
#define WP3		KEY_C
#define PURGE1	KEY_L
#define PURGE2	KEY_R

#define MWHEIGHT 96

#endif