//include
#include "All.h"


//FontMakeÅìÁ½WORD^Ì¶zñ
WORD	FontTbl[363] = {
	' ', '¢', '¤', '¦', '¨', '©', '«', '­', '¯', '±', 
	'³', 'µ', '·', '¹', '»', '½', '¿', 'Â', 'Ä', 'Æ', 
	'È', 'É', 'Ê', 'Ë', 'Ì', 'Í', 'Ð', 'Ó', 'Ö', 'Ù', 
	'Ü', 'Ý', 'Þ', 'ß', 'à', 'â', 'ä', 'æ', 'ç', 'è', 
	'é', 'ê', 'ë', 'í', 'ð', 'ñ', 'ª', '¬', '®', '°', 
	'²', '´', '¶', '¸', 'º', '¼', '¾', 'À', 'Ã', 'Å', 
	'Ç', 'Î', 'Ñ', 'Ô', '×', 'Ú', 'Ï', 'Ò', 'Õ', 'Ø', 
	'Û', '', '¡', '£', '¥', '§', 'Á', 'á', 'ã', 'å', 
	'A', 'C', 'E', 'G', 'I', 'J', 'L', 'N', 'P', 'R', 
	'T', 'V', 'X', 'Z', '\', '^', '`', 'c', 'e', 'g', 

	'i', 'j', 'k', 'l', 'm', 'n', 'q', 't', 'w', 'z', 
	'}', '~', '', '', '', '', '', '', '', '', 
	'', '', '', '', '', '', 'K', 'M', 'O', 'Q', 
	'S', 'U', 'W', 'Y', '[', ']', '_', 'a', 'd', 'f', 
	'h', 'o', 'r', 'u', 'x', '{', '|', 's', 'v', 'y', 
	'', '@', 'B', 'D', 'F', 'H', 'b', '', '', '', 
	'`', 'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 
	'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 
	't', 'u', 'v', 'w', 'x', 'y', '', '', '', '', 
	'', '', '', '', '', '', '', '', '', '', 

	'', '', '', '', '', '', '', '', '', '', 
	'', '', 'B', 'E', 'A', 'D', 'C', '^', '_', '[', 
	'I', 'H', 'h', '', 'e', 'f', '`', '', '|', '{', 
	'~', '', '', '', '', 'u', 'v', 'w', 'x', 'y', 
	'z', 'i', 'j', 'o', 'p', '{', 'Í', 'F', '', 'ã', 
	'û', 'e', 'ã', 'Å', 'å', 'o', 'Í', 'C', '', 'Ë', 
	'j', 'q', '¯', 'º', 'í', '', '', 'i', 'ß', '·', 
	'', '·', '\', 'ó', '', '¬', 'Ú', 'ß', '@', 'Ì', 
	'O', 'ê', 'I', 'ó', 'â', '~', 'ì', 'í', 'J', 'n', 
	'È', 's', '®', '', 'Ä', '', 'K', '', '', 'Ú', 

	'v', 'Ë', '@', '¶', 'm', 'ê', '', '·', '', 'à', 
	'¾', '', 'ê', 'p', '', 'O', '¹', 'C', '³', '©', 
	'§', 'ä', '[', 'l', 'ä', 'v', '³', 'h', 'õ', 'ó', 
	'Ô', 'G', 'Ó', '', 'Ó', 'O', 'û', '', 'e', 'm', 
	'F', 'ü', 'Í', 'U', 'î', 't', 'Î', 'U', 'L', 'ø', 
	'ñ', '', '', 'B', '©', 'Ê', 'í', '', '', '\', 
	'¢', 'æ', 
	'' };




//RXgN^
Font_Class::Font_Class()
{
	len = 0;

	char	str[100];

	for( int i=0 ; i<BNum ; i++ ){
		wsprintf( str, "DATA\\FONT\\FONTDAT%02d.bmp", i );
		lpFont[i] = IEX_Load2DObject( str );
	}
}

//fXgN^
Font_Class::~Font_Class()
{
	for( int i=0 ; i<BNum ; i++ ){
		IEX_Release2DObject( lpFont[i] );
		lpFont[i] = NULL;
	}
}

void	Font_Class::LenInit()
{
	len = 0;
}

//¶ñÌ`æ
void	Font_Class::DrawChar( WORD chr, int x, int y, int size, DWORD color )
{
	int		n, index;
	WORD	work;

	work = (chr>>8) | (chr<<8);
	for( index=0 ; FontTbl[index]!='' ; index++ ){
		if( FontTbl[index] == work ) break;
	}
	if( FontTbl[index]=='' ) return;

	n = index / (Scr*Scr);
	index %= (Scr*Scr);
	IEX_Render2DObject( x, y, size, size, lpFont[n], (int)(index%Scr)*FSize, (index/Scr)*FSize+1, FSize, FSize, RS_SUB, 0xffffffff );
	IEX_Render2DObject( x, y, size, size, lpFont[n], (int)(index%Scr)*FSize, (index/Scr)*FSize+1, FSize, FSize, RS_ADD, color );
}

//¶ñÌ`æ
BOOL	Font_Class::DrawString( LPSTR str, int x, int y, int size, DWORD color, int speed, BOOL flg )
{
	int	wx = x;
	int	wy = y;
	int j = 0;

	static int cnt = 0;

	if( ++cnt%speed==0 ){
		cnt = 0;
		if( ++len>lstrlen(str) )len = lstrlen(str);
	}

	if( flg ){
		for( int i=0 ; i<len; i+=2 ){
			if( str[i]=='n' ){ j=0; i--; wx = x;	wy += size+size/4; continue; }
			if( j++==MOJIX ){ j = 0; wx = x;	wy += size+size/4; }
			DrawChar( *(LPWORD)(&str[i]), wx, wy, size, color );
			wx += size;
		}
	}else{
		for( int i=0 ; i<len; i+=2 ){
			if( str[i]=='n' ){ j=0; i--; wx += size+size/4;	wy = y; continue; }
			if( j++==MOJIY ){ j=0; wx += size;	wy = y; }
			DrawChar( *(LPWORD)(&str[i]), wx, wy, size, color );
			wy += size;
		}
	}

	return len==lstrlen(str)?TRUE:FALSE;
}
