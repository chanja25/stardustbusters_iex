#include	"iExtreme.h"

#pragma comment( lib, "dxguid.lib" )
#pragma comment( lib, "d3d9.lib" )
#pragma comment( lib, "d3dx9.lib" )

#pragma comment( lib, "dsound.lib" )
#pragma comment( lib, "winmm.lib" )
#pragma comment( lib, "dinput8.lib" )

//*****************************************************************************
//
//
//
//*****************************************************************************

LPDIRECT3D9				lpD3D		= NULL;
LPDIRECT3DDEVICE9		lpD3DDevice	= NULL;
D3DPRESENT_PARAMETERS	d3dpp;
D3DXVECTOR3				D3DXVECTOR3Eye;
D3DXVECTOR3				D3DXVECTOR3At;
D3DXVECTOR3				D3DXVECTOR3Up;
D3DXMATRIX				matView;
D3DXMATRIX				matProjection;

HWND	IEXhWnd;
DWORD	IEXMode;
float	ScreenAdjust;

BOOL	bVertexFog = FALSE;

static	BOOL	bZWrite, bFog, bLight;
static	u32		dwFlagW, ambient = 0xFFFFFFFF;
static	u32		dwBFlagW = 255;

//*****************************************************************************
//
//		I-Extremeシステム初期化	
//
//*****************************************************************************

//
//		DirectX 8.1チェック
//

BOOL	CheckDXVersion( void )
{
	HINSTANCE	hDPNHPASTDLL;
	
	hDPNHPASTDLL = LoadLibrary( "dpnhpast.dll" );
	if( hDPNHPASTDLL == NULL ){
		FreeLibrary( hDPNHPASTDLL );
		OutputDebugString( "Couldn't LoadLibrary dpnhpast.dll\r\n" );
		return FALSE;
	}
	return TRUE;
}

//
//		初期化
//

BOOL	IEX_Initialize( HWND hWnd, BOOL bFullScreen, DWORD ScreenSize, BOOL bZBuf )
{
	int		Width, Height;

	IEXhWnd = hWnd;
	IEXMode = ScreenSize;

	switch( ScreenSize ){
	case SCREEN640:		Width = 640;	Height = 480;	break;
	case SCREEN800:		Width = 800;	Height = 600;	break;
	case SCREEN1024:	Width = 1024;	Height = 768;	break;
	case SCREEN480:		Width = 480;	Height = 272;	break;

	default:
			Width = (ScreenSize>>16);
			Height = (ScreenSize&0xFFFF);
			break;

	}

	ScreenAdjust = Width / 640.0f;

	// D3Dオブジェクトの作成
	lpD3D = Direct3DCreate9( D3D_SDK_VERSION );

	// アダプタの現在のディスプレイ モードを取得する
	D3DDISPLAYMODE d3ddm;
	lpD3D->GetAdapterDisplayMode( D3DADAPTER_DEFAULT, &d3ddm );

	// D3Dデバイスの作成
	ZeroMemory( &d3dpp, sizeof(D3DPRESENT_PARAMETERS) );
	if( bFullScreen ){
		if( ScreenSize == SCREEN480 ){
			Width = 640;	Height = 480;
		}

		d3dpp.BackBufferHeight				= Height;
		d3dpp.BackBufferWidth				= Width;
		d3dpp.BackBufferFormat				= d3ddm.Format;
		d3dpp.BackBufferCount				= 1;
		d3dpp.hDeviceWindow					= hWnd;

		d3dpp.MultiSampleType				= D3DMULTISAMPLE_NONE;
		d3dpp.SwapEffect					= D3DSWAPEFFECT_DISCARD;
		d3dpp.Windowed						= !bFullScreen;
		
		d3dpp.EnableAutoDepthStencil		= bZBuf;
		d3dpp.AutoDepthStencilFormat		= D3DFMT_D16;

		d3dpp.Flags							= 0;
		d3dpp.FullScreen_RefreshRateInHz	= D3DPRESENT_RATE_DEFAULT;
		d3dpp.PresentationInterval			= D3DPRESENT_INTERVAL_ONE;

	} else {
		d3dpp.BackBufferHeight				= Height;
		d3dpp.BackBufferWidth				= Width;

		d3dpp.Flags							= D3DPRESENTFLAG_LOCKABLE_BACKBUFFER;
		d3dpp.Windowed = TRUE;
		d3dpp.hDeviceWindow					= hWnd;
		d3dpp.SwapEffect = D3DSWAPEFFECT_DISCARD;

		d3dpp.BackBufferCount				= 1;
		d3dpp.BackBufferFormat				= D3DFMT_UNKNOWN;
		d3dpp.EnableAutoDepthStencil		= bZBuf;
		d3dpp.AutoDepthStencilFormat		= D3DFMT_D16;
		d3dpp.PresentationInterval			= D3DPRESENT_INTERVAL_IMMEDIATE;
	}

	BOOL	bVertexShaderEnable = FALSE;
    D3DCAPS9 Caps;
	ZeroMemory( &Caps, sizeof( Caps ) );
	//	HALチェック
	if( SUCCEEDED( lpD3D->GetDeviceCaps( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, &Caps ) ) ){
		if( FAILED( lpD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_HARDWARE_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &d3dpp, &lpD3DDevice ) ) ){
			if( FAILED( lpD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_MIXED_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &d3dpp, &lpD3DDevice ) ) ){
				if( FAILED( lpD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_HAL, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &d3dpp, &lpD3DDevice ) ) ) return FALSE;
			}
		}
		if( Caps.VertexShaderVersion >= D3DVS_VERSION( 1, 0 ) ) bVertexShaderEnable = true;
	}
	else {
		ZeroMemory( &Caps, sizeof( Caps ) );
		lpD3D->GetDeviceCaps( D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, &Caps );
		if( FAILED( lpD3D->CreateDevice( D3DADAPTER_DEFAULT, D3DDEVTYPE_REF, hWnd, D3DCREATE_SOFTWARE_VERTEXPROCESSING|D3DCREATE_MULTITHREADED, &d3dpp, &lpD3DDevice ) ) ) return FALSE;
	}

	//	フォグチェック
	D3DCAPS9	d3dCaps;
	lpD3DDevice->GetDeviceCaps( &d3dCaps );
	bVertexFog = (d3dCaps.RasterCaps&D3DPRASTERCAPS_FOGTABLE) ? FALSE : TRUE;

	IEX_InitRenderState();
	IEX_InitText();
	IEX_InitTextureManager();
	return TRUE;
}

void	IEX_Release( void )
{
	IEX_ReleaseText();
	RELEASE( lpD3DDevice );
	RELEASE( lpD3D );
}

HWND	IEX_GetWindow( void )
{
	return IEXhWnd;
}

LPDIRECT3DDEVICE9	IEX_GetD3DDevice( void ){ return lpD3DDevice; }
D3DPRESENT_PARAMETERS*		IEX_GetPresentParam( void ){ return &d3dpp; }


//*****************************************************************************
//
//		各種設定
//
//*****************************************************************************

//
//
//

void	IEX_ResetLight( int index )
{
	if( index != -1 ) lpD3DDevice->LightEnable( index, FALSE );
	 else {
		int		i;
        for( i=0 ; i<16 ; i++ ) lpD3DDevice->LightEnable( i, FALSE );
	}
}

//
//		環境光設定
//

void	IEX_SetAmbient( DWORD Color )
{
	lpD3DDevice->SetRenderState( D3DRS_AMBIENT, Color );
	ambient = 0xFFFFFFFF;
}

//
//		平行光作成
//

void	IEX_SetDirLight( int index, D3DVECTOR* dir, float r, float g, float b )
{
	D3DLIGHT9 Light;

	ZeroMemory( &Light, sizeof(D3DLIGHT9) );
	Light.Type			= D3DLIGHT_DIRECTIONAL;
	Light.Diffuse.r		= r;
	Light.Diffuse.g		= g;
	Light.Diffuse.b		= b;
	Light.Diffuse.a		= 1.0f;
	Light.Specular.r	= r;
	Light.Specular.g	= g;
	Light.Specular.b	= b;
	Light.Specular.a	= 1.0f;
	Light.Ambient.r		= .0f;
	Light.Ambient.g		= .0f;
	Light.Ambient.b		= .0f;
	Light.Ambient.a		= 1.0f;
	Light.Direction.x	= dir->x;
	Light.Direction.y	= dir->y;
	Light.Direction.z	= dir->z;
	lpD3DDevice->SetLight( index, &Light );
	lpD3DDevice->LightEnable( index, TRUE );
}

//
//		点光源作成
//

void	IEX_SetPointLight( int index, D3DVECTOR* Pos, float r, float g, float b, float range )
{
	D3DLIGHT9 Light;

	ZeroMemory( &Light, sizeof(D3DLIGHT9) );
	Light.Type			= D3DLIGHT_POINT;
	Light.Diffuse.r		= r;
	Light.Diffuse.g		= g;
	Light.Diffuse.b		= b;
	Light.Diffuse.a		= 1.0f;
	Light.Specular.r	= r;
	Light.Specular.g	= g;
	Light.Specular.b	= b;
	Light.Ambient.r		= .0f;
	Light.Ambient.g		= .0f;
	Light.Ambient.b		= .0f;
	Light.Ambient.a		= .0f;
	Light.Direction.x	= .0f;
	Light.Direction.y	= .0f;
	Light.Direction.z	= .0f;
	Light.Position.x	= Pos->x;
	Light.Position.y	= Pos->y;
	Light.Position.z	= Pos->z;
	Light.Range         = range;
	Light.Attenuation0	= 1.0f;
	Light.Attenuation1	= 0.8f / range;
	Light.Attenuation2	= .0f;

	lpD3DDevice->SetLight( index, &Light );
	lpD3DDevice->LightEnable( index, TRUE );
}

//
//		フォグ設定
//

void	IEX_SetFog( DWORD Mode, float Param1, float Param2, DWORD Color )
{
	lpD3DDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);
	/*	フォグカラー設定	*/ 
	lpD3DDevice->SetRenderState(D3DRS_FOGCOLOR, Color);
	/*	フォグモード設定	*/     
	if( !bVertexFog ){
		lpD3DDevice->SetRenderState( D3DRS_FOGVERTEXMODE,  D3DFOG_NONE );
		lpD3DDevice->SetRenderState( D3DRS_FOGTABLEMODE,   Mode );
    } else {
		lpD3DDevice->SetRenderState( D3DRS_FOGVERTEXMODE,  Mode );
		lpD3DDevice->SetRenderState( D3DRS_FOGTABLEMODE,   D3DFOG_NONE );
    }

	/*	パラメータ設定		*/ 
	if(D3DFOG_LINEAR == Mode){
		lpD3DDevice->SetRenderState(D3DRS_FOGSTART, *(DWORD *)(&Param1));
		lpD3DDevice->SetRenderState(D3DRS_FOGEND,   *(DWORD *)(&Param2));
	} else {
		lpD3DDevice->SetRenderState(D3DRS_FOGDENSITY, *(DWORD *)(&Param1));
	}
	bFog = FALSE;
	dwBFlagW = 255;
}

//
//		投影設定
//

void	IEX_SetProjection( float FovY, float Near, float Far )
{
    D3DXMatrixPerspectiveFovLH(
		&matProjection,								// 作成された射影行列
		FovY,												// 視野角
		(float)d3dpp.BackBufferWidth / (float)d3dpp.BackBufferHeight,	// アスペクト比
		Near,												// 前方投影面
		Far													// 後方投影面
	);

    lpD3DDevice->SetTransform( D3DTS_PROJECTION, &matProjection );
}

void	IEX_SetProjectionEx( float FovY, float Near, float Far, float asp )
{
    D3DXMatrixPerspectiveFovLH( &matProjection, FovY, asp, Near, Far );
    lpD3DDevice->SetTransform( D3DTS_PROJECTION, &matProjection );
}

//
//		レンダーステート設定
//

void	IEX_InitRenderState( void )
{
	lpD3DDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
	lpD3DDevice->SetRenderState( D3DRS_FILLMODE, D3DFILL_SOLID );
	lpD3DDevice->SetRenderState( D3DRS_SHADEMODE, D3DSHADE_GOURAUD );
	lpD3DDevice->SetRenderState( D3DRS_ZENABLE, TRUE );
	lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
	lpD3DDevice->SetRenderState( D3DRS_ALPHATESTENABLE, TRUE );
	lpD3DDevice->SetRenderState( D3DRS_ALPHAFUNC, D3DCMP_NOTEQUAL );
 
	lpD3DDevice->SetRenderState( D3DRS_ALPHAREF, 0 );
	lpD3DDevice->SetTextureStageState( 0, D3DTSS_COLOROP,   D3DTOP_MODULATE );
	lpD3DDevice->SetTextureStageState( 0, D3DTSS_COLORARG1, D3DTA_CURRENT );
	lpD3DDevice->SetTextureStageState( 0, D3DTSS_COLORARG2, D3DTA_TEXTURE );
	lpD3DDevice->SetTextureStageState( 0, D3DTSS_ALPHAOP,   D3DTOP_MODULATE );
	lpD3DDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG1, D3DTA_CURRENT );
	lpD3DDevice->SetTextureStageState( 0, D3DTSS_ALPHAARG2, D3DTA_TEXTURE );

	lpD3DDevice->SetSamplerState( 0, D3DSAMP_MIPFILTER, D3DTEXF_NONE  );
	lpD3DDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );
	lpD3DDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );
	lpD3DDevice->SetSamplerState( 0, D3DSAMP_ADDRESSU,  D3DTADDRESS_WRAP );
	lpD3DDevice->SetSamplerState( 0, D3DSAMP_ADDRESSV,  D3DTADDRESS_WRAP );

	IEX_SetAmbient( 0x20202020 );
	lpD3DDevice->SetRenderState( D3DRS_LIGHTING, TRUE );

	IEX_SetProjection( D3DX_PI/4, 10.0f, 1500.0f );

	lpD3DDevice->SetRenderState( D3DRS_NORMALIZENORMALS, TRUE );
}

//
//		レンダーステート設定
//

//*****************************************************************************
//
//
//
//*****************************************************************************

D3DMATERIAL9	InvertMaterial = {
	{ 1.0f, 1.0f, 1.0f, 1.0f },
	{ 1.0f, 1.0f, 1.0f, 1.0f },
	{ .0f, .0f, .0f, .0f },
	{ .0f, .0f, .0f, .0f },
	.0f
};

BOOL	IEX_SetRenderState( DWORD state, D3DMATERIAL9* lpMaterial, LPDIRECT3DTEXTURE9 lpTexture )
{
	BOOL	ret = FALSE;
	
	if( dwBFlagW != state ){
		IEX_RestoreRenderState();

		switch( state ){
		case RM_NORMAL:
				/*	アルファ設定		*/ 
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret = TRUE;
				break;
		case RM_SUB:
				lpD3DDevice->GetRenderState( D3DRS_AMBIENT, &ambient );
				lpD3DDevice->SetRenderState( D3DRS_AMBIENT, 0xFFFFFFFF );
				/*	アルファ設定		*/ 
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ZERO );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCCOLOR );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				lpD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
				bZWrite = TRUE;
				ret = TRUE;
				break;
		case RM_NEGA:
				/*	アルファ設定		*/ 
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_INVDESTCOLOR );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ZERO );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret = TRUE;
				break;
		case RM_MUL:
				lpD3DDevice->GetRenderState( D3DRS_AMBIENT, &ambient );
				lpD3DDevice->SetRenderState( D3DRS_AMBIENT, 0xFFFFFFFF );
				/*	アルファ設定		*/ 
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ZERO     );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_SRCCOLOR );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret  = TRUE;
				break;
		case RM_MUL2:
				/*	アルファ設定		*/ 
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_INVDESTCOLOR );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret = TRUE;
				break;

		case RM_INVERT:	/*	反転		*/ 		
				lpD3DDevice->SetRenderState( D3DRS_LIGHTING, FALSE );
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_INVDESTCOLOR );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ZERO );
				lpD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				lpD3DDevice->SetRenderState( D3DRS_FOGENABLE, FALSE );
				bFog    = TRUE;
				ret     = TRUE;
				bZWrite = TRUE;
				bLight  = TRUE; 

				lpD3DDevice->SetMaterial( &InvertMaterial );
				/*	テクスチャ設定		*/ 
				lpD3DDevice->SetTexture( 0, NULL );
				lpLastTexture = NULL;
				break;

		case RM_ADD:	/*	加算合成		*/ 
				lpD3DDevice->GetRenderState( D3DRS_AMBIENT, &ambient );
				lpD3DDevice->SetRenderState( D3DRS_AMBIENT, 0xFFFFFFFF );

				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
				lpD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
				lpD3DDevice->SetRenderState( D3DRS_FOGENABLE, FALSE );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret  = TRUE;
				bZWrite = TRUE;
				bFog    = TRUE;
				break;

		case RM_ADD2:	/*	加算合成2		*/ 
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
				lpD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret  = TRUE;
				bZWrite = TRUE;
				break;

		case RS_COPY2:
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_SRCALPHA );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_INVSRCALPHA );
				lpD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, FALSE );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret  = TRUE;
				bZWrite = TRUE;
				break;
		case RM_NONE:
				/*	アルファ設定		*/ 
				lpD3DDevice->SetRenderState( D3DRS_SRCBLEND,  D3DBLEND_ZERO );
				lpD3DDevice->SetRenderState( D3DRS_DESTBLEND, D3DBLEND_ONE );
				lpD3DDevice->SetRenderState( D3DRS_ALPHABLENDENABLE, TRUE );
				ret  = TRUE;
				break;
		}
	}

	if( state!=RM_INVERT ){
		//	マテリアル設定
		if( lpMaterial ) lpD3DDevice->SetMaterial( lpMaterial );
		//	テクスチャ設定
		if( lpTexture != lpLastTexture ) lpD3DDevice->SetTexture( 0, lpTexture );
		lpLastTexture = lpTexture;
	}
	dwBFlagW = state;
	return ret;
}

//
//		レンダーステート復元
//

void	IEX_RestoreRenderState( void )
{
	if( bFog                  ) lpD3DDevice->SetRenderState( D3DRS_FOGENABLE, TRUE );
	if( bZWrite               ) lpD3DDevice->SetRenderState( D3DRS_ZWRITEENABLE, TRUE );
	if( ambient != 0xFFFFFFFF ) lpD3DDevice->SetRenderState( D3DRS_AMBIENT, ambient );
	if( bLight                ) lpD3DDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
	bFog = FALSE;
	bZWrite = FALSE;
	bLight  = FALSE;
	ambient = 0xFFFFFFFF;

	dwBFlagW = 255;
}

//
//		フィルター設定
//

void	IEX_UseFilter( BOOL bFilter )
{
	if( bFilter ){
		lpD3DDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_LINEAR );
		lpD3DDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_LINEAR );
	} else {
		lpD3DDevice->SetSamplerState( 0, D3DSAMP_MINFILTER, D3DTEXF_POINT );
		lpD3DDevice->SetSamplerState( 0, D3DSAMP_MAGFILTER, D3DTEXF_POINT );
	}
}

//*****************************************************************************
//
//		行列関連
//
//*****************************************************************************

// CosZCosY + SinZSinXSinY      SinZCosX     -CosZSinY + SinZSinXCosY    0 
// -SinZCosY + CosZSinXSinY     CosZCosX    SinZSinY + CosZSinXCosY   0 
// CosXSinY                     -SinX       CosXCosY                      0 
// lx                           ly          lz                            1 

void	SetTransformMatrixZXY( D3DXMATRIX *Mat, float x, float y, float z, float ax, float ay, float az )
{
	float	sx, sy, sz, cx, cy, cz;

	sx = sinf(ax);	sy = sinf(ay);	sz = sinf(az);
	cx = cosf(ax);	cy = cosf(ay);	cz = cosf(az);
	
	Mat->_11 = cz*cy + sz*sx*sy;
	Mat->_12 = sz*cx;
	Mat->_13 = -cz*sy + sz*sx*cy;
	Mat->_14 = 0;

	Mat->_21 = -sz*cy + cz*sx*sy;
	Mat->_22 = cz*cx;
	Mat->_23 = sz*sy + cz*sx*cy;
	Mat->_24 = 0;

	Mat->_31 = cx*sy;
	Mat->_32 = -sx;
	Mat->_33 = cx*cy;
	Mat->_34 = 0;

	Mat->_41 = x;
	Mat->_42 = y;
	Mat->_43 = z;
	Mat->_44 = 1;
}

void	SetTransformMatrixXYZ( D3DXMATRIX *Mat, float x, float y, float z, float ax, float ay, float az )
{
	float	sx, sy, sz, cx, cy, cz;

	sx = sinf(ax);	sy = sinf(ay);	sz = sinf(az);
	cx = cosf(ax);	cy = cosf(ay);	cz = cosf(az);

	Mat->_11 = (float)( cy*cz + sx*sy*sz );
	Mat->_12 = (float)( cx*sz );
	Mat->_13 = (float)(-sy*cz + sx*cy*sz );
	Mat->_14 = (float)(0.0f);

	Mat->_21 = (float)(-cy*sz + sx*sy*cz );
	Mat->_22 = (float)( cx*cz );
	Mat->_23 = (float)( sy*sz + sx*cy*cz );
	Mat->_24 = (float)(0.0f);

	Mat->_31 = (float)( cx*sy );
	Mat->_32 = (float)(-sx );
	Mat->_33 = (float)( cx*cy );
	Mat->_34 = (float)(0.0f);

	Mat->_41 = x;
	Mat->_42 = y;
	Mat->_43 = z;
	Mat->_44 = (float)(1.0f);
}

//*****************************************************************************
//
//		視界関連
//
//*****************************************************************************

VIEWPOINT	ViewPoint;

//
//		視点行列設定
//

void	IEX_SetViewMatrix( void )
{
	float		x, y, z;
	D3DXVECTOR3	up;
	D3DXVECTOR3	view;
    D3DXMATRIX	MatRot;

	/*	回転行列の作成		*/ 
	SetTransformMatrixZXY( &MatRot, .0f, .0f, .0f, ViewPoint.Angle.x, ViewPoint.Angle.y, ViewPoint.Angle.z );

	view.x = .0f;
	view.y = .0f;
	view.z = 1.0f;
	D3DXVec3TransformCoord( &view, &view, &MatRot );
	x = view.x + ViewPoint.Pos.x;
	y = view.y + ViewPoint.Pos.y;
	z = view.z + ViewPoint.Pos.z;

	up.x = .0f;
	up.y = 1.0f;
	up.z = .0f;
	D3DXVec3TransformCoord( &up, &up, &MatRot );
	D3DXMatrixLookAtLH( &matView, 
						&ViewPoint.Pos, 
						&D3DXVECTOR3( x, y, z ), 
						&up );

	lpD3DDevice->SetTransform( D3DTS_VIEW, &matView );
}

//
//		視点設定
//

void	IEX_SetView( float posx, float posy, float posz, float anglex, float angley, float anglez )
{
	ViewPoint.Pos.x = posx;
	ViewPoint.Pos.y = posy;
	ViewPoint.Pos.z = posz;
	ViewPoint.Angle.x = anglex;
	ViewPoint.Angle.y = angley;
	ViewPoint.Angle.z = anglez;
	IEX_SetViewMatrix();
}

//
//		視点設定２
//

void	IEX_SetViewTarget( D3DXVECTOR3 *pos, D3DXVECTOR3 *target )
{
	ViewPoint.Pos.x = pos->x;
	ViewPoint.Pos.y = pos->y;
	ViewPoint.Pos.z = pos->z;

	D3DXMatrixLookAtLH( &matView, pos, target, &D3DXVECTOR3( .0f, 1.0f, .0f ) );
	lpD3DDevice->SetTransform( D3DTS_VIEW, &matView );
}

//
//		視点位置設定
//

void	IEX_SetViewPos( float posx, float posy, float posz )
{
	ViewPoint.Pos.x = posx;
	ViewPoint.Pos.y = posy;
	ViewPoint.Pos.z = posz;
	IEX_SetViewMatrix();
}

//
//		視点角度設定
//

void	IEX_SetViewAngle( float anglex, float angley, float anglez )
{
	ViewPoint.Angle.x = anglex;
	ViewPoint.Angle.y = angley;
	ViewPoint.Angle.z = anglez;
	IEX_SetViewMatrix();
}

//*****************************************************************************
//
//		２Ｄフォント関連
//
//*****************************************************************************

LPD3DXFONT	g_pd3dFont  = NULL;
LPD3DXFONT	g_pd3dFontM = NULL;

//
//		２Ｄフォント初期化
//

void	IEX_InitText( void )
{
	HFONT		hF;
	D3DXFONT_DESC	lf;

	/*	フォント設定	*/ 
	hF = (HFONT)GetStockObject( SYSTEM_FONT );
	GetObject( hF,sizeof( LOGFONT ),(LPSTR)&lf );
	lf.Height = 16;
	lf.Width  = 0;
	lf.Italic = 0;
	lf.CharSet = SHIFTJIS_CHARSET;
	strcpy_s( lf.FaceName,"ＭＳ ゴシック" );

	D3DXCreateFontIndirect( lpD3DDevice, &lf, &g_pd3dFont );

	lf.Height = 12;
	D3DXCreateFontIndirect( lpD3DDevice, &lf, &g_pd3dFontM );
}

void	IEX_ReleaseText( void )
{
	g_pd3dFont->Release();
	g_pd3dFontM->Release();
}

//
//		テキスト描画
//

void	IEX_DrawText( LPSTR str, int x, int y, int width, int height, DWORD color, BOOL bMini )
{
	RECT	rect;

	rect.left   = x;
	rect.top    = y;
	rect.right  = x + width;
	rect.bottom = y + height;

	lpD3DDevice->SetRenderState(D3DRS_FOGENABLE, FALSE);

	if( !bMini ) g_pd3dFont->DrawText(  NULL, str, -1, &rect, DT_LEFT|DT_WORDBREAK, color ); 
	 else        g_pd3dFontM->DrawText( NULL, str, -1, &rect, DT_LEFT|DT_WORDBREAK, color );

	lpD3DDevice->SetRenderState(D3DRS_FOGENABLE, TRUE);
	bFog = FALSE;
	dwBFlagW = 255;
}

