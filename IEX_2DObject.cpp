#include	"iExtreme.h"

//*****************************************************************************
//
//		２Ｄオブジェクト関連
//
//*****************************************************************************

//
//		２Ｄオブジェクト読み込み
//

LPIEX2DOBJ	IEX_Load2DObject( LPSTR filename )
{
	LPIEX2DOBJ	lpObj;

	//	構造体メモリ確保 
	lpObj = new IEX2DOBJ;
	ZeroMemory( lpObj, sizeof(IEX2DOBJ) );

	//	テクスチャ読み込み
	lpObj->width  = lpObj->height = 0;
	lpObj->lpTexture = IEX_LoadTexture(filename);
	//	失敗？
	if( !lpObj->lpTexture ){
		delete lpObj;
		return NULL;
	}
	
	//	情報取得
	LPDIRECT3DSURFACE9	temp;
	D3DSURFACE_DESC		desc;
	lpObj->lpTexture->GetSurfaceLevel( 0, &temp );
	temp->GetDesc(&desc);	
	temp->Release();
	//	サイズ保存
	lpObj->width  = desc.Width;
	lpObj->height = desc.Height;
	return lpObj;
}

//
//		２Ｄオブジェクト作成
//

LPIEX2DOBJ	IEX_Create2DObject( int width, int height, u8 flag )
{
	LPIEX2DOBJ	lpObj;
	D3DFORMAT	fmt = D3DFMT_R8G8B8;
	u32			usage = 0;
	D3DPOOL		pool = D3DPOOL_MANAGED;

	switch( flag ){
	case IEX2D_RENDERTARGET:	//	レンダー可能
					usage = D3DUSAGE_RENDERTARGET;
					fmt = IEX_GetPresentParam()->BackBufferFormat;
					pool = D3DPOOL_DEFAULT;
					break;

	case IEX2D_USEALPHA:		//	透明度付
					fmt = D3DFMT_A8R8G8B8;
					break;
	}
	/*	構造体メモリ確保	*/ 
	lpObj = new IEX2DOBJ;

	lpObj->width  = width;
	lpObj->height = height;
	lpObj->dwFlags= 0;
	D3DXCreateTexture( IEX_GetD3DDevice(), lpObj->width, lpObj->height, 0, usage, fmt, pool, &lpObj->lpTexture );

	return lpObj;
}

//
//		２Ｄオブジェクト解放
//

void	IEX_Release2DObject( LPIEX2DOBJ lpObj )
{
	if( !lpObj ) return;
	if( lpObj->lpTexture ) IEX_ReleaseTexture(lpObj->lpTexture);
	delete lpObj;
}

//
//		２Ｄオブジェクトレンダリング
//


void	IEX_Render2DObject( int DstX, int DstY, int DstW, int DstH, LPIEX2DOBJ lpObj, int SrcX, int SrcY, int width, int height, u32 dwFlags, u32 color )
{
	if( !lpObj ) return;
	
	TLVERTEX	v[4];

	v[0].sx = v[2].sx = (FLOAT)DstX * ScreenAdjust;
	v[1].sx = v[3].sx = (FLOAT)(DstX+DstW) * ScreenAdjust;
	v[0].sy = v[1].sy = (FLOAT)DstY * ScreenAdjust;
	v[2].sy = v[3].sy = (FLOAT)(DstY+DstH) * ScreenAdjust;

	v[0].tu = v[2].tu = (FLOAT)(SrcX+1)/(FLOAT)lpObj->width;
	v[1].tu = v[3].tu = (FLOAT)(SrcX+width)/(FLOAT)lpObj->width;
	v[0].tv = v[1].tv = (FLOAT)(SrcY+1)/(FLOAT)lpObj->height;
	v[2].tv = v[3].tv = (FLOAT)(SrcY+height)/(FLOAT)lpObj->height;

	v[0].color = v[1].color = v[2].color = v[3].color = color;
	v[0].sz    = v[1].sz    = v[2].sz    = v[3].sz    = .0f;
	v[0].rhw   = v[1].rhw   = v[2].rhw   = v[3].rhw   = 1.0f;

	IEX_Render2DPolygon( v, 2, lpObj, dwFlags );
}

void	IEX_Render2DObject( int DstX, int DstY, int DstW, int DstH, float Rotation, int flg, LPIEX2DOBJ lpObj, int SrcX, int SrcY, int width, int height, u32 dwFlags, u32 color )
{
	if( !lpObj ) return;
	
	TLVERTEX	v[4];

	//v[0].sx = v[2].sx = (FLOAT)DstX * ScreenAdjust;
	//v[1].sx = v[3].sx = (FLOAT)(DstX+DstW) * ScreenAdjust;
	//v[0].sy = v[1].sy = (FLOAT)DstY * ScreenAdjust;
	//v[2].sy = v[3].sy = (FLOAT)(DstY+DstH) * ScreenAdjust;

	D3DXMATRIX mat;

	//////// 回転 ////////
	D3DXMatrixRotationZ( &mat, Rotation );

	float dx,dy;
	if( flg==0 ){
		dx = (float)DstW;
		dy = (float)DstH;
		float x = mat._11 + mat._21 + mat._31;
		float y = mat._12 + mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[0].sx = x * ScreenAdjust;
		v[0].sy = y * ScreenAdjust;

		x = dx*mat._11 + mat._21 + mat._31;
		y = dx*mat._12 + mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[1].sx = x * ScreenAdjust;
		v[1].sy = y * ScreenAdjust;

		x = mat._11 + dy*mat._21 + mat._31;
		y = mat._12 + dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[2].sx = x * ScreenAdjust;
		v[2].sy = y * ScreenAdjust;

		x = dx*mat._11 + dy*mat._21 + mat._31;
		y = dx*mat._12 + dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[3].sx = x * ScreenAdjust;
		v[3].sy = y * ScreenAdjust;
	}
	if( flg==1 ){
		dx = (float)DstW/2;
		dy = (float)DstH/2;

		float x = -dx*mat._11 + -dy*mat._21 + mat._31;
		float y = -dx*mat._12 + -dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[0].sx = x * ScreenAdjust;
		v[0].sy = y * ScreenAdjust;

		x = dx*mat._11 + -dy*mat._21 + mat._31;
		y = dx*mat._12 + -dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[1].sx = x * ScreenAdjust;
		v[1].sy = y * ScreenAdjust;

		x = -dx*mat._11 + dy*mat._21 + mat._31;
		y = -dx*mat._12 + dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[2].sx = x * ScreenAdjust;
		v[2].sy = y * ScreenAdjust;

		x = dx*mat._11 + dy*mat._21 + mat._31;
		y = dx*mat._12 + dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[3].sx = x * ScreenAdjust;
		v[3].sy = y * ScreenAdjust;
	}
	if( flg==2 ){
		dx = (float)DstW/2;
		dy = (float)DstH;

		float x = -dx*mat._11 + mat._21 + mat._31;
		float y = -dx*mat._12 + mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[0].sx = x * ScreenAdjust;
		v[0].sy = y * ScreenAdjust;

		x = dx*mat._11 + mat._21 + mat._31;
		y = dx*mat._12 + mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[1].sx = x * ScreenAdjust;
		v[1].sy = y * ScreenAdjust;

		x = -dx*mat._11 + dy*mat._21 + mat._31;
		y = -dx*mat._12 + dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[2].sx = x * ScreenAdjust;
		v[2].sy = y * ScreenAdjust;

		x = dx*mat._11 + dy*mat._21 + mat._31;
		y = dx*mat._12 + dy*mat._22 + mat._32;

		x += (float)DstX;
		y += (float)DstY;
		v[3].sx = x * ScreenAdjust;
		v[3].sy = y * ScreenAdjust;
	}


	v[0].tu = v[2].tu = (FLOAT)(SrcX+1)/(FLOAT)lpObj->width;
	v[1].tu = v[3].tu = (FLOAT)(SrcX+width)/(FLOAT)lpObj->width;
	v[0].tv = v[1].tv = (FLOAT)(SrcY+1)/(FLOAT)lpObj->height;
	v[2].tv = v[3].tv = (FLOAT)(SrcY+height)/(FLOAT)lpObj->height;

	v[0].color = v[1].color = v[2].color = v[3].color = color;
	v[0].sz    = v[1].sz    = v[2].sz    = v[3].sz    = .0f;
	v[0].rhw   = v[1].rhw   = v[2].rhw   = v[3].rhw   = 1.0f;

	IEX_Render2DPolygon( v, 2, lpObj, dwFlags );
}

void	IEX_Render2DObjectNA( int DstX, int DstY, int DstW, int DstH, LPIEX2DOBJ lpObj, int SrcX, int SrcY, int width, int height, u32 dwFlags, u32 color )
{
	if( !lpObj ) return;

	TLVERTEX	v[4];

	v[0].sx = v[2].sx = (FLOAT)DstX;
	v[1].sx = v[3].sx = (FLOAT)(DstX+DstW);
	v[0].sy = v[1].sy = (FLOAT)DstY;
	v[2].sy = v[3].sy = (FLOAT)(DstY+DstH);

	v[0].tu = v[2].tu = (FLOAT)(SrcX+1)/(FLOAT)lpObj->width;
	v[1].tu = v[3].tu = (FLOAT)(SrcX+width)/(FLOAT)lpObj->width;
	v[0].tv = v[1].tv = (FLOAT)(SrcY+1)/(FLOAT)lpObj->height;
	v[2].tv = v[3].tv = (FLOAT)(SrcY+height)/(FLOAT)lpObj->height;

	v[0].color = v[1].color = v[2].color = v[3].color = color;
	v[0].sz    = v[1].sz    = v[2].sz    = v[3].sz    = .0f;
	v[0].rhw   = v[1].rhw   = v[2].rhw   = v[3].rhw   = 1.0f;

	IEX_Render2DPolygon( v, 2, lpObj, dwFlags );
}







