#include "All.h"

//*****************************************************************************************************************************
//
//		タイトル関連
//
//*****************************************************************************************************************************

//初期化
void	TitleFrame_Class::Init()
{
	lpTitle[0] = IEX_Load2DObject( "DATA\\TITLE\\TITLE.png" );
	lpTitle[1] = IEX_Load2DObject( "DATA\\TITLE\\TITLE_LOGO.png" );
	lpTitle[2] = IEX_Load2DObject( "DATA\\TITLE\\ENTER.png" );
	lpTitle[3] = IEX_Load2DObject( "DATA\\TITLE\\start_exit.png" );
	lpTitle[4] = IEX_Load2DObject( "DATA\\TITLE\\Logo.png" );
	lpTitle[5] = IEX_Load2DObject( "DATA\\2D\\ScoreCnt.png" );

	cnt		= 0;
	mode	= 0;
	select	= 0;
	fg		= FALSE;

	//LPVOID v;
	//DWORD size;

	//D3DLOCKED_RECT lock;
	//lpTitle[0]->lpTexture->LockRect( 0, &lock, NULL, 0 );
	//v = lock.pBits;
	//size = lock.Pitch;
	//lpTitle[0]->lpTexture->UnlockRect( 0 );

	//lpTitle[1]->lpTexture->LockRect( 0, &lock, NULL, 0 );
	//lock.pBits = NULL;
	//lock.Pitch = 0;
	//lpTitle[1]->lpTexture->UnlockRect( 0 );
	//GameManage.GetFile()->Write( "a.txt", "abc", 3, 1, 0 );

	GetScript()->LoadScript( "TITLE" );
}

//解放
void	TitleFrame_Class::Release(){
	for(int i=0;i<5;i++){
		IEX_Release2DObject( lpTitle[i] );
	}
}

//処理
void	TitleFrame_Class::MainFrame()
{
	Script->Proc();

	Proc();

	//フェード処理
	if( !Fade.GetFlag() )Fade.Proc();
}

//処理
void	TitleFrame_Class::Proc()
{
	switch( mode ){
		case 3:
			if( (KEY(KEY_A) | KEY(KEY_ENTER)) == 3 )mode++;
			break;
		case 4:
			if( KEY(KEY_UP) == 3 ){
				select = (select==0)? 2: select-1;
			}
			if( KEY(KEY_DOWN) == 3 ){
				select = (select==2)? 0: select+1;
			}

			if( (KEY(KEY_A) | KEY(KEY_ENTER)) == 3 ){
				mode++;
				Fade.FadeOut( BLACK, 3 );
			}
			break;
		case 6:
			if( select==0 ){
				Sound::StopSound( 18 );
				GameManage.SetNextMode( MODE_MAIN );
			}else if( select==1 ){
				mode = 10;
			}else exit(NULL);
			break;
		case 11:
			if( (KEY(KEY_A) | KEY(KEY_ENTER)) == 3 ){
				mode++;
				Fade.FadeOut( BLACK, 3 );
			}
			break;
	}
}

//描画
void	TitleFrame_Class::DrawFrame()
{
	Draw();

	//フェード描画
	Fade.Draw();
}

//描画
void	TitleFrame_Class::Draw()
{
	cnt++;
	switch( mode ){
		case 0:
			Fade.FadeIn( BLACK, 3 );
			mode++;
			break;
		case 1:
			IEX_Render2DObject( 320-256, 240-128, 512, 256, lpTitle[4], 0, 0, 512, 256 );
			if( cnt>=120 ){
				mode++;
				cnt = 0;
				Fade.FadeOut( BLACK, 3 );
			}
			break;
		case 2:
			IEX_Render2DObject( 320-256, 240-128, 512, 256, lpTitle[4], 0, 0, 512, 256 );
			if( cnt>=120 ){
				mode++;
				cnt = 0;
				Fade.FadeIn( BLACK, 3 );
				if( !fg )Sound::PlaySound( 18 , TRUE );
				fg = TRUE;
			}
			break;
		case 3:
			IEX_Render2DObject( 0, 0, 640, 480, lpTitle[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 0, -100, 640, 480, lpTitle[1], 0, 0, 512, 512 );

			if( cnt< 30 ){
				IEX_Render2DObject( 320-128, 320, 256, 64, lpTitle[2], 0, 0, 256, 64 );
			}
			cnt = ( cnt >= 60 )? 0: cnt;
			break;
		case 4:
			cnt = 0;
			IEX_Render2DObject( 0, 0, 640, 480, lpTitle[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 0, -100, 640, 480, lpTitle[1], 0, 0, 512, 512 );

			IEX_Render2DObject( 320-128, 300, 256, 64, lpTitle[3], 0, 128*(select==0), 512, 128 );
			IEX_Render2DObject( 320-128, 350, 256, 64, lpTitle[3], 0, 512+128*(select==1), 512, 128 );
			IEX_Render2DObject( 320-128, 400, 256, 64, lpTitle[3], 0, 256+128*(select==2), 512, 128 );
			break;
		case 5:
			if( cnt==120 )mode++;
			IEX_Render2DObject( 0, 0, 640, 480, lpTitle[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 0, -100, 640, 480, lpTitle[1], 0, 0, 512, 512 );

			IEX_Render2DObject( 320-128, 300, 256, 64, lpTitle[3], 0, 128*(select==0), 512, 128 );
			IEX_Render2DObject( 320-128, 350, 256, 64, lpTitle[3], 0, 512+128*(select==1), 512, 128 );
			IEX_Render2DObject( 320-128, 400, 256, 64, lpTitle[3], 0, 256+128*(select==2), 512, 128 );
			break;
		case 10:
			Fade.FadeIn( BLACK, 3 );
			cnt = 0;
			mode++;
			break;
		case 11:
			IEX_Render2DObject( 0, 0, 640, 480, lpTitle[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-260, 10, 512, 128, lpTitle[3], 0, 768, 512, 128 );

			for( int i=0;i<5;i++ ){
				int score = GameManage.GetHiScore( i );
				DrawScore( score, 320-24*6, 150+i*64, 48, 48 );
			}
			cnt = 0;
			break;
		case 12:
			if( cnt>=120 )mode++;
			IEX_Render2DObject( 0, 0, 640, 480, lpTitle[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-260, 10, 512, 128, lpTitle[3], 0, 768, 512, 128 );

			for( int i=0;i<5;i++ ){
				int score = GameManage.GetHiScore( i );
				DrawScore( score, 320-24*6, 150+i*64, 48, 48 );
			}
			break;
		case 13:
			mode = 2;
			break;
	}
}

void	TitleFrame_Class::DrawScore( int score, int x, int y, int width, int height )
{
	int i, j, k;
	i = 1;
	j = 10;
	k = score;

	while(k != 0 || i==1 ){
		int a = k%j;
		IEX_Render2DObject( x+width*6-i*width, y, width, height, lpTitle[5], a%4*64, a/4*64, 64, 64 );
		i++;
		k /= 10;
	}
}