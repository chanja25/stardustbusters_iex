#include	"iExtreme.h"

#define Li2Double(x) ((double)((x).HighPart) * 4.294967296E9 + (double)((x).LowPart))

typedef LONG (WINAPI *PROCNTQSI)(UINT,PVOID,ULONG,PULONG);

static	PROCNTQSI	SystemInfoProc;
//static	HMODULE		hModuleDLL;
static	int			NumProcessors;

#define SystemBasicInformation       0
#define SystemPerformanceInformation 2
#define SystemTimeInformation        3

typedef struct
{
    DWORD   dwUnknown1;
    ULONG   uKeMaximumIncrement;
    ULONG   uPageSize;
    ULONG   uMmNumberOfPhysicalPages;
    ULONG   uMmLowestPhysicalPage;
    ULONG   uMmHighestPhysicalPage;
    ULONG   uAllocationGranularity;
    PVOID   pLowestUserAddress;
    PVOID   pMmHighestUserAddress;
    ULONG   uKeActiveProcessors;
    BYTE    bKeNumberProcessors;
    BYTE    bUnknown2;
    WORD    wUnknown3;
} SYSTEM_BASIC_INFORMATION;

typedef struct
{
    LARGE_INTEGER   liIdleTime;
    DWORD           dwSpare[76];
} SYSTEM_PERFORMANCE_INFORMATION;

typedef struct
{
    LARGE_INTEGER liKeBootTime;
    LARGE_INTEGER liKeSystemTime;
    LARGE_INTEGER liExpTimeZoneBias;
    ULONG         uCurrentTimeZoneId;
    DWORD         dwReserved;
} SYSTEM_TIME_INFORMATION;

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

static PROCNTQSI GetQSIProcAdrs()
{
	PROCNTQSI NtQuerySystemInformation;
	
    NtQuerySystemInformation = (PROCNTQSI)GetProcAddress( GetModuleHandle( "ntdll" ), "NtQuerySystemInformation" );

    if( !NtQuerySystemInformation ) return (PROCNTQSI)NULL;
	 else return (PROCNTQSI)NtQuerySystemInformation;
}


static int GetNumProcessors( void )
{
    SYSTEM_BASIC_INFORMATION       SysBaseInfo;
    LONG                           status;

    status = SystemInfoProc( SystemBasicInformation, &SysBaseInfo, sizeof(SysBaseInfo), NULL);

    if (status != NO_ERROR) return 0;
	return (int)SysBaseInfo.bKeNumberProcessors;
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

BOOL	IEX_WIN_InitCpuUsage( void )
{
	SystemInfoProc = (PROCNTQSI)GetQSIProcAdrs();
	if( !SystemInfoProc ) return FALSE;

	NumProcessors = GetNumProcessors();
	return TRUE;
}

void	IEX_WIN_ReleaseCpuUsage( void )
{

}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

static LARGE_INTEGER           liOldIdleTime   = {0,0};
static LARGE_INTEGER           liOldSystemTime = {0,0};

int		IEX_WIN_GetCpuUsage( void )
{
	SYSTEM_PERFORMANCE_INFORMATION SysPerfInfo;
	SYSTEM_TIME_INFORMATION        SysTimeInfo;
	double                         dbIdleTime;
	double                         dbSystemTime;
	LONG                           status;

	int cpuUsage = 0; // CPU �g�p�� [%]

	DWORD	ret;
	// get new system time
	status = SystemInfoProc(SystemTimeInformation,&SysTimeInfo,sizeof(SysTimeInfo),&ret);
	if( status != NO_ERROR ) return 0;
	
	// get new CPU's idle time
	status = SystemInfoProc(SystemPerformanceInformation,&SysPerfInfo,sizeof(SysPerfInfo),NULL);
	if( status != NO_ERROR ) return 0;
	
	// if it's a first call - skip it
	if( liOldIdleTime.QuadPart != 0 ){

		dbIdleTime = (double)(SysPerfInfo.liIdleTime.QuadPart - liOldIdleTime.QuadPart);

		dbSystemTime = (double)(SysTimeInfo.liKeSystemTime.QuadPart - liOldSystemTime.QuadPart);

		dbIdleTime = dbIdleTime / dbSystemTime;
		dbIdleTime = 100.0 - dbIdleTime * 100.0 / (double)NumProcessors + 0.5;

		cpuUsage = (int)dbIdleTime;
	}
	
	// store new CPU's idle and system time
	liOldIdleTime   = SysPerfInfo.liIdleTime;
	liOldSystemTime = SysTimeInfo.liKeSystemTime;

	return cpuUsage;
}
