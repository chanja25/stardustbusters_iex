#ifndef __DISPLAY_H__
#define __DISPLAY_H__


class Display_Class{
private:
	IEX2DOBJ HpGage[4];
	float Rotation;

	IEX2DOBJ WpGage[4];
	int wpType;

	IEX2DOBJ BHpGage[3];
	int	  Boss;
	float BHp;

	IEX2DOBJ MsgObj[2];
	int mflg;
	int y;
	int sp;

	IEX2DOBJ ScoreObj[2];
	int score;
public:
	Display_Class();

	int	 GetSp();
	void SetMsg();
	void CloseMsg();

	void SetMsgChara( int type );

	void BossReset();

	void ScoreProc();
	void MsgProc();
	void BHpProc();
	void HpProc();
	void WpProc();
	void Proc();

	void ScoreDraw();
	void MsgDraw();
	void BHpDraw();
	void HpDraw();
	void WpDraw();
	void Draw();
};

#endif