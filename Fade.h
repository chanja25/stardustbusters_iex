#ifndef __FADE_H__
#define __FADE_H__

enum{
	BLACK = 0,
	WHITE,
	RED,
	GREEN,
	BLUE,
};

class Fade_Class{
private:
	IEX2DOBJ Obj;
	int  mode;	//フェイドモード
	int  alpha;	//透過値
	int	 speed; //フェイドスピード
	int	 r,g,b; //フェイドカラー
	DWORD color;
	BOOL flag;	//フェイドフラグ	
public:
	Fade_Class(){
		mode = 0;
		alpha = 0;
		speed = 0;
		r = g = b = 0;
		flag = 0;
		color = 0;
	}
	~Fade_Class(){}

	void Set2DObj( LPIEX2DOBJ lpObj );

	void SetColor( int c );
	void SetFade( int m, int a, int c, int s );
	void FadeIn( int c, int s );
	void FadeIn( DWORD c, int s );
	void FadeIn( int s );
	void FadeOut( int c, int s );
	void FadeOut( DWORD c, int s );
	void FadeOut( int s );

	BOOL GetFlag(){ return flag; }
	D3DXCOLOR GetARGB(){ return D3DCOLOR_ARGB( alpha, r, g, b ); }

	void Proc();
	void Draw();
};

#endif