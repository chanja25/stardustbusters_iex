#ifndef __PLAYER_H__
#define __PLAYER_H__


//キャラクタークラス
class Character_Class{
protected:
	IEX3DOBJ	Obj;	//3Dモデル
	IEXMESH		bullet;	//銃弾

	D3DXVECTOR3 pos;	//位置
	D3DXVECTOR3 fvec;	//前ベクトル
	D3DXVECTOR3 rvec;	//右ベクトル
	D3DXVECTOR3 angle;	//向き
	D3DXVECTOR3 move;	//移動量
	D3DXVECTOR3 target;	//ターゲット位置
	float		tDist;	//ターゲットまでの距離

	int		maxhp;	//最大HP
	int		hp;		//現在のHP
	int		wpType;	//武器タイプ
	int		motion;	//モーション
	float	speed;	//移動スピード
	int		mode;	//モード
	int		no;		//ナンバー
	float	scale;	//スケール
	float	alpha;	//透過値

	float	hDist;
	int		timer;

	BOOL dflg;		//死亡フラグ
	int	 deadCnt;	//死亡カウンター

	int ChainSpeed;
	int ChainTimer;
	BOOL flg;

	Weapon_Class *weapon;
public:
	LPIEX3DOBJ	GetObj();
	D3DXMATRIX	GetBone( int no );
	D3DXVECTOR3	GetBonePos( int no );
	void		SetPos( D3DXVECTOR3 Pos );
	D3DXVECTOR3	GetPos();
	void		SetAngle( D3DXVECTOR3 Angle );
	D3DXVECTOR3	GetAngle();
	void		SetMove( D3DXVECTOR3 Move );
	D3DXVECTOR3	GetMove();
	int			GetMaxHp();
	void		SetHp( int Hp );
	int			GetHp();
	void		SetAlpha( float Alpha );
	float		GetAlpha();
	void		SetSpeed( float Speed );
	float		GetSpeed();
	void		SetMode( int Mode );
	int			GetMode();
	float		GetHitDist();
	void		SetNo( int No );
	int			GetNo();
	void		SetDead();
	BOOL		GetDead();
	int			GetChainSpeed();
	void		SetWeaponType( int wpType );
	int			GetWeaponType();

	Weapon_Class *GetWeapon();

	virtual D3DXVECTOR3	GetHitPos() = 0;
	virtual void DamageHp( int damage, D3DXVECTOR3 pos ) = 0;
	virtual Bullet_Class *SetBullet() = 0;
	virtual void UpData() = 0;
	virtual void Mode() = 0;

	virtual void Move() = 0;
	virtual void Init() = 0;
	virtual void Rele() = 0;
	virtual void Proc() = 0;
	virtual void Draw() = 0;
};

//プレイヤークラス
class Player_Class:public Character_Class{
private:
	D3DXVECTOR3 mpos;
	int remain;

	int cntX;
	int cntY;
	int cflg;
public:
	Player_Class();
	void		SetWeapon( int no );
	void		LostWeapon();

	int	 GetRemain(){ return remain; }
	int  GetCflg(){ return cflg; }

	Bullet_Class *SetBullet();
	D3DXVECTOR3	GetHitPos();
	void DamageHp( int damage, D3DXVECTOR3 pos );
	void Mode();
	void Action();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};

#endif