#ifndef __SOUND_H__
#define __SOUND_H__

//0�`
#define SE_START	 0
#define SE_COUNT	 50
#define SE_END		 SE_START + SE_COUNT
//51�`
#define PVOICE_START SE_END
#define PVOICE_COUNT 20
#define PVOICE_END	 PVOICE_START + PVOICE_COUNT
//71�`
#define EVOICE_START PVOICE_END
#define EVOICE_COUNT 20
#define EVOICE_END	 EVOICE_START + EVOICE_COUNT
#define SOUND_MAX	 EVOICE_END

enum{
	SE = 0,
	PLAYER_VOICE,
	ENEMY_VOICE,
};

//SOUND�N���X
class Sound{
private:
	static int SoundType[SOUND_MAX];
public:
	static void	Init();
	static void PlaySound( int no, BOOL loop = 0 );
	static void StopSound( int no );
	static void StopSound();
	static void	SetValue( int no, int value );
};

#endif