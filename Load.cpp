#include "All.h"


//*****************************************************************************************************************************
//
//		ロード関連
//
//*****************************************************************************************************************************

//ロード用スレッド
DWORD	LoadThread( void *arg )
{
	if( MainFrame )MainFrame->Release();

	int mode = GameManage.GetNextMode();

	if( mode==MODE_TITLE ){
		MainFrame = new TitleFrame_Class();
	}
	if( mode==MODE_MAIN ){
		MainFrameC = new MainFrame_Class();
		MainFrame = MainFrameC;
	}
	if( mode==MODE_CLEAR ){
		MainFrame = new ClearFrame_Class();
	}
	if( mode==MODE_END ){
		MainFrame = new EndFrame_Class();
	}
	MainFrame->Init();

	return 0;
}

//初期化
void	GameManage_Class::LoadInit()
{
	int s = rand()%4;
	switch( s ){
		case 0:
			lpLoad[0] = IEX_Load2DObject( "DATA\\LOAD\\Load1.png" );
			break;
		case 1:
			lpLoad[0] = IEX_Load2DObject( "DATA\\LOAD\\Load2.png" );
			break;
		case 2:
			lpLoad[0] = IEX_Load2DObject( "DATA\\LOAD\\Load3.png" );
			break;
		case 3:
			lpLoad[0] = IEX_Load2DObject( "DATA\\LOAD\\Load4.png" );
			break;
	}
	lpLoad[1] = IEX_Load2DObject( "DATA\\Load\\Load_anime.png" );
	lpLoad[2] = IEX_Load2DObject( "DATA\\Load\\NowLoading.png" );
	LoadCnt = 0;
	DWORD ThreadParam, ThreadId = 1;
	hThread = CreateThread( NULL, 0, (LPTHREAD_START_ROUTINE)LoadThread, &ThreadParam, 0, &ThreadId );
}

//解放
void	GameManage_Class::LoadRelease()
{
	for( int i=0;i<1;i++ ){
		IEX_Release2DObject( lpLoad[i] );
		lpLoad[i] = NULL;
	}
	GameMode = NextMode;
}

//処理
BOOL	GameManage_Class::LoadProc()
{
	if( NextMode!=GameMode )GameMode = MODE_LOAD;
	else return TRUE;
	LoadCnt++;
	DWORD ExitCode;

	GetExitCodeThread( hThread, &ExitCode );
	if( ExitCode!=STILL_ACTIVE ){
		//if( NextMode==MODE_MAIN && LoadCnt<=300 )return FALSE;
		CloseHandle( hThread );
		LoadCnt = 0;
		LoadRelease();
	}
	return FALSE;
}

//描画
BOOL	GameManage_Class::LoadDraw()
{
	if( NextMode==GameMode )return TRUE;

	if( NextMode==MODE_TITLE || NextMode==MODE_END || NextMode==MODE_CLEAR )return FALSE;

	IEX_Render2DObject( 0, 0, 640, 480, lpLoad[0], 0, 0, 512, 512 );
	IEX_Render2DObject( 640-284, 480-40, 48, 48, LoadCnt*RAD, 1, lpLoad[1], 0, 0, 128, 128 );

	static int a = 0;
	a = ( LoadCnt%10==0 )? (a==3)? 0: a+1: a;

	IEX_Render2DObject( 640-260, 480-64, 256-32+a*11, 48, lpLoad[2], 0, 0, 512-64+a*22, 64 );

	return FALSE;
}
