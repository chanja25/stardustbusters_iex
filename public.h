#ifndef __PUBLIC_H__
#define __PUBLIC_H__

//ラジアン度
#define RAD 0.01745329252f//D3DX_PI/180

//任意軸回転用マトリックス作成
LPD3DXMATRIX RotQuaternion( float x, float y, float z, float angle );
LPD3DXMATRIX RotQuaternion( D3DXVECTOR3 vec, float angle );

//マトリックスセット
void	SetInitMatrix( D3DXMATRIX *mat );
void	SetVecMatrix( D3DXMATRIX *mat, D3DXVECTOR3 *pos, int flg );
void	SetPosMatrix( D3DXMATRIX *mat, float x, float y, float z );
void	SetPosMatrix( D3DXMATRIX *mat, D3DXVECTOR3 pos );
void	SetScaleMatrix( D3DXMATRIX *mat, float scale );
void	SetScaleMatrix( D3DXMATRIX *mat, float x, float y, float z );
void	SetScaleMatrix( D3DXMATRIX *mat, D3DXVECTOR3 scale );
//マトリックスゲット
void	GetPosMatrix( D3DXMATRIX *mat, D3DXVECTOR3 *pos );
void	GetVecMatrix( D3DXMATRIX *mat, D3DXVECTOR3 *pos, int flg );

//マトリックスのXYZ情報の正規化
void	MatrixNormalize( D3DXMATRIX *mat );
//ベクトルとマトリックスの掛け算
void	VecXMatrix( D3DXVECTOR3 *vec, D3DXMATRIX *mat );

//外積・内積
float	Cross3D( D3DXVECTOR3 *out, D3DXVECTOR3 A,D3DXVECTOR3 B );
float	Dot3D( D3DXVECTOR3 A, D3DXVECTOR3 B );
float	Cross2D( float ax, float ay, float bx, float by, float angle );
float	Cross2D( D3DXVECTOR3 p1, D3DXVECTOR3 p2, D3DXVECTOR3 v2 );
float	Dot2D( float ax, float ay, float bx, float by, float angle );
float	Dot2D( D3DXVECTOR3 p1, D3DXVECTOR3 p2, D3DXVECTOR3 v2 );

//計算
float	GetDist3D( D3DXVECTOR3 p1, D3DXVECTOR3 p2 );

//UVアニメ
void	UVAnimetion( LPIEXMESH lpMesh, float tu, float tv );

#endif