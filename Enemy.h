#ifndef	__ENEMY_H__
#define __ENEMY_H__


class Enemy_Class;
class EnemyManage_Class{
private:
	int num;
	Enemy_Class *Enemy;
public:
	EnemyManage_Class();
	~EnemyManage_Class();
	int		GetNum();
	Enemy_Class *GetEnemy();
	Enemy_Class *GetEnemy( int no );
	void	AllClear();
	void	AllDeath();
	void	Clear( Enemy_Class **Enemy );
	void	Clear( int no );

	void	EnemyNext( Enemy_Class **Enemy );
	void	CreateEnemy( Enemy_Class *enemy );
	void	CreateEnemy( int kind, int type, D3DXVECTOR3 pos, D3DXVECTOR3 target );
	void	CreateBoss( int kind );

	void	Proc();
	void	Draw();
};

class Enemy_Class:public Character_Class{
protected:
	Enemy_Class *next;
	Enemy_Class *before;
	int type;
	int score;
private:
	BOOL sflg;
public:
	Enemy_Class();
	Enemy_Class( int kind, int type, D3DXVECTOR3 pos, D3DXVECTOR3 target );
	Bullet_Class *SetBullet();

	void SetType( int type );
	int	 GetType();
	void SetNext( Enemy_Class *enemy );
	Enemy_Class *GetNext();
	void SetBefore( Enemy_Class *enemy );
	Enemy_Class *GetBefore();
	int	 GetScore(){ return score; };

	BOOL HitPlayer();
	D3DXVECTOR3	GetHitPos();
	void DamageHp( int damage, D3DXVECTOR3 pos );
	void Mode();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};

class RAF:public Enemy_Class{
private:
	int con;
	int cnt;
public:
	RAF(){};
	Bullet_Class *SetBullet();
	int GetCon();
	void SetCon( int con );

	D3DXVECTOR3	GetHitPos();
	void DamageHp( int damage, D3DXVECTOR3 pos );

	void Mode();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};

class CON:public Enemy_Class{
private:
	float angleZ;
	RAF *p;
	int cno;
	int cnt;
public:
	CON(){};
	~CON(){};
	void SetEnemy();

	D3DXVECTOR3	GetHitPos();
	void DamageHp( int damage, D3DXVECTOR3 pos );

	void Mode();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};


class MS:public Enemy_Class{
private:
	D3DXVECTOR3 mpos;
	int cnt;
public:
	MS(){};
	Bullet_Class *SetBullet();
	Bullet_Class *SetLaser();

	D3DXVECTOR3	GetHitPos();
	D3DXVECTOR3	GetHitPos( int no );
	float GetHitDist( int no );
	void DamageHp( int damage, D3DXVECTOR3 pos );

	void Mode();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};


class HI_RING:public Enemy_Class{
private:
	int wp;
	int cnt;
public:
	HI_RING(){};
	//Bullet_Class *SetBullet();
	int GetWp();
	void SetWp( int wp );

	D3DXVECTOR3	GetHitPos();
	void DamageHp( int damage, D3DXVECTOR3 pos );

	void Mode();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};

class HI_WP:public Enemy_Class{
private:
	HI_RING *p;
	int wno;
	int cnt;
	float angleZ;
public:
	HI_WP(){};
	void SetEnemy();
	Bullet_Class *SetBullet();

	D3DXVECTOR3	GetHitPos();
	void DamageHp( int damage, D3DXVECTOR3 pos );

	void Mode();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};

class HI_CORE:public Enemy_Class{
private:
	HI_RING *p;
	int cnt;
public:
	HI_CORE(){};
	Bullet_Class *SetBullet();

	D3DXVECTOR3	GetHitPos();
	void DamageHp( int damage, D3DXVECTOR3 pos );

	void Mode();
	void Move();
	void UpData();

	void Init();
	void Rele();
	void Proc();
	void Draw();
};

#endif