#include "All.h"

//*****************************************************************************************************************************
//
//		タイトル関連
//
//*****************************************************************************************************************************

//初期化
void	EndFrame_Class::Init()
{
	lpEnd[0] = IEX_Load2DObject( "DATA\\End\\End.png" );
	lpEnd[1] = IEX_Load2DObject( "DATA\\End\\GameOver.png" );
	lpEnd[2] = NULL;

	cnt = 0;
	mode = 0;

	GetScript()->LoadScript( "NEND" );
}

//解放
void	EndFrame_Class::Release(){
	for(int i=0;i<3;i++){
		IEX_Release2DObject( lpEnd[i] );
	}
}

//処理
void	EndFrame_Class::MainFrame()
{
	Script->Proc();

	Proc();

	//フェード処理
	if( !Fade.GetFlag() )Fade.Proc();
}

//処理
void	EndFrame_Class::Proc()
{
	switch( mode ){
		case 1:
			if( (KEY(KEY_A) | KEY(KEY_ENTER)) == 3 ){
				Fade.FadeOut( BLACK, 3 );
				mode++;
			}
			break;
		case 3:
			GameManage.SetNextMode( MODE_TITLE );
			mode++;
			break;
	}
}

//描画
void	EndFrame_Class::DrawFrame()
{
	Draw();

	//フェード描画
	Fade.Draw();
}

//描画
void	EndFrame_Class::Draw()
{
	switch( mode ){
		case 0:
			Fade.FadeIn( BLACK, 3 );
			mode++;
			break;
		case 1:
			IEX_Render2DObject( 0, 0, 640, 480, lpEnd[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-256, 240-256, 512, 512, lpEnd[1], 0, 0, 512, 512 );
			break;
		case 2:
			IEX_Render2DObject( 0, 0, 640, 480, lpEnd[0], 0, 0, 512, 512 );
			IEX_Render2DObject( 320-256, 240-256, 512, 512, lpEnd[1], 0, 0, 512, 512 );
			if( ++cnt==120 )mode++;
			break;
	}
}
