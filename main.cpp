#include "All.h"


//*****************************************************************************************************************************
//
//		メイン初期化
//
//*****************************************************************************************************************************

BOOL	MAIN_Initialize( void )
{
	//ランダム用
	srand( timeGetTime() );
	//初期モード設定
	//GameManage.SetNextMode( MODE_CLEAR );
	GameManage.SetNextMode( MODE_TITLE );

	Sound::Init();

	return TRUE;
}


//*****************************************************************************************************************************
//
//		メインフレーム
//
//*****************************************************************************************************************************
BOOL	MAIN_Frame()
{
	if( GameManage.LoadProc() ){
		MainFrame->MainFrame();
	}
	return TRUE;
}

//*****************************************************************************************************************************
//
//		ドローフレーム
//
//*****************************************************************************************************************************
BOOL	MAIN_DrawFrame()
{
	if( GameManage.LoadDraw() ){
		MainFrame->DrawFrame();
	}
	return TRUE;
}

//メインフレームクラスの初期化
void	MainFrame_Class::Init()
{
	IEX_SetAmbient( 0x505050 );
	IEX_SetFog( D3DFOG_LINEAR, .0f, 500.0f, 0xBBBBFF );

	IEX_SetView( 0, 5, -20, 0, 0, 0 );

	//ステージのロード
	lpStage = IEX_LoadIMO( "DATA\\STAGE\\zimen.imo" );//IEX_LoadMeshFromX( "DATA\\STAGE\\zimen.x" );
	lpSky = IEX_LoadIMO( "DATA\\STAGE\\sky.imo" );//IEX_LoadMeshFromX( "DATA\\STAGE\\Sky.x" );
	lpKabe = IEX_LoadMeshFromX( "DATA\\STAGE\\Kabe.x" );

	//キャラクターのロード
	lpObj[0] = IEX_Load3DObject( "DATA\\OBJ\\PLAYER\\Player00.IEM" );
	lpObj[1] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\BOSS\\Boss3_wp.IEM" );
	lpObj[2] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\ZAKO\\zako_1.IEM" );
	lpObj[3] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\ZAKO\\zako_2.IEM" );
	lpObj[4] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\ZAKO\\zako_3.IEM" );
	lpObj[5] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\ZAKO\\bag.IEM" );
	lpObj[6] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\BOSS\\Boss1_con.IEM" );
	lpObj[7] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\BOSS\\Boss1.IEM" );
	lpObj[8] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\BOSS\\Boss2.IEM" );
	lpObj[9] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\BOSS\\Boss3_core.IEM" );
	lpObj[10] = IEX_Load3DObject( "DATA\\OBJ\\ENEMY\\BOSS\\Boss3_ring.IEM" );

	//武器のロード
	lpWeapon[0] = IEX_LoadMeshFromX( "DATA\\OBJ\\WEAPON\\gun.x" );
	lpWeapon[1] = IEX_LoadMeshFromX( "DATA\\OBJ\\WEAPON\\missile.x" );
	lpWeapon[2] = IEX_LoadMeshFromX( "DATA\\OBJ\\WEAPON\\laser.x" );
	lpWeapon[3] = IEX_LoadMeshFromX( "DATA\\OBJ\\WEAPON\\g.x" );
	lpWeapon[4] = IEX_LoadMeshFromX( "DATA\\OBJ\\WEAPON\\mis.x" );
	lpWeapon[5] = IEX_LoadMeshFromX( "DATA\\OBJ\\WEAPON\\la.x" );

	//2Dオブジェクトのロード
	lp2DObj[0] = IEX_Load2DObject( "DATA\\2D\\hp_frame.png" );
	lp2DObj[1] = IEX_Load2DObject( "DATA\\2D\\hp_back.png" );
	lp2DObj[2] = IEX_Load2DObject( "DATA\\2D\\hp_hari.png" );
	lp2DObj[3] = NULL;//IEX_Load2DObject( "DATA\\2D\\0123.png" );
	lp2DObj[4] = IEX_Load2DObject( "DATA\\2D\\c_main.png" );
	lp2DObj[5] = IEX_Load2DObject( "DATA\\2D\\c_meta.png" );
	lp2DObj[6] = IEX_Load2DObject( "DATA\\2D\\ok_or_no.png" );
	lp2DObj[7] = IEX_Load2DObject( "DATA\\2D\\No_aicon.png" );
	lp2DObj[8] = IEX_Load2DObject( "DATA\\2D\\ga_aicon.png" );
	lp2DObj[9] = IEX_Load2DObject( "DATA\\2D\\mi_aicon.png" );
	lp2DObj[10] = IEX_Load2DObject( "DATA\\2D\\le_aicon.png" );
	lp2DObj[11] = IEX_Load2DObject( "DATA\\2D\\special_unit.png" );
	lp2DObj[12] = IEX_Load2DObject( "DATA\\2D\\e_flame_.png" );
	lp2DObj[13] = IEX_Load2DObject( "DATA\\2D\\e_hp.png" );
	lp2DObj[14] = IEX_Load2DObject( "DATA\\2D\\boss_1_aicon.png" );
	lp2DObj[15] = IEX_Load2DObject( "DATA\\2D\\boss_2_aicon.png" );
	lp2DObj[16] = IEX_Load2DObject( "DATA\\2D\\boss_3_aicon.png" );
	lp2DObj[17] = IEX_Load2DObject( "DATA\\2D\\m_window.png" );
	lp2DObj[18] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１通常.png" );
	lp2DObj[19] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１笑い.png" );
	lp2DObj[20] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１照れ.png" );
	lp2DObj[21] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１イライラ.png" );
	lp2DObj[22] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１ムッ.png" );
	lp2DObj[23] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１焦り.png" );
	lp2DObj[24] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１ため息？.png" );
	lp2DObj[25] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１デフォ.png" );
	lp2DObj[26] = IEX_Load2DObject( "DATA\\2D\\Chara\\キャラ１デフォじと.png" );
	lp2DObj[27] = IEX_Load2DObject( "DATA\\2D\\Score.png" );
	lp2DObj[28] = IEX_Load2DObject( "DATA\\2D\\ScoreCnt.png" );
	lp2DObj[29] = IEX_Load2DObject( "DATA\\2D\\wait.png" );

	lp2DObj[30] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ１通常.png" );
	lp2DObj[31] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ１笑い.png" );
	lp2DObj[32] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ１さささ.png" );
	lp2DObj[33] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ１イライラ.png" );

	lp2DObj[34] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ３通常.png" );
	lp2DObj[35] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ３笑い.png" );
	lp2DObj[36] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ３むむむ.png" );
	lp2DObj[37] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ３べー.png" );
	lp2DObj[38] = IEX_Load2DObject( "DATA\\2D\\Chara\\chara\\キャラ３ふふふ.png" );

	IEX_InitParticle( "DATA\\particle.png" );

	mode = 1;

	score = 0;

	flg  = FALSE;

	Player = new Player_Class();
	Player->Init();

	Enemy = new EnemyManage_Class();

	Bullet = NULL;

	Camera.Init(3);

	Event = new Event_Class();

	Script->LoadScript( "Start" );

	Display = new Display_Class();

	Fade.Set2DObj( lp2DObj[29] );
	Fade.SetFade( 1, 255, BLACK, 3 );
}

//解放
void	MainFrame_Class::Release(){
	IEX_ReleaseMesh( lpStage );
	//IEX_ReleaseMesh( lpArea );
	IEX_ReleaseMesh( lpSky );
	IEX_ReleaseMesh( lpKabe );
	for( int i=0;i<11;i++ ){
		IEX_Release3DObject(lpObj[i]);
	}
	for( int i=0;i<6;i++ ){
		IEX_ReleaseMesh(lpWeapon[i]);
	}
	for( int i=0;i<39;i++ ){
		IEX_Release2DObject(lp2DObj[i]);
	}
	Bullet_Class *bullet,*bu;
	bullet = Bullet;
	if( bullet ){
		while( bullet->GetNext() ){
			bu = bullet;
			bullet->NextBullet( &bullet );
			delete bu;
		}
		delete bullet;
	}
	delete Display;
	delete Event;
	delete Enemy;
	delete Player;

	GameManage.SetScore( score );
}

//メインフレームの主処理
void	MainFrame_Class::MainFrame()
{
	Script->Proc();

	Proc();

	Event->Proc();

	//フェード処理
	if( !Fade.GetFlag() )Fade.Proc();
}

//メインフレームの主処理
void	MainFrame_Class::Proc()
{
	if( (KEY(KEY_L) & KEY(KEY_R))==3 ){
		Script->LoadScript( "debug" );
	}

	if( KEY(KEY_ENTER) ){
		//Enemy->AllDeath();
	}

	Player->Proc();

	Enemy->Proc();

	Bullet_Class *bu;
	bu = Bullet;
	while( bu ){
		if( bu->GetPos().z-Player->GetPos().z > 500.0f || bu->GetPos().z-Player->GetPos().z<-50 ){
			bu->Clear( &bu );
		}
		if( bu ){
			if( bu->HitBullet() ){
				bu->Clear( &bu );
				continue;
			}
			if( bu ){
				bu->Proc();
				bu->NextBullet( &bu );
			}
		}
	}

	Display->Proc();

	static BOOL flg = FALSE;
	if( KEY( KEY_D )==3 ){
		flg = !flg;
		if( flg )Camera.Init(2);
		else Camera.Init(3);
	}
	Camera.Proc();

	IEX_ExecuteParticles();
}

//メインフレームの描画
void	MainFrame_Class::DrawFrame()
{
	Draw();

	Event->Draw();

	//フェード描画
	Fade.Draw();
}

//メインフレームの描画
void	MainFrame_Class::Draw()
{
	if( !flg ){
		float uy = -50;
		float ty = 100;
		static float z = 0;
		z -= Player->GetSpeed();
		if( z<=-2000 ){
			z += 2000;
		}
		IEX_SetMeshPos( lpStage, 0, uy, Player->GetPos().z+z );
		IEX_RenderMesh( lpStage, RS_COPY, 1.0f );
		IEX_SetMeshPos( lpStage, 0, uy, 1000+Player->GetPos().z+z );
		IEX_SetMeshAngle( lpStage, 0, D3DX_PI, 0 );
		IEX_RenderMesh( lpStage, RS_COPY, 1.0f );
		IEX_SetMeshAngle( lpStage, 0, 0, 0 );
		IEX_SetMeshPos( lpStage, 0, uy, 2000+Player->GetPos().z+z );
		IEX_RenderMesh( lpStage, RS_COPY, 1.0f );

		IEX_SetMeshPos( lpSky, 0, ty, Player->GetPos().z+z );
		IEX_RenderMesh( lpSky, RS_COPY, 1.0f );
		IEX_SetMeshAngle( lpSky, 0, D3DX_PI, 0 );
		IEX_SetMeshPos( lpSky, 0, ty, 1000+Player->GetPos().z+z );
		IEX_RenderMesh( lpSky, RS_COPY, 1.0f );
		IEX_SetMeshAngle( lpSky, 0, 0, 0 );
		IEX_SetMeshPos( lpSky, 0, ty, 2000+Player->GetPos().z+z );
		IEX_RenderMesh( lpSky, RS_COPY, 1.0f );

		IEX_SetMeshAngle( lpKabe, 0, 0, 0 );
		IEX_SetMeshPos( lpKabe, 0, 0, Player->GetPos().z + 450.0f );
		IEX_RenderMesh( lpKabe, RS_COPY, 1.0f );

		IEX_SetMeshAngle( lpKabe, RAD*180, 0, 0 );
		IEX_SetMeshPos( lpKabe, 0, 0, Player->GetPos().z - 450.0f );
		IEX_RenderMesh( lpKabe, RS_COPY, 1.0f );
	}else{
		EffectStar( Player->GetPos().z );
	}

	Camera.View();

	Player->Draw();

	Enemy->Draw();

	Bullet_Class *bu;
	bu = Bullet;
	while( bu ){
		bu->Draw();
		bu->NextBullet( &bu );
	}

	IEX_DrawParticles();
	Display->Draw();

}
