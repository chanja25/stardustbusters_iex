#include "All.h"


//*****************************************************************************************************************************
//
//		プレイヤー関連
//
//*****************************************************************************************************************************

//コンストラクタ
Player_Class::Player_Class()
{
	remain	= REMAIN;
	pos		= D3DXVECTOR3(.0f,(MAXTOP+MAXUNDER)/2,.0f);	//位置
}

//初期化
void	Player_Class::Init()
{
	MainFrameC->SetChara( &Obj, 0 );
	MainFrameC->SetWeapon( &bullet, 4 );

	angle	= D3DXVECTOR3(.0f,.0f,.0f);		//アングル
	move	= D3DXVECTOR3(.0f,.0f,.0f);		//移動量
	target	= D3DXVECTOR3(.0f,.0f,.0f);		//ターゲット

	tDist = .0f;	//ターゲットまでの距離

	maxhp	= PHP;	//最大HP
	hp		= maxhp;//現在のHP
	wpType	= -1;	//武器タイプ
	motion	= 0;	//モーション
	speed	= PSP;	//移動スピード
	mode	= 0;	//モード
	no		= 0;	//ナンバー
	scale	= .02f;	//スケール
	alpha	= 0.6f;	//透過値

	mpos = D3DXVECTOR3( 0, 0, -50 );

	hDist	= PHSIZE;

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = -1;

	cntX = 0;
	cntY = 0;

	cflg = -1;

	weapon = NULL;

	ChainSpeed = GCSP1;
	ChainTimer = 0;

	flg = TRUE;

	//マトリックスの初期化
	SetInitMatrix( &Obj.TransMatrix );
	//Objのx軸取得
	GetVecMatrix( &Obj.TransMatrix, &rvec, 0 );
	//Objのz軸取得
	GetVecMatrix( &Obj.TransMatrix, &fvec, 2 );
}

void	Player_Class::Rele()
{
	if( weapon ){
		weapon->Release();
		delete weapon;
	}
}

D3DXVECTOR3 Player_Class::GetHitPos()
{
	D3DXVECTOR3 pos( this->pos );
	pos.y += 0.5f;
	return pos;
}

void	Player_Class::DamageHp( int damage, D3DXVECTOR3 pos )
{
	if( deadCnt ) return;
	if( cflg>=0 ){
		hp -= damage;
		if( hp< 0 )hp = 0;
		MainFrameC->GetCamera()->SetVib( 5, 10 );
		EffectFire( pos.x, pos.y, pos.z, move );
	}
}

//モード
void	Player_Class::Mode()
{
}

//モード
void	Player_Class::Action()
{
}

#define SP	0.3f
#define RSP 2.0f
#define BoostTime 120

//行動
void	Player_Class::Move()
{
	//	コントローラーの軸取得
	//最大1.0まで
	float	AxisX = .0f;
	float	AxisY = .0f;
	if( !MainFrameC->GetEvent()->GetEventFlag() ){
		AxisX = KEY_GetAxisX()*0.001f;
		AxisY = -KEY_GetAxisY()*0.001f;
	}

	//武器装着
	if( cflg!=5 && cflg>=0 && !MainFrameC->GetEvent()->GetEventFlag() ){
		if( !weapon ){
			//武器無装備時
			if( KEY(WP1)==3 ){
				wpType = 0;
			}
			if( KEY(WP2)==3 ){
				wpType = 1;
			}
			if( KEY(WP3)==3 ){
				wpType = 2;
			}
			if( wpType>=0 )SetWeapon( wpType );
		}else if( !weapon->GetMoveFlag() ){
			//武器装備時
			int NextWp = wpType;
			if( KEY(PURGE1) && KEY(PURGE2) ){
				//NextWp = -1;
			}
			if( KEY(WP1)==3 ){
				//NextWp = 0;
				if( wpType==NextWp )NextWp = -1;
			}
			if( KEY(WP2)==3 ){
				//NextWp = 1;
				if( wpType==NextWp )NextWp = -1;
			}
			if( KEY(WP3)==3 ){
				//NextWp = 2;
				if( wpType==NextWp )NextWp = -1;
			}
			if( NextWp!=wpType ){
				wpType = NextWp;
				weapon->SetLost();
			}
		}else{
			GameManage.SetFlg();
			//武器の換装中
			AxisX = .0f;
			AxisY = .0f;
		}
		if( KEY(SHOOT) && !weapon ){
			MainFrameC->SetBullet( this->SetBullet() ); 
		}
	}
	if( weapon ){
		if( weapon->GetLost()==2 ){
			LostWeapon();
		}
	}

	switch( MainFrameC->GetMode() )
	{
	case 0:
		//	待機状態
		if( fabs(AxisY)>.3f ){
			//z軸のベクトルをx軸にそって最大1度ずつ傾けていく
			VecXMatrix( &fvec, RotQuaternion( rvec, AxisY*RAD ) );
			//ベクトルの正規化(z軸)
			D3DXVec3Normalize( &fvec, &fvec );
		}
		if( fabs(AxisX)>.3f ){
			//x軸のベクトルをz軸にそって最大1度ずつ傾けていく
			VecXMatrix( &rvec, RotQuaternion( fvec, -AxisX*RAD ) );
			//ベクトルの正規化(x軸)
			D3DXVec3Normalize( &rvec, &rvec );
		}
		//z軸方向にspeedの量だけ移動させていく
		GetVecMatrix( &Obj.TransMatrix, &move, 2 );
		D3DXVec3Normalize( &move, &move );
		move *= speed;
		pos += move;
		break;
	case 1:
		if( cflg<5 && cflg>=0 ){
			if( fabs(AxisX)>.3f ){
				//傾き最大+-30度までに指定
				if( (angle.z<30.0f && AxisX<.0f) || (angle.z>-30.0f && AxisX>.0f) ){
					//x軸のベクトルをz軸にそって最大1度ずつ傾けていく
					D3DXVECTOR3 vec = D3DXVECTOR3( .0f, .0f, 1.0f );
					VecXMatrix( &rvec, RotQuaternion( vec, -AxisX*RAD ) );
					//ベクトルの正規化(x軸)
					D3DXVec3Normalize( &rvec, &rvec );
					angle.z += -AxisX;
				}
			}else if(fabs(angle.z)>0.5f){
				float s;
				if(angle.z<0)s = 1.0f;
				else s = -1.0f;
				//x軸のベクトルをz軸にそって最大1度ずつ傾けていく
				D3DXVECTOR3 vec = D3DXVECTOR3( .0f, .0f, 1.0f );
				VecXMatrix( &rvec, RotQuaternion( vec, s*RAD ) );
				//ベクトルの正規化(x軸)
				D3DXVec3Normalize( &rvec, &rvec );
				angle.z += s;
			}
			move.x = AxisX*SP;
			move.y = AxisY*SP;
			move.z = speed;
			if( timer>=0 ){
				float sp;
				if( timer<18 )sp = timer/12.0f+speed;
				else if( timer>BoostTime-18 )sp = (BoostTime-timer)/12.0f+speed;
				else sp = 3.0f;
				move.z *= sp;
				timer++;
			}

			if( timer>BoostTime )timer = -1;
			if( AxisY>0 && cntY>0 && timer==-1 ){
				cntX = 0;
				cntY = 0;
				timer = 0;
			}
			if( AxisY<0 && cntY<0 && fabs(angle.z)<0.5f ){
				cntX = 0;
				cntY = 0;
				cflg = 5;
			}
			if( AxisX<0 && cntX>0 ){
				cntX = 0;
				cntY = 0;
				cflg = 6;
			}
			if( AxisX>0 && cntX<0 ){
				cntX = 0;
				cntY = 0;
				cflg = 7;
			}

			if(cflg==1)cntY++;
			if(cflg==2)cntY--;
			if(cflg==3)cntX++;
			if(cflg==4)cntX--;

			if( !MainFrameC->GetEvent()->GetEventFlag() ){
				//if( KEY(UP)==2 )cflg = 1;
				if( KEY(DOWN)==2 )cflg = 2;
				if( KEY(LEFT)==2 )cflg = 3;
				if( KEY(RIGHT)==2 )cflg = 4;
			}

			if(fabs(cntY)>5){
				cflg = 0;
				cntY = 0;
			}
			if(fabs(cntX)>5){
				cflg = 0;
				cntX = 0;
			}
		}else if( cflg==5 ){
			//z軸のベクトルをx軸にそって傾けていく
			D3DXVECTOR3 vec = D3DXVECTOR3( 1.0f, .0f, .0f );
			VecXMatrix( &fvec, RotQuaternion( vec, -RSP*RAD ) );
			//ベクトルの正規化(x軸)
			D3DXVec3Normalize( &fvec, &fvec );

			move.x = .0f;
			move.y = sinf( cntY*RSP*RAD )*speed;
			move.z = cosf( cntY*RSP*RAD )*speed + speed;
			if( timer>=0 ){
				float sp;
				if( timer<18 )sp = timer/12.0f+speed;
				else if( timer>BoostTime-18 )sp = (BoostTime-timer)/12.0f+speed;
				else sp = 3.0f;
				move.y *= sp;
				move.z *= sp;
			}

			cntY++;
			if( cntY>=360/RSP ){
				cflg = 0;
				cntY = 0;
			}
		}else if( cflg==6 ){
			//x軸のベクトルをz軸にそって傾けていく
			D3DXVECTOR3 vec = D3DXVECTOR3( .0f, .0f, 1.0f );
			VecXMatrix( &rvec, RotQuaternion( vec, 34.2f*RAD ) );
			//ベクトルの正規化(x軸)
			D3DXVec3Normalize( &rvec, &rvec );

			move.x = -SP*4.0f;
			move.y = .0f;
			move.z = speed;
			if( timer>=0 ){
				float sp;
				if( timer<18 )sp = timer/12.0f+speed;
				else if( timer>BoostTime-18 )sp = (BoostTime-timer)/12.0f+speed;
				else sp = 3.0f;
				move.z *= sp;
				timer++;
			}

			cntX++;
			if( cntX>=10 ){
				cflg = 0;
				cntX = 0;
			}
		}else if( cflg==7 ){
			//x軸のベクトルをz軸にそって傾けていく
			D3DXVECTOR3 vec = D3DXVECTOR3( .0f, .0f, 1.0f );
			VecXMatrix( &rvec, RotQuaternion( vec, -34.2f*RAD ) );
			//ベクトルの正規化(x軸)
			D3DXVec3Normalize( &rvec, &rvec );

			move.x = SP*4.0f;
			move.y = .0f;
			move.z = speed;
			if( timer>=0 ){
				float sp;
				if( timer<18 )sp = timer/12.0f+speed;
				else if( timer>BoostTime-18 )sp = (BoostTime-timer)/12.0f+speed;
				else sp = 3.0f;
				move.z *= sp;
				timer++;
			}

			cntX++;
			if( cntX>=10 ){
				cflg = 0;
				cntX = 0;
			}
		}
		break;
	}
	if( cflg==-1 ){
		if( pos.x==0 && pos.y==(MAXTOP+MAXUNDER)/2 ){
			move.z = speed;
			mpos.z += 0.5f;
			if( mpos.z >= 0 ){
				mpos.z = 0;
				cflg = 0;
				deadCnt = 120;
			}
		}else{
			pos.x = ( pos.x>0 )? pos.x-0.1f: pos.x+0.1f;
			pos.x = ( fabs(pos.x)<=0.1f )? 0: pos.x;

			pos.y = ( pos.y>(MAXTOP+MAXUNDER)/2 )? pos.y-0.1f: pos.y+0.1f;
			pos.y = ( fabs(pos.y-(MAXTOP+MAXUNDER)/2)<=0.1f )? (MAXTOP+MAXUNDER)/2: pos.y;
		}
	}else if( cflg==-2 ){
		move.z = speed;
		mpos.y -= 0.1f;
	}else{
		deadCnt = ( deadCnt )? deadCnt-1: 0;
		alpha = ( alpha>1.0f )? 1.0f: ( !deadCnt )? alpha+0.05f: alpha ;
	}

	//プレイヤーの座標移動
	pos += move;
	if( cflg!=5 ){
		if( pos.x < MAXLEFT ){
			pos.x = MAXLEFT;
			move.x = .0f;
		}
		if( pos.x >  MAXRIGHT ){
			pos.x =  MAXRIGHT;
			move.x = .0f;
		}
		if( pos.y <  MAXUNDER ){
			pos.y =  MAXUNDER;
			move.y = .0f;
		}
		if( pos.y >  MAXTOP ){
			pos.y =  MAXTOP;
			move.y = .0f;
		}
	}
}

//更新
void	Player_Class::UpData()
{
	D3DXVECTOR3 p = pos + mpos;
	//位置をマトリックスにセット
	SetPosMatrix( &Obj.TransMatrix, p );
	//y軸のベクトル取得
	D3DXVECTOR3 tvec;

	//外積からz,x軸に垂直な軸取得
	Cross3D( &tvec, rvec, fvec );
	//正規化
	D3DXVec3Normalize( &tvec, &tvec );

	//マトリックスにx,y,z軸の情報をセット
	SetVecMatrix( &Obj.TransMatrix, &rvec, 0 );
	SetVecMatrix( &Obj.TransMatrix, &tvec, 1 );
	SetVecMatrix( &Obj.TransMatrix, &fvec, 2 );
	//マトリックスにスケール情報をセット
	SetScaleMatrix( &Obj.TransMatrix, scale );
}

//処理
void	Player_Class::Proc()
{

	Mode();
	
	Move();
	Action();

	UpData();

	if( weapon )weapon->Proc();


	if( !flg )ChainTimer++;
	if( ChainTimer>=ChainSpeed ){
		flg = TRUE;
	}

	if( hp<=0 ){
		dflg = TRUE;
		cflg = -2;
	}

	float y = pos.y + mpos.y;
	if( y <= MAXUNDER - 3.0f ){
		if( remain>0 ){
			remain--;
			Init();
		}else{
			static BOOL flg = TRUE;
			if( flg )MainFrameC->GetScript()->LoadScript( "END" );
			flg = FALSE;
		}
	}
}

//描画
void	Player_Class::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );

	if( weapon )weapon->Draw();
	D3DXVECTOR3 pos,vec;
	pos = this->pos + mpos;
	GetVecMatrix( &Obj.TransMatrix, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	pos -= vec*4.8f;
	EffectBoost( pos.x, pos.y, pos.z, move, vec );

	if( dflg ){
		D3DXVECTOR3 p = pos;
		for( int i=0; i<5;i++ ){
			int axis = rand()%360;
			float dist = rand()%5*0.5f;
			p.x += sinf( RAD*axis )*dist;
			p.z += cosf( RAD*axis )*dist;
			EffectFire( p.x, p.y, p.z, D3DXVECTOR3( 0, 0, 0.5f ) );
		}
	}
}
