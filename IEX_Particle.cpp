#include	"iExtreme.h"

//*****************************************************************************
//
//		グローバル定義		
//
//*****************************************************************************

typedef struct tagPARTICLEDATA	{
	u8		bFlags;
	int		type;			//	形

	int		aFrame;			//	出現フレーム
	ARGB	aColor;			//	出現カラー

	int		eFrame;			//	消滅フレーム
	ARGB	eColor;			//	出現カラー

	int		mFrame;			//	最高フレーム
	ARGB	mColor;			//	最高カラー

	D3DXVECTOR3	Pos;
	D3DXVECTOR3	Move;
	D3DXVECTOR3	Power;
	float		rotate;
	float		stretch;

	int		CurFrame;

	float	r, g, b;
	float	CurAlpha;
	float	angle;
	float	scale;

	u8	flag;
	
} PARTICLEDATA, *LPPARTICLEDATA;

#define	PARTICLE_MAX	4096
PARTICLEDATA	ParticleData[PARTICLE_MAX];

LPIEX2DOBJ	lpParticleObj = NULL;

static	LVERTEX			ParticleVL[4];

//*****************************************************************************
//
//		初期化
//
//*****************************************************************************

void	IEX_InitParticle( LPSTR filename )
{
	int		i;

	for( i=0 ; i<PARTICLE_MAX ; i++ ){
		ParticleData[i].bFlags = 0;
	}
	lpParticleObj = IEX_Load2DObject( filename );
}

void	IEX_ReleaseParticle( void )
{
	if( !lpParticleObj ) return;

	IEX_Release2DObject( lpParticleObj );
	lpParticleObj = NULL;
}

//*****************************************************************************
//
//		
//
//*****************************************************************************

void	IEX_SetParticle( int type, int aFrame, ARGB aColor, int eFrame, ARGB eColor, int mFrame, ARGB mColor, 
						D3DVECTOR* Pos, D3DVECTOR* Move, D3DVECTOR* Power, float rotate, float stretch, float scale, u8 flag ){

	int		i;

	/*	空き検索		*/ 
	for( i=0 ; i<PARTICLE_MAX ; i++ ){
		if( ParticleData[i].bFlags != 0 ) continue;

		ParticleData[i].type = type;

		ParticleData[i].aFrame = aFrame;
		ParticleData[i].aColor = aColor;
		ParticleData[i].eFrame = eFrame;
		ParticleData[i].eColor = eColor;
		ParticleData[i].mFrame = mFrame;
		ParticleData[i].mColor = mColor;

		ParticleData[i].Pos   = *Pos;
		ParticleData[i].Move  = *Move;
		ParticleData[i].Power = *Power;
		ParticleData[i].rotate = rotate;
		ParticleData[i].stretch = stretch;

		ParticleData[i].CurFrame = 0;
		ParticleData[i].bFlags   = 1;

		ParticleData[i].scale = scale;
		ParticleData[i].angle = .0f;

		ParticleData[i].flag = flag;
		break;
	}
}

void	IEX_SetParticle( LPPARTICLE p )
{
	IEX_SetParticle( p->type, p->aFrame, p->aColor, p->eFrame, p->eColor, p->mFrame, p->mColor, 
						&p->Pos, &p->Move, &p->Power, p->rotate, p->stretch, p->scale, p->flag );
}

void	IEX_SetParticle( int type, int aFrame, float aAlpha, int eFrame, float eAlpha, int mFrame, float mAlpha, 
						D3DVECTOR* Pos, D3DVECTOR* Move, D3DVECTOR* Power,
						float r, float g, float b, float scale, u8 flag )
{
	ARGB	color;
	DWORD	aa, ea, ma;

	aa = ((DWORD)(aAlpha*255.0f) << 24);
	ea = ((DWORD)(eAlpha*255.0f) << 24);
	ma = ((DWORD)(mAlpha*255.0f) << 24);
	color = ((DWORD)(r*255.0f) << 16) | ((DWORD)(g*255.0f) << 8) | (DWORD)(b*255.0f);

	IEX_SetParticle( type, aFrame, aa|color, eFrame, ea|color, mFrame, ma|color, Pos, Move, Power, .0f, 1.0f, scale, flag );
}

//*****************************************************************************
//
//		
//
//*****************************************************************************

void	IEX_ExecuteParticle( int n )
{
	float	work;
	DWORD	a1, r1, g1, b1, a2, r2, g2, b2;
	LPPARTICLEDATA	p;

	p = &ParticleData[n];

	if(	p->bFlags == 0 ) return;

	p->Pos   += p->Move;
	p->Move  += p->Power;
	p->angle += p->rotate;
	p->scale *= p->stretch;

	/*	カラー計算		*/ 
	if( p->CurFrame < p->aFrame ){
		p->CurAlpha = .0f;
	} else {
		if( p->CurFrame < p->mFrame ){
			work = (p->CurFrame - p->aFrame) / (float)(p->mFrame - p->aFrame);
			a1 = (  p->aColor>>24)         ;
			r1 = ( (p->aColor>>16) & 0xFF );
			g1 = ( (p->aColor>> 8) & 0xFF );
			b1 = ( (p->aColor    ) & 0xFF );
		} else {
			work = (p->eFrame - p->CurFrame) / (float)(p->eFrame - p->mFrame);
			a1 = (  p->eColor>>24)         ;
			r1 = ( (p->eColor>>16) & 0xFF );
			g1 = ( (p->eColor>> 8) & 0xFF );
			b1 = ( (p->eColor    ) & 0xFF );
		}

		a2 = (  p->mColor>>24)         ;
		r2 = ( (p->mColor>>16) & 0xFF );
		g2 = ( (p->mColor>> 8) & 0xFF );
		b2 = ( (p->mColor    ) & 0xFF );
		//	カラー設定
		p->CurAlpha = ((a2 - a1)*work + a1) / 255.0f;
		p->r        = ((r2 - r1)*work + r1) / 255.0f;
		p->g        = ((g2 - g1)*work + g1) / 255.0f;
		p->b        = ((b2 - b1)*work + b1) / 255.0f;
	}

	/*	フレーム進行		*/ 
	p->CurFrame ++;
	if( p->CurFrame >= p->eFrame ){
		p->bFlags = 0;
	}
}

void	IEX_ExecuteParticles( void )
{
	int		i;

	for( i=0 ; i<PARTICLE_MAX ; i++ ){
		if(	ParticleData[i].bFlags == 0 ) continue;
		IEX_ExecuteParticle(i);
	}
}

//*****************************************************************************
//
//		パーティクルのレンダリング
//
//*****************************************************************************

void	IEX_RenderParticle2( int index, int type, D3DXMATRIX* mat )
{
	LPDIRECT3DDEVICE9	lpDevice = IEX_GetD3DDevice();
	LPPARTICLEDATA	p;

	p = &ParticleData[index];

	float	x1, x2, y1, y2, s, c;
	s = sinf(p->angle) * p->scale;
	c = cosf(p->angle) * p->scale;
	//	頂点設定
	x1 = s - c;
	x2 = c + s;
	y1 = c + s;
	y2 = c - s;

	ParticleVL[0].x = mat->_11*x1 + mat->_21*y1 + ParticleData[index].Pos.x;
	ParticleVL[0].y = mat->_12*x1 + mat->_22*y1 + ParticleData[index].Pos.y;
	ParticleVL[0].z = mat->_13*x1 + mat->_23*y1 + ParticleData[index].Pos.z;

	ParticleVL[1].x = mat->_11*x2 + mat->_21*y2 + ParticleData[index].Pos.x;
	ParticleVL[1].y = mat->_12*x2 + mat->_22*y2 + ParticleData[index].Pos.y;
	ParticleVL[1].z = mat->_13*x2 + mat->_23*y2 + ParticleData[index].Pos.z;

	ParticleVL[2].x = -(mat->_11*x2 + mat->_21*y2) + ParticleData[index].Pos.x;
	ParticleVL[2].y = -(mat->_12*x2 + mat->_22*y2) + ParticleData[index].Pos.y;
	ParticleVL[2].z = -(mat->_13*x2 + mat->_23*y2) + ParticleData[index].Pos.z;

	ParticleVL[3].x = -(mat->_11*x1 + mat->_21*y1) + ParticleData[index].Pos.x;
	ParticleVL[3].y = -(mat->_12*x1 + mat->_22*y1) + ParticleData[index].Pos.y;
	ParticleVL[3].z = -(mat->_13*x1 + mat->_23*y1) + ParticleData[index].Pos.z;

	//	タイプ設定
	ParticleVL[0].tu = ParticleVL[2].tu = (float)(type%4) * 0.25f+0.02f;
	ParticleVL[1].tu = ParticleVL[3].tu = ParticleVL[0].tu + 0.25f-0.02f;
	ParticleVL[0].tv = ParticleVL[1].tv = (float)(type/4) * 0.25f+0.02f;
	ParticleVL[3].tv = ParticleVL[2].tv = ParticleVL[0].tv + 0.25f-0.02f;

	//	減算用反転
	if( p->flag == RM_SUB ){
		IEX_SetRenderState( RM_INVERT, NULL, NULL );
		//	色設定
		ParticleVL[0].color = ParticleVL[1].color = ParticleVL[2].color = ParticleVL[3].color = 0xFFFFFFFF;
		lpDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, ParticleVL, sizeof(LVERTEX) );
		//	加算合成
		IEX_SetRenderState( RM_ADD, NULL, lpParticleObj->lpTexture );
	} else {
		IEX_SetRenderState( p->flag, NULL, lpParticleObj->lpTexture );
	}

	lpDevice->SetRenderState(D3DRS_LIGHTING, FALSE );
	//	色設定
	ParticleVL[0].color = ParticleVL[1].color = ParticleVL[2].color = ParticleVL[3].color = ((DWORD)(p->CurAlpha*255.0f)<<24) | ((DWORD)(p->r*255.0f)<<16) | ((DWORD)(p->g*255.0f)<<8) | ((DWORD)(p->b*255.0f));
	lpDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, ParticleVL, sizeof(LVERTEX) );

	//	減算用反転
	if( p->flag == RM_SUB ){
		IEX_SetRenderState( RM_INVERT, NULL, NULL );
		//	色設定
		ParticleVL[0].color = ParticleVL[1].color = ParticleVL[2].color = ParticleVL[3].color = 0xFFFFFFFF;
		lpDevice->DrawPrimitiveUP( D3DPT_TRIANGLESTRIP, 2, ParticleVL, sizeof(LVERTEX) );
	}
}

//
//
//

void	IEX_DrawParticles( void )
{
	int		i;
	D3DXMATRIX	mat;
	LPDIRECT3DDEVICE9	lpDevice = IEX_GetD3DDevice();
	lpDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_NONE );
	lpDevice->SetRenderState( D3DRS_SHADEMODE, D3DSHADE_FLAT );

	//	ワールド行列設定
	D3DXMatrixIdentity( &mat );
	lpDevice->SetTransform( D3DTS_WORLD, &mat );

	D3DXMatrixInverse( &mat, NULL, &matView );
	lpDevice->SetFVF(D3DFVF_LVERTEX);
	//	タイプ設定
	for( i=0 ; i<PARTICLE_MAX ; i++ ){
		if(	ParticleData[i].bFlags == 0     ) continue;
		if(	ParticleData[i].CurAlpha <= .0f ) continue;
		//	パーティクルレンダリング
		IEX_RenderParticle2( i, ParticleData[i].type, &mat );
	}

	lpDevice->SetRenderState( D3DRS_CULLMODE, D3DCULL_CCW );
	lpDevice->SetRenderState( D3DRS_SHADEMODE, D3DSHADE_GOURAUD );
	lpDevice->SetRenderState( D3DRS_LIGHTING, TRUE );
}



