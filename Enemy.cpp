#include "All.h"



//*****************************************************************************************************************************
//
//		エネミー関連
//
//*****************************************************************************************************************************
Enemy_Class::Enemy_Class()
{
	int kind = rand()%5;
	switch( kind ){
	case 0:
		maxhp	= EZHP1;	//最大HP
		hDist	= EHSIZE1;
		scale	= ESCALE1;	//スケール
		score	= EZSCORE1;
		break;
	case 1:
		maxhp	= EZHP2;	//最大HP
		hDist	= EHSIZE2;
		scale	= ESCALE2;	//スケール
		score	= EZSCORE2;
		break;
	case 2:
		maxhp	= EZHP3;	//最大HP
		hDist	= EHSIZE3;
		scale	= ESCALE3;	//スケール
		score	= EZSCORE3;
		break;
	case 3:
		maxhp	= EZHP4;	//最大HP
		hDist	= EHSIZE4;
		scale	= ESCALE4;	//スケール
		score	= EZSCORE4;
		break;
	case 4:
		maxhp	= EZHP5;	//最大HP
		hDist	= EHSIZE5;
		scale	= ESCALE5;	//スケール
		score	= EZSCORE5;
		break;
	}
	MainFrameC->SetChara( &Obj, kind + 2 );

	if( type==-1 )type = rand()%4;
	this->type = type;

	pos.x = 0;	//位置
	pos.y = 0;	//位置
	pos.z = MainFrameC->GetPlayer()->GetPos().z + 550.0f;
	target.x = 0;		//ターゲット
	target.y = 0;		//ターゲット
	target.z = MainFrameC->GetPlayer()->GetPos().z + 400.0f;
}

Enemy_Class::Enemy_Class(  int kind, int type, D3DXVECTOR3 pos, D3DXVECTOR3 target )
{
	switch( kind ){
	case 0:
		maxhp	= EZHP1;	//最大HP
		hDist	= EHSIZE1;
		scale	= ESCALE1;	//スケール
		score	= EZSCORE1;
		break;
	case 1:
		maxhp	= EZHP2;	//最大HP
		hDist	= EHSIZE2;
		scale	= ESCALE2;	//スケール
		score	= EZSCORE2;
		break;
	case 2:
		maxhp	= EZHP3;	//最大HP
		hDist	= EHSIZE3;
		scale	= ESCALE3;	//スケール
		score	= EZSCORE3;
		break;
	case 3:
		maxhp	= EZHP4;	//最大HP
		hDist	= EHSIZE4;
		scale	= ESCALE4;	//スケール
		score	= EZSCORE4;
		break;
	case 4:
		maxhp	= EZHP5;	//最大HP
		hDist	= EHSIZE5;
		scale	= ESCALE5;	//スケール
		score	= EZSCORE5;
		break;
	}
	MainFrameC->SetChara( &Obj, kind + 2 );

	if( type==-1 )type = rand()%4;
	this->type = type;

	this->pos	 = pos;	//位置
	this->target = target;		//ターゲット
	this->target.z = pos.z - (pos.z-target.z)/3;
}

void	Enemy_Class::Init()
{
	next = NULL;
	before = NULL;

	MainFrameC->SetWeapon( &bullet, 4 );


	angle		 = D3DXVECTOR3( .0f, .0f, .0f);		//アングル
	move		 = D3DXVECTOR3( .0f, .0f, .0f);		//移動量

	if( type<3 )angle.y = D3DX_PI;

	tDist = .0f;	//ターゲットまでの距離

	hp		= maxhp;
	speed	= ESP;	//移動スピード
	alpha	= 1.0f;	//透過値

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = 0;

	weapon = NULL;

	ChainSpeed = EGCSP;
	ChainTimer = 0;

	flg = TRUE;
	sflg = FALSE;

	float d,dx,dy,dz;
	dx = target.x - pos.x;
	dy = target.y - pos.y;
	dz = target.z - pos.z;
	d = sqrtf( dx*dx + dz*dz );
	dx /= d;
	dz /= d;
	d = sqrtf( d*d + dy*dy );
	dy /= d;

	move.x = dx*speed;
	move.z = dz*speed;
	move.y = dy*speed;
}

void	Enemy_Class::Rele()
{
}

BOOL	Enemy_Class::HitPlayer()
{
	if( type>4 && type!=7 )return FALSE;
	float dist;
	dist = MainFrameC->GetPlayer()->GetHitDist() + hDist;
	if( type==7 ){
		if( dist > GetDist3D( GetBonePos( 18 ), MainFrameC->GetPlayer()->GetPos() ) ){
			MainFrameC->GetPlayer()->DamageHp( BHDAMAGE, MainFrameC->GetPlayer()->GetPos() );
			return TRUE;
		}
	}else if( dist > GetDist3D( pos, MainFrameC->GetPlayer()->GetPos() ) ){
		int damage = EHDAMAGE;
		if( type==4 )damage /= 3;
		MainFrameC->GetPlayer()->DamageHp( damage, MainFrameC->GetPlayer()->GetPos() );
		hp = 0;
		return TRUE;
	}
	return FALSE;
}

D3DXVECTOR3 Enemy_Class::GetHitPos()
{
	return pos;
}

void	Enemy_Class::DamageHp( int damage, D3DXVECTOR3 pos )
{
    hp -= damage;
	if( hp< 0 )hp = 0;
	EffectFire( pos.x, pos.y, pos.z, move );
}

void	Enemy_Class::Mode()
{
	static int cnt = 0;
	float count = 0;

	if( !sflg )cnt++;
	switch( type ){
	case 0:
		if( cnt>=60 ){
			cnt = 0;
			if( !sflg && rand()%5==0 ){
				timer = 60;
				sflg = TRUE;
			}
		}
		if( sflg && --timer<=0 )sflg = FALSE;
		break;
	case 1:
		if( cnt>=60 ){
			cnt = 0;
			if( !sflg && rand()%5==0 ){
				timer = 60;
				sflg = TRUE;
			}
		}
		if( sflg && --timer<=0 )sflg = FALSE;
		break;
	case 2:
		break;
	case 3:
		break;
	case 4:
		angle.y += RAD * 20;
		break;
	}
	if( sflg && flg )MainFrameC->SetBullet( SetBullet() );
}

void	Enemy_Class::Move()
{
	pos += move;
	//if( MainFrameC->GetPlayer()->GetCflg()==5 ){
	//	D3DXVECTOR3 m = move;
	//	D3DXVec3Normalize( &m, &m );
	//	pos += m*(MainFrameC->GetPlayer()->GetSpeed()/5);
	//}
	//if( pos.z > MainFrameC->GetPlayer()->GetPos().z+150){
	//	pos += move;
	//}else{
	//	pos.z = MainFrameC->GetPlayer()->GetPos().z+150;
	//}
}

void	Enemy_Class::UpData()
{
	IEX_ObjectFrameNext( &Obj );

	IEX_SetObjectPos( &Obj, pos.x, pos.y, pos.z );
	IEX_SetObjectAngle( &Obj, angle.x, angle.y, angle.z );
	IEX_SetObjectScale( &Obj, scale );
}

void	Enemy_Class::Proc()
{
	Mode();
	Move();

	UpData();
	
	if( hp<= 0 )dflg = TRUE;

	if( !flg )ChainTimer++;
	if( ChainTimer>=ChainSpeed ){
		flg = TRUE;
	}
}

void	Enemy_Class::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );
}

void	Enemy_Class::SetNext( Enemy_Class *enemy )
{
	next = enemy;
}

Enemy_Class	*Enemy_Class::GetNext()
{
	return next;
}

void	Enemy_Class::SetBefore( Enemy_Class *enemy )
{
	before = enemy;
}

Enemy_Class	*Enemy_Class::GetBefore()
{
	return before;
}

//*****************************************************************************************************************************
//
//		ラフレシア
//
//*****************************************************************************************************************************
void	RAF::Init()
{
	next = NULL;
	before = NULL;

	con = 0;

	MainFrameC->SetChara( &Obj, 7 );
	MainFrameC->SetWeapon( &bullet, 5 );

	target = MainFrameC->GetPlayer()->GetPos();
	pos.x = 0;
	pos.y = (MAXTOP + MAXUNDER)/2;
	pos.z = target.z + 500;

	angle	= D3DXVECTOR3( .0f, D3DX_PI, .0f);	//アングル
	move	= D3DXVECTOR3( .0f, .0f, .0f);		//移動量

	tDist = .0f;	//ターゲットまでの距離

	maxhp	= EBHP1;	//最大HP
	hp		= maxhp;
	speed	= ESP;	//移動スピード
	scale	= BSCALE1;	//スケール
	alpha	= 1.0f;	//透過値

	hDist	= BHSIZE1;

	type = 5;
	mode = 0;
	score	= EBSCORE1;

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = 0;

	weapon = NULL;

	ChainSpeed = EGCSP;
	ChainTimer = 0;

	flg = FALSE;
	cnt = 0;
	IEX_SetObjectMotion( &Obj, 1 );
}

void	RAF::Rele()
{
	MainFrameC->GetScript()->LoadScript( "Stage1_Clear" );
}

Bullet_Class *RAF::SetBullet()
{
	Bullet_Class *bu = new Bullet_Class;

	D3DXMATRIX mat = Obj.TransMatrix;
	D3DXVECTOR3 vec( 0, 0, -1 );
	vec = pos + vec*3.0f;

	SetVecMatrix( &mat, &vec, 3 );

	vec = D3DXVECTOR3( 0, 0, -1 );

	bu->Init( bullet, mat, vec, EBScale2, 3.0f, 1, EBDamage2, 0, FALSE );
	
	Bullet_Class *bu1 = new Bullet_Class;
	bu1->Init( bullet, mat, vec, EBScale2*0.7f, 3.0f, 2, 0, 0, FALSE );
	bu->SetNext( bu1 );
	bu1->SetBefore( bu );

	ChainTimer = 0;
	flg = FALSE;
	Sound::PlaySound( 2, FALSE );
	return bu;
}


D3DXVECTOR3 RAF::GetHitPos()
{
	if( mode<7 ){
		D3DXVECTOR3 p = pos;
		p.z -= 25.0f;
		return p;
	}else{
		return pos;
	}
}

void	RAF::DamageHp( int damage, D3DXVECTOR3 pos )
{
	if( mode>=7 ){
		hp -= damage;
		if( hp< 0 )hp = 0;
		EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 0 ) );
	}else{
		EffectFire( pos.x, pos.y, pos.z-80.0f, D3DXVECTOR3( 0, 0, 0 ) );
	}
}

void	RAF::SetCon( int con )
{
	this->con = con;
}

int		RAF::GetCon()
{
	return con;
}

void	RAF::Mode()
{
	D3DXVECTOR3 p;
	switch( mode ){
	case 0:
		if( fabs(pos.z-target.z)<150.0f )mode++;
		break;
	case 1:
		pos.x += 0.1f;
		angle.z += 0.001f;
		if( pos.x>30.0f )mode++;
		break;
	case 2:
		pos.y += 0.1f;
		angle.z += 0.01f;
		if( pos.y>35.0f )mode++;
		break;
	case 3:
		pos.x -= 0.1f;
		angle.z -= 0.001f;
		if( pos.x<-20.0f )mode++;
		break;
	case 4:
		pos.x += 0.1f;
		angle.z -= 0.01f;
		if( pos.x>.0f )mode++;
		break;
	case 5:
		pos.y -= 0.1f;
		angle.z -= 0.01f;
		if( pos.y<.0f )mode = 1;
		break;
	case 6:
		if( fabs(pos.x)>0.01f ){
			if( pos.x<0 )pos.x += 0.1f;
			if( pos.x>0 )pos.x -= 0.1f;
		}else pos.x = 0;
		if( fabs(pos.y-(MAXTOP + MAXUNDER)/2)>0.01f ){
			if( pos.y<(MAXTOP + MAXUNDER)/2 )pos.y += 0.1f;
			if( pos.y>(MAXTOP + MAXUNDER)/2 )pos.y -= 0.1f;
		}else pos.y = (MAXTOP + MAXUNDER)/2;

		p = pos;
		p.z = 0;
		if( GetDist3D( p, D3DXVECTOR3( 0, (MAXTOP + MAXUNDER)/2, 0 ) )<2.0f ){
			IEX_SetObjectMotion( &Obj, 2 );
			mode = 8;
			hDist = 20.0f;
		}
		break;
	case 7:
		pos.x += 0.1f;
		angle.z += 0.001f;
		if( pos.x>30.0f )mode = rand()%4 + 8;
		break;
	case 8:
		pos.y += 0.1f;
		angle.z += 0.01f;
		if( pos.y>30.0f ){
			mode = rand()%4 + 7;
		}
		break;
	case 9:
		pos.x -= 0.1f;
		angle.z -= 0.001f;
		if( pos.x<-20.0f ){
			mode = rand()%4 + 7;
			mode = (mode==9)?11:mode;
		}
		break;
	case 10:
		pos.y -= 0.1f;
		angle.z -= 0.01f;
		if( pos.y<.0f ){
			mode = 7;
			mode = (mode==10)?11:mode;
		}
		break;
	case 11:
		pos.x += 0.1f;
		angle.z -= 0.01f;
		if( pos.x>20.0f ){
			mode = rand()%4 + 7;
		}
		break;
	case 12:
		break;
	}

	static BOOL flg = FALSE;
	static float csize = 0;
	if( mode>6 ){
		cnt++;

		if( cnt>=180 && cnt<=360 ){
			D3DXVECTOR3 Pos = pos;
			Pos.z -= 10.0f;
			EffectChage1( Pos, MainFrameC->GetPlayer()->GetMove().z, 15.0f );
			Pos.z -= 10.0f;
			EffectChage2( Pos, csize );
			csize += 0.13f;
			flg = TRUE;
		}
		if( cnt>=360 ){
			csize -= 0.05f;
			D3DXVECTOR3 Pos = pos;
			Pos.z -= 30.0f;
			EffectChage2( Pos, csize );
			MainFrameC->SetBullet( this->SetBullet() );
			if( cnt>=660 ){
				csize = 0;
				cnt = 0;
				flg = FALSE;
			}
		}
	}
	if( mode<=5 && con<=0 )mode = 6;
	if( hp <= 0 ){
		cnt = 0;
		deadCnt++;
		mode = 255;
		for( int i=0;i<15;i++ ){
			D3DXVECTOR3 pos;
			pos = this->pos;
			int rot = rand()%360;
			int dist = rand()%20;
			pos.x += cosf( RAD*rot )*dist;
			pos.y += sinf( RAD*rot )*dist;
			pos.z -= 10.0f;
			EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 1 ) );
		}
		alpha -= 0.003334f;;
		if( deadCnt>=300 )dflg = TRUE;
	}
}

void	RAF::Move()
{
	target = MainFrameC->GetPlayer()->GetPos();
	if( mode!=0 ){
		pos.z = target.z + 150.0f;
	}
}

void	RAF::UpData()
{
	IEX_ObjectFrameNext( &Obj );
	IEX_SetObjectPos( &Obj, pos.x, pos.y, pos.z );
	IEX_SetObjectScale( &Obj, scale );
	IEX_SetObjectAngle( &Obj, angle.x, angle.y, angle.z );
}

void	RAF::Proc()
{
	Mode();
	Move();
	UpData();
}

void	RAF::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );
}

//ラフレシアについているコンテナ
void	CON::Init()
{
	next = NULL;
	before = NULL;

	MainFrameC->SetChara( &Obj, 6 );


	Enemy_Class *e;
	e = MainFrameC->GetEnemy()->GetEnemy();
	while( e->GetType()!=5 ){
		MainFrameC->GetEnemy()->EnemyNext( &e );
	}
	p = (RAF*)e;
	angleZ = RAD*60 * p->GetCon();
	p->SetCon( p->GetCon()+1 );
	cno = p->GetCon();

	target = MainFrameC->GetPlayer()->GetPos();
	pos.x = 0;
	pos.y = (MAXTOP + MAXUNDER)/2;
	pos.z = target.z + 500;

	angle	= D3DXVECTOR3( .0f, .0f, .0f );		//アングル
	move	= D3DXVECTOR3( .0f, .0f, .0f );		//移動量

	type = 6;

	tDist = .0f;	//ターゲットまでの距離

	maxhp	= EZHP5;	//最大HP
	hp		= maxhp;
	speed	= ESP;	//移動スピード
	scale	= BSCALE1;	//スケール
	alpha	= 1.0f;	//透過値

	score	= EZSCORE5;
	hDist	= EHSIZE5;

	mode = 0;

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = 0;

	weapon = NULL;

	ChainSpeed = ECONSP;
	ChainTimer = 0;

	cnt	= 0;

	flg = TRUE;
}


void	CON::Rele()
{
	p->SetCon( p->GetCon() - 1 );
}

D3DXVECTOR3 CON::GetHitPos()
{
	return GetBonePos( 0 );
}

void	CON::DamageHp( int damage, D3DXVECTOR3 pos )
{
	hp -= damage;
	if( hp< 0 )hp = 0;
	EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 0 ) );
}

void	CON::SetEnemy()
{
	MainFrameC->GetEnemy()->CreateEnemy( 3, 4, GetBonePos(0), MainFrameC->GetPlayer()->GetPos() );
}

void	CON::Mode()
{
	//if( KEY(KEY_ENTER)==3)hp = 0;

	if( hp>0 ){
		int n1,n2,n3;
		n1 = (cno==6)?3:cno;
		n2 = (n1<4)?n1+2:n1-5+2;
		n3 = (n2<4)?n2+2:n2-5+2;
		if( n1==p->GetMode() || n2==p->GetMode() || n3==p->GetMode() ){
			if( !flg )return;
			if( rand()%3==0 )SetEnemy();
			flg = FALSE;
			ChainTimer = 0;
		}
	}

	if( hp <= 0 ){
		D3DXVECTOR3 pos = GetBonePos( 0 );
		cnt = 0;
		deadCnt++;
		mode = 255;
		for( int i=0;i<15;i++ ){
			D3DXVECTOR3 pos;
			pos = GetBonePos( 0 );
			int rot = rand()%360;
			int dist = rand()%10;
			pos.x += cosf( RAD*rot )*dist;
			pos.y += sinf( RAD*rot )*dist;
			pos.z -= 10.0f;
			EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 1 ) );
		}
		alpha -= 0.003334f*2;
		if( deadCnt>=150 )dflg = TRUE;
	}
}

void	CON::Move()
{
	target = MainFrameC->GetPlayer()->GetPos();
	pos = p->GetPos();
	angle = p->GetAngle();
	angle.z += angleZ;
}

void	CON::UpData()
{
	IEX_ObjectFrameNext( &Obj );
	IEX_SetObjectPos( &Obj, pos.x, pos.y, pos.z );
	IEX_SetObjectScale( &Obj, scale );
	IEX_SetObjectAngle( &Obj, angle.x, angle.y, angle.z );
}

void	CON::Proc()
{
	Mode();
	Move();
	UpData();
	
	if( !flg )ChainTimer++;
	if( ChainTimer>=ChainSpeed ){
		flg = TRUE;
	}
}

void	CON::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );
}

//*****************************************************************************************************************************
//
//		赤い彗星
//
//*****************************************************************************************************************************
void	MS::Init()
{
	next = NULL;
	before = NULL;

	MainFrameC->SetChara( &Obj, 8 );
	MainFrameC->SetWeapon( &bullet, 4 );

	target = MainFrameC->GetPlayer()->GetPos();
	pos.x = 0;
	pos.y = -(MAXTOP + MAXUNDER)/2;
	pos.z = target.z + 500;

	mpos	= D3DXVECTOR3( 0, 0, 0 );
	angle	= D3DXVECTOR3( .0f, D3DX_PI, .0f );		//アングル
	move	= D3DXVECTOR3( .0f, .0f, .0f );		//移動量

	tDist = .0f;	//ターゲットまでの距離

	maxhp	= EBHP2;	//最大HP
	hp		= maxhp;
	speed	= ESP;	//移動スピード
	scale	= BSCALE2;	//スケール
	alpha	= 1.0f;	//透過値

	score	= EBSCORE2;

	hDist	= BHSIZE2;

	type = 7;
	mode = 0;

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = 0;

	weapon = NULL;

	ChainSpeed = EGCSP;
	ChainTimer = 0;

	flg = FALSE;
	cnt = 0;
	IEX_SetObjectMotion( &Obj, 1 );
}

void	MS::Rele()
{
	MainFrameC->GetScript()->LoadScript( "Stage2_Clear" );
}

Bullet_Class *MS::SetBullet()
{
	Bullet_Class *bu = new Bullet_Class;

	D3DXMATRIX mat = GetBone( 14 );
	D3DXVECTOR3 vec;
	GetVecMatrix( &mat, &vec, 0 );
	vec = GetBonePos( 14 ) + -vec*21.0f;
	vec.x += cosf( angle.z )*0.7f;
	vec.y += sinf( angle.z )*0.7f;

	mat = Obj.TransMatrix;
	SetVecMatrix( &mat, &vec, 3 );
	GetVecMatrix( &mat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu->Init( bullet, mat, vec, EBScale, EBulletSpeed, 0, BBDamage*2, 0, FALSE );

	Bullet_Class *bu1 = bu;
	for( int i=1;i<4;i++ ){
		Bullet_Class *bu2 = new Bullet_Class;
		mat = GetBone( 14 );
		GetVecMatrix( &mat, &vec, 0 );
		vec = GetBonePos( 14 ) + -vec*21.0f;
		vec.x += cosf( RAD*i*90+angle.z )*0.7f;
		vec.y += sinf( RAD*i*90+angle.z )*0.7f;

		mat = Obj.TransMatrix;
		SetVecMatrix( &mat, &vec, 3 );
		GetVecMatrix( &mat, &vec, 2 );
		D3DXVec3Normalize( &vec, &vec );
		bu2->Init( bullet, mat, vec, EBScale, EBulletSpeed, 0, BBDamage*2, 0, FALSE );
		bu1->SetNext( bu2 );
		bu2->SetBefore( bu1 );
		bu1 = bu2;
	}

	ChainTimer = 0;
	flg = FALSE;
	Sound::PlaySound( 0, FALSE );
	return bu;
}

Bullet_Class *MS::SetLaser()
{
	Bullet_Class *bu = new Bullet_Class;

	D3DXMATRIX mat = GetBone( 5 );
	D3DXVECTOR3 vec;
	GetVecMatrix( &mat, &vec, 2 );
	vec = GetBonePos( 5 ) + vec*20.0f;

	mat = Obj.TransMatrix;
	SetVecMatrix( &mat, &vec, 3 );
	GetVecMatrix( &mat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu->Init( bullet, mat, vec, EBScale3, 3.0f, 3, EBDamage2, 0, FALSE );

	Bullet_Class *bu1 = bu;
	for( int i=1;i<6;i++ ){
		Bullet_Class *bu2 = new Bullet_Class;
		mat = GetBone( 5+i );
		GetVecMatrix( &mat, &vec, 2 );
		vec = GetBonePos( 5+i ) + vec*20.0f;

		mat = Obj.TransMatrix;
		SetVecMatrix( &mat, &vec, 3 );
		GetVecMatrix( &mat, &vec, 2 );
		D3DXVec3Normalize( &vec, &vec );
		bu2->Init( bullet, mat, vec, EBScale3, 3.0f, 3, EBDamage2, 0, FALSE );
		bu1->SetNext( bu2 );
		bu2->SetBefore( bu1 );
		bu1 = bu2;
	}

	ChainTimer = 0;
	flg = TRUE;
	Sound::PlaySound( 2, FALSE );
	return bu;
}

D3DXVECTOR3 MS::GetHitPos()
{
	D3DXVECTOR3 pos = GetHitPos( 0 );
	pos.z -= 10.0f;
	return pos;
}

D3DXVECTOR3 MS::GetHitPos( int no )
{
	D3DXVECTOR3 vec;
	D3DXMATRIX mat;
	switch( no ){
	case 0:
		//中心
		return GetBonePos( 0 );
	case 1:
		//右上レーザー1
		mat = GetBone( 6 );
		GetVecMatrix( &mat, &vec, 2 );
		vec = vec*20.0f;
		return GetBonePos( 6 ) + vec;
	case 2:
		//左上レーザー1
		mat = GetBone( 9 );
		GetVecMatrix( &mat, &vec, 2 );
		vec = vec*20.0f;
		return GetBonePos( 9 ) + vec;
	case 3:
		//体下
		return GetBonePos( 15 );
	case 4:
		//右膝
		return GetBonePos( 17 );
	case 5:
		//左膝
		return GetBonePos( 20 );
	case 6:
		//右足
		return GetBonePos( 18 );
	case 7:
		//左足
		return GetBonePos( 21 );
	}
	return D3DXVECTOR3( 0, 0, 0 );
}

float	MS::GetHitDist( int no )
{
	switch( no ){
	case 0:
		//中心
		return BHSIZE21;
	case 1:
		//右上レーザー
		return BHSIZE22;
	case 2:
		//左上レーザー
		return BHSIZE23;
	case 3:
		//体下
		return BHSIZE24;
	case 4:
		//右膝
		return BHSIZE25;
	case 5:
		//左膝
		return BHSIZE26;
	case 6:
		//右足
		return BHSIZE27;
	case 7:
		//左足
		return BHSIZE28;
	}
	return 0;
}

void	MS::DamageHp( int damage, D3DXVECTOR3 pos )
{
	hp -= damage;
	if( hp< 0 )hp = 0;
	EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 0 ) );
}

void	MS::Mode()
{
	static int bmode;
	target = MainFrameC->GetPlayer()->GetPos();
	switch( mode ){
	case 0:
		//敵が一定位置に来るまで待つ
		if( pos.z-target.z<=151.0f ){
			IEX_SetObjectMotion( &Obj, 2 );
			mode++;
			bmode = mode;
		}
		break;
	case 1:
		//基本分岐
		if( Obj.Motion!=0 )break;
		mode = rand()%7;
		if( mode==0 || mode==1 || mode==2 )mode = 9;
		if( mode==3 || mode==4 )mode = 11;
		if( mode==5 || mode==6 )mode = 14;
		
		if( mode!=14 )bmode = mode;
		cnt = 0;
		break;
	case 2:
		//右に移動
		angle.z = ( angle.z<RAD*20 )?angle.z + 0.02f:RAD*20;
		cnt++;
		mpos.x += 0.2f;
		if( cnt>=60 ){
			cnt = 0;
			mode = 4;
		}
		break;
	case 3:
		//左に移動
		angle.z = ( angle.z>-RAD*20 )?angle.z - 0.02f:-RAD*20;
		cnt++;
		mpos.x -= 0.2f;
		if( cnt>=60 ){
			cnt = 0;
			mode = 4;
		}
		break;
	case 4:
		//左右移動後の角度修正(分岐)
		angle.z = ( angle.z<0 )?angle.z + 0.02f:angle.z - 0.02f;
		angle.z = ( fabs(angle.z)<= 0.01f )?0:angle.z;
		mode = ( angle.z==0 )?bmode:mode;
		break;
	case 5:
		//上に移動
		cnt++;
		mpos.y += 0.2f;
		if( cnt>=60 ){
			cnt = 0;
			mode = bmode;
		}
		break;
	case 6:
		//下に移動
		cnt++;
		mpos.y -= 0.2f;
		if( cnt>=60 ){
			cnt = 0;
			mode = bmode;
		}
		break;
	case 7:
		//腕の銃発射
		if( cnt==0 )IEX_SetObjectMotion( &Obj, 3 );
		cnt++;
		if( Obj.dwFrame==222 ){
			MainFrameC->SetWeapon( &bullet, 3 );
			cnt = 0;
			mode++;
			bmode = mode;
		}
		break;
	case 8:
		//銃発射停止
		if( cnt==0 ){
			mode = rand()%3;
			if( mode==0 )mode = 14;
			else{
				D3DXMATRIX mat = GetBone( 14 );
				D3DXVECTOR3 vec;
				GetVecMatrix( &mat, &vec, 0 );
				vec = GetBonePos( 14 ) + -vec*21.0f;
				if( vec.x<target.x )mode = 2;
				else mode = 3;
				cnt = -1;
			}
		}
		if( cnt==1 )IEX_SetObjectMotion( &Obj, 4 );
		cnt++;
		if( Obj.dwFrame==284 ){
			cnt = 0;
			mode = 1;
			bmode = mode;
		}
		break;
	case 9:
		//レーザー発射
		if( cnt==0 )IEX_SetObjectMotion( &Obj, 6 );
		cnt++;
		if( Obj.dwFrame==411 ){
			MainFrameC->SetWeapon( &bullet, 5 );
			cnt = 0;
			mode++;
			bmode = mode;
		}
		break;
	case 10:
		//レーザー発射停止
		if( cnt==0 ){
			mode = rand()%3;
			if( mode==0 )mode = 14;
			else{
				if( GetBonePos(11).x<target.x )mode = 2;
				else mode = 3;
				cnt = -1;
			}
		}
		if( cnt==1 )IEX_SetObjectMotion( &Obj, 7 );
		cnt++;
		if( Obj.Motion==0 ){
			cnt = 0;
			mode = 1;
			bmode = mode;
		}
		break;
	case 11:
		//格闘準備(プレイヤーに向かってくる)
		if( cnt==0 )IEX_SetObjectMotion( &Obj, 1 );
		cnt++;
		mpos.z -= 1.0f;
		if( mpos.z+pos.z<=target.z+15.0f ){
			mpos.z = target.z+15.0f - pos.z;
			cnt = 0;
			mode++;
			bmode = mode;
		}

		if( fabs(GetBonePos( 15 ).x-target.x)>0.3f ){
			if( GetBonePos( 15 ).x<target.x ){
				mpos.x += 0.3f;
			}else{
				mpos.x -= 0.3f;
			}
		}
		if( fabs(GetBonePos( 15 ).y-target.y)>0.3f ){
			if( GetBonePos( 15 ).y<target.y ){
				mpos.y += 0.3f;
			}else{
				mpos.y -= 0.3f;
			}
		}
		break;
	case 12:
		//格闘準備2
		if( cnt==0 )IEX_SetObjectMotion( &Obj, 2 );
		cnt++;
		if( Obj.Motion==0 ){
			cnt = 0;
			mode++;
			bmode = mode;
		}
		break;
	case 13:
		//格闘攻撃
		if( cnt==0 )IEX_SetObjectMotion( &Obj, 5 );
		cnt++;
		if( Obj.dwFrame==380 ){
			cnt = 0;
			bmode = 1;
			mode = 14;
		}
		break;
	case 14:
		cnt++;
		if( cnt>=120 ){
  			cnt = 1;
			mode = bmode;
		}
		break;
	}

	if( Obj.Motion==8 ){
		D3DXMATRIX mat = GetBone( 14 );
		D3DXVECTOR3 vec;
		GetVecMatrix( &mat, &vec, 0 );
		vec = GetBonePos( 14 ) + -vec*21.0f;
		if( fabs(vec.y-target.y)>0.3f ){
			if( vec.y<target.y ){
				mpos.y += 0.2f;
			}else{
				mpos.y -= 0.2f;
			}
		}
	}
	if( Obj.Motion==6 ){
		if( fabs(GetBonePos( 9 ).y-target.y)>0.3f ){
			if( GetBonePos( 9 ).y<target.y ){
				mpos.y += 0.2f;
			}else{
				mpos.y -= 0.2f;
			}
		}
	}

	if( mode<11 || mode==14 ){
		mpos.z = (mpos.z<0)?mpos.z+0.5f:mpos.z;
		mpos.z = (mpos.z>0)?0:mpos.z;
	}
}

void	MS::Move()
{
	pos.z = (mode>0)?target.z+151.0f:pos.z;

	if( Obj.dwFrame==503 ){
		MainFrameC->SetBullet( this->SetBullet() );
	}
	static int lcnt = 0;
	if( Obj.dwFrame==411 ){
		flg = ( lcnt>=20 )?FALSE:TRUE;
		if( flg )MainFrameC->SetBullet( this->SetLaser() );
		lcnt++;
		lcnt = ( lcnt>=30 )?0:lcnt;
	}
	if( hp <= 0 ){
		if( deadCnt==0 )IEX_SetObjectMotion( &Obj, 9 );
		mode = 255;
		deadCnt++;
	}
	if( Obj.Motion==9 ){
		mpos.y -= 0.1f;
		for( int i=0;i<21;i++ ){
			D3DXVECTOR3 pos;
			pos = GetBonePos( i );
			EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3(0,0,1) );
		}
		if( deadCnt>=300 )dflg = TRUE;
	}
}

void	MS::UpData()
{
	D3DXVECTOR3 pos;
	pos = this->pos + mpos;
	IEX_ObjectFrameNext( &Obj );
	IEX_SetObjectPos( &Obj, pos.x, pos.y, pos.z );
	IEX_SetObjectScale( &Obj, scale );
	IEX_SetObjectAngle( &Obj, angle.x, angle.y, angle.z );
}

void	MS::Proc()
{
	Mode();
	Move();
	UpData();

}

void	MS::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );
}


//*****************************************************************************************************************************
//
//		ひまわり
//
//*****************************************************************************************************************************
void	HI_RING::Init()
{
	next = NULL;
	before = NULL;

	wp = 0;

	MainFrameC->SetChara( &Obj, 10 );

	target = MainFrameC->GetPlayer()->GetPos();
	pos.x = 0;
	pos.y = (MAXTOP + MAXUNDER)/2;
	pos.z = target.z + 1500;

	angle	= D3DXVECTOR3( .0f, D3DX_PI, .0f);		//アングル
	move	= D3DXVECTOR3( .0f, .0f, .0f);		//移動量

	tDist = .0f;	//ターゲットまでの距離

	maxhp	= EBHP3_WP*4 + EBHP3_CORE;	//最大HP
	hp		= maxhp;
	speed	= ESP;		//移動スピード
	scale	= BSCALE3;	//スケール
	alpha	= 1.0f;		//透過値

	score	= EBSCORE3;
	type = 8;

	mode = 0;

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = 0;

	weapon = NULL;

	cnt = 0;

	flg = FALSE;
}


void	HI_RING::Rele()
{
	float a = (float)MainFrameC->GetPlayer()->GetHp()/(float)MainFrameC->GetPlayer()->GetMaxHp();
  	MainFrameC->AddScore( MainFrameC->GetPlayer()->GetRemain()*50000 + (int)(a*50000) );

	MainFrameC->GetScript()->LoadScript( "Stage3_Clear" );
}

D3DXVECTOR3	HI_RING::GetHitPos()
{
	return D3DXVECTOR3( 0, 0, 0 );
}

void	HI_RING::DamageHp( int damage, D3DXVECTOR3 pos )
{
}

void	HI_RING::SetWp( int wp )
{
	this->wp = wp;
}

int		HI_RING::GetWp()
{
	return wp;
}

void	HI_RING::Mode()
{
	switch( mode ){
	case 0:
		if( fabs(pos.z-target.z)<200.0f )mode++;
		break;
	case 1:
		if( pos.x >= 145 )mode++;
		break;
	case 2:
		cnt++;
		if( cnt==180 ){
			cnt = 0;
			if( rand()%3==0 ){
				mode++;
			}
		}
		break;
	case 3:
		cnt++;
		if( cnt==180 ){
			cnt = 0;
			if( rand()%3==0 ){
				mode--;
			}
		}
		break;
	case 10:
		if( pos.x <= 0 ){
			cnt = 0;
			mode++;
		}
		break;
	case 11:
		if( fabs(pos.z-(target.z-100))<50.0f )mode++;
		break;
	case 12:
		break;
	}

	if( wp==0 ){
		mode = ( mode<10 )? 10: mode;
	}

	if( hp <= 0 ){
		cnt = 0;
		deadCnt++;
		mode = 255;
		for( int i=0;i<15;i++ ){
			//D3DXVECTOR3 pos;
			//pos = this->pos + D3DXVECTOR3( 0, 0, -1 )*20.0f;
			//int rot = rand()%360;
			//int dist = rand()%20;
			//pos.x += cosf( RAD*rot )*20.0f;
			//pos.y += sinf( RAD*rot )*20.0f;
			//pos.z -= 10.0f;
			//EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 1 ) );
		}
		alpha -= 0.003334f;
		if( deadCnt>=300 )dflg = TRUE;
	}
}

void	HI_RING::Move()
{
	target = MainFrameC->GetPlayer()->GetPos();
	if( mode!=0 && mode <= 10 ){
		pos.z = target.z + 200.0f;
	}else if( mode>11 ){
		pos.z = target.z - 50;
	}

	switch( mode ){
	case 0:
		pos.z -= 1.0f;
		break;
	case 1:
		pos.x += 1.0f;
		break;
	case 2:
		angle.z += RAD*0.4f;
		break;
	case 3:
		angle.z += RAD*0.3f;
		break;
	case 10:
		pos.x -= 1.0f;
		break;
	case 12:
		pos.x += 0.4f;
		angle.z += 0.002f;
		if( pos.x>30.0f )mode = rand()%4 + 13;
		break;
	case 13:
		pos.y += 0.4f;
		angle.z += 0.02f;
		if( pos.y>30.0f ){
			mode = rand()%4 + 12;
		}
		break;
	case 14:
		pos.x -= 0.4f;
		angle.z -= 0.002f;
		if( pos.x<-20.0f ){
			mode = rand()%4 + 12;
			mode = (mode==14)?16:mode;
		}
		break;
	case 15:
		pos.y -= 0.4f;
		angle.z -= 0.02f;
		if( pos.y<.0f ){
			mode = 12;
			mode = (mode==15)?16:mode;
		}
		break;
	case 16:
		pos.x += 0.4f;
		angle.z -= 0.02f;
		if( pos.x>20.0f ){
			mode = rand()%4 + 12;
		}
		break;
	}
}

void	HI_RING::UpData()
{
	Enemy_Class *e = next;
	hp = 0;
	while( e && e->GetType()>=9 ){
		hp += e->GetHp();
		e = e->GetNext();
	}

	IEX_ObjectFrameNext( &Obj );
	IEX_SetObjectPos( &Obj, pos.x, pos.y, pos.z );
	IEX_SetObjectScale( &Obj, scale );
	IEX_SetObjectAngle( &Obj, angle.x, angle.y, angle.z );
}

void	HI_RING::Proc()
{
	Mode();
	Move();
	UpData();
}

void	HI_RING::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );
}


//ひまわりの武器
void	HI_WP::Init()
{
	next = NULL;
	before = NULL;

	MainFrameC->SetChara( &Obj, 1 );
	MainFrameC->SetWeapon( &bullet, 5 );

	Enemy_Class *e;
	e = MainFrameC->GetEnemy()->GetEnemy();
	while( e->GetType()!=8 ){
		MainFrameC->GetEnemy()->EnemyNext( &e );
	}
	p = (HI_RING*)e;
	angleZ = RAD*90 * p->GetWp();
	p->SetWp( p->GetWp()+1 );
	wno = p->GetWp();

	target = MainFrameC->GetPlayer()->GetPos();
	pos.x = 0;
	pos.y = (MAXTOP + MAXUNDER)/2;
	pos.z = target.z + 1500;

	angle	= D3DXVECTOR3( .0f, .0f, .0f );		//アングル
	move	= D3DXVECTOR3( .0f, .0f, .0f );		//移動量

	type = 9;

	tDist = .0f;	//ターゲットまでの距離

	maxhp	= EBHP3_WP;	//最大HP
	hp		= maxhp;
	speed	= ESP;		//移動スピード
	scale	= BSCALE3;	//スケール
	alpha	= 1.0f;		//透過値

	score	= EBSCORE3_WP;
	hDist	= BHSIZE3;

	mode = 0;

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = 0;

	weapon = NULL;

	ChainSpeed = EHIWP;
	ChainTimer = 0;

	cnt = 0;

	flg = TRUE;
}

void	HI_WP::Rele()
{
	p->SetWp( p->GetWp() - 1 );
	p->SetHp( p->GetHp() - 1 );
}

Bullet_Class *HI_WP::SetBullet()
{
	Bullet_Class *bu = new Bullet_Class;

	D3DXMATRIX mat;
	D3DXVECTOR3 vec;
	vec = GetBonePos( 2 );

	mat = Obj.TransMatrix;
	SetVecMatrix( &mat, &vec, 3 );
	GetVecMatrix( &mat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu->Init( bullet, mat, vec, EBScale3, 3.0f, 3, BBDamage, 0, FALSE );

	Bullet_Class *bu1 = bu;

	Bullet_Class *bu2 = new Bullet_Class;
	vec = GetBonePos( 3 );

	mat = Obj.TransMatrix;
	SetVecMatrix( &mat, &vec, 3 );
	GetVecMatrix( &mat, &vec, 2 );
	D3DXVec3Normalize( &vec, &vec );
	bu2->Init( bullet, mat, vec, EBScale3, 3.0f, 3, BBDamage, 0, FALSE );
	bu1->SetNext( bu2 );
	bu2->SetBefore( bu1 );
	bu1 = bu2;

	ChainTimer = 0;
	flg = TRUE;
	return bu;
}

D3DXVECTOR3 HI_WP::GetHitPos()
{
	return GetBonePos( 1 );
}

void	HI_WP::DamageHp( int damage, D3DXVECTOR3 pos )
{
	hp -= damage;
	if( hp< 0 )hp = 0;
	EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 0 ) );
}

void	HI_WP::SetEnemy()
{
	MainFrameC->GetEnemy()->CreateEnemy( rand()%3, 2, GetBonePos(0), MainFrameC->GetPlayer()->GetPos() );
}

void	HI_WP::Mode()
{
	int r;
	if( p->GetMode() > 1 && p->GetMode() < 10 && GetDist3D(MainFrameC->GetPlayer()->GetPos(), pos)<500 ){
		switch( mode ){
			case 0:
				r = rand()%6;
				if( r<=1 ){
					mode = 1;
				}else if( r<=3 ){
					mode = 2;
				}else{
					mode = 3;
				}
				break;
			case 1:
				cnt++;
				MainFrameC->SetBullet( this->SetBullet() );
				if( cnt==300 ){
					mode = 0;
					cnt = 0;
				}
				break;
			case 2:
				cnt++;
				if( cnt%10==0 )SetEnemy();
				if( cnt==300 ){
					mode = 0;
					cnt = 0;
				}
				break;
			case 3:
				cnt++;
				MainFrameC->SetBullet( this->SetBullet() );
				if( cnt%10==0 )SetEnemy();
				if( cnt==300 ){
					mode = 0;
					cnt = 0;
				}
				break;
		}
	}

	if( hp <= 0 ){
		cnt = 0;
		deadCnt++;
		mode = 255;
		for( int i=0;i<10;i++ ){
			D3DXVECTOR3 pos;
			pos = GetBonePos(1);
			int rot = rand()%360;
			int dist = rand()%20;
			pos.x += cosf( RAD*rot )*20.0f;
			pos.y += sinf( RAD*rot )*20.0f;
			pos.z -= 10.0f;
			EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 1 ) );
		}
		alpha -= 0.003334f;
		if( deadCnt>=300 )dflg = TRUE;
	}
}

void	HI_WP::Move()
{
	target = MainFrameC->GetPlayer()->GetPos();
	pos = p->GetPos();
	angle = p->GetAngle();
	angle.z += angleZ;

}

void	HI_WP::UpData()
{
	IEX_ObjectFrameNext( &Obj );
	IEX_SetObjectPos( &Obj, pos.x, pos.y, pos.z );
	IEX_SetObjectScale( &Obj, scale );
	IEX_SetObjectAngle( &Obj, angle.x, angle.y, angle.z );
}

void	HI_WP::Proc()
{
	Mode();
	Move();
	UpData();
	
	if( !flg )ChainTimer++;
	if( ChainTimer>=ChainSpeed ){
		flg = TRUE;
	}
}

void	HI_WP::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );
}


//ひまわりのコア
void	HI_CORE::Init()
{
	next = NULL;
	before = NULL;

	MainFrameC->SetChara( &Obj, 9 );
	MainFrameC->SetWeapon( &bullet, 5 );

	Enemy_Class *e;
	e = MainFrameC->GetEnemy()->GetEnemy();
	while( e->GetType()!=8 ){
		MainFrameC->GetEnemy()->EnemyNext( &e );
	}
	p = (HI_RING*)e;

	target = MainFrameC->GetPlayer()->GetPos();
	pos.x = 0;
	pos.y = (MAXTOP + MAXUNDER)/2;
	pos.z = target.z + 1500;

	angle	= D3DXVECTOR3( .0f, D3DX_PI, .0f );	//アングル
	move	= D3DXVECTOR3( .0f, .0f, .0f );		//移動量

	type = 10;

	tDist = .0f;	//ターゲットまでの距離

	maxhp	= EBHP3_CORE;	//最大HP
	hp		= maxhp;
	speed	= ESP;		//移動スピード
	scale	= BSCALE1;	//スケール
	alpha	= 1.0f;		//透過値

	score	= EBSCORE3_CORE;
	hDist	= BHSIZE3;

	mode = 0;

	dflg	= FALSE;//死亡フラグ
	deadCnt = 0;	//死亡カウンター

	timer = 0;

	weapon = NULL;

	ChainSpeed = EHIWP;
	ChainTimer = 0;

	cnt = 0;

	flg = TRUE;

	IEX_SetObjectMotion( &Obj, 1 );
}


void	HI_CORE::Rele()
{
	p->SetHp( p->GetHp() - 1 );
}

Bullet_Class *HI_CORE::SetBullet()
{
	Bullet_Class *bu = new Bullet_Class;

	D3DXMATRIX mat = Obj.TransMatrix;
	D3DXVECTOR3 vec( 0, 0, -1 );
	vec = pos + vec*60.0f;

	SetVecMatrix( &mat, &vec, 3 );

	vec = D3DXVECTOR3( 0, 0, -1 );

	bu->Init( bullet, mat, vec, EBScale2, 3.0f, 1, BBDamage, 0, FALSE );

	Bullet_Class *bu1 = new Bullet_Class;
	bu1->Init( bullet, mat, vec, EBScale2*0.5f, 3.0f, 2, 0, 0, FALSE );
	bu->SetNext( bu1 );
	bu1->SetBefore( bu );

	ChainTimer = 0;
	flg = FALSE;
	Sound::PlaySound( 2, FALSE );
	return bu;
}

D3DXVECTOR3 HI_CORE::GetHitPos()
{
	if( mode>1 ){
		D3DXVECTOR3 p = pos;
		p.z -= 25.0f;
		return p;
	}else{
		return pos;
	}
}

void	HI_CORE::DamageHp( int damage, D3DXVECTOR3 pos )
{
	if( Obj.Motion!=1 ){
		hp -= damage;
		if( hp< 0 )hp = 0;
		EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 0 ) );
	}else{
		EffectFire( pos.x, pos.y, pos.z-80.0f, D3DXVECTOR3( 0, 0, 0 ) );
	}
}

void	HI_CORE::Mode()
{
	int r;

	static BOOL flg = FALSE;
	static float csize = 0;

	if( p->GetMode()>11 ){
		switch( mode ){
			case 0:
				r = rand()%3;
				if( r<=1 ){
					mode = 2;
				}else{
					mode = 4;
				}
				break;
			case 1:
				IEX_SetObjectMotion( &Obj, 3 );
				mode = 4;
				break;
			case 2:
				IEX_SetObjectMotion( &Obj, 2 );
				mode = 3;
				cnt = -180;
				break;
			case 3:
				cnt++;

				if( cnt>=180 && cnt<=360 ){
					D3DXVECTOR3 Pos = pos;
					Pos.z -= 10.0f;
					EffectChage1( Pos, MainFrameC->GetPlayer()->GetMove().z, 15.0f );
					Pos.z -= 10.0f;
					EffectChage2( Pos, csize );
					csize += 0.13f;
					flg = TRUE;
				}
				if( cnt>=360 ){
					csize -= 0.05f;
					D3DXVECTOR3 Pos = pos;
					Pos.z -= 30.0f;
					EffectChage2( Pos, csize );
					if( cnt%2==0 )MainFrameC->SetBullet( this->SetBullet() );
					if( cnt>=660 ){
						csize = 0;
						cnt = 0;
						flg = FALSE;
						if( rand()%3==0 )mode = 1;
					}
				}
				break;
			case 4:
				cnt++;
				if( cnt>=180 ){
					mode = 0;
					cnt = 0;
				}
				break;
		}
	}else{
		if( mode>1 ){
			IEX_SetObjectMotion( &Obj, 3 );
			mode = 0;
		}
	}


	if( hp <= 0 ){
		cnt = 0;
		deadCnt++;
		mode = 255;
		for( int i=0;i<15;i++ ){
			D3DXVECTOR3 pos;
			pos = this->pos;
			int rot = rand()%360;
			int dist = rand()%20;
			pos.x += cosf( RAD*rot )*dist;
			pos.y += sinf( RAD*rot )*dist;
			pos.z -= 10.0f;
			EffectFire( pos.x, pos.y, pos.z, D3DXVECTOR3( 0, 0, 1 ) );
		}
		alpha -= 0.003334f;
		if( deadCnt>=300 )dflg = TRUE;
	}
}

void	HI_CORE::Move()
{
	target = MainFrameC->GetPlayer()->GetPos();
	pos = p->GetPos();
	pos.z += 200;

	angle.z += 0.01f;
}

void	HI_CORE::UpData()
{
	IEX_ObjectFrameNext( &Obj );
	IEX_SetObjectPos( &Obj, pos.x, pos.y, pos.z );
	IEX_SetObjectScale( &Obj, scale );
	IEX_SetObjectAngle( &Obj, angle.x, angle.y, angle.z );
}

void	HI_CORE::Proc()
{
	Mode();
	Move();
	UpData();
	
	if( !flg )ChainTimer++;
	if( ChainTimer>=ChainSpeed ){
		flg = TRUE;
	}
}

void	HI_CORE::Draw()
{
	IEX_Render3DObject( &Obj, RS_COPY, alpha );
}

//*****************************************************************************************************************************
//
//		エネミー管理関連
//
//*****************************************************************************************************************************

EnemyManage_Class::EnemyManage_Class()
{
	num = 0;
	Enemy = NULL;
}

EnemyManage_Class::~EnemyManage_Class()
{
	AllClear();
}

int		EnemyManage_Class::GetNum()
{
	return num;
}

Enemy_Class *EnemyManage_Class::GetEnemy()
{
	return Enemy;
}

Enemy_Class *EnemyManage_Class::GetEnemy( int no )
{
	if( num<=no )return NULL;
	Enemy_Class *enemy;
	enemy = Enemy;
	while( enemy ){
		if( enemy->GetNo()==no )return enemy;
		EnemyNext( &enemy );
	}
	return NULL;
}

void	EnemyManage_Class::AllClear()
{
	Enemy_Class *enemy,*en;
	enemy = Enemy;
	if( enemy ){
		while( enemy->GetNext() ){
			en = enemy;
			EnemyNext( &enemy );
			delete en;
		}
		delete enemy;
	}
	Enemy = NULL;
}

void	EnemyManage_Class::AllDeath()
{
	Enemy_Class *enemy;
	enemy = Enemy;
	while( enemy ){
		if( enemy->GetType()<5 )enemy->SetHp( 0 );
		else{
			if( enemy->GetHp()>100 )enemy->SetHp( 100 );
		}
		EnemyNext( &enemy );
	}
}

void	EnemyManage_Class::Clear( Enemy_Class **enemy )
{
	//if( (*enemy) )num--;
	//else return;

	if( (*enemy)->GetBefore() ){
		Enemy_Class *en;
  		en = (*enemy)->GetBefore();
		if( (*enemy)->GetNext() ){
			en->SetNext( (*enemy)->GetNext() );
			(*enemy)->Rele();
			delete *enemy;
			*enemy = en->GetNext();
			(*enemy)->SetBefore( en );
		}else{
			en->SetNext( NULL );
			(*enemy)->Rele();
			delete *enemy;
			*enemy = NULL;
		}
	}else if( (*enemy)->GetNext() ){
		Enemy_Class **en;
		en = &Enemy;
 		*en = (*enemy)->GetNext();
		(*en)->SetBefore( NULL );
		(*enemy)->Rele();
		delete *enemy;
		*enemy = *en;
	}else{
		Enemy_Class **en;
		en = &Enemy;
		(*enemy)->Rele();
		delete *enemy;
		*enemy = NULL;
		*en = NULL;
	}
}

void	EnemyManage_Class::Clear( int no )
{
	if( num<=no )return;
	Enemy_Class *enemy;
	enemy = GetEnemy( no );
	Clear( &enemy );
}

void	EnemyManage_Class::EnemyNext( Enemy_Class **Enemy )
{
	if( *Enemy )*Enemy = (*Enemy)->GetNext();
}

void	EnemyManage_Class::CreateEnemy( Enemy_Class *enemy )
{
	if( !enemy )return;
	/*if( num>=100 ){
		delete enemy;
		return;
	}*/
	enemy->Init();

	enemy->SetNo( num );
	num++;

	Enemy_Class **en,*e;
	en = &Enemy;
	e = *en;
	if( !*en ){
		*en = enemy;
		return;
	}
 	while( (*en)->GetNext() ){
		EnemyNext( en );
	}
	enemy->SetBefore( *en );
	(*en)->SetNext( enemy );
	*en = e;
}

void	EnemyManage_Class::CreateEnemy( int kind, int type, D3DXVECTOR3 pos, D3DXVECTOR3 target )
{
	Enemy_Class *enemy;
	enemy = new Enemy_Class( kind, type, pos, target );

	CreateEnemy( enemy ); 
}

void	EnemyManage_Class::CreateBoss( int kind )
{
	Enemy_Class *enemy;
	switch( kind ){
	case 0:
		enemy = new RAF();
		CreateEnemy( enemy );
		for( int i=0;i<6;i++ ){
			Enemy_Class *enemy = new CON();
			CreateEnemy( enemy );
		}
		break;
	case 1:
		enemy = new MS();
		CreateEnemy( enemy );
		break;
	case 2:
		enemy = new HI_RING();
		CreateEnemy( enemy );
		for( int i=0;i<4;i++ ){
			Enemy_Class *enemy = new HI_WP();
			CreateEnemy( enemy );
		}
		enemy = new HI_CORE();
		CreateEnemy( enemy );
		break;
	}

}

BOOL	EnemyDeth( Enemy_Class *enemy )
{
	if( enemy->GetDead() ){
		MainFrameC->AddScore( enemy->GetScore() );
		return TRUE;
	}

	if( enemy->GetPos().z<MainFrameC->GetPlayer()->GetPos().z-50 ){
		if( enemy->GetMove().z<0 )return TRUE;
	}
	return FALSE;
}

void	EnemyManage_Class::Proc()
{
	Enemy_Class *enemy;
	enemy = Enemy;
	while( enemy ){
		enemy->HitPlayer();
		if( EnemyDeth( enemy ) ){
			Clear( &enemy );
			continue;
		}
		if( enemy ){
			enemy->Proc();
			EnemyNext( &enemy );
		}
	}
}
 
void	EnemyManage_Class::Draw()
{
	Enemy_Class *enemy;
	enemy = Enemy;
	while( enemy ){
		enemy->Draw();
		EnemyNext( &enemy );
	}
}
