#include	"All.h"


//*****************************************************************************************************************************
//
//		カメラ関連
//
//*****************************************************************************************************************************

#define CameraHeight 4.0f
#define CameraDist 30.0f

//初期化
void Camera_Class::Init( int mode )
{
	this->mode = mode;

	switch( mode ){
	case 0:
		//3人称視点1
		SetPlayerAngle();
		angleX = D3DX_PI/18;
		RotSpeed = .05f;
		dist = 30.0f;
		distnow = dist;

		SetView();
		break;
	case 1:
		//3人称視点2
		pos.x = 0.0f;
		pos.y = 5.0f;
		pos.z = -10.0f;

		target = MainFrameC->GetPlayer()->GetPos();
		target.y += CameraHeight;
		break;
	case 2:
		//エースコンバット見たいな？？
		dist = CameraDist;
		pos = MainFrameC->GetPlayer()->GetPos();
		pos.y += CameraHeight;
		pos.z -= dist;

		target = MainFrameC->GetPlayer()->GetPos();
		target.y += CameraHeight;
		break;
	case 3:
		//Z軸シューティング？？
		dist = CameraDist;
		pos.y = (MAXTOP+MAXUNDER)/2;
		pos.x = 0;
		pos.z -= dist;

		target.y = (MAXTOP+MAXUNDER)/2;
		target.x = 0;
		target.z = MainFrameC->GetPlayer()->GetPos().z;
		break;
	default:
		break;
	}

	SetViewMatrix();

	vib.x = vib.y = vib.z = .0f;
	VibPowerX = VibPowerY = VibPowerZ = 0;
	VibTime = VibCnt = 0;
	VibFlag = FALSE;

	flag = FALSE;
}

//処理
void Camera_Class::Proc()
{
	if( StopProc() )return;

	VibProc();

	SetView();
}

//表示設定
void Camera_Class::View()
{
	D3DXVECTOR3 Pos,Target;
	Pos = pos;
	Target = target;
	pos += vib;
	target += vib;
	D3DXVECTOR3 up;
	if( mode==2 ){
		GetVecMatrix( &MainFrameC->GetPlayer()->GetObj()->TransMatrix, &up, 1 );
	}else{
		up = D3DXVECTOR3( .0f, 1.0f, .0f );
	}
	SetViewMatrix(up);

	pos = Pos;
	target = Target;
}

//カメラの視点設定1
void Camera_Class::SetView( D3DXVECTOR3 Aspect, D3DXVECTOR3 Target )
{
	HitCameraPos();
	pos = MainFrameC->GetPlayer()->GetPos();
	pos = Aspect + pos;
	target = Target;
	flag = FALSE;
}

//カメラの視点設定2
void Camera_Class::SetView( D3DXVECTOR3 Target )
{
	float d,dx,dz;
	float ddx,ddy,ddz;
	switch( mode )
	{
	case 0:
		HitCameraPos();
		target = Target;
		float angle;
		pos = MainFrameC->GetPlayer()->GetPos();
		dx = pos.x - target.x;
		dz = pos.z - target.z;
		angle = atan2f( dx, dz );
		angle += D3DX_PI;
		pos.x = sinf( angle+D3DX_PI )*distnow;
		pos.z = cosf( angle+D3DX_PI )*distnow;
		flag = FALSE;
		break;
	case 1:
		target = Target;

		//	プレイヤーとの距離
		dx = target.x - pos.x;
		dz = target.z - pos.z;
		d = sqrtf( dx*dx + dz*dz );

		if( d > 7.0f )
		{
			dx /= d;
			dz /= d;
			//	カメラの理想の位置を決める
			ldpos.x = target.x - dx*7.0f;
			ldpos.z = target.z - dz*7.0f;
			ldpos.y = target.y + 1.0f;
		}

		if( d < 3.0f )
		{
			dx /= d;
			dz /= d;
			//	カメラの理想の位置を決める
			ldpos.x = target.x - dx*3.0f;
			ldpos.z = target.z - dz*3.0f;
			ldpos.y = target.y + 0.5f;
		}

		//	理想の位置までのベクトル
		ddx = ldpos.x - pos.x;
		ddy = ldpos.y - pos.y;
		ddz = ldpos.z - pos.z;

		//理想に近づく
		pos.x += ddx/8;
		pos.y += ddy/8;
		pos.z += ddz/8;

		pos.y = target.y + 0.5f;
		break;
	default:
		break;
	}
}

//カメラの視点設定3
void Camera_Class::SetView()
{
	D3DXVECTOR3 t;
	float dx,dz,d;
	float ddx,ddy,ddz;
	D3DXVECTOR3 up,back;
	switch( mode )
	{
	case 0:
		HitCameraPos();
		target = MainFrameC->GetPlayer()->GetPos();
		target.y += 6.0f;
		pos = target;
		pos.x += sinf( angleY+D3DX_PI )*distnow*cosf( angleX );
		pos.y += sinf( angleX )*distnow;
		pos.z += cosf( angleY+D3DX_PI )*distnow*cosf( angleX );
		break;
	case 1:
		t = MainFrameC->GetPlayer()->GetPos();
		t.y += CameraHeight;
		target = t;

		//	プレイヤーとの距離
		dx = target.x - pos.x;
		dz = target.z - pos.z;
		d = sqrtf( dx*dx + dz*dz );

		if( d > 7.0f )
		{
			dx /= d;
			dz /= d;
			//	カメラの理想の位置を決める
			ldpos.x = target.x - dx*7.0f;
			ldpos.z = target.z - dz*7.0f;
			ldpos.y = target.y + 1.0f;
		}

		if( d < 3.0f )
		{
			dx /= d;
			dz /= d;
			//	カメラの理想の位置を決める
			ldpos.x = target.x - dx*3.0f;
			ldpos.z = target.z - dz*3.0f;
			ldpos.y = target.y + 0.5f;
		}

		//	理想の位置までのベクトル
		ddx = ldpos.x - pos.x;
		ddz = ldpos.z - pos.z;
		ddy = ldpos.y - pos.y;

		//理想に近づく
		pos.x += ddx/8;
		pos.y += ddy/8;
		pos.z += ddz/8;

		pos.y = target.y + 0.5f;
		break;
	case 2:
		pos = MainFrameC->GetPlayer()->GetPos();
		GetVecMatrix( &MainFrameC->GetPlayer()->GetObj()->TransMatrix, &up, 1 );
		D3DXVec3Normalize( &up, &up );
		GetVecMatrix( &MainFrameC->GetPlayer()->GetObj()->TransMatrix, &back, 2 );
		D3DXVec3Normalize( &back, &back );
		pos.x += CameraHeight*up.x;
		pos.y += CameraHeight*up.y;
		pos.z += CameraHeight*up.z;

		target = MainFrameC->GetPlayer()->GetPos();
		target.x += CameraHeight*up.x+dist*back.x;
		target.y += CameraHeight*up.y+dist*back.y;
		target.z += CameraHeight*up.z+dist*back.z;
		break;
	case 3:
		t = MainFrameC->GetPlayer()->GetPos();

		pos.y = t.y + CameraHeight;
		pos.z = t.z - dist;

		target.y = t.y;
		target.z = t.z;
		break;
	default:
		break;
	}
}

//カメラの上方向を決める
void Camera_Class::SetViewMatrix( D3DXVECTOR3 up )
{
	LPDIRECT3DDEVICE9	lpDevice = IEX_GetD3DDevice();
	D3DXMatrixLookAtLH( &matView, &pos, &target, &up );
	lpDevice->SetTransform( D3DTS_VIEW, &matView );
}

//カメラにプレイヤーのアングル設定
void Camera_Class::SetPlayerAngle()
{
	angleY = MainFrameC->GetPlayer()->GetAngle().y;
	flag = FALSE;
}

//回転フラグセット
void Camera_Class::SetRotFlag()
{
	flag = TRUE;
}

#define DIST_SPEED 2.0f

//カメラの位置がプレイヤーとの間に壁がある場合壁の手前に位置を変更する
void Camera_Class::HitCameraPos()
{
	D3DXVECTOR3 pos,vec,out;
	float Dist = 200.0f;
	pos = MainFrameC->GetPlayer()->GetPos();
	vec.x = sinf( angleY+D3DX_PI )*dist*cosf( angleX );
	vec.y = sinf( angleX )*dist;
	vec.z = cosf( angleY+D3DX_PI )*dist*cosf( angleX );

	if( IEX_RayPickMesh( MainFrameC->GetStage(), &out, &pos, &vec, &Dist )!=-1 ){
		D3DXVECTOR3 d;
		d.x = pos.x - out.x;
		d.y = pos.y - out.y;
		d.z = pos.z - out.z;
		Dist = sqrtf( d.x*d.x + d.y*d.y + d.z*d.z );
		if( Dist<dist ){
			HitDist( Dist );
			return;
		}
	}
	if( distnow<dist ){
		distnow += DIST_SPEED;
		if( distnow>=dist )distnow = dist;
	}
}

void Camera_Class::HitDist( float Dist )
{
	Dist -= 0.5f;
	if( distnow>Dist ){
		distnow -= DIST_SPEED;
		if( distnow<Dist )distnow = Dist;
	}
}

#define ROTSPEED .3f
//カメラを指定したアングルまでrspずつ回転させる
BOOL Camera_Class::RotAngle( float *angle, float Angle, float rsp )
{
	Angle += D3DX_PI;

	float x1, x2, z1, z2, l1, l2;
	x1 = sinf( *angle );
	z1 = cosf( *angle );
	x2 = sinf( Angle );
	z2 = cosf( Angle );
	l1 = sqrtf( x1*x1 + z1*z1 );
	l2 = sqrtf( x2*x2 + z2*z2 );

	float gaiseki = ( x1*z2 - z1*x2 );
	float naiseki = ( x1*x2 + z1*z2 )/(l1*l2);
	naiseki += 1;

	if( naiseki<.0005f ){
		return FALSE;
	}
	if( naiseki<rsp )rsp = naiseki;

	if( gaiseki>.0f ){
		*angle += rsp;
	}else{
		*angle -= rsp;
	}
	return TRUE;
}

//カメラのアングルを指定したアングルまで一定スピードで変更する
void Camera_Class::RotCameraAngle( float Angle )
{
	if( !flag )return;
	flag = RotAngle( &angleY, Angle, ROTSPEED );
}

//カメラのアングルをプレイヤーのアングルまで一定速度ずつ変更する
void Camera_Class::RotPlayerAngle()
{
	RotCameraAngle( MainFrameC->GetPlayer()->GetAngle().y );
}


#define ANGLEX_MAX ((D3DX_PI/180)*85.0f)
//カメラをx軸にそって変更する
void Camera_Class::RotUp()
{
	angleX -= RotSpeed/2;
	if( angleX<-ANGLEX_MAX )angleX = -ANGLEX_MAX;
}

//カメラをx軸にそって変更する
void Camera_Class::RotDown()
{
	angleX += RotSpeed/2;
	if( angleX>ANGLEX_MAX )angleX = ANGLEX_MAX;
}

//カメラをy軸にそって変更する
void Camera_Class::RotRight()
{
	if( flag )return;
	angleY += RotSpeed;
}

//カメラをy軸にそって変更する
void Camera_Class::RotLeft()
{
	if( flag )return;
	angleY -= RotSpeed;
}

//カメラのストップ用プロシージャー
BOOL Camera_Class::StopProc()
{
	if( StopFlag ){
		StopCnt++;
		if( StopCnt>=StopTime ){
			StopFlag = FALSE;
		}
	}
	return StopFlag;
}

//カメラのストップカウントセット
void Camera_Class::SetStop( int time )
{
	StopCnt = 0;
	StopTime = time;
	StopFlag = TRUE;
}

//バイブレーション用プロシージャー
void Camera_Class::VibProc()
{
	if( VibFlag ){
		vib.x = (rand()%VibPowerX*2-VibPowerX+1)*.1f;
		vib.y = (rand()%VibPowerY*2-VibPowerY+1)*.1f;
		vib.z = (rand()%VibPowerZ*2-VibPowerZ+1)*.1f;
		if( VibTime<=VibCnt++){
			vib.x = vib.y = vib.z = .0f;
			VibFlag = FALSE;
		}
	}
}

//バイブレーションセット1
void Camera_Class::SetVib( int px, int py, int pz, int Time )
{
	VibPowerX = px + 1;
	VibPowerY = py + 1;
	VibPowerZ = pz + 1;
	VibTime = Time;
	VibCnt = 0;
	VibFlag = TRUE;
}

//バイブレーションセット2
void Camera_Class::SetVib( int p, int Time )
{
	VibPowerX = p + 1;
	VibPowerY = p + 1;
	VibPowerZ = p + 1;
	VibTime = Time;
	VibCnt = 0;
	VibFlag = TRUE;
}

//バイブレーションフラグカット
BOOL Camera_Class::GetVibFlag()
{
	return VibFlag;
}

//カメラの位置セット
void Camera_Class::SetPos( D3DXVECTOR3 Pos )
{
	pos = Pos;
}

//カメラの位置ゲット
D3DXVECTOR3 Camera_Class::GetPos()
{
	return pos;
}

//カメラのターゲットセット
void Camera_Class::SetTarget( D3DXVECTOR3 Target )
{
	target = Target;
}

//カメラのターゲットゲット
D3DXVECTOR3 Camera_Class::GetTarget()
{
	return target;
}

//カメラのアングルセット
void Camera_Class::SetAngle( float Angle )
{
	angleY = Angle;
}

//カメラのアングルゲット
float Camera_Class::GetAngle()
{
	return angleY;
}

//カメラのアングルの回転スピードセット
void Camera_Class::SetRotSpeed( float rsp )
{
	RotSpeed = rsp;
}

//カメラのアングルの回転スピードゲット
float Camera_Class::GetRotSpeed()
{
	return RotSpeed;
}

//カメラのモードセット
void Camera_Class::SetMode(int Mode)
{
	mode = Mode;
}

//カメラのモードゲット
int Camera_Class::GetMode()
{
	return mode;
}
