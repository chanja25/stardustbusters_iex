#ifndef	__FILE_H__
#define	__FILE_H__

#define	PASS	25

class FileC{
private:
	char	*TextBuf;
	int		TextIP;
	int		TextSize;
public:
	FileC();
	~FileC();

	void	DataRelease();
	char*	Read( char *filename, BOOL fg = TRUE );
	void	Write( char *filename, char *str, BOOL mode = TRUE, BOOL fg = TRUE );
	void	Write( char *filename, LPVOID lpBuf, DWORD size, BOOL mode = TRUE, BOOL fg = TRUE );

	char	*GetFile();
	int		GetFSize();
};

#endif