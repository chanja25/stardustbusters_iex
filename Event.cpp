#include "All.h"



Event_Class::Event_Class()
{
	EventFlag = FALSE;
	for( int i=0;i<256;i++ ){
		fdata.str[i] = '\0';
	}
	fdata.flg = FALSE;
}

Font_Class *Event_Class::GetFont()
{
	return &Font;
}

void	Event_Class::SetEventFlag( BOOL flg )
{
	EventFlag = flg;
}

BOOL	Event_Class::GetEventFlag()
{
	return EventFlag;
}

BOOL	Event_Class::GetFontFlag()
{
	return fdata.flg;
}

void	Event_Class::Proc()
{
}

void	Event_Class::Draw()
{
	if( fdata.str[0] ){
		DrawFont();
	}
}

void	Event_Class::DrawFont()
{
	fdata.flg = Font.DrawString( fdata.str, fdata.x, fdata.y, fdata.size, fdata.color, fdata.speed );
}

void	Event_Class::SetString( char *str, int x, int y, int size, DWORD color, int speed )
{
	for( int i=0;i<256;i++ ){
		fdata.str[i] = '\0';
	}
	lstrcpy( fdata.str, str );
	fdata.x = x;
	fdata.y = y;
	fdata.size	= size;
	fdata.color	= (255<<24) + color;
	fdata.speed	= speed;
	Font.LenInit();
}

void	Event_Class::DeleteFont()
{
	for( int i=0;i<256;i++ ){
		fdata.str[i] = '\0';
	}
	fdata.flg = FALSE;
}