#ifndef __ALL_H__
#define __ALL_H__

#include "iextreme.h"
#include "typedef.h"
#include "File.h"
#include "define.h"
#include "timer.h"
#include "frame.h"
#include "system.h"
#include "public.h"
#include "Script.h"
#include "Font.h"
#include "Effect.h"
#include "Event.h"
#include "Fade.h"
#include "Sound.h"
#include "Camera.h"
#include "Weapon.h"
#include "Character.h"
#include "Enemy.h"
#include "Display.h"

#include "GameFrame.h"

#endif