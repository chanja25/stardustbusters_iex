#include	"iextreme.h"

//*****************************************************************************************************************************
//
//		キー情報関連
//
//*****************************************************************************************************************************

//
//		キー情報配列
//

static	u8	KeyInfo[20], JoyInfo[20];
static	int		KeyMap[20] = { VK_UP, VK_DOWN, VK_LEFT, VK_RIGHT, 'Z', 'X', 'C', 'V', 'A', 'S', VK_RETURN, VK_SPACE, VK_CONTROL, VK_SHIFT, '1', '2', '3', '4', '5', '6'  };
static	int		JoyMap[20] = { 0,1,2,3, 5,6,4,7,8,9,10,11,12,13,14,15,16,17,18,19 };
static	int		PadAxisX, PadAxisY;

static	BOOL	bJoypad = FALSE;
static	u32		g_dwNumForceFeedbackAxis;

LPDIRECTINPUT8			g_pDI = NULL;
LPDIRECTINPUTDEVICE8	g_pJoystick = NULL;
DIDEVCAPS				g_diDevCaps;
LPDIRECTINPUTEFFECT		g_pEffect   = NULL;

//
//		キー情報設定
//

BOOL	KEY_SetInfo( void )
{
	int		i, work;
	HRESULT	hr;

	PadAxisX = PadAxisY = 0;

	/*	ジョイパッド	*/ 
	if( bJoypad ){
		if( FAILED(g_pJoystick->Poll()) ){
			hr = g_pJoystick->Acquire();
			while( hr == DIERR_INPUTLOST ) hr = g_pJoystick->Acquire();
			g_pJoystick->Poll();
		}

		DIJOYSTATE2 dijs;
		if( g_pJoystick->GetDeviceState(sizeof(DIJOYSTATE2),&dijs) == DI_OK ){
			PadAxisX = dijs.lX;
			PadAxisY = dijs.lY;
			/*	上		*/ 
			if( dijs.lY < -(1000/2) ){
				if( JoyInfo[0] & 0x01 ) JoyInfo[0] = 1; else JoyInfo[0] = 3;
			} else {
				if( JoyInfo[0] & 0x01 ) JoyInfo[0] = 2; else JoyInfo[0] = 0;
			}
			/*	下		*/ 
			if( dijs.lY > (1000/2) ){
				if( JoyInfo[1] & 0x01 ) JoyInfo[1] = 1; else JoyInfo[1] = 3;
			} else {
				if( JoyInfo[1] & 0x01 ) JoyInfo[1] = 2; else JoyInfo[1] = 0;
			}
			/*	左		*/ 
			if( dijs.lX < -(1000/2) ){
				if( JoyInfo[2] & 0x01 ) JoyInfo[2] = 1; else JoyInfo[2] = 3;
			} else {
				if( JoyInfo[2] & 0x01 ) JoyInfo[2] = 2; else JoyInfo[2] = 0;
			}
			/*	右		*/ 
			if( dijs.lX > (1000/2) ){
				if( JoyInfo[3] & 0x01 ) JoyInfo[3] = 1; else JoyInfo[3] = 3;
			} else {
				if( JoyInfo[3] & 0x01 ) JoyInfo[3] = 2; else JoyInfo[3] = 0;
			}
			/*	ボタン		*/ 
			for( i=0 ; i<15 ; i++ ){
				if( dijs.rgbButtons[i] & 0x80 ){
					if( JoyInfo[i+4] & 0x01 ) JoyInfo[i+4] = 1; else JoyInfo[i+4] = 3;
				} else {
					if( JoyInfo[i+4] & 0x01 ) JoyInfo[i+4] = 2; else JoyInfo[i+4] = 0;
				}
			}
		}
	}

	/*	キーボード		*/ 
	for( i=0 ; i<19 ; i++ ){
		if( JoyInfo[JoyMap[i]] != 0x00 ){
			KeyInfo[i] = JoyInfo[JoyMap[i]];
			continue;
		}
		if( GetAsyncKeyState(KeyMap[i]) & 0x8000 ) work = 1; else work = 0;
		if( KeyInfo[i] & 0x01 ){ if( work ) KeyInfo[i] = 1; else KeyInfo[i] = 2; }
		 else                  { if( work ) KeyInfo[i] = 3; else KeyInfo[i] = 0; }

	}
	//	キーによる軸設定
	if( GetAsyncKeyState(KeyMap[KEY_UP]    ) & 0x8000 ) PadAxisY = -1000;
	if( GetAsyncKeyState(KeyMap[KEY_DOWN]  ) & 0x8000 ) PadAxisY =  1000;
	if( GetAsyncKeyState(KeyMap[KEY_LEFT]  ) & 0x8000 ) PadAxisX = -1000;
	if( GetAsyncKeyState(KeyMap[KEY_RIGHT] ) & 0x8000 ) PadAxisX =  1000;

	return TRUE;
}

//
//		キー情報取得
//

u8	KEY_Get( KEYCODE key )
{
	if( GetForegroundWindow() != IEX_GetWindow() ) return 0;
	return KeyInfo[key];
}

int		KEY_GetAxisX( void )
{
	return PadAxisX;
}

int		KEY_GetAxisY( void )
{
	return PadAxisY;
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

//
//
//

BOOL CALLBACK EnumJoysticksCallback( const DIDEVICEINSTANCE* pdidInstance, VOID* pContext )
{
    HRESULT hr;

	hr = g_pDI->CreateDevice( pdidInstance->guidInstance, &g_pJoystick, NULL);
    if( FAILED(hr) ) return DIENUM_CONTINUE;

    return DIENUM_STOP;
}

// ジョイスティックの軸を列挙する関数
BOOL CALLBACK EnumAxesCallback(LPCDIDEVICEOBJECTINSTANCE lpddoi, LPVOID pvRef)
{
	if( g_dwNumForceFeedbackAxis < 2 ) g_dwNumForceFeedbackAxis ++;
	// 軸の値の範囲を設定（-1000〜1000）
	DIPROPRANGE diprg;
	ZeroMemory(&diprg, sizeof(diprg));
	diprg.diph.dwSize       = sizeof(diprg); 
	diprg.diph.dwHeaderSize	= sizeof(diprg.diph); 
	diprg.diph.dwObj	    = lpddoi->dwType;
	diprg.diph.dwHow	    = DIPH_BYID;
	diprg.lMin	            = -1000;
	diprg.lMax	            = +1000;
	if( g_pJoystick->SetProperty(DIPROP_RANGE, &diprg.diph) != DI_OK ) return DIENUM_STOP;
		
	return DIENUM_CONTINUE;
}


//
//		入力コントローラー初期化
//

BOOL	IEX_InitInput( HWND hWnd )
{
	bJoypad = FALSE;

	if( DirectInput8Create( GetModuleHandle(NULL), DIRECTINPUT_VERSION, IID_IDirectInput8, (void**)&g_pDI, NULL) != DI_OK ) return FALSE;
	g_pDI->Initialize( GetModuleHandle(NULL), DIRECTINPUT_VERSION);

    g_pDI->EnumDevices( DI8DEVCLASS_GAMECTRL, EnumJoysticksCallback, g_pDI, DIEDFL_ATTACHEDONLY);

    if( NULL == g_pJoystick ) return FALSE;

	if( g_pJoystick->SetDataFormat( &c_dfDIJoystick2 ) != DI_OK ) return FALSE;
	if( g_pJoystick->SetCooperativeLevel( hWnd, DISCL_EXCLUSIVE | DISCL_FOREGROUND ) != DI_OK ) return FALSE;

	//	自動センタリング無効
	DIPROPDWORD	dipdw;
    dipdw.diph.dwSize       = sizeof(DIPROPDWORD);
    dipdw.diph.dwHeaderSize = sizeof(DIPROPHEADER);
    dipdw.diph.dwObj        = 0;
    dipdw.diph.dwHow        = DIPH_DEVICE;
    dipdw.dwData            = DIPROPAUTOCENTER_OFF;
    g_pJoystick->SetProperty( DIPROP_AUTOCENTER, &dipdw.diph );

	// 入力制御開始
	g_pJoystick->Acquire();
	bJoypad = TRUE;

	// コールバック関数を使って各軸のモードを設定
	g_dwNumForceFeedbackAxis = 0;
	if( g_pJoystick->EnumObjects(EnumAxesCallback, NULL, DIDFT_AXIS) != DI_OK ){
		return TRUE;
	}

    // This application needs only one effect: Applying raw forces.
    u32				rgdwAxes[2]     = { DIJOFS_X, DIJOFS_Y };
    LONG            rglDirection[2] = { 0, 0 };
    DICONSTANTFORCE cf;

	cf.lMagnitude = 0;

	DIEFFECT eff;
	ZeroMemory( &eff, sizeof(eff) );
	eff.dwSize                  = sizeof(DIEFFECT);
	eff.dwFlags                 = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	eff.dwDuration              = INFINITE;
	eff.dwSamplePeriod          = 0;
	eff.dwGain                  = DI_FFNOMINALMAX;
	eff.dwTriggerButton         = DIEB_NOTRIGGER;
	eff.dwTriggerRepeatInterval = 0;
	eff.cAxes                   = g_dwNumForceFeedbackAxis;
	eff.rgdwAxes                = rgdwAxes;
	eff.rglDirection            = rglDirection;
	eff.lpEnvelope              = 0;
	eff.cbTypeSpecificParams    = sizeof(DICONSTANTFORCE);
	eff.lpvTypeSpecificParams   = &cf;
	eff.dwStartDelay            = 0;
	// Create the prepared effect
	if( FAILED( g_pJoystick->CreateEffect( GUID_Triangle, &eff, &g_pEffect, NULL ) ) ) return TRUE;

	if( NULL == g_pEffect ) return TRUE;

	g_pEffect->Start( 1, 0 );

    return TRUE;
}

void	IEX_ReleaseInput( void )
{
	if( g_pJoystick ){
		g_pJoystick->Unacquire();
		g_pJoystick->Release();
	}
	if( g_pEffect ){
		g_pEffect->Stop();
		g_pEffect->Release();
	}
	if( g_pDI ) g_pDI->Release();
}

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************

void	KEY_SetFF( int XForce, int YForce, u32 Magnitude )
{
	LONG				rglDirection[2] = { 0, 0 };
	DICONSTANTFORCE		cf;

	if( !g_pEffect ) return;

	if( g_dwNumForceFeedbackAxis == 1 ){
		cf.lMagnitude = Magnitude;
		rglDirection[0] = 0;
	} else {
		rglDirection[0] = XForce;
		rglDirection[1] = YForce;
		cf.lMagnitude = Magnitude;
	}

	DIEFFECT eff;
	ZeroMemory( &eff, sizeof(eff) );
	eff.dwSize                = sizeof(DIEFFECT);
	eff.dwFlags               = DIEFF_CARTESIAN | DIEFF_OBJECTOFFSETS;
	eff.cAxes                 = g_dwNumForceFeedbackAxis;
	eff.rglDirection          = rglDirection;
	eff.lpEnvelope            = 0;
	eff.cbTypeSpecificParams  = sizeof(DICONSTANTFORCE);
	eff.lpvTypeSpecificParams = &cf;
	eff.dwStartDelay            = 0;

	g_pEffect->SetParameters( &eff, DIEP_DIRECTION | DIEP_TYPESPECIFICPARAMS | DIEP_START );
}


//*****************************************************************************************************************************
//
//		マウス
//
//*****************************************************************************************************************************

LPDIRECTINPUTDEVICE8 g_pMouse;
HANDLE	g_hMouseEvent;
MOUSEDATA	MouseData;

void	KEY_InitializeMouse( HWND hWnd )
{
	HRESULT             hr;

	hr = g_pDI->CreateDevice( GUID_SysMouse, &g_pMouse, NULL);
	if( FAILED(hr) ) return;

	hr = g_pMouse->SetDataFormat(&c_dfDIMouse2);
	if( FAILED(hr) ) return;

	hr = g_pMouse->SetCooperativeLevel(hWnd, DISCL_NONEXCLUSIVE | DISCL_BACKGROUND);
	if( FAILED(hr) ) return;

	DIPROPDWORD diprop = { sizeof(DIPROPDWORD), sizeof(DIPROPHEADER) }; 
	diprop.diph.dwObj = 0;
	diprop.diph.dwHow = DIPH_DEVICE;
	diprop.dwData = DIPROPAXISMODE_REL;
	hr = g_pMouse->SetProperty( DIPROP_AXISMODE, &diprop.diph );
	if( FAILED(hr) ) return;

	g_pMouse->Acquire();

}

void	KEY_SetMouseData( void )
{
	if ( g_pMouse != NULL ){
		DIMOUSESTATE2 mouse;
		HRESULT hr = g_pMouse->GetDeviceState( sizeof(DIMOUSESTATE2), &mouse );
		if SUCCEEDED ( hr ){
			MouseData.X = mouse.lX; 
			MouseData.Y = mouse.lY; 
			MouseData.Z = mouse.lZ;

			BYTE	work;

			if( mouse.rgbButtons[0] & 0x80 ) work = 1; else work = 0;
			if( MouseData.L & 0x01 ){ if( work ) MouseData.L = 1; else MouseData.L = 2; }
			else                    { if( work ) MouseData.L = 3; else MouseData.L = 0; }

			if( mouse.rgbButtons[1] & 0x80 ) work = 1; else work = 0;
			if( MouseData.R & 0x01 ){ if( work ) MouseData.R = 1; else MouseData.R = 2; }
			else                    { if( work ) MouseData.R = 3; else MouseData.R = 0; }

			if( mouse.rgbButtons[2] & 0x80 ) work = 1; else work = 0;
			if( MouseData.M & 0x01 ){ if( work ) MouseData.M = 1; else MouseData.M = 2; }
			else                    { if( work ) MouseData.M = 3; else MouseData.M = 0; }

		} else {
			// デバイス再アクセス
			g_pMouse->Acquire();
		}
	}
}

void	KEY_GetMouseData( LPMOUSEDATA md )
{
	CopyMemory( md, &MouseData, sizeof(MOUSEDATA) );
}
