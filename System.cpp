#include "All.h"

//*****************************************************************************************************************************
//
//
//
//*****************************************************************************************************************************
//ゲーム管理クラス
GameManage_Class	GameManage;
GameFrame_Class		*MainFrame = NULL;
MainFrame_Class		*MainFrameC = NULL;

DWORD	dwMainMode = 0;

void	MAIN_Release( void );

Frame frame;

BOOL	SYS_Initialize( void )
{
	MAIN_Initialize();
	frame.init();
	return TRUE;
}

void	SYS_Release( void )
{
	GameManage.Release();
	delete MainFrame;
	//IEX_Release();
}

BOOL	SYS_Main( void )
{
	if( !frame.update() ) return FALSE;

	KEY_SetInfo();

	//処理フレーム
	MAIN_Frame();

	return TRUE;
}

//
//
//

BOOL	SYS_Draw( void )
{
	if( !frame.getRender() ) return TRUE;

	LPDIRECT3DDEVICE9	lpDevice = IEX_GetD3DDevice();

	lpDevice->BeginScene(); 

	//	Zバッファ&ターゲットをクリア
	lpDevice->Clear( 0, NULL, D3DCLEAR_TARGET|D3DCLEAR_ZBUFFER, 0xFF000000, 1.0f, 0 );

	//処理フレーム
	MAIN_DrawFrame();

	//	フレーム表示
	char	str[64];
	extern	HWND	hWndMain;
	wsprintf( str, "FPS %03d\n", frame.getFPS() );
	//SetWindowText( hWndMain, str );
	//IEX_DrawText( str, 10,10,200,20, 0xFFFFFF00 );

	// シーン終了
	lpDevice->EndScene();

	// バックバッファの内容をプライマリに転送
	if( FAILED( lpDevice->Present( NULL, NULL, NULL, NULL ) )){
		// リセット
		lpDevice->Reset( IEX_GetPresentParam() );
	}

	return TRUE;
}

