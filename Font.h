#ifndef __FONT_H__
#define __FONT_H__


//文字サイズ
#define FSize 32
//一行毎に出す文字数
#define MOJIX 14
//一列毎に出す文字数
#define MOJIY 3
//BMP一列当たりの文字数
#define Scr 8
//BMPの数
#define BNum 6

//フォントクラス
class Font_Class{
private:
	LPIEX2DOBJ lpFont[BNum];
	int len;

	void DrawChar( WORD chr, int x, int y, int size, DWORD color );
public:
	Font_Class();
	~Font_Class();
	void LenInit();
	BOOL DrawString( LPSTR str, int x, int y, int size, DWORD color, int speed = 10, BOOL flg = TRUE );
};

#endif