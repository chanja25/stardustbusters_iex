#ifndef __CAMERA_H__
#define __CAMERA_H__


//カメラクラス
class Camera_Class{
private:
	D3DXVECTOR3 pos;
	D3DXVECTOR3 ldpos;
	D3DXVECTOR3 target;
	D3DXVECTOR3 vib;//カメラの振動
	float angleY;
	float angleX;
	float RotSpeed;
	float dist;
	float distnow;


	int mode;
	int VibPowerX;
	int VibPowerY;
	int VibPowerZ;
	int VibTime;
	int VibCnt;

	BOOL StopFlag;
	int  StopTime;
	int  StopCnt;

	BOOL VibFlag;
	BOOL flag;
public:
	void HitCameraPos();
	void HitDist( float Dist );

	void SetView( D3DXVECTOR3 Aspect, D3DXVECTOR3 Target );
	void SetView( D3DXVECTOR3 Target );
	void SetView();

	void SetViewMatrix( D3DXVECTOR3 up = D3DXVECTOR3( .0f, 1.0f, .0f ) );

	void SetPlayerAngle();

	void SetPos( D3DXVECTOR3 Pos );
	void SetPosY( float y );
	D3DXVECTOR3 GetPos();
	void SetTarget( D3DXVECTOR3 Target );
	D3DXVECTOR3 GetTarget();
	void SetDist( float Dist );
	void SetAngle( float angle );
	float GetAngle();
	void SetRotSpeed( float rsp );
	float GetRotSpeed();
	float GetDist();

	BOOL StopProc();
	void SetStop( int time );

	void VibProc();
	void SetVib( int px, int py, int pz, int time );
	void SetVib( int p, int Time );
	BOOL GetVibFlag();

	void SetMode( int Mode );
	int GetMode();

	BOOL Camera_Class::RotAngle( float *angle, float Angle, float rsp );
	void SetRotFlag();
	void RotPlayerAngle();
	void RotCameraAngle( float Angle );
	void RotUp();
	void RotDown();
	void RotRight();
	void RotLeft();

	void Init( int mode = 0 );
	void Proc();
	void View();
};

#endif