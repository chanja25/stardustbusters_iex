#include "All.h"


//*****************************************************************************************************************************
//
//		スクリプト関連
//
//*****************************************************************************************************************************
//初期化
void	Script_Class::Init()
{
	ScriptSize = 0;
	TextIP = 0;

	WaitCnt = 0;
	WaitFlag = FALSE;
	WaitStrFlag = FALSE;
	EndFlag = FALSE;
}


//スクリプトのロード
void	Script_Class::LoadScript( char *filename )
{
	//HANDLE hfile;
	//DWORD dum;
	char fname[256];

	File.DataRelease();
	wsprintf( fname, "DATA\\SCRIPT\\%s.txt", filename );

	Init();

	TextBuf = File.Read( fname );
	ScriptSize = File.GetFSize();
	////	ファイルを開く
	//hfile = CreateFile( fname, GENERIC_READ,
	//					FILE_SHARE_READ,
	//					(LPSECURITY_ATTRIBUTES)NULL,
	//					OPEN_EXISTING,
	//					FILE_ATTRIBUTE_NORMAL,
	//					(HANDLE) NULL );

	////	ファイルが正常に開けていたら実行
	//if( hfile != INVALID_HANDLE_VALUE ){
	//	ScriptSize = GetFileSize( hfile, NULL );
	//	TextBuf = new char[ScriptSize];
	//	ReadFile( hfile, TextBuf, ScriptSize, &dum, NULL );
	//	CloseHandle(hfile);
	//}
}

//コードをセットする
void	Script_Class::Proc()
{
	while( TextIP<ScriptSize ){
		if( WaitFlag )return;
		if( EndFlag )return;
		if( WaitCnt>0 || WaitCnt<0 ){
			WaitCnt--;
			if( KEY(KEY_ENTER)==3 && WaitStrFlag )WaitCnt = 0;
			return;
		}else if( WaitStrFlag ){
			SetWaitStrFlag(FALSE);
			MainFrameC->GetEvent()->DeleteFont();
		}
		if( GetScript() )return;
	}
}

#define cmp !lstrcmp

//スクリプトのセット
BOOL	Script_Class::GetScript()
{
	char cmd[32];
	GetParam( cmd );
	//フェイドイン
	if( cmp( cmd, "F.I" ) ){
		DWORD color = GetParamLong();
		int sp = GetParamInt();
		SetWait(255/sp);
		MainFrame->GetFade()->FadeIn( color, sp );
		return TRUE;
	}
	//フェイドアウト
	else if( cmp( cmd, "F.O" ) ){
		DWORD color = GetParamLong();
		int sp = GetParamInt();
		SetWait(255/sp);
		MainFrame->GetFade()->FadeOut( color, sp );
		return TRUE;
	}
	//フェイドイン
	if( cmp( cmd, "F.I2" ) ){
		int sp = GetParamInt();
		SetWait(255/sp);
		MainFrame->GetFade()->FadeIn( sp );
		return TRUE;
	}
	//フェイドアウト
	else if( cmp( cmd, "F.O2" ) ){
		int sp = GetParamInt();
		SetWait(255/sp);
		MainFrame->GetFade()->FadeOut( sp );
		return TRUE;
	}
	else if( cmp( cmd, "PLSOUND" ) ){
		int no = GetParamInt();
		int loop = GetParamInt();
		Sound::PlaySound( no, loop );
	}
	else if( cmp( cmd, "STSOUND" ) ){
		int no = GetParamInt();
		Sound::StopSound( no );
	}
	else if( cmp( cmd, "ENDSOUND" ) ){
		Sound::StopSound();
	}
	else if( cmp( cmd, "VASOUND" ) ){
		int no = GetParamInt();
		int value = GetParamInt();
		Sound::SetValue( no, value );
	}
	//メッセージボックスセット
	else if( cmp( cmd, "SETMSG" ) ){
		MainFrameC->GetDisp()->SetMsg();
		SetWait( MWHEIGHT/MainFrameC->GetDisp()->GetSp()+1 );
		return TRUE;
	}
	//メッセージボックスを閉じる
	else if( cmp( cmd, "CLOSEMSG" ) ){
		MainFrameC->GetDisp()->CloseMsg();
		SetWait( MWHEIGHT/MainFrameC->GetDisp()->GetSp()+1 );
		return TRUE;
	}
	//メッセージボックスに表示するキャラ設定
	else if( cmp( cmd, "MCHARA" ) ){
		MainFrameC->GetDisp()->SetMsgChara( GetParamInt() );
		return TRUE;
	}
	//メッセージセット
	else if( cmp( cmd, "MSG" ) ){
		char str[256];
		int x,y,size,sp,time;
		DWORD color;
		lstrcpy( str, GetParamStr() );
		x = GetParamInt();
		y = GetParamInt();
		size = GetParamInt();
		sp = GetParamInt();
		color = GetParamLong();
		time = GetParamInt();
		x = (x>=0)? x: 320-lstrlen(str)*size/4;
		y = (y>=0)? y: 240-(lstrlen(str)/12+1)*size/4;
		MainFrameC->GetEvent()->SetString( str, x, y, size, color, sp );
		if( time>=0 )SetWait( lstrlen(str)*sp + time );
		else SetWait( -1 );
		SetWaitStrFlag( TRUE );
		return TRUE;
	}
	else if( cmp( cmd, "MES" ) ){
		char str[256];
		int x,y,size,sp,time;
		DWORD color;
		lstrcpy( str, GetParamStr() );
		x = 270;
		y = 405;
		size = 16;
		sp = GetParamInt();
		color = GetParamLong();
		time = GetParamInt();
		MainFrameC->GetEvent()->SetString( str, x, y, size, color, sp );
		if( time>=0 )SetWait( lstrlen(str)*sp + time );
		else SetWait( -1 );
		SetWaitStrFlag( TRUE );
		return TRUE;
		return TRUE;
	}
	else if( cmp( cmd, "DELMES" ) ){
		MainFrameC->GetEvent()->DeleteFont();
		return TRUE;
	}
	//イベント開始
	else if( cmp( cmd, "EVENT" ) ){
		SetEventFlag( TRUE ); 
		return TRUE;
	}
	//イベント終了
	else if( cmp( cmd, "EVENTEND" ) ){
		SetEventFlag( FALSE );
		return TRUE;
	}
	//ステージ消去
	else if( cmp( cmd, "STAGEDEL" ) ){
	IEX_SetFog( D3DFOG_LINEAR, .0f, 2000.0f, 0xBBBBFF );
		MainFrameC->SetFlg();
	}
	//敵のセット
	else if( cmp( cmd, "ENEMY" ) ){
		int kind = GetParamInt();
		int type = GetParamInt();
		D3DXVECTOR3 pos;
		D3DXVECTOR3 target;
		pos.x = GetParamFloat();
		pos.y = GetParamFloat();
		pos.z = GetParamFloat() + MainFrameC->GetPlayer()->GetPos().z;
		target.x = GetParamFloat();
		target.y = GetParamFloat();
		target.z = GetParamFloat();
		MainFrameC->GetEnemy()->CreateEnemy( kind, type, pos, target );
	}
	//ボスのセット
	else if( cmp( cmd, "BOSS" ) ){
		int kind = GetParamInt();
		MainFrameC->GetEnemy()->CreateBoss( kind );
	}
	//武器セット
	else if( cmp( cmd, "SETWEAPON" ) ){
		if( !MainFrameC->GetPlayer()->GetWeapon() ){
			int type = GetParamInt();
			MainFrameC->GetPlayer()->SetWeapon( type );
			MainFrameC->GetPlayer()->SetWeaponType( type );
			SetWait( 120 );
		}
		return TRUE;
	}
	//武器パージ
	else if( cmp( cmd, "LOSTWEAPON" ) ){
		if( MainFrameC->GetPlayer()->GetWeapon() ){
			MainFrameC->GetPlayer()->GetWeapon()->SetLost();
			MainFrameC->GetPlayer()->SetWeaponType( -1 );
			SetWait( 60 );
		}
		return TRUE;
	}
	//攻撃
	else if( cmp( cmd, "ATTACK" ) ){
		static int cnt = 0;
		if( MainFrameC->GetPlayer()->GetWeapon() ){
			MainFrameC->SetBullet( MainFrameC->GetPlayer()->GetWeapon()->SetBullet() );
			SetWait( MainFrameC->GetPlayer()->GetWeapon()->GetChainSpeed() );
			MainFrameC->GetPlayer()->GetWeapon()->SetFlag( TRUE );
			MainFrameC->GetPlayer()->GetWeapon()->SetBullet( MainFrameC->GetPlayer()->GetWeapon()->GetBullet()+1 );
		}else{
			MainFrameC->SetBullet( MainFrameC->GetPlayer()->SetBullet() );
			SetWait( MainFrameC->GetPlayer()->GetChainSpeed() );
		}
		if( ++cnt<GetParamInt() ){
			while(TRUE){
				TextIP--;
				if( TextBuf[TextIP]=='\n' ){
					break;
				}
			}
		}else cnt = 0;
		return TRUE;
	}
	//バイブレーションセット
	else if( cmp( cmd, "VIB" ) ){
		int power = GetParamInt();
		int time = GetParamInt();
		MainFrameC->GetCamera()->SetVib( power, time );
		return TRUE;
	}
	//現在のモード変更
	else if( cmp( cmd, "MCHANGE" ) ){
		int mode = GetParamInt();
		GameManage.SetNextMode( mode );
		return TRUE;
	}
	//一時停止
	else if( cmp( cmd, "WAIT" ) ){
		SetWait( GetParamInt() );
		return TRUE;
	}
	//停止
	else if( cmp( cmd, "SWAIT" ) ){
		SetWaitFlag(TRUE);
		return TRUE;
	}
	//ゲームエンド
	else if( cmp( cmd, "GAMEEND" ) ){
		exit( NULL );
		return FALSE;
	}
	//スクリプト終了
	else if( cmp( cmd, "END" ) ){
		SetEndFlag(TRUE);
		return TRUE;
	}
	//スクリプトロード
	else if( cmp( cmd, "LOADSCR" ) ){
		char fname[64];
		lstrcpy( fname, GetParamStr() );
		LoadScript( fname );
		return TRUE;
	}
	return FALSE;
}

//次のパラメータの所まで移動する
BOOL	Script_Class::GetParamTop()
{
	for(;TextIP<ScriptSize;TextIP++){
		if(TextBuf[TextIP]=='/' && TextBuf[TextIP+1]=='/'){
			for(;TextIP<ScriptSize;TextIP++){
				if( TextBuf[TextIP]=='\n' )break;
				if( IsDBCSLeadByte( TextBuf[TextIP] ) )TextIP++;
			}
			continue;
		}
		if(TextBuf[TextIP]=='/' && TextBuf[TextIP+1]=='*'){
			for(;TextIP<ScriptSize;TextIP++){
				if( TextBuf[TextIP]=='*' && TextBuf[TextIP+1]=='/' )break;
				if( IsDBCSLeadByte( TextBuf[TextIP] ) )TextIP++;
			}
			continue;
		}
		if(TextBuf[TextIP]=='\r')continue;
		if(TextBuf[TextIP]=='\t')continue;
		if(TextBuf[TextIP]=='\n')continue;
		if(TextBuf[TextIP]==',')continue;
		if(TextBuf[TextIP]==' ')continue;
		if(TextBuf[TextIP]=='(')continue;
		if(TextBuf[TextIP]==')')continue;
		if(TextBuf[TextIP]=='{')continue;
		if(TextBuf[TextIP]=='}')continue;
		if(TextBuf[TextIP]==';')continue;
		return TRUE;
	}
	return FALSE;
}

//パラメータを取得
char	*Script_Class::GetParam( char *param )
{
	int i;
	GetParamTop();
	for(i=0;TextIP<ScriptSize;i++,TextIP++){
		if(TextBuf[TextIP]=='"'){
			TextIP++;
			for(;TextIP<ScriptSize;i++,TextIP++){
				if( TextBuf[TextIP]=='"' )break;
				param[i] = TextBuf[TextIP];
			}
			break;
		}
		if(TextBuf[TextIP]=='\r')break;
		if(TextBuf[TextIP]=='\n')break;
		if(TextBuf[TextIP]=='\t')break;
		if(TextBuf[TextIP]=='\0')break;
		if(TextBuf[TextIP]==',')break;
		if(TextBuf[TextIP]==' ')break;
		param[i] = TextBuf[TextIP];
	}
	TextIP++;
	param[i] = '\0';
	return param;
}

char	*Script_Class::GetTextBuf()
{
	return TextBuf;
}

void	Script_Class::ParamNext()
{
	char param[32];
	GetParam( param );
}

//文字列型のパラメータ取得
char	*Script_Class::GetParamStr()
{
	char param[256];
	return GetParam( param );
}

//int型のパラメータ取得
int		Script_Class::GetParamInt()
{
	char param[32];
	GetParam( param );
	return atoi( param );
}

//float型のパラメータ取得
float	Script_Class::GetParamFloat()
{
	char param[32];
	GetParam( param );
	return (float)atof( param );
}

//DWORD型のパラメータ取得
LONG	Script_Class::GetParamLong()
{
	char param[32];
	GetParam( param );
	if( param[0]=='0' && param[1]=='x' ){
		long num = 0;
		int i;
		for( i=0;param[i+2]!='\0';i++ );
		i--;
		int j = 0;
		for(;i>=0;i--,j++){
			if( param[i+2]=='1' )num += 1<<(j*4);
			else if( param[i+2]=='2' )num += 2<<(j*4);
			else if( param[i+2]=='3' )num += 3<<(j*4);
			else if( param[i+2]=='4' )num += 4<<(j*4);
			else if( param[i+2]=='5' )num += 5<<(j*4);
			else if( param[i+2]=='6' )num += 6<<(j*4);
			else if( param[i+2]=='7' )num += 7<<(j*4);
			else if( param[i+2]=='8' )num += 8<<(j*4);
			else if( param[i+2]=='9' )num += 9<<(j*4);
			else if( param[i+2]=='a' )num += 10<<(j*4);
			else if( param[i+2]=='b' )num += 11<<(j*4);
			else if( param[i+2]=='c' )num += 12<<(j*4);
			else if( param[i+2]=='d' )num += 13<<(j*4);
			else if( param[i+2]=='e' )num += 14<<(j*4);
			else if( param[i+2]=='f' )num += 15<<(j*4);
			else if( param[i+2]=='A' )num += 10<<(j*4);
			else if( param[i+2]=='B' )num += 11<<(j*4);
			else if( param[i+2]=='C' )num += 12<<(j*4);
			else if( param[i+2]=='D' )num += 13<<(j*4);
			else if( param[i+2]=='E' )num += 14<<(j*4);
			else if( param[i+2]=='F' )num += 15<<(j*4);
		}
		return num;
	}else{
		return atol( param );
	}
}

//イベントフラグセット
void	Script_Class::SetEventFlag( BOOL flg )
{
	MainFrameC->GetEvent()->SetEventFlag( flg );
}

//一時停止
void	Script_Class::SetWait( int time )
{
	WaitCnt = time;
}

//停止フラグ
void	Script_Class::SetWaitFlag( BOOL flg )
{
	WaitFlag = flg;
}

//停止フラグ
void	Script_Class::SetWaitStrFlag( BOOL flg )
{
	WaitStrFlag = flg;
}

//終了フラグ
void	Script_Class::SetEndFlag( BOOL flg )
{
	EndFlag = flg;
}
